//
//  AccidentDetail.h
//  Optimus1
//
//  Created by iphtech7 on 22/04/14.
//
//

#import <Foundation/Foundation.h>

@interface AccidentDetail : NSObject
{
    NSString * date;
    NSString * time;
    NSString * location;
    NSString * PoliceOfficerAndStation;
    NSString * incidentDescription;
}

@property (strong,nonatomic) NSString * date;
@property (strong,nonatomic) NSString * time;
@property (strong,nonatomic) NSString * location;
@property (strong,nonatomic) NSString * PoliceOfficerAndStation;
@property (strong,nonatomic) NSString * incidentDescription;

@end
