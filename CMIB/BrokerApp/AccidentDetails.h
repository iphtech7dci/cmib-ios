//
//  AccidentDetails.h
//  Optimus1
//
//  Created by iphtech7 on 21/04/14.
//
//

#import "GAITrackedViewController.h"


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSData+Base64.h"
#import <CoreLocation/CoreLocation.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@class AppDelegate;

@interface AccidentDetails : GAITrackedViewController<UIWebViewDelegate, UITextFieldDelegate, UITextViewDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate , UIActionSheetDelegate , MFMailComposeViewControllerDelegate, CLLocationManagerDelegate , UITableViewDataSource, UITableViewDelegate>

{
    
    UITextField * dateTextView;
    UITextField * timeTextView;
    UITextField * locationTextView;
    UITextField * policeOfficerTextView;
    UITextField * incidentTextView;
    
    UIButton *sendButton;
    //    UIButton *addPhotosButton;
    //    UIButton  * dropDownButton;
    
    //    UIImageView * uploadImage1;
    //    UIImageView * uploadImage2;
    //    UIImageView * uploadImage3;
    //    UIImageView * uploadImage4;
    //    UILabel * addPhotoLabel;
    //    int imageCount;
    
    IBOutlet UIView * indecatorView;
    UIView * confirmationView;
    UIToolbar *numberToolbar;
    UIScrollView * scrollView;
    CGPoint offset;
    
    NSString * longitude;
    NSString * latitude;
    //UITableView * tabelView;
    
    // NSString * longitude_libPhoto;
    // NSString * latitude_libPhoto;
    
    
    CGFloat contentOffSet;
    CGFloat xoffset;
    CGFloat yoffset ;
    
    NSMutableArray * officeArray;
    NSMutableArray * latiArray;
    NSMutableArray * longiArray;
    NSMutableArray * dateTimeArray;
    NSString * emailRecieverString;
    
    BOOL shouldShowDropDownList;
    CGRect TableFrame;
    int checForEmptyArray;
    CLLocationManager *locationManager;
    // NSString * photoDateTimeStr;
    // BOOL isCamera;
    
    AppDelegate *appDelegate;
    
    
    
}
@property (nonatomic, retain)NSString * longitude_libPhoto;
@property (nonatomic, retain)NSString * latitude_libPhoto;

@property (nonatomic, retain) NSMutableArray * dateTimeArray;
//@property (nonatomic, retain) NSString * photoDateTimeStr;
@property (nonatomic, retain) NSMutableArray * latiArray;
@property (nonatomic, retain) NSMutableArray * longiArray;
@property (nonatomic, retain) NSString * emailRecieverString;
@property (nonatomic, retain) NSMutableArray * officeArray;
//@property (nonatomic, retain) UITableView * tabelView;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) UIScrollView * scrollView;
@property (nonatomic, retain) UIView * confirmationView;
@property (nonatomic, retain) UIView * indecatorView;


@property(strong,nonatomic)UITextField * dateTextView;
@property(strong,nonatomic)UITextField * timeTextView;
@property(strong,nonatomic)UITextField * locationTextView;
@property(strong,nonatomic)UITextField * policeOfficerTextView;
@property(strong,nonatomic)UITextField * incidentTextView;


@property (nonatomic, retain) UITextView * line1;

-(void)findCurrentLocation;
-(void)initializeNavigationBar;
-(void) moveViewDown;
-(void)dismissKeyboard;
-(void)makeForm;
-(void)moveScrolUP:(int)ht;
-(void)moveScrollDown;
-(BOOL)validateEmailWithString:(NSString*)email;
//-(void)dismissView;
-(void)callBack;
//-(void)addPhotos;
-(void)sendData;
-(void)sendmail;
-(void)showActionSheet;
-(void)capturePhoto;

@end
