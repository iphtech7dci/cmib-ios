//
//  AccidentDetails.m
//  Optimus1
//
//  Created by iphtech7 on 21/04/14.
//
//

#import "AccidentDetails.h"
#import "YourVehicleDetails.h"
#import "ClaimsAndPhotos.h"
#import "Constants.h"
#import "ClaimWhatToDo.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"
#import "AccidentDetail.h"

#define DATE_TEXTFIELD_TAG 300
#define TIME_TEXTFIELD_TAG 301
#define LOCATION_TEXTFIELD_TAG 302
#define POLICE_TEXTFIELD_TAG 303
#define INCIDENT_TEXTFIELD_TAG 304


@interface AccidentDetails ()
{
    CGFloat yValue;
    int currentTxtFldTag;
    UIDatePicker *datePicker;
}

@end

@implementation AccidentDetails
@synthesize dateTextView,timeTextView,locationTextView,policeOfficerTextView,incidentTextView;
@synthesize indecatorView;
@synthesize confirmationView;
@synthesize scrollView;
@synthesize latitude;
@synthesize longitude;
//@synthesize tabelView;
@synthesize officeArray;
@synthesize emailRecieverString;
@synthesize latiArray;
@synthesize longiArray;
//@synthesize photoDateTimeStr;
@synthesize dateTimeArray;
@synthesize longitude_libPhoto;
@synthesize latitude_libPhoto;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     //self.view.backgroundColor=[UIColor redColor];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
    
    //imageCount = 0;
    shouldShowDropDownList = YES;
    checForEmptyArray = 0;
    self.latiArray = [[NSMutableArray alloc]init];
    self.longiArray = [[NSMutableArray alloc]init];
    self.dateTimeArray = [[NSMutableArray alloc]init];
    self.screenName = @"Claims & Photos";
    UIImageView * bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    if ([Utility isIphone_5]) {
        bgImage.frame = CGRectMake(0, 0, 320, 568);
    }
    bgImage.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    [self.view addSubview:bgImage];
    [bgImage release];
    
    [self initializeNavigationBar];
    
    
    [self makeForm];
    [self assignSavedvalue];
    
   
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)] ;
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(prevBtn)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBtn)];
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)];
    NSArray *itemArray = [[NSArray alloc] initWithObjects:item3, spaceItem, item1, item2, nil];
    numberToolbar.items = itemArray;
    
    [numberToolbar sizeToFit];
    [item1 release];
    [item2 release];
    [item3 release];
    [itemArray release];
    
    
    self.dateTextView.inputAccessoryView = numberToolbar;
 //   [self.dateTextView ]
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
//    datePicker.timeZone = [NSTimeZone defaultTimeZone];
//    datePicker.minuteInterval = 5;
    
    [self.dateTextView setInputView:datePicker];

    self.timeTextView.inputAccessoryView = numberToolbar;
    [self.timeTextView setInputView:datePicker];
    
    self.locationTextView.inputAccessoryView = numberToolbar;
    self.policeOfficerTextView.inputAccessoryView = numberToolbar;
    self.incidentTextView.inputAccessoryView = numberToolbar;
    
    
    self.confirmationView  = [[UIView alloc]init];
    [self.confirmationView setAlpha:0.8];
    [[self.confirmationView layer] setCornerRadius:10];
    
    self.confirmationView.frame = CGRectMake(30, 150, 260, 100);
    if ([Utility isIphone_5]) {
        self.confirmationView.frame = CGRectMake(30, 180, 260, 100);
    }
    [self.confirmationView setBackgroundColor:[UIColor darkGrayColor]];
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 160, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setNumberOfLines:2];
    [label setText:@"Your message has been sent"];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:DONE_BUTTON_IMAGE] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(105, 65, 60, 30)];
    
    [self.confirmationView addSubview:btn];
    [self.confirmationView addSubview:label];
    [self.confirmationView setHidden:YES];
    [self.view addSubview:self.confirmationView];
    [self.view bringSubviewToFront:self.indecatorView];
    
    [label release];
    
        
    
}
-(void)initializeNavigationBar
{
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Accident Details";
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(270, 0, 23, 23)];
    photoImage.image = [UIImage imageNamed:@"whiteQuestion.png"];
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
}
-(void) moveViewDown
{
    [UIView beginAnimations:@"ResizeForKeyboard" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x,64,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}

-(void)dismissKeyboard
{
    [self moveScrollDownForTextfield];
   // [self moveViewDown];
    [self.view endEditing:YES];
   // self.scrollView.contentOffset = CGPointMake(0,0);
}
-(void)nextBtn
{
    if(currentTxtFldTag<304)
    {
        currentTxtFldTag = currentTxtFldTag + 1;
    }
    
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
    
    
}
-(void)prevBtn
{
    if(currentTxtFldTag>300)
    {
        currentTxtFldTag=currentTxtFldTag -1;
    }
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
    
    
}

-(void) datePickerValueChanged:(id) sender
{
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
     UIDatePicker *picker = (UIDatePicker*)sender;
    if(currentTxtFldTag==300)
    {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
   
    NSString *theDate = [dateFormat stringFromDate:picker.date];
    txtfld.text = [NSString stringWithFormat:@"%@",theDate];
    }
    else
    {
        datePicker.timeZone = [NSTimeZone defaultTimeZone];
        datePicker.minuteInterval = 1;
        datePicker.datePickerMode = UIDatePickerModeTime;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"h:mm a"];
//        NSDate *exampleDateFromString = [dateFormatter dateFromString:@"8:48 AM"];
//        picker.date = exampleDateFromString;
        NSString *theDate = [dateFormatter stringFromDate:picker.date];
        txtfld.text = [NSString stringWithFormat:@"%@",theDate];
    }
    
   // self.dateTextView.text = [NSString stringWithFormat:@"%@",theDate];
   
    
    
}

-(void)moveScrollDownForTextfield
{
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0,0);
  //  self.scrollView.contentSize = CGSizeMake(200, yValue);  // yt
    [UIView commitAnimations];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    locationManager.delegate = self;
    [locationManager startUpdatingHeading];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    if (locationManager) {
        locationManager.delegate = nil;
        [locationManager stopUpdatingLocation];
    }
}
-(void)makeForm
{
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 370)];
    //[self.scrollView setScrollEnabled:YES];
    self.scrollView.backgroundColor=[UIColor whiteColor];

    self.scrollView.contentSize = CGSizeMake(250, 10);
    
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(0, 0, 320, 460);
    }
    
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollView];
    
     yValue = 20;
    CGFloat width = 280;
    CGFloat height = 15;
    //CGFloat bottumButtonWidth = 138;
    CGFloat litleSpace = 5.0;
    CGFloat largeSpace = 8.0;
    CGFloat xValue = 20.0;
    // CGFloat lineHeight = 2.0;
    //  CGFloat texViewHieght = 80.0;
    CGFloat textFieldHeight = 30;
    
    UILabel * dateGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [dateGeadingLable setBackgroundColor:[UIColor clearColor]];
    [dateGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [dateGeadingLable setText:@"Date"];//astha
    [scrollView addSubview:dateGeadingLable];
    [dateGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    
    
    self.dateTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.dateTextView setPlaceholder:@"Date"];//astha
    [self.dateTextView setTag:DATE_TEXTFIELD_TAG];
    [self.dateTextView.layer setCornerRadius:2.0f];
    [self.dateTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.dateTextView setDelegate:self];
     self.dateTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.dateTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    
    UILabel * timeLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [timeLable setBackgroundColor:[UIColor clearColor]];
    [timeLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [timeLable setText:@"Time"];//astha
    [scrollView addSubview:timeLable];
    [timeLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    
    self.timeTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.timeTextView setPlaceholder:@"Time"];//astha
    [self.timeTextView setTag:TIME_TEXTFIELD_TAG];
    [self.timeTextView.layer setCornerRadius:2.0f];
    [self.timeTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.timeTextView setDelegate:self];
     self.timeTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.timeTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * locationLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [locationLable setBackgroundColor:[UIColor clearColor]];
    [locationLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [locationLable setText:@"Location"];//astha
    [scrollView addSubview:locationLable];
    [locationLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    self.locationTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.locationTextView setPlaceholder:@"Location"];//astha
    [self.locationTextView setTag:LOCATION_TEXTFIELD_TAG];
    [self.locationTextView.layer setCornerRadius:2.0f];
    [self.locationTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.locationTextView setDelegate:self];
     self.locationTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.locationTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel *policeOfficerGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [policeOfficerGeadingLable setBackgroundColor:[UIColor clearColor]];
    [policeOfficerGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [policeOfficerGeadingLable setText:@"Police Officer and Station(If applicable)"];//astha
    [scrollView addSubview:policeOfficerGeadingLable];
    [policeOfficerGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    self.policeOfficerTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.policeOfficerTextView setPlaceholder:@"Police Officer and Station(If applicable)"];//astha
    [self.policeOfficerTextView setTag:POLICE_TEXTFIELD_TAG];
    [self.policeOfficerTextView.layer setCornerRadius:2.0f];
    [self.policeOfficerTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.policeOfficerTextView setDelegate:self];
     self.policeOfficerTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.policeOfficerTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel *incidentGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [incidentGeadingLable setBackgroundColor:[UIColor clearColor]];
    [incidentGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [incidentGeadingLable setText:@"Incident Description"];//astha
    [scrollView addSubview:incidentGeadingLable];
    [incidentGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    
    
    self.incidentTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight+50)];
   
    [self.incidentTextView setPlaceholder:@"Incident Description"];//astha
    [self.incidentTextView setTag:INCIDENT_TEXTFIELD_TAG];
    [self.incidentTextView.layer setCornerRadius:2.0f];
    [self.incidentTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.incidentTextView setDelegate:self];
     self.incidentTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.incidentTextView];
    
    yValue = yValue+textFieldHeight+largeSpace+60;
    
    sendButton = [[UIButton alloc] initWithFrame:CGRectMake(xValue , yValue,width, 42)];
    [sendButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [sendButton setTitle:@"Save & Go Back" forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [sendButton addTarget:self action:@selector(sendData) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:sendButton];
    
    yValue = yValue+50;
    self.scrollView.contentSize = CGSizeMake(200, yValue);
    
    
    
    
}
-(void) assignSavedvalue
{
    if(appDelegate.arrayAccidentDetail!=nil)
    {
        AccidentDetail * dtl=[[AccidentDetail alloc]init];
       
        dtl=[appDelegate.arrayAccidentDetail objectAtIndex:0];
        dateTextView.text=dtl.date;
        timeTextView.text=dtl.time;
        locationTextView.text=dtl.location;
        policeOfficerTextView.text=dtl.PoliceOfficerAndStation;
        incidentTextView.text=dtl.incidentDescription;
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.scrollView.contentSize = CGSizeMake(200, 600);
    switch (textField.tag) {
        case DATE_TEXTFIELD_TAG:
        {
           //textField.inputAccessoryView = numberToolbar;
            currentTxtFldTag=textField.tag;
           
           
           datePicker.datePickerMode = UIDatePickerModeDate;
           NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
           [formatter setDateFormat:@"dd-MM-yyyy"];
           [textField setText:[formatter stringFromDate:datePicker.date]];

           break;
        }
        case TIME_TEXTFIELD_TAG:
        {
           // textField.inputAccessoryView = numberToolbar;
              currentTxtFldTag=textField.tag;
            datePicker.datePickerMode = UIDatePickerModeTime;
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
           [formatter setDateFormat:@"h:mm a"];
            [textField setText:[formatter stringFromDate:datePicker.date]];
            
            break;
        }
        case LOCATION_TEXTFIELD_TAG:
        {
           // textField.inputAccessoryView = numberToolbar;
              currentTxtFldTag=textField.tag;
            
            if(![Utility isIphone_5])
                [self moveScrolUP:200];
            
            
            break;
        }
        case POLICE_TEXTFIELD_TAG:
        {
           // textField.inputAccessoryView = numberToolbar;
              currentTxtFldTag=textField.tag;
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+150;
            }
            else if(t>50) {
                
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
            }
            
            [self moveScrolUP:t];
            break;
        }
        case INCIDENT_TEXTFIELD_TAG:
        {
           // textField.inputAccessoryView = numberToolbar;
              currentTxtFldTag=textField.tag;
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
            }
            
            // gourav 06 Nov start
            t= t+115;
            // gourav 06 Nov end
            
            [self moveScrolUP:t];
            break;
        }
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}



// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([[textField text] length] == 0) {
        if([string isEqualToString:@" "] || [string isEqualToString:@"\""]){
            NSLog(@"blank space can not inserted at initial position");
            return NO;
        }
    }
    
    if ([string isEqualToString:@". "]) {
        return NO;
    }


    return YES;
}// return NO to not change text


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self moveScrollDownForTextfield];
    [textField resignFirstResponder];
    return YES;
}
#pragma mark-
#pragma mark movesScrollview UP and DOWN-
-(void)moveScrolUP:(int)ht
{
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, ht);
    [UIView commitAnimations];
    
    
    CGRect frame = self.scrollView.frame;
    frame.origin.y = -ht;
    
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView commitAnimations];
    
}

-(void)moveScrollDown
{
    CGRect frame = CGRectMake(20, 18, 290, 350);
    if ([Utility isIphone_5]) {
        frame = CGRectMake(20, 18, 290, 440);
    }
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, offset.y);
    [UIView commitAnimations];
    
}

#pragma mark-
#pragma mark UITextViewDelegate Methods-

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (self.line1.textColor == [UIColor lightGrayColor]) {
        self.line1.text = @"";
        self.line1.textColor = [UIColor blackColor];
    }
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    offset = self.scrollView.contentOffset;
    int t;
    if ([Utility isIphone_5]) {
        t= 200;
    }
    else{
        t = 260;
    }
    [self moveScrolUP:t];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    
    
    
    return YES;
   
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
    
}

#pragma mark
#pragma mark IBActions-
-(void)callBack
{
    
    
    [self.navigationController popViewControllerAnimated:YES];
}


/*-(void)addPhotos
 {
 if (![self.addPhotoLabel.text isEqualToString:@"Photos 4/4"]) {
 [self showActionSheet];
 }
 
 
 }*/
-(void)sendData
{
    AccidentDetail * dtl=[[AccidentDetail alloc]init];
    appDelegate.arrayAccidentDetail=[[NSMutableArray alloc]init];
    
    dtl.date=dateTextView.text;
    dtl.time=timeTextView.text;
    dtl.location=locationTextView.text;
    dtl.PoliceOfficerAndStation=policeOfficerTextView.text;
    dtl.incidentDescription=incidentTextView.text;
    [appDelegate.arrayAccidentDetail addObject:dtl];
    
    
    if(dateTextView.text.length >0 || timeTextView.text.length >0 || locationTextView.text.length >0|| policeOfficerTextView.text.length >0|| incidentTextView.text.length >0)  // gourav 09 sep
    {
        appDelegate.checkForAccident=YES;
    }
    else
        appDelegate.checkForAccident=NO;

    
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark upload photo method and delegate-

-(void)findCurrentLocation
{
    
    
    //isCamera = YES;
    
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.latitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    
}
-(NSString *)Base64Encode:(NSData *)Inputdata{
    //Point to start of the data and set buffer sizes
    const uint8_t* input = (const uint8_t*)[Inputdata bytes];
    NSInteger length = [Inputdata length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];}

-(void)dealloc
{
    [super dealloc];
    [officeArray release];
    //[tabelView release];
    if (latiArray) {
        [latiArray release];
    }
    if (longiArray) {
        [longiArray release];
    }
    if (locationManager) {
        [locationManager release];
    }
    
    
}



#pragma mark-
#pragma mark tableView Delegate methods

#pragma mark-
#pragma mark UIWebViewDelegate methods-

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    if ([absoluteStr hasSuffix:@"Claims"])
    {
        int controllers = self.navigationController.viewControllers.count;
        for (int i = 0; i<controllers; i++) {
            if ([[[self.navigationController viewControllers] objectAtIndex:i] isKindOfClass:[ClaimWhatToDo class]]) {
                [self.navigationController popViewControllerAnimated:YES];
                return YES;
            }
        }
        
        
        ClaimWhatToDo *claimWhatToDo = [[ClaimWhatToDo alloc] initWithNibName:@"ClaimWhatToDo" bundle:nil];
        [self.navigationController pushViewController:claimWhatToDo animated:YES];
        [claimWhatToDo release];
    }
    
    return YES;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
