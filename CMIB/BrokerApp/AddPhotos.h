//
//  AddPhotos.h
//  Optimus1
//
//  Created by iphtech7 on 21/04/14.
//
//
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSData+Base64.h"
#import <CoreLocation/CoreLocation.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
//#import <ImageIO/CGImageSource.h>
//#import <ImageIO/CGImageProperties.h>
//#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "GAITrackedViewController.h"
#import "GAITrackedViewController.h"
@class AppDelegate;
@interface AddPhotos : GAITrackedViewController<UIWebViewDelegate, UITextFieldDelegate, UITextViewDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate , UIActionSheetDelegate , MFMailComposeViewControllerDelegate, CLLocationManagerDelegate , UITableViewDataSource, UITableViewDelegate>
{
    UIButton * addPhotosButton;
    UIImageView * uploadImage1;
    UIImageView * uploadImage2;
    UIImageView * uploadImage3;
    UIImageView * uploadImage4;
    UIImageView * uploadImage5;
    UIImageView * uploadImage6;
    UIImageView * uploadImage7;
    UIImageView * uploadImage8;
    UILabel * addPhotoLabel;
    BOOL isCamera;
    
     NSString * longitude_libPhoto;
     NSString * longitude;
     NSString * latitude_libPhoto;
     NSString * latitude;
    
    int checForEmptyArray;
    //int imageCount;
    
     CLLocationManager *locationManager;
    
    NSMutableArray * longiArray;
    NSMutableArray *latiArray;
    NSMutableArray *dateTimeArray;
    
    UIButton *sendButton;
    
    NSData * photo1Data;
    NSData * photo2Data;
    NSData * photo3Data;
    NSData * photo4Data;
    NSData * photo5Data;
    NSData * photo6Data;
    NSData * photo7Data;
    NSData * photo8Data;
    
    AppDelegate *appDelegate;
   
}
@property(nonatomic,strong)  NSString * longitude;
@property(nonatomic,strong)  NSString * latitude;


@property(nonatomic,strong)NSData * photo1Data;
@property(nonatomic,strong)NSData * photo2Data;
@property(nonatomic,strong)NSData * photo3Data;
@property(nonatomic,strong)NSData * photo4Data;
@property(nonatomic,strong)NSData * photo5Data;
@property(nonatomic,strong)NSData * photo6Data;
@property(nonatomic,strong)NSData * photo7Data;
@property(nonatomic,strong)NSData * photo8Data;


@end

