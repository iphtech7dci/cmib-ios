//
//  AddPhotos.m
//  Optimus1
//
//  Created by iphtech7 on 21/04/14.
//
//

#import "AddPhotos.h"
#import "Utility.h"
#import "Constants.h"
#import "PhotosDiscription.h"
#import "AppDelegate.h"

@interface AddPhotos ()

@end

@implementation AddPhotos
@synthesize photo1Data,photo2Data,photo3Data,photo4Data,photo5Data,photo6Data,photo7Data,photo8Data;
@synthesize latitude,longitude;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
    [self initializeNavigationBar];
    if([appDelegate.arrayPhotos count]>0)
    {
        //appDelegate.positionOfPhotos++;
    }
    else
    {
        appDelegate.arrayPhotos=[[NSMutableArray alloc]init];
        appDelegate.imageCount = 0;
        appDelegate.positionOfPhotos = 0;
    }
    
    checForEmptyArray = 0;
    longiArray = [[NSMutableArray alloc]init];
    latiArray = [[NSMutableArray alloc]init];
    dateTimeArray = [[NSMutableArray alloc]init];
    
    addPhotosButton = [[UIButton alloc] initWithFrame:CGRectMake(20 ,10,280, 42)];
    [addPhotosButton setBackgroundImage:[UIImage imageNamed:ADD_PHOTO_IMAGE] forState:UIControlStateNormal];
    [addPhotosButton setTitle:@"Add Photos" forState:UIControlStateNormal];
    [[addPhotosButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [addPhotosButton addTarget:self action:@selector(addPhotos) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addPhotosButton];
    [addPhotosButton release];
    
    uploadImage1 = [[UIImageView alloc]initWithFrame:CGRectMake(10+10+2 , 60+1,70-2, 70-2)];
    uploadImage1.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage1 layer]setBorderWidth:1];
    [[uploadImage1 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage1];
    [uploadImage1 release];
    
    
    uploadImage2 = [[UIImageView alloc]initWithFrame:CGRectMake(80+10 , 60,70, 70)];
    uploadImage2.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage2 layer]setBorderWidth:1];
    [[uploadImage2 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage2];
    [uploadImage2 release];
    
   
    
    uploadImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(150+10 , 60,70, 70)];
    uploadImage3.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage3 layer]setBorderWidth:1];
    [[uploadImage3 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage3];
    [uploadImage3 release];
    
    uploadImage4 = [[UIImageView alloc]initWithFrame:CGRectMake(220+10 , 60+1,70-2, 70-2)];
    uploadImage4.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage4 layer]setBorderWidth:1];
    [[uploadImage4 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage4];
    [uploadImage4 release];
    
    uploadImage5 = [[UIImageView alloc]initWithFrame:CGRectMake(10+10+2, 140+1,70-2, 70-2)];
    uploadImage5.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage5 layer]setBorderWidth:1];
    [[uploadImage5 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage5];
    [uploadImage5 release];
    
    
    uploadImage6 = [[UIImageView alloc]initWithFrame:CGRectMake(80+10 , 140,70, 70)];
    uploadImage6.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage6 layer]setBorderWidth:1];
    [[uploadImage6 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage6];
    [uploadImage6 release];
    
    
    
    uploadImage7 = [[UIImageView alloc]initWithFrame:CGRectMake(150+10 , 140,70, 70)];
    uploadImage7.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage7 layer]setBorderWidth:1];
    [[uploadImage7 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage7];
    [uploadImage7 release];
    
    uploadImage8 = [[UIImageView alloc]initWithFrame:CGRectMake(220+10 , 140+1,70-2, 70-2)];
    uploadImage8.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    [[uploadImage8 layer]setBorderWidth:1];
    [[uploadImage8 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.view addSubview:uploadImage8];
    [uploadImage8 release];

    addPhotoLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 220, 100, 20)];
    [addPhotoLabel setBackgroundColor:[UIColor clearColor]];
    [addPhotoLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    [addPhotoLabel setTextAlignment:UITextAlignmentCenter];
    [addPhotoLabel setText:@"Photos 0/8"];
    [self.view addSubview:addPhotoLabel];
    [addPhotoLabel release];
    
    sendButton = [[UIButton alloc] initWithFrame:CGRectMake(20 ,260,280, 42)];
    [sendButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [sendButton setTitle:@"Save & Go Back" forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [sendButton addTarget:self action:@selector(sendData) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sendButton];
    [sendButton release];
    
    
    
    UIImageView *roundedImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10+10, 60, 280, 70)];
    //imgView.image = [UIImage imageNamed:@"RoundRect_Full_Image"];
    [[roundedImgView layer]setBorderWidth:2.0f];
    [[roundedImgView layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [[roundedImgView layer] setCornerRadius:5];
    [self.view addSubview:roundedImgView];
    [roundedImgView release];
    
    
    
    
    UIImageView *roundedImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10+10, 140, 280, 70)];
    //imgView.image = [UIImage imageNamed:@"RoundRect_Full_Image"];
    [[roundedImgView1 layer]setBorderWidth:2.0f];
    [[roundedImgView1 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [[roundedImgView1 layer] setCornerRadius:5];
    [self.view addSubview:roundedImgView1];
    [roundedImgView1 release];

    
    
    if([appDelegate.arrayPhotos count]>0)
    {
        [self assignSavedvalue];
        
        // gourav 09 sep start
        NSString *temp =@"Photos ";
        temp = [temp stringByAppendingFormat:@"%d/8",appDelegate.imageCount];
        [addPhotoLabel setText:temp];
        // gourav 09 sep end
    }

    
[self findCurrentLocation];
    isCamera  = NO;


}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self ShowStatusBar:YES];
    
}
-(void) assignSavedvalue
{
    if(appDelegate.arrayPhotos!=nil)
    {
       PhotosDiscription *pto = (PhotosDiscription *)[appDelegate.arrayPhotos objectAtIndex:0];
        
        if(pto.photo1!=nil)
        {
            UIImage *image1 = [UIImage imageWithData:pto.photo1];
            //uploadImage1.image = image1;
            uploadImage1.image = [self imageWithImage:image1 convertToSize:CGSizeMake(70, 70)];
            self.photo1Data = pto.photo1;     //UIImageJPEGRepresentation(uploadImage1.image,4.0);
        }
        if(pto.photo2!=nil)
        {
            
            UIImage *image2 = [UIImage imageWithData:pto.photo2];
            //uploadImage2.image = image2;
            uploadImage2.image = [self imageWithImage:image2 convertToSize:CGSizeMake(70, 70)];
            self.photo2Data = pto.photo2;     // UIImageJPEGRepresentation(uploadImage2.image,4.0);
        }
        
        if(pto.photo3!=nil)
        {
            
            UIImage *image3 = [UIImage imageWithData:pto.photo3];
            // uploadImage3.image = image3;
            uploadImage3.image = [self imageWithImage:image3 convertToSize:CGSizeMake(70, 70)];
            self.photo3Data = pto.photo3;     //UIImageJPEGRepresentation(uploadImage3.image,4.0);
        }
        
        if(pto.photo4!=nil)
        {
            
            UIImage *image4 = [UIImage imageWithData:pto.photo4];
            // uploadImage4.image = image4;
            uploadImage4.image = [self imageWithImage:image4 convertToSize:CGSizeMake(70, 70)];
            self.photo4Data = pto.photo4;     //UIImageJPEGRepresentation(uploadImage4.image,4.0);
        }
        
        if(pto.photo5!=nil)
        {
            
            UIImage *image5 = [UIImage imageWithData:pto.photo5];
            // uploadImage5.image = image5;
            uploadImage5.image = [self imageWithImage:image5 convertToSize:CGSizeMake(70, 70)];
            self.photo5Data = pto.photo5;     //UIImageJPEGRepresentation(uploadImage5.image,4.0);
        }
        
        if(pto.photo6!=nil)
        {
            
            UIImage *image6 = [UIImage imageWithData:pto.photo6];
            //uploadImage6.image = image6;
            uploadImage6.image = [self imageWithImage:image6 convertToSize:CGSizeMake(70, 70)];
            self.photo6Data = pto.photo6;     //UIImageJPEGRepresentation(uploadImage6.image,4.0);
        }
        
        if(pto.photo7!=nil)
        {
            
            UIImage *image7 = [UIImage imageWithData:pto.photo7];
            // uploadImage7.image = image7;
            uploadImage7.image = [self imageWithImage:image7 convertToSize:CGSizeMake(70, 70)];
            self.photo7Data = pto.photo7;     //UIImageJPEGRepresentation(uploadImage7.image,4.0);
        }
        
        if(pto.photo8!=nil)
        {
            
            UIImage *image8 = [UIImage imageWithData:pto.photo8];
            // uploadImage8.image = image8;
            uploadImage8.image = [self imageWithImage:image8 convertToSize:CGSizeMake(70, 70)];
            self.photo8Data = pto.photo8;     //UIImageJPEGRepresentation(uploadImage8.image,4.0);
        }
        
        
        // gourav 06 Nov start
        if([pto.longitudeArray count]>0 && pto.longitudeArray!=nil)
            longiArray = [pto.longitudeArray mutableCopy];
        
        if([pto.latitudeArray count]>0 && pto.latitudeArray!=nil)
            latiArray = [pto.latitudeArray mutableCopy];
        
        if([pto.dateTimeArray count]>0 && pto.dateTimeArray!=nil)
            dateTimeArray = [pto.dateTimeArray mutableCopy];
        
        // gourav 06 Nov end
        
    }
}

-(void) sendData
{
    [appDelegate.arrayPhotos removeAllObjects];
    PhotosDiscription *pto = [[PhotosDiscription alloc]init];
    
    
//    self.photo1Data = UIImageJPEGRepresentation(uploadImage1.image,4.0);
//    self.photo2Data = UIImageJPEGRepresentation(uploadImage2.image,4.0);
//    self.photo3Data = UIImageJPEGRepresentation(uploadImage3.image,4.0);
//    self.photo4Data = UIImageJPEGRepresentation(uploadImage4.image,4.0);
//    self.photo5Data = UIImageJPEGRepresentation(uploadImage5.image,4.0);
//    self.photo6Data = UIImageJPEGRepresentation(uploadImage6.image,4.0);
//    self.photo7Data = UIImageJPEGRepresentation(uploadImage7.image,4.0);
//    self.photo8Data = UIImageJPEGRepresentation(uploadImage8.image,4.0);
    
    pto.photo1=self.photo1Data;
    pto.photo2=self.photo2Data;
    pto.photo3=self.photo3Data;
    pto.photo4=self.photo4Data;
    pto.photo5=self.photo5Data;
    pto.photo6=self.photo6Data;
    pto.photo7=self.photo7Data;
    pto.photo8=self.photo8Data;
    
    pto.latitudeArray  = [NSArray arrayWithArray:latiArray];
    pto.longitudeArray = [NSArray arrayWithArray:longiArray];
    pto.dateTimeArray  = [NSArray arrayWithArray:dateTimeArray];
    [appDelegate.arrayPhotos addObject:pto];
    [pto release];
    
    
    if(self.photo1Data !=nil ||self.photo2Data !=nil ||self.photo3Data !=nil||self.photo4Data !=nil||self.photo5Data !=nil||self.photo6Data !=nil||self.photo7Data !=nil||self.photo8Data !=nil)  // gourav 09 sep
    {
        appDelegate.checkForPhotos=YES;
    }
    else
        appDelegate.checkForPhotos=NO;

    
    [self.navigationController popViewControllerAnimated:YES];
    if([appDelegate.arrayPhotos count]>1)
    {
        appDelegate.positionOfPhotos++;
    }
    
}

-(void)initializeNavigationBar
{
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"Photos";
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(270, 0, 22, 17)];
    photoImage.image = [UIImage imageNamed:@"whiteCam.png"];
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
}

-(void)addPhotos
{
    if (![addPhotoLabel.text isEqualToString:@"Photos 8/8"]) {
        [self showActionSheet];
    }
    
    
}
-(void)showActionSheet
{
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Choose from library", nil];
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:[self capturePhoto];break;
            
        case 1:[self getPhotFromPhotoLibrary];break;
            
        default:
            break;
    }
}

-(void)capturePhoto
{
    // [self findCurrentLocation];
    @try
    {
        isCamera = YES;
        
        
        UIImagePickerController*   photoPicker = [[UIImagePickerController alloc] init];
        photoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        photoPicker.delegate = self;
        
        [self presentModalViewController:photoPicker animated:YES];
        [photoPicker release];
        
         [self performSelector:@selector(ShowStatusBar:) withObject:NO afterDelay:0.5f];
        
    }
    @catch (NSException *exception)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera" message:@"Camera is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
    
}

-(void)ShowStatusBar :(BOOL) show
{
    if(!show)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
        {
            
            for(UIView *v in appDelegate.window.subviews)
            {
                if([v isKindOfClass:[UIImageView class]])
                {
                    UIImageView *imageView = (UIImageView *)v;
                    [imageView setBackgroundColor:[UIColor clearColor]];
                    [imageView setImage:[UIImage imageNamed:@""]];
                    //[v removeFromSuperview];
                    
                }
                
            }
        }
    }
    else
    {
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
        {
            for(UIView *v in appDelegate.window.subviews)
            {
                if([v isKindOfClass:[UIImageView class]])
                {
                    UIImageView *imageView = (UIImageView *)v;
                    [imageView setImage:[UIImage imageNamed:STAUS_BAR_DARK_GREY]];
                }
                
            }
            
        }
        
        
    }
    
    
}



-(void)getPhotFromPhotoLibrary
{
    isCamera = NO;
    UIImagePickerController*  pickerLibrary = [[UIImagePickerController alloc] init];
    pickerLibrary.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [pickerLibrary setDelegate:self];
    [pickerLibrary.navigationBar setTintColor:[UIColor colorWithRed:20.0/255.0f green:16.0/255.0f blue:117.0/255.0f alpha:1.0]];
    //[pickerLibrary.navigationBar setTintColor:[UIColor colorWithRed:28.0/255.0f green:28.0/255.0f blue:28.0/255.0f alpha:1.0]];
    
    [self presentModalViewController:pickerLibrary animated:YES];
    [pickerLibrary release];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    
    UIImage * myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSURL *referenceURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:referenceURL resultBlock:^(ALAsset *asset)
    {
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        NSDictionary *metadata = rep.metadata;
        
        NSDictionary * dict = [metadata objectForKey:@"{GPS}"];
        NSDictionary * dateTimeDict = [metadata objectForKey:@"{TIFF}"];
        
        NSLog(@"date time dicttionary = %@",dateTimeDict);
        if (dict) {
            
            longitude_libPhoto = [dict objectForKey:@"Longitude"];
            longitude_libPhoto= [dict objectForKey:@"Latitude"];
            
        }
        if (!isCamera) {
            if (longitude_libPhoto) {
                NSLog(@"hello");
                checForEmptyArray = checForEmptyArray+1;
                [longiArray addObject:[NSString stringWithFormat:@"%f",[longitude_libPhoto floatValue]]];
                longitude_libPhoto = nil;
                
            }
            else{
                [longiArray addObject:@"No GPS data available for photo."];
            }
            if (longitude_libPhoto) {
                NSLog(@"hi");
                checForEmptyArray = checForEmptyArray+1;
                [latiArray addObject:[NSString stringWithFormat:@"%f",[latitude_libPhoto floatValue]]];
                longitude_libPhoto = nil;
            }
            else{
                [latiArray addObject:@"No GPS data available for photo."];
            }
            
            
            
            if (dateTimeDict) {
                NSString * dateTime = [dateTimeDict objectForKey:@"DateTime"];
                if (dateTime!= nil && dateTime.length >0) {
                    [dateTimeArray addObject:dateTime];
                }
                else      // gourav 09 sep
                    [dateTimeArray addObject:@"No date and time available for photo."];
            }
            else{
                [dateTimeArray addObject:@"No date and time available for photo."];
            }
            
            
            
        }
        
        
        
        
    } failureBlock:^(NSError *error) {
        // error handling
        // NSLog(@"error is %@",error);
    }];
    [library release];
    
    if (isCamera) {
        if (self.longitude) {
            NSLog(@"hello");
            checForEmptyArray = checForEmptyArray+1;
            [longiArray addObject:[NSString stringWithFormat:@"%f",[self.longitude floatValue]]];
            // self.longitude = nil;
            
        }
        else{
            [longiArray addObject:@"No GPS data available for photo."];
        }
        if (self.latitude) {
            
            NSLog(@"hi");
            checForEmptyArray = checForEmptyArray+1;
            [latiArray addObject:[NSString stringWithFormat:@"%f",[self.latitude floatValue]]];
            // self.latitude = nil;
        }
        else{
            [latiArray addObject:@"No GPS data available for photo."];
        }
        
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString * DateStr  = [DateFormatter stringFromDate:[NSDate date]];
        
        if (DateStr) {
            
            [dateTimeArray addObject:DateStr];
        }
    }
 
    
//    CGImageRef imgRef = [myImage copyCGImageAtTime:time actualTime:NULL error:nil];
//    UIImage *thumbnail = [[UIImage alloc] initWithCGImage:imgRef];
//    CFRelease(imgRef);
    
    
    appDelegate.imageCount++;
    if (appDelegate.imageCount == 1) {
        
        //uploadImage1.image = myImage;
        uploadImage1.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        
        [addPhotoLabel setText:@"Photos 1/8"];
        
        self.photo1Data = UIImageJPEGRepresentation(myImage, 0.4);
        
    }else if (appDelegate.imageCount ==2)
    {
        //uploadImage2.image = myImage;
        uploadImage2.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        [addPhotoLabel setText:@"Photos 2/8"];
        self.photo2Data = UIImageJPEGRepresentation(myImage,0.4);
        
    }else if(appDelegate.imageCount == 3){
        //uploadImage3.image = myImage;
        uploadImage3.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        [addPhotoLabel setText:@"Photos 3/8"];
        self.photo3Data = UIImageJPEGRepresentation(myImage,0.4);
        
    }else if (appDelegate.imageCount == 4)
    {
        //uploadImage4.image = myImage;
        uploadImage4.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        
        [addPhotoLabel setText:@"Photos 4/8"];
        self.photo4Data = UIImageJPEGRepresentation(myImage,0.4);
        
    }else if (appDelegate.imageCount == 5)
    {
        //uploadImage5.image = myImage;
        uploadImage5.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        [addPhotoLabel setText:@"Photos 5/8"];
        self.photo5Data = UIImageJPEGRepresentation(myImage,0.4);
        
    }else if (appDelegate.imageCount == 6)
    {
        //uploadImage6.image = myImage;
        uploadImage6.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        [addPhotoLabel setText:@"Photos 6/8"];
        self.photo6Data = UIImageJPEGRepresentation(myImage,0.4);
        
    }else if (appDelegate.imageCount == 7)
    {
        //uploadImage7.image = myImage;
        uploadImage7.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        [addPhotoLabel setText:@"Photos 7/8"];
        self. photo7Data = UIImageJPEGRepresentation(myImage,0.4);
        
    }else if (appDelegate.imageCount == 8)
    {
        //uploadImage8.image = myImage;
        uploadImage8.image = [self imageWithImage:myImage convertToSize:CGSizeMake(70, 70)];
        [addPhotoLabel setText:@"Photos 8/8"];
        self.photo8Data = UIImageJPEGRepresentation(myImage,0.4);
        
    }

    [self dismissViewControllerAnimated:YES completion:nil];
    
     [self ShowStatusBar:YES];
    
    
}

//gourav 09 sep
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}




-(void)findCurrentLocation
{
    
    
    isCamera = YES;
    
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.latitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    
}
-(NSString *)Base64Encode:(NSData *)Inputdata{
    //Point to start of the data and set buffer sizes
    const uint8_t* input = (const uint8_t*)[Inputdata bytes];
    NSInteger length = [Inputdata length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];}

-(void)callBack
{
//    if([appDelegate.arrayPhotos count]>1)
//    {
//        appDelegate.positionOfPhotos--;
//    }
//    else
//    {
//        appDelegate.positionOfPhotos=0;
//    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
