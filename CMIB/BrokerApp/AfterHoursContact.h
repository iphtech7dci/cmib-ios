//
//  AfterHoursContact.h
//  BrokerApp
//
//  Created by iPHTech2 on 11/06/13.
//
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>

@class  AppDelegate;


@interface AfterHoursContact : GAITrackedViewController<UIAlertViewDelegate , UIWebViewDelegate , MFMailComposeViewControllerDelegate>
{
    AppDelegate * appDelegate;
    NSString * telephoneNumber;
}

@property (nonatomic, retain) NSString * telephoneNumber;

-(void)initailizeNavigationBar;
-(void)initializeTheView;
-(void)callBack;
//-(void)openUrl:(UITapGestureRecognizer *) gr;

-(void)sendMail:(NSString *)mailAddress;
@end
