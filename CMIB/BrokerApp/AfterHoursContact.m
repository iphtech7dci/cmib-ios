//
//  AfterHoursContact.m
//  BrokerApp
//
//  Created by iPHTech2 on 11/06/13.
//
//

#import "AfterHoursContact.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "BrokerInfo.h"
#import "Utility.h"
#import "ClaimWhatToDo.h"
#import "ClaimsAndPhotos.h"

@interface AfterHoursContact ()

@end

@implementation AfterHoursContact
@synthesize telephoneNumber;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    //[self.navigationController.navigationBar setTintColor:[UIColor orangeColor]];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
//    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                    [UIColor whiteColor],NSForegroundColorAttributeName,
//                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    //self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
     //self.title = @"After Hours Contacts";
     self.screenName = @"After Hours Contacts";
    
    
    // gourav start 05 nov
    
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(0, 0, 180, 44);
    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:18];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.shadowColor = [UIColor darkGrayColor];
    titleView.shadowOffset = CGSizeMake(0, -1);
    titleView.text = @"After Hours Contacts";
    //titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    // gourav end 05 nov
    
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];

    [self initailizeNavigationBar];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
   
    
    [self initializeTheView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initializeTheView
{
       
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    dict = [appDelegate.appInfoDict objectForKey:BROKER_INFO_KEY];
    BrokerInfo * info  = [dict objectForKey:@"broker"];
    
       
     NSString *description = [NSString stringWithFormat:@"<html><head><style> .text {font-family:Helvetica; margin:10; font-size:12; color:black;}a:link {COLOR: black; text-decoration:none;}</style></head><body class='text'>%@ </body></html>", info.claims_instructions];
        
    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height-90)];
    if ([Utility isIphone_5]) {
        webview.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
    }
        
    [webview.scrollView setBounces:NO];
    [webview setOpaque:NO];
    [webview setDelegate:self];
    [webview setBackgroundColor:[UIColor clearColor]];
  
    [webview.scrollView setScrollEnabled:YES];
    [webview loadHTMLString:description baseURL:nil];
    [self.view addSubview:webview];
    [webview release];

}

#pragma mark-
#pragma mark IBActions-----

-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark AlertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSString * str = [NSString stringWithFormat:@"tel://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
   
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    
   if ([absoluteStr hasPrefix:@"tel:"]) {
        self.telephoneNumber = [absoluteStr stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
        self.telephoneNumber = [self.telephoneNumber stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:self.telephoneNumber message:@"Would you like to call this number?" delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
        [alert show];
        [alert release];
    }
   
    if ([absoluteStr hasPrefix:@"mailto:"]) {
         self.telephoneNumber = [absoluteStr stringByReplacingOccurrencesOfString:@"mailto:" withString:@""];
        [self sendMail:self.telephoneNumber];
        return NO;
        
    }
    if ([absoluteStr hasPrefix:@"whattodo:"]) {
        if(self.navigationController.viewControllers.count>0){
        for(id controllers in self.navigationController.viewControllers ){
            if([controllers isKindOfClass:[ClaimWhatToDo class]]){
                ClaimWhatToDo *claimWhatTo = (ClaimWhatToDo *)controllers;
                [self.navigationController popViewControllerAnimated:claimWhatTo];
                return YES;
            }
            
        }
            ClaimWhatToDo *claimWhattodo = [[ClaimWhatToDo alloc] initWithNibName:@"ClaimWhatToDo" bundle:nil];
            [self.navigationController pushViewController:claimWhattodo animated:YES];
        }
        else{
            ClaimWhatToDo *claimWhattodo = [[ClaimWhatToDo alloc] initWithNibName:@"ClaimWhatToDo" bundle:nil];
            [self.navigationController pushViewController:claimWhattodo animated:YES];
        }
        
        return YES;
    }
    if ([absoluteStr hasPrefix:@"claimandphoto:"]) {
        if(self.navigationController.viewControllers.count>0){
            for(id controllers in self.navigationController.viewControllers ){
                if([controllers isKindOfClass:[ClaimsAndPhotos class]]){
                    ClaimsAndPhotos *claimWhatTo = (ClaimsAndPhotos *)controllers;
                    [self.navigationController popViewControllerAnimated:claimWhatTo];
                    return YES;
                }
               
            }
            ClaimsAndPhotos *claimWhattodo = [[ClaimsAndPhotos alloc] initWithNibName:@"ClaimsAndPhotos" bundle:nil];
            [self.navigationController pushViewController:claimWhattodo animated:YES];
        }
        else{
            ClaimsAndPhotos *claimWhattodo = [[ClaimsAndPhotos alloc] initWithNibName:@"ClaimsAndPhotos" bundle:nil];
            [self.navigationController pushViewController:claimWhattodo animated:YES];
        }
        
        return YES;
    }
    
    
    if ([url.scheme isEqualToString:@"tel"]) {
        return NO;
    }
    return YES;
}



-(void)sendMail:(NSString *)mailAddress
{
    MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    NSArray *toRecipients = [NSArray arrayWithObject:mailAddress];
    
    [controller setToRecipients:toRecipients];
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
    if (controller)
        [self presentModalViewController:controller animated:YES];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissModalViewControllerAnimated:YES];
}


@end
