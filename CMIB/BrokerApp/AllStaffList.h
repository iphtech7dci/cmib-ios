//
//  AllStaffList.h
//  BrokerApp
//
//  Created by iPHTech2 on 01/06/13.
//
//

#import <UIKit/UIKit.h>
#import "OfficeInfo.h"


@class AppDelegate;


@interface AllStaffList : UIViewController<UITableViewDataSource, UITableViewDelegate , UISearchBarDelegate, UITextFieldDelegate>
{
    AppDelegate * appDelegate;
    NSMutableArray * staffArray;
    NSMutableArray * searchedStaff;
    UITableView * tableView;
    UISearchBar * searchBar;
    NSArray * searchIndexArray;
    
    BOOL searching;
    BOOL isStaffDetail;
    NSString *locationName;
    NSString * locationSelected;
    NSMutableArray * lastNameArray;
}


@property (nonatomic, retain) NSString * locationName;
@property (nonatomic, retain) NSString * locationSelected;
@property (nonatomic, retain) NSMutableArray * lastNameArray;
@property(nonatomic) BOOL isStaffDetail;
@property (nonatomic, retain) NSMutableArray * searchedStaff;
@property (nonatomic, retain) NSArray * searchIndexArray;
@property (nonatomic, retain) UISearchBar * searchBar;
@property (nonatomic, retain) UITableView * tableView;
@property (nonatomic, retain) NSMutableArray * staffArray;


@property(nonatomic,assign) int type;   // gourav 27Oct

@property(nonatomic, retain) UIImageView * searchBackGroundView;
@property(nonatomic, retain) UITextField * searchTextField;


-(void)initailizeNavigationBar;
-(void)getSortedArray:(NSMutableArray *)inputArray;
-(void)initailizeSearchView;
-(void)callBack;


@end
