//
//  AllStaffList.m
//  BrokerApp
//
//  Created by iPHTech2 on 01/06/13.
//
//

#import "AllStaffList.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "Staff.h"
#import "Utility.h"
#import "StaffDetails.h"
#import "InsuranceInfo.h"
#import "InsuranceDetailViewController.h"

@interface AllStaffList ()

@end

@implementation AllStaffList

@synthesize staffArray,locationName;
@synthesize tableView;
@synthesize searchBar;
@synthesize searchIndexArray;
@synthesize searchedStaff;
@synthesize isStaffDetail;
@synthesize lastNameArray;
@synthesize locationSelected;
@synthesize searchBackGroundView;
@synthesize searchTextField;
@synthesize type;  // gourav 27 Oct


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    }
    return self;
}

-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
   
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
}

-(void)getSortedArray:(NSMutableArray *)inputArray
{
    NSString * first = @"firstName";
    NSString * last = @"lastName";
    
    NSMutableArray * tempArray = [[NSMutableArray alloc]init];
    
    
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    
    for (int i = 0; i<[inputArray count]; i++) {
        Staff * staf  = nil;
        staf = [inputArray objectAtIndex:i];
        dict = (NSMutableDictionary *)[NSDictionary dictionaryWithObjectsAndKeys:staf.firstName, first, staf.lastName, last, nil];
        [tempArray addObject:dict];
        
    }
    
    
    NSSortDescriptor *lastDescriptor =
    [[NSSortDescriptor alloc] initWithKey:last
                                ascending:YES
     
                                 selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray *descriptors = [NSArray arrayWithObjects:lastDescriptor, nil];
    
	NSArray *sortedElements = [inputArray sortedArrayUsingDescriptors:descriptors];
    [self.staffArray removeAllObjects];
	[self.staffArray addObjectsFromArray:sortedElements];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    [self initailizeNavigationBar];
    [self initailizeSearchView];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.staffArray  = [[NSMutableArray alloc]init];
    if (isStaffDetail) {
        self.title = @"Key Staff";
        NSMutableArray * tempArray = [NSMutableArray arrayWithArray:[appDelegate.appInfoDict objectForKey:STAFF_INFO_KEY]];
        for (int i = 0; i<[tempArray count]; i++)
        {
            Staff * staf = [tempArray objectAtIndex:i];
            if (appDelegate.allkey)
            {
                 [self.staffArray addObject:staf];
            }
            else
            {
                if ([staf.officeLocation isEqualToString:appDelegate.checklocation])
                {
                    [self.staffArray addObject:staf];
                    
                }
            }
            
            
        }
        [self getSortedArray:self.staffArray];
//        if(![self.locationSelected isEqualToString:@"AllKeys"])
//        {
//            NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF.officeLocation LIKE[c] %@", self.locationSelected];
//            NSArray *filteredArray = [self.staffArray filteredArrayUsingPredicate:pred];
//            [self.staffArray removeAllObjects];
//            [self.staffArray addObjectsFromArray:filteredArray];
//        }
    }
    else{
        
//        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(20, 5, 265, 40)];
//        [label setBackgroundColor:[UIColor clearColor]];
//        [label setText:@"Insurers Assist Numbers"];
//        [label setFont:[UIFont boldSystemFontOfSize:17]];
//        [label adjustsFontSizeToFitWidth];
//        [label setTextColor:[UIColor whiteColor]];
//        self.navigationItem.titleView = label;
//        [label release];
        
        
        // gourav start 05 nov
        
        
        CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
        UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
        _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
        _headerTitleSubtitleView.autoresizesSubviews = NO;
        
        CGRect titleFrame = CGRectMake(-5, 0, 195, 44);
        UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:17];
        titleView.textAlignment = NSTextAlignmentLeft;
        titleView.textColor = [UIColor whiteColor];
        titleView.shadowColor = [UIColor darkGrayColor];
        titleView.shadowOffset = CGSizeMake(0, -1);
        titleView.text = @"Insurers Assist Numbers";
        //titleView.adjustsFontSizeToFitWidth = YES;
        [_headerTitleSubtitleView addSubview:titleView];
        
        self.navigationItem.titleView = _headerTitleSubtitleView;
        
        // gourav end 05 nov

        
        
        NSMutableArray * tempArray = [NSMutableArray arrayWithArray:[appDelegate.appInfoDict objectForKey:INSURANCE_INFO_KEY]]; //new
        
        
        NSArray *sortedArray = [tempArray sortedArrayUsingComparator:^(InsuranceInfo *firstObject, InsuranceInfo *secondObject)
        {
            return [firstObject.company compare:secondObject.company];
        }];
        self.staffArray  = [NSMutableArray arrayWithArray:sortedArray];
    }
    
    
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 53, 320, 380-63) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.backgroundView = [[[UIView alloc] initWithFrame:self.tableView.bounds] autorelease];
    self.tableView.backgroundView.backgroundColor = [UIColor whiteColor];
    
    
    //self.tableView.tableHeaderView = self.searchBar;
	//self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    
    
    [self.view addSubview:self.tableView];
    if ([Utility isIphone_5]) {
        self.tableView.frame = CGRectMake(0, 53, 320, 460-53);
    }
    
    self.searchedStaff = [[NSMutableArray alloc]init];
    //self.searchIndexArray = [[NSArray alloc]initWithObjects:@"A",@"B",@"C",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z", nil];
    
    //self.automaticallyAdjustsScrollViewInsets = NO;
    
    //[self.tableView setBackgroundColor:[UIColor blueColor]];
    
    
    //self.extendedLayoutIncludesOpaqueBars = YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     [self setSearchBarInDefaultForm];
  
    
    
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_RED_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    //[self.navigationController.navigationBar setBackgroundImage:<#(UIImage *)#> forBarMetrics:<#(UIBarMetrics)#>]
    
    /*
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleBlackTranslucent]; // UIStatusBarStyleLightContent
        UIImageView *statusBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        [statusBarView setBackgroundColor:[UIColor colorWithRed:231.0f/255.0f green:72.0f/255.0f blue:27.0f/255.0f alpha:1.0f]];
        [statusBarView setImage:[UIImage imageNamed:st]];
        [appDelegate.window addSubview:statusBarView];
    }
     */

    
}

#pragma mark-
#pragma mark get SearchView-

-(void)initailizeSearchView
{
//    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
//    [self.searchBar setDelegate:self];
//    [self.searchBar setPlaceholder:@"Search"];
//    [self.searchBar setTintColor:[UIColor blackColor]];
//    //[self.view addSubview:self.searchBar];
    
    
    
    
    searchBackGroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 48)];
    [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
    [self.view addSubview:searchBackGroundView];
    
    
    searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, 320, 48)];
    searchTextField.textColor = [UIColor whiteColor];
    
    // searchTextField.placeholder = @"Search";
    
    searchTextField.delegate = self;
    [self.view addSubview:searchTextField];
    
    
//    UIImageView *redImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 48, 320, 5)];
//    [redImageView setImage:[UIImage imageNamed:@"DarkGreyImage.png"]];
//    [self.view addSubview:redImageView];
    
    
    
}







#pragma mark-
#pragma mark UITextFieldDelegate Methods-
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    return YES;
//}// return NO to disallow editing.
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    //[textField resignFirstResponder];
    
    
    //    if(textField.text.length==0)
    //    {
    //        [searchBackGroundView setImage:[UIImage imageNamed:@"SearchViewWithText.png"]];
    //    }
    
    
    
    //[searchBackGroundView setImage:[UIImage imageNamed:@"SearchViewWithText.png"]];

    searchTextField.text =@"";
    searching = NO;
    self.tableView.scrollEnabled = YES;
    [self.searchedStaff removeAllObjects];
    [self.tableView reloadData];
    [textField resignFirstResponder];
    [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
    
    return YES;
}// return NO to disallow editing.

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    self.tableView.scrollEnabled =YES;
    self.searchBar.showsCancelButton = YES;
    
    
    if(textField.text.length==0)
    {
        //[textField resignFirstResponder];
        [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite_Blank.png"]];
    }
    
    
    
    
}// became first responder
// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length==0 )
    {
        [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
    }
    
    
    
    [textField resignFirstResponder];
    
    if ([textField.text length] > 0 )
    {
        searching = YES;
        
        self.tableView.scrollEnabled = YES;
        
    }
    
    
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ([textField.text length] > 0 )
    {
        // searching = YES;
        //        self.myTableView.scrollEnabled = YES;
        //        [self searchTableView];
        //        [self.myTableView reloadData];
        
        // [self performSelector:@selector(searchingCalled) withObject:nil afterDelay:0.1f];
        
        
        if(textField.text.length != 1)
        {
            searching = YES;
            [self performSelector:@selector(searchingCalled) withObject:nil afterDelay:0.1f];
        }
        
        
        
    }
    else
    {
        
        if([string length] >0)
        {
            searching = YES;
            [self performSelector:@selector(searchingCalled) withObject:nil afterDelay:0.1f];
        }
        else
        {
            [self setSearchBarInDefaultForm];
        }
        
    }
    
    
    
    return YES;
}// return NO to not change text


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


-(void)searchingCalled
{
    self.tableView.scrollEnabled = YES;
    [self searchTableView];
    [self.tableView reloadData];
}

-(void)setSearchBarInDefaultForm
{
    searchTextField.text =@"";
    searching = NO;
    self.tableView.scrollEnabled = YES;
    [self.searchedStaff removeAllObjects];
    [self.tableView reloadData];
    [searchTextField resignFirstResponder];
    [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
    
}


#pragma mark-
#pragma mark UITable TableView Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searching) {
        return [self.searchedStaff count];
    }
    
    return [self.staffArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView1 setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell  = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
    }
    
    cell.accessoryView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:ACCESSORY_RED_IMAGE]] autorelease];
    NSString * name ;
    if(isStaffDetail)
    {
        if (searching && [self.searchedStaff count]>0)
        {
            Staff * staf = [self.searchedStaff objectAtIndex:indexPath.row];
            name = [NSString stringWithFormat:@"%@ %@", staf.firstName, staf.lastName];
        }
        else
        {
            Staff * staf = [self.staffArray objectAtIndex:indexPath.row];
            name = [NSString stringWithFormat:@"%@ %@", staf.firstName, staf.lastName];
        }
        
    }
    else{
        if (searching && [self.searchedStaff count]>0)
        {
            InsuranceInfo * info = [self.searchedStaff objectAtIndex:indexPath.row];
            name = [NSString stringWithFormat:@"%@ ", info.company];
        }
        else
        {
            InsuranceInfo * info  = [self.staffArray objectAtIndex:indexPath.row];
            name = [NSString stringWithFormat:@"%@", info.company];
        }
    }
    
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 59, 320, 1)];
    [lineView setBackgroundColor:[UIColor darkGrayColor]];
    [cell.contentView addSubview:lineView];
    
    cell.textLabel.text =name;
    
    cell.textLabel.textColor = [UIColor darkGrayColor];
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *officeArray = [[NSMutableArray alloc] initWithArray:[appDelegate.appInfoDict objectForKey:OFFICE_INFO_KEY]];//Vish
    
   
    
    [tableview deselectRowAtIndexPath:indexPath animated:YES];
    
    if (isStaffDetail) {
        Staff * staf;
        if (searching) {
            staf = [self.searchedStaff objectAtIndex:indexPath.row];
        }
        else{
            staf = [self.staffArray objectAtIndex: indexPath.row];
        }
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF.location LIKE[c] %@", staf.officeLocation];
        NSArray *filteredArray = [officeArray filteredArrayUsingPredicate:pred];
        OfficeInfo *office;
        if(filteredArray)
        {
            office = [filteredArray objectAtIndex:0];
        }

        StaffDetails * details = [[StaffDetails alloc]initWithNibName:@"StaffDetails" bundle:nil];
        details.staf = staf;
        details.officeInfoObj = office;
        
        [self.navigationController pushViewController:details animated:YES];
        
        [details release];
    }
    else{
        
        InsuranceInfo * info ;
        if (searching) {
            info = [self.searchedStaff objectAtIndex:indexPath.row];
        }
        else{
            info = [self.staffArray objectAtIndex:indexPath.row];
        }
        
        InsuranceDetailViewController * detail = [[InsuranceDetailViewController alloc]initWithNibName:@"InsuranceDetailViewController" bundle:nil];
        detail.insuInfo = info;
        [self.navigationController pushViewController:detail animated:YES];
        [detail release];
    }
    
   [self setSearchBarInDefaultForm]; //  gourav 11 Nov
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if(indexPath.row ==0)
//        return 20;
    
    return 60;
}

/*
 - (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
 
 return self.searchIndexArray;
 
 
 }
 
 
 - (NSInteger)tableView:(UITableView *)tableView2 sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
 NSMutableArray *myarray = [[NSMutableArray alloc] init];
 NSString *firstName;
 
 if (isStaffDetail) {
 for (int i = 0; i<[self.staffArray count]; i++) {
 Staff *sd = [self.staffArray objectAtIndex:i];
 firstName = [NSString stringWithFormat:@"%@ %@", sd.firstName, sd.lastName];
 [myarray addObject:firstName];
 }
 }
 else
 {
 for (int i = 0; i<[self.staffArray count]; i++) {
 InsuranceInfo *iF = [self.staffArray objectAtIndex:i];
 [myarray addObject:iF.company];
 }
 }
 
 
 NSInteger newRow = [self indexForFirstChar:title inArray:self.searchIndexArray];
 NSLog(@"[tempArray objectAtIndex:newRow]: %@",[self.searchIndexArray objectAtIndex:newRow]);
 NSPredicate *predicate = [NSPredicate predicateWithFormat:
 @"SELF beginswith[c] %@",[self.searchIndexArray objectAtIndex:newRow]];
 NSArray *aNames = [myarray filteredArrayUsingPredicate:predicate];
 if (aNames.count > 0) {
 newRow = [myarray indexOfObject:[aNames objectAtIndex:0]];
 [tableView2 scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:newRow inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
 
 }
 return index;
 
 }
 - (NSInteger)indexForFirstChar:(NSString *)character inArray:(NSArray *)array
 {
 NSUInteger count = 0;
 for (NSString *str in array) {
 if ([str hasPrefix:character]) {
 return count;
 }
 count++;
 }
 return 0;
 }
 
 */

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark
#pragma mark IBAction Methods-

-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}




#pragma mark-
#pragma mark searchBarDelegate Method

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    
    return [self.searchBar resignFirstResponder];
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    self.tableView.scrollEnabled =YES;
    self.searchBar.showsCancelButton = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    searching = NO;
    self.searchBar.showsCancelButton = NO;
    self.searchBar.text = nil;
    [self.searchBar resignFirstResponder];
    [self.tableView reloadData];
    
    
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 0 )
    {
        searching = YES;
        self.tableView.scrollEnabled = YES;
        [self searchTableView];
        [self.tableView reloadData];
        
    }
    else
    {
        searching = NO;
        tableView.scrollEnabled = YES;
        [self.searchedStaff removeAllObjects];
        [self.tableView reloadData];
        [self.searchBar resignFirstResponder];
        
    }
    
    
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar1
{
    [self.searchBar resignFirstResponder];
    
    if ([searchBar1.text length] > 0 )
    {
        searching = YES;
        
        tableView.scrollEnabled = YES;
        
    }
}


-(void) searchTableView
{
    
    NSString *searchText = searchTextField.text;//searchBar.text;
    
    NSMutableArray *nameArr = [[NSMutableArray alloc] init];
    
    NSMutableArray * searchAry = [[NSMutableArray alloc]init];
    
    if (isStaffDetail)
    {
        for (int i = 0; i < [self.staffArray count]; i++)
        {
            Staff * staf = [self.staffArray objectAtIndex:i];
            NSString * name = [NSString stringWithFormat:@"%@ %@", staf.firstName, staf.lastName];
            [nameArr addObject:name];
            
        }
        
        for(int i = 0; i<[nameArr count]; i++)
        {
            NSString * sTemp = [nameArr objectAtIndex:i];
            NSRange titleResultRange = [sTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (titleResultRange.length > 0)
            {
                NSLog(@"copy item %@",[nameArr objectAtIndex:i]);
                
                [searchAry addObject:[self.staffArray objectAtIndex:i]];
                
                
                
            }
        }
        
    }
    else
    {
        for (int i = 0; i < [self.staffArray count]; i++)
        {
            InsuranceInfo * info  = [self.staffArray objectAtIndex:i];
            NSString * name = [NSString stringWithFormat:@"%@", info.idName];
            [nameArr addObject:name];
            
        }
        
        for(int i = 0; i<[nameArr count]; i++)
        {
            NSString * sTemp = [nameArr objectAtIndex:i];
            NSRange titleResultRange = [sTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (titleResultRange.length > 0)
            {
                NSLog(@"copy item %@",[nameArr objectAtIndex:i]);
                
                [searchAry addObject:[self.staffArray objectAtIndex:i]];
                
                
                
            }
        }
        
    }
    
    self.searchedStaff = [[[[NSOrderedSet alloc] initWithArray:searchAry] array] mutableCopy];
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [self.searchBar resignFirstResponder];
    
    for(id subview in [self.searchBar subviews])
    {
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview setEnabled:YES];
        }
    }
}


@end
