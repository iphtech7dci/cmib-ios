//
//  AppDelegate.h
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import "DataHandle.h"
#import "sqlite3.h"
#import <CFNetwork/CFNetwork.h>
#import "GAITracker.h"
#import "GAI.h"
#import <CoreData/CoreData.h>
#import "DirectNumbers.h"
@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate , UITabBarControllerDelegate , UITabBarDelegate , NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIAlertViewDelegate>
{
    NSMutableDictionary * appInfoDict;
    UINavigationController *navController;
    sqlite3	      *  database;
    DirectNumbers *directVC;
    DataHandle * dataHandle;
    BOOL shouldAllowEditing;
    BOOL isFaceBookSharing;
    UITabBar   *  tabBar;
    UINavigationController *naviController;
    BOOL isSleep;
    
    NSMutableArray * arrayInsuredetail;
    NSMutableArray * arrayVehicleDetail;
    NSMutableArray * arrayThirdParty;
    NSMutableArray * arrayAccidentDetail;
    NSMutableArray * arrayPhotos;
    
    NSString *checklocation;
    
    NSMutableArray * arrayMotervehicleIncident;
    
    int imageCount;
    BOOL allkey;
    BOOL checkForInsured;
    BOOL checkFprVehicle;
    BOOL checkForTrdParty;
    BOOL checkForAccident;
    BOOL checkForPhotos;
    
    int countOfAddPolicy;
    int positionOfPhotos;
}
@property (nonatomic, assign) sqlite3 *database;
@property (nonatomic) BOOL isFaceBookSharing;
@property (nonatomic) BOOL allkey;
@property (nonatomic,strong) NSString *checklocation;
@property (nonatomic) BOOL shouldAllowEditing;
@property (retain, nonatomic)  DataHandle       * dataHandle;
@property (nonatomic, retain) UINavigationController * navController;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSMutableDictionary * appInfoDict;
@property (strong, nonatomic) ViewController *viewController;
@property(strong,nonatomic) DirectNumbers *directVC;
@property (nonatomic, retain) UITabBarController   *  tabBarController;
@property (nonatomic, retain) NSMutableArray * tabBarArray;
@property (nonatomic, retain) UITabBar   *  tabBar;
@property(nonatomic, retain) id<GAITracker> tracker;
@property(nonatomic, assign) BOOL okToWait;
@property(nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);

@property (nonatomic, retain) NSMutableArray * arrayInsuredetail;
@property (nonatomic, retain) NSMutableArray * arrayVehicleDetail;
@property (nonatomic, retain) NSMutableArray * arrayThirdParty;
@property (nonatomic, retain) NSMutableArray * arrayAccidentDetail;
@property (nonatomic, retain) NSMutableArray * arrayPhotos;
@property (nonatomic, retain) NSMutableArray * arrayMotervehicleIncident;

@property (nonatomic,strong)  NSMutableArray * pressRoomArray;


@property int imageCount;
@property int countOfAddPolicy;

@property BOOL checkForInsured;
@property BOOL checkFprVehicle;
@property BOOL checkForTrdParty;
@property BOOL checkForAccident;
@property BOOL checkForPhotos;
@property int positionOfPhotos;
-(void)createEditableCopyOfDatabaseIfNeeded;
- (void)openDatabaseConnection;
- (void)closeDatabaseConnection;
-(void)setViewToTabbar;

@end
