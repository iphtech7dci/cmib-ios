//
//  AppDelegate.m
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "OfficesViewController.h"
#import "MyProfileViewController.h"
#import "InformationsViewController.h"
#import "ShareFeature.h"
#import "DirectNumbers.h"
#import "Constants.h"



static NSString *const kTrackingId = @"UA-42488522-37";

static NSString *const kAllowTracking = @"allowTracking";


@implementation AppDelegate
@synthesize tabBarController,checklocation,allkey;
@synthesize tabBarArray,database;
@synthesize appInfoDict,directVC;
@synthesize navController;
@synthesize dataHandle;
@synthesize shouldAllowEditing;
@synthesize tabBar;
@synthesize tracker = tracker_;
@synthesize isFaceBookSharing;
@synthesize arrayInsuredetail,arrayVehicleDetail,arrayThirdParty,arrayAccidentDetail,arrayPhotos,arrayMotervehicleIncident,imageCount;
@synthesize checkForAccident,checkForInsured,checkForPhotos,checkForTrdParty,checkFprVehicle,countOfAddPolicy,positionOfPhotos;
@synthesize pressRoomArray;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [appInfoDict release];
    
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
   
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        // For iOS 8
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        // For iOS < 8
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

    countOfAddPolicy=1;
    positionOfPhotos=0;
    self.allkey = NO;
    //GA traking code
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];
    //[[GAI sharedInstance] trackerWithName:@"Steadfast" trackingId:kTrackingId];
    
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.appInfoDict = [[NSMutableDictionary alloc]init];
    self.tabBar = [[UITabBar alloc]init];
    self.tabBar.delegate = self;
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    
    [self createEditableCopyOfDatabaseIfNeeded];
	[self openDatabaseConnection];
    self.dataHandle = [[DataHandle alloc] initWithDbConnection:database];
 
    [self setViewToTabbar];

    return YES;
}



- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
   
    
    NSString *tokenStr = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    tokenStr = [tokenStr stringByReplacingOccurrencesOfString:@" " withString:@""];

  //  NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.iphmusic.com/MRIB_Group_Brokerapp/collect_id.php?registration_id=%@",tokenStr]]];
    
      NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.brokerapps.com.au/pushnotifications/PN/collect_id.php?registration_id=%@&app_name=%@",tokenStr,APP_NAME_FOR_PN]]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
   
    [connection start];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    // Inform the user that registration failed.
    
    NSLog(@"Failed to get token, error: %@", error);
    
    UIAlertView *failed = [[UIAlertView alloc] initWithTitle:@"Sorry " message:@"Its looks like we are having trouble trying to register you for the push notification " delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    
    [failed show];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
{
    //Called when the OS receives a RemoteNotification and the app is running (in the background/suspended or in the foreground.
    
    NSString *url_String1 = (NSString *)[[userInfo objectForKey:@"aps"] objectForKey:@"url"];
    
    if (url_String1!=nil) {
        if(![url_String1 isKindOfClass:[NSNull class]])
        {
            [[NSUserDefaults standardUserDefaults] setObject:url_String1 forKey:@"PUSH_URL"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    if (application.applicationState == UIApplicationStateActive)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Push Notification" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
        handler(UIBackgroundFetchResultNewData);
        
    }
    else if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive)
    {
        // Do something else rather than showing an alert view, because it won't be displayed.
        
        if(url_String1!=nil)
            // [self openNotification_URL:url_String1];
            [self performSelector:@selector(openNotification_URL:) withObject:url_String1 afterDelay:0.1f];
        
        handler(UIBackgroundFetchResultNewData);
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *url_Str = [[NSUserDefaults standardUserDefaults] stringForKey:@"PUSH_URL"];
    [self openNotification_URL:url_Str];
    
}

-(void)openNotification_URL:(NSString *)urlStr
{
    if (!urlStr || urlStr.length==0) {
        return;
    }
    
    
    if ([urlStr hasPrefix:@"http://"] || [urlStr hasPrefix:@"https://"])
        urlStr = urlStr;
    else
        urlStr = [NSString stringWithFormat:@"http://%@",urlStr];
    
    
    NSLog(@"str= %@",urlStr);
    
    if([[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]])
        NSLog(@"URL opened");
    else
        NSLog(@"Error in opening URL ");
    
    
}


-(void)application:(UIApplication *)application
performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [self sendHitsInBackground];
    completionHandler(UIBackgroundFetchResultNewData);
}


- (void)sendHitsInBackground {
    self.okToWait = YES;
    __weak AppDelegate *weakSelf = self;
    __block UIBackgroundTaskIdentifier backgroundTaskId =
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        weakSelf.okToWait = NO;
    }];
    
    if (backgroundTaskId == UIBackgroundTaskInvalid) {
        return;
    }
    
    self.dispatchHandler = ^(GAIDispatchResult result) {
        // If the last dispatch succeeded, and we're still OK to stay in the background then kick off
        // again.
        if (result == kGAIDispatchGood && weakSelf.okToWait ) {
            [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
        } else {
            [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskId];
        }
    };
    [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
}


#pragma mark-
#pragma mark Deal With DataBase-





-(void)createEditableCopyOfDatabaseIfNeeded
{
    
	BOOL success = NO;
	NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"MyProfile.sqlite"];
	success = [fileManager fileExistsAtPath:writableDBPath];
    if (success)
    {
        NSLog(@"success");
        return;
    }
    else{
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"MyProfile.sqlite"];
        
        success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
        if (!success) {
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }
    
}

- (void)openDatabaseConnection {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"MyProfile.sqlite"];
    
    if (sqlite3_open([path UTF8String], &database) != SQLITE_OK) {
        
        sqlite3_close(database);
        NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(database));
        
    }
}

- (void)closeDatabaseConnection {
    
    if (sqlite3_close(database) != SQLITE_OK) {
        NSAssert1(0, @"Error: failed to close database with message '%s'.", sqlite3_errmsg(database));
    }
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    isSleep = YES;
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self sendHitsInBackground];
    // exit(1);
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
//    if(!self.isFaceBookSharing)
//    {
//        [self.tabBarController removeFromParentViewController];
//        [self.tabBarController release];
//        
//        [self setViewToTabbar];
//    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    NSLog(@"%@", self.navController.viewControllers);
    id objectController = [[self.navController viewControllers] lastObject];
    if([objectController isKindOfClass:[ShareFeature class]])
    {
        
        return [[(ShareFeature *)objectController facebook] handleOpenURL:url];
    }
    return YES;
    
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[UINavigationController class]])
    {
        
        
        naviController = (UINavigationController *)viewController;
        
        if (naviController) {
            [naviController popToRootViewControllerAnimated:NO];
        }
    }
    
    
    
    return YES;
}
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    //[naviController popToRootViewControllerAnimated:NO];
    
}

#pragma mark-
#pragma mark set tab bar controllers-

-(void)setViewToTabbar
{
    self.tabBarArray = [[NSMutableArray alloc]init];
    
    
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    UINavigationController *viewNavController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    [self.tabBarArray addObject:viewNavController];
    
    
    self.viewController.title = @"MENU";
    [ViewController release];
    
    DirectNumbers * offices = [[DirectNumbers alloc]initWithNibName:@"DirectNumbers" bundle:nil];
    UINavigationController * officeNavController = [[UINavigationController alloc]initWithRootViewController:offices];
    [officeNavController setNavigationBarHidden:YES];
    [self.tabBarArray addObject:officeNavController];
    
    offices.title = @"DIRECTORY";
    [offices release];
    
    
    MyProfileViewController * profile = [[MyProfileViewController alloc]initWithNibName:@"MyProfileViewController" bundle:nil];
    UINavigationController * profileNavController = [[UINavigationController alloc]initWithRootViewController:profile];
    [profileNavController setNavigationBarHidden:YES];
    [self.tabBarArray addObject:profileNavController];
    profile.title = @"MY PROFILE";
    [profile release];
    
    
    InformationsViewController * information = [[InformationsViewController alloc]initWithNibName:@"InformationsViewController" bundle:nil];
    UINavigationController * informationNavController = [[UINavigationController alloc]initWithRootViewController:information];
    [informationNavController setNavigationBarHidden:YES];
    [self.tabBarArray addObject:informationNavController];
    
    information.title = @"INFORMATION";
    [information release];
    
    
    self.tabBarController = [[UITabBarController alloc] init] ;
    [self.tabBarController setDelegate:self];
    
    self.tabBarController.viewControllers = self.tabBarArray;
    self.window.rootViewController = self.tabBarController;
    
    //[[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"TabBarBG.png"]];
     [[UITabBar appearance] setBarTintColor:[UIColor blackColor]];
    
    //[[UITabBar appearance] setBackgroundColor:BACKGROUND_COLOR_PINK];
    
    NSArray *tabBarItems = [[self.tabBarController tabBar] items];
    
    for (int i=0; i<[tabBarItems count]; i++)
    {
        
        UITabBarItem *item = [tabBarItems objectAtIndex:i];
        
        
        if ([[UIDevice currentDevice] systemVersion].floatValue >= 8.0) {
            
            switch (i) {
                case 0:
                {
                    UIImage *deselectedImage = [[UIImage imageNamed:@"deselectedMenu.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    UIImage *selectedImage = [[UIImage imageNamed:@"selectedMenu.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    
                    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:deselectedImage];
                }
                break;
                
                case 1:
                {
                    UIImage *deselectedImage = [[UIImage imageNamed:@"deselectedOfficeHome.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    UIImage *selectedImage = [[UIImage imageNamed:@"selectedOfficeHome.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    
                    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:deselectedImage];
                }
                    break;
                case 2:
                {
                    UIImage *deselectedImage = [[UIImage imageNamed:@"deselectedProfileBtn.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    UIImage *selectedImage = [[UIImage imageNamed:@"selectedProfileBtn.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    
                    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:deselectedImage];
                }
                    break;
                case 3:
                {
                    UIImage *deselectedImage = [[UIImage imageNamed:@"deselectedInfo.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    UIImage *selectedImage = [[UIImage imageNamed:@"selectedInfo.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    
                    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:deselectedImage];
                }
                    break;

                 default:
                    break;
            }
                    
            
            
        }
        
        else
        {
            
            
            switch (i) {
                case 0:
                {
                    [item setImage:[UIImage imageNamed:@"selectedMenu.png"]];
                    [item setFinishedSelectedImage:[UIImage imageNamed:@"selectedMenu.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"deselectedMenu.png"]];
                    
                }
                    break;
                case 1:
                {
                    [item setImage:[UIImage imageNamed:@"deselectedOfficeHome.png"]];
                    [item setFinishedSelectedImage:[UIImage imageNamed:@"selectedOfficeHome.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"deselectedOfficeHome.png"]];
                    
                }
                    break;
                case 2:
                {
                    [item setImage:[UIImage imageNamed:@"deselectedProfileBtn.png"]];
                    [item setFinishedSelectedImage:[UIImage imageNamed:@"selectedProfileBtn.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"deselectedProfileBtn.png"]];
                    
                }
                    break;
                case 3:
                {
                    [item setImage:[UIImage imageNamed:@"deselectedInfo.png"]];
                    [item setFinishedSelectedImage:[UIImage imageNamed:@"selectedInfo.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"deselectedInfo.png"]];
                    
                }
                    break;
                    
                default:
                    break;
            }

            
            
            
        }
        
    }
    
    // Gourav 09 feb start
    
    if ([[UIDevice currentDevice] systemVersion].floatValue >= 8.0)
    {
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateNormal];
    
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    }
    
    // Gourav 09 feb end
    
        

    
    [self.window makeKeyAndVisible];
    
    [self.tabBarArray release];
    [viewNavController release];
    [officeNavController release];
    [profileNavController release];
    [InformationsViewController release];

    
}



#pragma mark-
#pragma mark- NSURLCONNECTION METHODS

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  }



-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
}



@end
