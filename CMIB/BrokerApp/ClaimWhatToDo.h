//
//  ClaimWhatToDo.h
//  BrokerApp
//
//  Created by iPHTech2 on 31/05/13.
//
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "GAITrackedViewController.h"

@class  AppDelegate;


@interface ClaimWhatToDo : GAITrackedViewController<UIWebViewDelegate , UIAlertViewDelegate , MFMailComposeViewControllerDelegate>
{
    
    AppDelegate * appDelegate; // gourav 10 sep
    IBOutlet UITextView * textview;
    NSString * numberStr;
}
@property (nonatomic, retain) NSString * numberStr;
@property (nonatomic, retain)UITextView * textview;

@property(nonatomic, retain) NSMutableArray * officeArray; // gourav 10 sep 


-(void)initializeNavigationBar;
-(void)setContents;
-(void)sendMailToReceipent:(NSString *)receipent;
-(void)move;
-(void)callBack;


@end
