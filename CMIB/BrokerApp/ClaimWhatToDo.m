//
//  ClaimWhatToDo.m
//  BrokerApp
//
//  Created by iPHTech2 on 31/05/13.
//
//

#import "ClaimWhatToDo.h"
#import "ClaimsAndPhotos.h"
#import "Claims.h"
#import "Utility.h"

#import "StaffFilteredLocations.h"
#import "AllStaffList.h"
#import "AfterHoursContact.h"

@interface ClaimWhatToDo ()

@end

@implementation ClaimWhatToDo
@synthesize textview;
@synthesize numberStr;
@synthesize officeArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    }
    return self;
}

-(void)initializeNavigationBar
{
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    self.title = @"Claims - What to do";
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(270, 0, 21, 21)];
    photoImage.image = [UIImage imageNamed:QUESTION_MARK_IMAGE];
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeNavigationBar];
    
    //self.textview.text = CLAIM_TEXT;
    
    [self setContents];
    
    // gourav 10 sep start
    
    self.officeArray  = [[NSMutableArray alloc]init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.officeArray = [appDelegate.appInfoDict objectForKey:OFFICE_INFO_KEY];
    
    // gourav 10 sep end
}


-(void)setContents
{
    CGFloat yValue =  0.0;
    
    UIScrollView * scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0,320,self.view.frame.size.height)];
    scrollview.contentSize = CGSizeMake(320, 300);
    scrollview.scrollEnabled =YES;
    [scrollview setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:scrollview];
    
    
    UILabel * heading = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 280, 15)];
    [heading setBackgroundColor:[UIColor clearColor]];
    [heading setText:@"Claims Procedure"];
    //[heading setTextColor:[UIColor colorWithRed:29.0/255.0f green:66.0/255.0f blue:111.0/255.0f alpha:1.0]];
   // [heading setTextColor:[UIColor redColor]];
    [heading setFont:[UIFont boldSystemFontOfSize:15.0]];
    [scrollview addSubview:heading];
    [heading release];
    
    yValue = yValue+25;
    
    
    NSString* initialText = @"In the event of a potential claim you can notify us via this app. Go to the Claims & Photos screen . ";
    
    
    NSString *description = [NSString stringWithFormat:@"<html><head><style> #claim_view{font-family:helvetica; font-size:12; margin:10;}a.ex1:link {color:#EA2713; text-decoration:underline;} a.ex2:link {color:black; text-decoration:none;} </style></head><body id='claim_view'><p>%@ <a class='ex1' href=\"Claims\"><b>Go to Claims & Photos.</b></a></p><p>A CMIB Insurance Services claims representative will receive your information and will notify you of the claims process</p><p>You should: </p><li>Take all reasonable precautions to minimise the claim and prevent further loss, damage or liability.</li><li>Do not make any statements or admission of liability.</li><li>Advise the police if a theft, malicious damage or lost property has occurred and obtain a copy of report and event number as well as police station location and the officer's details.</li><p>For motor vehicle claims:</p><li>Do not admit any liability</li><li>Record all details of any third party including drivers name, address, phone, vehicle description and registration number.</li><li>Record third party Insurer details if possible</li><li>Take photos of the area and damage using this app or add via your camera roll.</li><p>For assistance during business hours we can be contacted at:</p><b>Telephone: </b><a class='ex2' href='tel:1300 133 577 '>1300 133 577 </a><a class='ex2' href='tel:(03) 9421 5088 '>or (03) 9421 5088</a></br><br><b>Email:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='ex2' href='http://claims@cmibinsurance.com.au'>claims@cmibinsurance.com.au</a></br><br><font color = black><b>CMIB Insurance Services </b><br><b>Level 3, Building 7</b><br><b>Botanicca Corporate Park</b><br><b>570-588 Swan Street</b><br><b>RICHMOND VIC 3121</b></font><br><br><b><font color = black >For Emergency After Hours Assistance – <a class='ex1' href=\"AfterHours\"><b>After Hours Contacts</b></a></font></body></html>",initialText];
    
    // <font size='2'> claims@mrib.com</font>
    
//    UILabel *claimEmailLabel = [[UILabel alloc] initWithFrame:CGRectMake(160,209, 130, 16)];
//    //[claimEmailLabel setBackgroundColor:[UIColor redColor]];
//    [claimEmailLabel setFont:[UIFont fontWithName:@"Arial" size:14]];
//    [claimEmailLabel setText:@"claims@mrib.com"];
//    [scrollview addSubview:claimEmailLabel];
//    
//    
//    UILabel *claimEmailLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(160,480, 130, 16)];
//    [claimEmailLabel1 setBackgroundColor:[UIColor redColor]];
//    [claimEmailLabel1 setFont:[UIFont fontWithName:@"Arial" size:14]];
//    [claimEmailLabel1 setText:@"claims@mrib.com"];
//    [scrollview addSubview:claimEmailLabel1];
    
    /*
    
    // gourav 14 nov start
    for(int i=0; i<2; i++)
    {
        UILabel *claimEmailLabel = [[UILabel alloc] init];
        if(i==0)
            [claimEmailLabel setFrame:CGRectMake(160,209, 130, 16)];
        else
            [claimEmailLabel setFrame:CGRectMake(155,509, 130, 16)];
                                     
        //[claimEmailLabel setBackgroundColor:[UIColor redColor]];
        [claimEmailLabel setFont:[UIFont fontWithName:@"Arial" size:14]];
        [claimEmailLabel setText:@"claims@mrib.com"];
        [scrollview addSubview:claimEmailLabel];
    }
     // gourav 14 nov end
    
    */
    
    
  //  NSString *description = [NSString stringWithFormat:@"<html><head><style> #claim_view{font-family:helvetica; font-size:12; margin:10;}a.ex1:link {color:blue; text-decoration:underline;} a.ex2:link {color:blue; text-decoration:none;} a.ex3:link {color:black; text-decoration:none;} </style></head><body id='claim_view'><u><b>Motor Vehicle Claims:</b></u><li>Do not admit any liability.</li><li>Advise the police/ambulance if anyone is injured.</li><li>%@<a class='ex1' href=\"Claims\"><b>Claims & Photos</b></a> page. Fill in all the details and take photos of the area and damage. An MRIB claims representative will receive your information and will notify you of the claims process.</li><br><b>For Emergency Assistance: </b><br><li>During office hours: Call MRIB Accident Management on &nbsp;<a class='ex3' href='tel:0800 408 0322'><b><u>0800 408 0322</b></u></a>&nbsp;or email <a class='ex1' href='http://claims@mrib.com'><b>claims@mrib.com</b></a>.</br></li><li>After Hours: Visit the <a class='ex2' href=\"AfterHours\"><u><b>After Hours Contacts</b></u></a> page.</li><br><u><b>Non-Motor Vehicle Claims:</b></u><br><li>Take all reasonable precautions to minimise the claim and prevent further loss, damage or liability.</li><li>Do not make any statements or admission of liability.</li><li>Advise the police if a theft, malicious damage or lost property has occurred and obtain a copy of report and event number as well as police station location and the officer's details.</li><li>Call ambulance/police if anyone is injured.</li><li>In the event of a claim you can notify us via the <a class='ex2'href=\"Claims\"><u><b>Claims & Photos</b></u></a> page. Fill in all the details and take photos of any damage. An MRIB claims representative will receive your information and will notify you of the claims process.</li><br><b>For Emergency Assistance: </b><br><li>During office hours:Call MRIB’s claims team on&nbsp;<a class='ex3' href='tel:01494 455 655'><u><b>01494 455 655</b></u></a>&nbsp;or email <a class='ex1' href='http://claims@mrib.com'><b>claims@mrib.com</b></a>.</br></li><li>After Hours: Visit the <a class='ex2' href=\"AfterHours\"><u><b>After Hours Contacts</b></u></a> page.</li><br></body></html>",initialText];
    
    
    CGSize fontSize1 = [description sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
    
    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 35, 320, self.view.frame.size.height+250)];
    if ([Utility isIphone_5]) {
        webview.frame = CGRectMake(0, 35, 320, self.view.frame.size.height+250);
    }
    
  //  [webview.];
    [webview.scrollView setBounces:NO];
    [webview setOpaque:NO];
  
    [webview setDelegate:self];
    [webview setBackgroundColor:[UIColor clearColor]];
    [webview.scrollView setScrollEnabled:NO];
    
    [webview loadHTMLString:description baseURL:nil];
    [scrollview addSubview:webview];
    [webview release];
    
    yValue = yValue+self.view.frame.size.height+10;
    
    scrollview.contentSize = CGSizeMake(320, yValue+280); //540
    
    if ([Utility isIphone_5]) {
        scrollview.contentSize = CGSizeMake(320, yValue+280);
    }

    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.numberStr]];
    }
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    self.numberStr = @"";
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    if ([absoluteStr hasSuffix:@"Claims"])
    {
        int controllers = self.navigationController.viewControllers.count;
        for (int i = 0; i<controllers; i++) {
            if ([[[self.navigationController viewControllers] objectAtIndex:i] isKindOfClass:[ClaimsAndPhotos class]]) {
                [self.navigationController popViewControllerAnimated:YES];
                return YES;
            }
        }
        
        
        Claims *claimWhatToDo = [[Claims alloc] initWithNibName:@"Claims" bundle:nil];
        [self.navigationController pushViewController:claimWhatToDo animated:YES];
        [claimWhatToDo release];
    }
    
    // gourav 10-nov-2014 start
    
    if ([absoluteStr hasSuffix:@"AfterHours"])
    {
//        int controllers = self.navigationController.viewControllers.count;
//        for (int i = 0; i<controllers; i++) {
//            if ([[[self.navigationController viewControllers] objectAtIndex:i] isKindOfClass:[ClaimsAndPhotos class]]) {
//                [self.navigationController popViewControllerAnimated:YES];
//                return YES;
//            }
//        }
        
        AfterHoursContact * contact = [[AfterHoursContact alloc]initWithNibName:@"AfterHoursContact" bundle:nil];
        [self.navigationController pushViewController:contact animated:YES];
        [contact release];
        
    }
    
    // gourav 10-nov-2014 end

    
    
    
    
    
    /*
    
    // gourav 10 sep start
    if ([absoluteStr hasSuffix:@"KeyStaff"])
    {
//        int controllers = self.navigationController.viewControllers.count;
//        for (int i = 0; i<controllers; i++) {
//            if ([[[self.navigationController viewControllers] objectAtIndex:i] isKindOfClass:[ClaimsAndPhotos class]]) {
//                [self.navigationController popViewControllerAnimated:YES];
//                return YES;
//            }
//        }
        
        [self showStaffList];
       
    }

    // gourav 10 sep end
    */
    
    if ([absoluteStr hasPrefix:@"tel:"]) {
        self.numberStr = [absoluteStr stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        //self.numberStr = [absoluteStr stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
        
        NSString* str = [self.numberStr stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:str message:@"Would you like to call this number" delegate:self  cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
        [alert show];
        [alert release];
        return NO;
        
        
    }
    if ([absoluteStr hasPrefix:@"http://"]) {
        
        self.numberStr = [absoluteStr stringByReplacingOccurrencesOfString:@"http://" withString:@""];
        self.numberStr = [self.numberStr stringByReplacingOccurrencesOfString:@"/" withString:@""];
        [self sendMailToReceipent:self.numberStr];
        //self.numberStr = [absoluteStr stringByReplacingOccurrencesOfString:@"mailto:" withString:@"http://"];
        //[[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.numberStr]];
        return NO;
    }
    
    return YES;
    
}
-(void)sendMailToReceipent:(NSString *)receipent
{
    MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    NSArray *toRecipients = [NSArray arrayWithObject:receipent];
    
    [controller setToRecipients:toRecipients];
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
    if (controller)
        [self presentModalViewController:controller animated:YES];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)move
{
    NSMutableArray *viewArrays = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    
    
    for (int i =0; i<[viewArrays count]; i++)
    {
        if ([[viewArrays objectAtIndex:i] isKindOfClass:NSClassFromString(@"ClaimsAndPhotos")])
        {
            
            [self.navigationController popToViewController:[viewArrays objectAtIndex:i] animated:YES];
            break;
        }
    }
    
}
#pragma mark-
#pragma mark IBActions-

-(void)callBack
{
    /* NSArray* tempVCA = [self.navigationController viewControllers];
     
     for(UIViewController *tempVC in tempVCA)
     {
     if([tempVC isKindOfClass:[ClaimsAndPhotos class]])
     {
     [self.navigationController popViewControllerAnimated:YES];
     return;
     }
     }
     */
    [self.navigationController popViewControllerAnimated:YES];
}

// gourav 10 sep
-(void)showStaffList
{
    if ([self.officeArray count]>1) {
        StaffFilteredLocations * list = [[StaffFilteredLocations alloc]initWithNibName:@"StaffFilteredLocations" bundle:nil];
        list.officeListArray = self.officeArray;
        [self.navigationController pushViewController:list animated:YES];
        [list release];
    }
    else{
        AllStaffList * staffList = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
        
        staffList.isStaffDetail = YES;
        [self.navigationController pushViewController:staffList animated:YES];
        [staffList release];
    }
    
}
 

@end
