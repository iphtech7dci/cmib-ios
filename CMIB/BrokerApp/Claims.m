//
//  Claims.m
//  Optimus1
//
//  Created by iPHTech13 on 18/04/14.
//
//

#import "Claims.h"
#import "Constants.h"
#import "Utility.h"
#import "MoterVehicleIncident.h"
#import "ClaimsAndPhotos.h"

@interface Claims ()

@end

@implementation Claims

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appdelegate.navController = self.navigationController;
    
    
    UIImageView * bgimageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
   // bgimageView.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    if ([Utility isIphone_5]) {
        bgimageView.frame = CGRectMake(0, 0, 320, 568);
    }
    [self.view addSubview:bgimageView];
    [bgimageView release];
    
    
    
    [self initailizeNavigationBar];
    [self setShareButtons];
    
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    self.title = @"Claims";
    self.screenName = @"Claims";
    self.navigationController.navigationBar.frame = CGRectMake(0, 20, 320, 44);
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    
}
-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(280, 0, 20, 20)];
    photoImage.image = [UIImage imageNamed:CLAIM_BUTTON_IMAGE];
    [button addTarget:self action:@selector(doExport) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
    
    
    
}
-(void)setShareButtons
{
    
    UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectMake(20, 20, 280, 130)];
    //[webview setDelegate:self];
    [[webview scrollView] setScrollEnabled:NO];
    
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body {font-family:\"Helvetica\"; font-size:13; color:'rgb(38,92,124);}a:link {COLOR: 'rgb(38,92,124)';}</style></head><body style=\"margin:10\";>%@</body></html>", USE_OUR_GENERAL_CLAIM];
    
    [webview loadHTMLString:htmlString baseURL:nil];
    
    [self.view addSubview:webview];
    
    
    
    
    CGRect GeneralClaimFrame = CGRectMake(20, 20+150, 280, 44);
    CGRect MoterVehicleFrame = CGRectMake(20, 74+150, 280, 44);
   
    
    UIButton *GeneralClaimButton = [[UIButton alloc] initWithFrame:GeneralClaimFrame];
    [GeneralClaimButton setBackgroundImage:[UIImage imageNamed:OFFICEINFOTITLE_BUTTON] forState:UIControlStateNormal];
    [GeneralClaimButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [GeneralClaimButton addTarget:self action:@selector(doGeneralClaim) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *MoterVehicleButton = [[UIButton alloc] initWithFrame:MoterVehicleFrame];
    [MoterVehicleButton setBackgroundImage:[UIImage imageNamed:OFFICEINFOTITLE_BUTTON] forState:UIControlStateNormal];
    [MoterVehicleButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [MoterVehicleButton addTarget:self action:@selector(doMoterVehicle) forControlEvents:UIControlEventTouchUpInside];
    
    
   
    
    [self.view addSubview:GeneralClaimButton];
    [self.view addSubview:MoterVehicleButton];
  
    
    [GeneralClaimButton release];
    [MoterVehicleButton release];
    
    
    
    //setlabels
    
    CGRect GeneralClaimLabelFrame = CGRectMake(35, 33+150, 250, 17);
    CGRect MoterVehiclelFrame = CGRectMake(35, 86+150, 250, 17);
    
    
    UILabel * GeneralClaimLabel = [[UILabel alloc]initWithFrame:GeneralClaimLabelFrame];
    [GeneralClaimLabel setBackgroundColor:[UIColor clearColor]];
    GeneralClaimLabel.text = @"General Claim";
    [GeneralClaimLabel setTextColor:[UIColor whiteColor]];
    
    
    UILabel * MoterVehiclelLabel = [[UILabel alloc]initWithFrame:MoterVehiclelFrame];
    [MoterVehiclelLabel setBackgroundColor:[UIColor clearColor]];
    MoterVehiclelLabel.text = @"Motor Vehicle Claim";
    [MoterVehiclelLabel setTextColor:[UIColor whiteColor]];
    
    
    [self.view addSubview:GeneralClaimLabel];
    [self.view addSubview:MoterVehiclelLabel];
    
    [GeneralClaimLabel release];
    [MoterVehiclelLabel release];
   
    
}

-(void)doGeneralClaim
{
    ClaimsAndPhotos *form=[[ClaimsAndPhotos alloc]initWithNibName:@"ClaimsAndPhotos" bundle:nil];
    [self.navigationController pushViewController:form animated:YES];
   
}

-(void)doMoterVehicle
{
    MoterVehicleIncident *moter=[[MoterVehicleIncident alloc]initWithNibName:@"MoterVehicleIncident" bundle:nil];
    [self.navigationController pushViewController:moter animated:YES];
}


-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)doExport{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
