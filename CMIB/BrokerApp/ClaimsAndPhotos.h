//
//  ClaimsAndPhotos.h
//  BrokerApp
//
//  Created by iPHTech2 on 30/05/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSData+Base64.h"
#import <CoreLocation/CoreLocation.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
//#import <ImageIO/CGImageSource.h>
//#import <ImageIO/CGImageProperties.h>
//#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "GAITrackedViewController.h"
@class AppDelegate;

@interface ClaimsAndPhotos : GAITrackedViewController<UIWebViewDelegate, UITextFieldDelegate, UITextViewDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate , UIActionSheetDelegate , MFMailComposeViewControllerDelegate, CLLocationManagerDelegate , UITableViewDataSource, UITableViewDelegate>
{
    UITextField * nametextField ;
    UITextField * emailTextField ;
    UITextField * mobileTextField ;
    
    UITextView * line1;
     UITextView * line2;
     UITextView * line3;
    
    UIButton *sendButton;
    UIButton *addPhotosButton;
    UIButton  * dropDownButton;
    
    UIImageView * uploadImage1;
    UIImageView * uploadImage2;
    UIImageView * uploadImage3;
    UIImageView * uploadImage4;
    UILabel * addPhotoLabel;
    int imageCount;
    
    IBOutlet UIView * indecatorView;
    UIView * confirmationView;
    UIToolbar *numberToolbar;
    UIScrollView * scrollView;
    CGPoint offset;
    
    NSString * longitude;
    NSString * latitude;
    UITableView * tabelView;
    
    NSString * longitude_libPhoto;
    NSString * latitude_libPhoto;
    
    
    CGFloat contentOffSet;
    CGFloat xoffset;
    CGFloat yoffset ;
    
    NSMutableArray * officeArray;
    NSMutableArray * latiArray;
    NSMutableArray * longiArray;
    NSMutableArray * dateTimeArray;
    NSString * emailRecieverString;
    
    BOOL shouldShowDropDownList;
    CGRect TableFrame;
    int checForEmptyArray;
    CLLocationManager *locationManager;
    NSString * photoDateTimeStr;
    BOOL isCamera;

    AppDelegate *appDelegate;
    
    UILabel * title;

}

@property (nonatomic, retain)NSString * longitude_libPhoto;
@property (nonatomic, retain)NSString * latitude_libPhoto;

@property (nonatomic, retain) NSMutableArray * dateTimeArray;
@property (nonatomic, retain) NSString * photoDateTimeStr;
@property (nonatomic, retain) NSMutableArray * latiArray;
@property (nonatomic, retain) NSMutableArray * longiArray;
@property (nonatomic, retain) NSString * emailRecieverString;
@property (nonatomic, retain) NSMutableArray * officeArray;
@property (nonatomic, retain) UITableView * tabelView;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) UIScrollView * scrollView;
@property (nonatomic, retain) UIView * confirmationView;
@property (nonatomic, retain) UIView * indecatorView;
@property (nonatomic, retain) UILabel * addPhotoLabel;
@property (nonatomic, retain) UIImageView * uploadImage1;
@property (nonatomic, retain) UIImageView * uploadImage2;
@property (nonatomic, retain) UIImageView * uploadImage3;
@property (nonatomic, retain) UIImageView * uploadImage4;
@property (nonatomic, retain) UITextField * nametextField ;
@property (nonatomic, retain) UITextField * emailTextField ;
@property (nonatomic, retain) UITextField * mobileTextField ;
@property (nonatomic, retain) UITextView * line1;
//@property (nonatomic, retain) UITextView * line2 ;
//@property (nonatomic, retain) UITextView * line3 ;


-(void)showTableView;
-(void)findCurrentLocation;
-(void)initializeNavigationBar;
-(void) moveViewDown;
-(void)dismissKeyboard;
-(void)makeForm;
-(void)moveScrolUP:(int)ht;
-(void)moveScrollDown;
- (BOOL)validateEmailWithString:(NSString*)email;
-(void)dismissView;
-(void)callBack;
-(void)addPhotos;
-(void)sendData;
-(void)sendmail;
-(void)showActionSheet;
-(void)capturePhoto;
-(void)getPhotFromPhotoLibrary;
-(void)showAnimationWithxValye:(CGFloat)xVal andyValue:(CGFloat)yVal;


@end
