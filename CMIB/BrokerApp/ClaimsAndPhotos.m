//
//  ClaimsAndPhotos.m
//  BrokerApp
//
//  Created by iPHTech2 on 30/05/13.
//
//

#import "ClaimsAndPhotos.h"
#import "Constants.h"
#import "ClaimWhatToDo.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"

#define NAME_TEXTFIELD_TAG 200
#define EMAIL_TEXTFIELD_TAG  201
#define MOBIL_TEXTFIELD_TAG 202
#define WHAT_HAPPENED_TAG 203

@interface ClaimsAndPhotos ()
{
    UILabel * title1;
    int currentTxtFldTag;
    
    CGFloat addPhotoImage_Y_Value;  // gourav 04 sep
}

@end


@implementation ClaimsAndPhotos

@synthesize line1;
//@synthesize line2;
//@synthesize line3;
@synthesize nametextField;
@synthesize emailTextField;
@synthesize mobileTextField;
@synthesize uploadImage1;
@synthesize uploadImage2;
@synthesize uploadImage3;
@synthesize uploadImage4;
@synthesize addPhotoLabel;
@synthesize indecatorView;
@synthesize confirmationView;
@synthesize scrollView;
@synthesize latitude;
@synthesize longitude;
@synthesize tabelView;
@synthesize officeArray;
@synthesize emailRecieverString;
@synthesize latiArray;
@synthesize longiArray;
@synthesize photoDateTimeStr;
@synthesize dateTimeArray;
@synthesize longitude_libPhoto;
@synthesize latitude_libPhoto;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    }
    return self;
}

-(void)initializeNavigationBar
{
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Claims & Photos";
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(270, 0, 23, 20)];  // 40 30
    photoImage.image = [UIImage imageNamed:CAMERA_IMAGE];
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
    
    imageCount = 0;
    shouldShowDropDownList = YES;
    checForEmptyArray = 0;
    self.latiArray = [[NSMutableArray alloc]init];
    self.longiArray = [[NSMutableArray alloc]init];
    self.dateTimeArray = [[NSMutableArray alloc]init];
    self.screenName = @"Claims & Photos";
    UIImageView * bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    if ([Utility isIphone_5]) {
        bgImage.frame = CGRectMake(0, 0, 320, 568);
    }
    bgImage.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    [self.view addSubview:bgImage];
    [bgImage release];
    
    [self initializeNavigationBar];
    
    
    [self makeForm];
    
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)] ;
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(prevBtn)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBtn)];
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)];
    NSArray *itemArray = [[NSArray alloc] initWithObjects:item3, spaceItem, item1, item2, nil];
    numberToolbar.items = itemArray;
    
    [numberToolbar sizeToFit];

    [numberToolbar sizeToFit];
    
    self.nametextField.inputAccessoryView = numberToolbar;
    self.emailTextField.inputAccessoryView = numberToolbar;
    self.mobileTextField.inputAccessoryView = numberToolbar;
    self.line1.inputAccessoryView=numberToolbar;
    
    
    
    self.confirmationView  = [[UIView alloc]init];
    [self.confirmationView setAlpha:0.8];
    [[self.confirmationView layer] setCornerRadius:10];
    
    self.confirmationView.frame = CGRectMake(30, 150, 260, 100);
    if ([Utility isIphone_5]) {
        self.confirmationView.frame = CGRectMake(30, 180, 260, 100);
    }
    [self.confirmationView setBackgroundColor:[UIColor darkGrayColor]];
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 160, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setNumberOfLines:2];
    [label setText:@"Your message has been sent"];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:DONE_BUTTON_IMAGE] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(100, 65, 60, 30)];
    
    [self.confirmationView addSubview:btn];
    [self.confirmationView addSubview:label];
    [self.confirmationView setHidden:YES];
    [self.view addSubview:self.confirmationView];
    [self.view bringSubviewToFront:self.indecatorView];
    
    [label release];
    
    [self findCurrentLocation];
    isCamera  = NO;
    
}
-(void) moveViewDown
{
    [UIView beginAnimations:@"ResizeForKeyboard" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x,64,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}

-(void)dismissKeyboard {
    [self moveScrollDown];
  //  [self moveViewDown];  //yt
  //  UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
  //  [txtfld resignFirstResponder];
    [self.view endEditing:YES];
}
-(void)nextBtn
{
    if(currentTxtFldTag<203)
    {
        currentTxtFldTag=currentTxtFldTag + 1;
    }
    
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
}

-(void)prevBtn
{
    if(currentTxtFldTag>200)
    {
        currentTxtFldTag=currentTxtFldTag -1;
    }
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    locationManager.delegate = self;
    [locationManager startUpdatingHeading];
    
    [self ShowStatusBar:YES];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    if (locationManager) {
        locationManager.delegate = nil;
        [locationManager stopUpdatingLocation];
    }
}
#pragma mark
#pragma mark Form make-

-(void)makeForm
{
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 0, 300, 370)];
    
    self.scrollView.contentSize = CGSizeMake(250, 10);
    
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(20, 0, 285, 460);
    }
    
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollView];
    
    CGFloat yValue = 20;
    CGFloat width = 280;
    CGFloat height = 15;
    //CGFloat bottumButtonWidth = 138;
    CGFloat litleSpace = 5.0;
    CGFloat largeSpace = 8.0;
    CGFloat xValue = 0.0;
    //CGFloat lineHeight = 2.0;
    CGFloat texViewHieght = 80.0;
    CGFloat textFieldHeight = 40;
    CGFloat photoButtonHeight = 70.0f;
    CGFloat spaceBetweenButtons = 18;
    CGFloat webviewHeight = 60.0;
    
    UILabel * subHeading = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [subHeading setBackgroundColor:[UIColor clearColor]];
    [subHeading setFont:[UIFont boldSystemFontOfSize:13.0]];
    //[subHeading setTextColor:[UIColor colorWithRed:246.0/155.0f green:96.0/255.0f blue:20.0/255.0f alpha:1.0]];
   // [subHeading setTextColor:[UIColor redColor]];
   // [subHeading setTextColor:[UIColor colorWithRed:29.0/255.0f green:66.0/255.0f blue:111.0/255.0f alpha:1.0]];
    
   // [subHeading setTextColor:[UIColor colorWithRed:191.0/155.0f green:0.0/255.0f blue:51.0/255.0f alpha:1.0]];
    
    [subHeading setTextColor:[UIColor blackColor]];
    
    [subHeading setText:@"Claims Notifications"];
    [self.scrollView addSubview:subHeading];
    [subHeading release];
    
    yValue = yValue+height;
    
    
    //    font-size:12; color:black;}a:link {COLOR: Blue;}</style>
    // font-size:12; color:'rgb(38,92,124);}a:link {COLOR: 'rgb(38,92,124)';}</style>   
    UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width+10, webviewHeight+10)];
    [webview setDelegate:self];
    [[webview scrollView] setScrollEnabled:NO];
    
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body {font-family:\"Helvetica\"; font-size:12; color:black;}a:link {COLOR: #F17A2D;}</style></head><body style=\"margin:10\";>%@ </br><a href=\"Claims\">More claims information here.</a></body></html>", SUBHEADING_TEXT];
    
    [webview loadHTMLString:htmlString baseURL:nil];
    
    [scrollView addSubview:webview];
    [webview release];
    
    
    yValue = yValue+webviewHeight+spaceBetweenButtons;
    
//    UIView * customView1 = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, width, lineHeight)];
//    [customView1 setBackgroundColor:[UIColor grayColor]];
//    [customView1 setAlpha:0.3f];
//    [self.scrollView addSubview:customView1];
//    [customView1 release];
//    
//    yValue  = yValue+lineHeight+largeSpace+10;
    
    UILabel * nameGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [nameGeadingLable setBackgroundColor:[UIColor clearColor]];
    [nameGeadingLable setFont:[UIFont systemFontOfSize:13.0]];//[UIFont boldSystemFontOfSize:13.0]];
    [nameGeadingLable setText:@"Name"];//astha
    [nameGeadingLable setTextColor:[UIColor darkGrayColor]];
    [scrollView addSubview:nameGeadingLable];
    [nameGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    
    
    self.nametextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue+10, yValue, width-10, textFieldHeight)];
    [self.nametextField setPlaceholder:@"Text"];//astha
    [self.nametextField setTag:NAME_TEXTFIELD_TAG];
    //[self.nametextField setBackground:[UIImage imageNamed:TEXTFILED_BACKGROUND]];
    
    
    UIImageView *textFieldBackGroundView = [[UIImageView alloc] initWithFrame:CGRectMake(xValue-20, yValue, width +20, textFieldHeight)];
    [textFieldBackGroundView setImage:[UIImage imageNamed:TEXTFILED_BACKGROUND]];
    [self.scrollView addSubview:textFieldBackGroundView];
    
    //[textFieldBackGroundView setBackgroundColor:[UIImage imageNamed:TEXTFILED_BACKGROUND]];
    
    //[self.nametextField.layer setCornerRadius:2.0f];
    //[self.nametextField setBorderStyle:UITextBorderStyleRoundedRect];
    [self.nametextField setDelegate:self];
    [self.scrollView addSubview:self.nametextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    
    UILabel * emailLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [emailLable setBackgroundColor:[UIColor clearColor]];
    [emailLable setFont:[UIFont systemFontOfSize:13.0]];
    [emailLable setText:@"Email"];//astha
    [emailLable setTextColor:[UIColor darkGrayColor]];
    [scrollView addSubview:emailLable];
    [emailLable release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue+10, yValue, width-10, textFieldHeight)];
    [self.emailTextField setPlaceholder:@"Text"];///astha
    [self.emailTextField setTag:EMAIL_TEXTFIELD_TAG];
    //[self.emailTextField.layer setCornerRadius:2.0f];
   // [self.emailTextField setBorderStyle:UITextBorderStyleRoundedRect];
    //[self.emailTextField setBackground:[UIImage imageNamed:TEXTFILED_BACKGROUND]];
    
    UIImageView *textFieldBackGroundView1 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue-20, yValue, width +20, textFieldHeight)];
    [textFieldBackGroundView1 setImage:[UIImage imageNamed:TEXTFILED_BACKGROUND]];
    [self.scrollView addSubview:textFieldBackGroundView1];
    
    [self.emailTextField setDelegate:self];
    [self.emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.scrollView addSubview:self.emailTextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * mobileLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [mobileLable setBackgroundColor:[UIColor clearColor]];
    [mobileLable setFont:[UIFont systemFontOfSize:13.0]];
    [mobileLable setText:@"Mobile"];//astha
    [mobileLable setTextColor:[UIColor darkGrayColor]];
    [scrollView addSubview:mobileLable];
    [mobileLable release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.mobileTextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue+10, yValue, width-10, textFieldHeight)];
    [self.mobileTextField setPlaceholder:@"Text"];///astha
    //[self.mobileTextField.layer setBorderColor:[[UIColor lightGrayColor]CGColor]];
    //[self.mobileTextField.layer setBorderWidth:1.0f];
    //[self.mobileTextField.layer setCornerRadius:2.0f];
    //[self.mobileTextField setBorderStyle:UITextBorderStyleRoundedRect];
    //[self.mobileTextField setBackground:[UIImage imageNamed:TEXTFILED_BACKGROUND]];
    
    UIImageView *textFieldBackGroundView2 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue-20, yValue, width +20, textFieldHeight)];
    [textFieldBackGroundView2 setImage:[UIImage imageNamed:TEXTFILED_BACKGROUND]];
    [self.scrollView addSubview:textFieldBackGroundView2];

    
    [self.mobileTextField setKeyboardType:UIKeyboardTypeNumberPad];
    [self.mobileTextField setTag:MOBIL_TEXTFIELD_TAG];
    [self.mobileTextField setDelegate:self];
    [self.scrollView addSubview:self.mobileTextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * whatHappendLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [whatHappendLable setBackgroundColor:[UIColor clearColor]];
    [whatHappendLable setFont:[UIFont systemFontOfSize:13.0]];
    [whatHappendLable setText:@"What's happened?"];//astha
    [whatHappendLable setTextColor:[UIColor darkGrayColor]];
    [scrollView addSubview:whatHappendLable];
    [whatHappendLable release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.line1 = [[UITextView alloc]initWithFrame:CGRectMake(xValue+10, yValue, width-10, texViewHieght)];
    //  self.line2 = [[UITextView alloc]initWithFrame:CGRectMake(xValue, yValue, width, texViewHieght)];
    //  self.line3 = [[UITextView alloc]initWithFrame:CGRectMake(xValue, yValue, width, texViewHieght)];
    
    [self.line1 setBackgroundColor:[UIColor clearColor]];
    self.line1.text = @"Line 1 \nLine 2 \nLine 3";
    self.line1.textColor = [UIColor lightGrayColor]; //astha
    [self.line1 setDelegate:self];
    [line1 setFont:[UIFont systemFontOfSize:16.0]];
    //self.line1.layer.cornerRadius = 5.0f;
    //self.line1.layer.borderWidth = 2.0;
    //self.line1.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    //[self.mobileTextField setBorderStyle:UITextBorderStyleRoundedRect];
    
    UIImageView *textFieldBackGroundView5 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue-20, yValue, width +20, texViewHieght)];
    [textFieldBackGroundView5 setImage:[UIImage imageNamed:TEXTVIEW_BACKGROUND]];
    [self.scrollView addSubview:textFieldBackGroundView5];

    
    //[self.line1 setBackgroundColor:[UIColor colorWithRed:206.0f/255.0f green:206.0f/255.0f blue:206.0f/255.0f alpha:1.0f]];
    [self.line1 setDelegate:self];
    [self.line1 setTag:WHAT_HAPPENED_TAG];
    [self.scrollView addSubview:self.line1];
    [line1 release];
    
    yValue = yValue+texViewHieght+20; //+textFieldHeight-5;
    
    
    
    addPhotosButton = [[UIButton alloc] initWithFrame:CGRectMake(0 , yValue,width, 42)];
    [addPhotosButton setBackgroundImage:[UIImage imageNamed:ADD_PHOTO_IMAGE] forState:UIControlStateNormal];
    [addPhotosButton setTitle:@"Add Photos" forState:UIControlStateNormal];
    [[addPhotosButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [addPhotosButton addTarget:self action:@selector(addPhotos) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:addPhotosButton];
    
    yValue = yValue+textFieldHeight+(largeSpace); // (largeSpace*2)
    
    addPhotoImage_Y_Value = yValue;  // gourav 04 sep
    
    xValue =3.0f;  // gourav 04 sep
    

    CGFloat lineWidth = 1.0;
    self.uploadImage1 = [[UIImageView alloc]initWithFrame:CGRectMake(xValue-3, yValue+1,photoButtonHeight, photoButtonHeight)];
    self.uploadImage1.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];//UPLOAD_PHOTO_IMAGE @"left_begen.png"
    //[[self.uploadImage1 layer]setBorderWidth:lineWidth];
    //[[self.uploadImage1 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.scrollView addSubview:self.uploadImage1];
    [uploadImage1 release];
    xValue = xValue+photoButtonHeight;
    
    
    self.uploadImage2 = [[UIImageView alloc]initWithFrame:CGRectMake(xValue-3, yValue+1,photoButtonHeight, photoButtonHeight)];
    self.uploadImage2.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    //[[self.uploadImage2 layer]setBorderWidth:lineWidth];
    //[[self.uploadImage2 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.scrollView addSubview:self.uploadImage2];
    [uploadImage2 release];
    
    xValue = xValue+photoButtonHeight;
    
    self.uploadImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(xValue-3, yValue+1,photoButtonHeight, photoButtonHeight)];
    self.uploadImage3.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    //[[self.uploadImage3 layer]setBorderWidth:lineWidth];
    //[[self.uploadImage3 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.scrollView addSubview:self.uploadImage3];
    [uploadImage3 release];
    
    xValue = xValue+photoButtonHeight;
    
    self.uploadImage4 = [[UIImageView alloc]initWithFrame:CGRectMake(xValue-3, yValue+1,photoButtonHeight, photoButtonHeight)];
    self.uploadImage4.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];//UPLOAD_PHOTO_IMAGE @"left_End.png"]
    //[[self.uploadImage4 layer]setBorderWidth:lineWidth];
   //[[self.uploadImage4 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.scrollView addSubview:self.uploadImage4];
    [uploadImage4 release];
    
    
    xValue = 0;
    yValue = yValue+photoButtonHeight+largeSpace;
    /*
    self.addPhotoLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [self.addPhotoLabel setBackgroundColor:[UIColor clearColor]];
    [self.addPhotoLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    [self.addPhotoLabel setTextAlignment:UITextAlignmentCenter];
    [self.addPhotoLabel setText:@"Photos 0/4"];
    [self.scrollView addSubview:self.addPhotoLabel];
    [addPhotoLabel release];
*/

    
    
    
/*    UIImageView * dropDownImageView = [[UIImageView alloc]initWithFrame:CGRectMake(xValue+3 , yValue+25,width-5, 45)];
    
    [dropDownImageView setBackgroundColor:[UIColor clearColor]];
    [dropDownImageView setImage:[UIImage imageNamed:DROPDOWN_BUTTON_IMAGE]];
    [self.scrollView addSubview:dropDownImageView];
    [dropDownImageView release];
    
    
    title1 = [[UILabel alloc]initWithFrame:CGRectMake(xValue+20 , yValue+20,width-40, 55)];
    [title1 setBackgroundColor:[UIColor clearColor]];
    [title1 setText:@"Choose office"];
    [title1 setTextColor:[UIColor whiteColor]];
    [title1 setTextAlignment:NSTextAlignmentCenter];
    [title1 setFont:[UIFont boldSystemFontOfSize:18]];
     [self.scrollView addSubview:title1];
    [title release];
    
    
    dropDownButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dropDownButton.frame = CGRectMake(xValue+240 ,yValue+20,80, 50);
    [dropDownButton addTarget:self action:@selector(showThelist) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:dropDownButton];
 
    yValue = yValue+42+(largeSpace*2);
    
 */   //xValue = xValue+bottumButtonWidth+2;
    //yValue = yValue+height+(largeSpace*2);
    
    sendButton = [[UIButton alloc] initWithFrame:CGRectMake(xValue , yValue,width, 42)];
    [sendButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [sendButton setTitle:@"Send" forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [sendButton addTarget:self action:@selector(sendData) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:sendButton];
    
    yValue = yValue+50;
    self.scrollView.contentSize = CGSizeMake(200, yValue);
    
    
    /*
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(1, addPhotoImage_Y_Value, 280, 70)];
    //imgView.image = [UIImage imageNamed:@"RoundRect_Full_Image"];
    [[imgView layer]setBorderWidth:2.0f];
    [[imgView layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [[imgView layer] setCornerRadius:5];
    [self.scrollView addSubview:imgView];
    [imgView release];
     */
    //[self.scrollView bringSubviewToFront:imgView];
    
    
}





#pragma mark-
#pragma mark UITextFieldDelegate Methods-
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}// return NO to disallow editing.
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTxtFldTag=textField.tag;
    switch (textField.tag) {
        case NAME_TEXTFIELD_TAG:
        {
            //textField.inputAccessoryView = numberToolbar;
            
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                t = t +80;
            }
            
            [self moveScrolUP:t];
            break;
        }
        case EMAIL_TEXTFIELD_TAG:
        {
            
            //textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                t = t +80;
            }
            
            [self moveScrolUP:t];
            break;
        }
        case MOBIL_TEXTFIELD_TAG:
        {
            
           // textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                t = t +80;
            }
            
            
            // gourav 06 Nov start
            t= t+70;
//
//            if(textField.tag == ADDRESS2_TAG)
//                t= t+70;
//            if(textField.tag == POSTCODE_TAG)
//                t= t+115;
            // gourav 06 Nov end

            [self moveScrolUP:t];
            break;
        }
        default:
            break;
    }
    
}// became first responder
// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case (NAME_TEXTFIELD_TAG):
        {
            //[self moveScrollDown];
        }
            break;
        case (EMAIL_TEXTFIELD_TAG):
        {
            BOOL check=[self validateEmailWithString:textField.text];
            if(check==YES)
            {
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email ID" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                self.emailTextField.text = @"";
                [alert release];
                
                
            }

            
        }
            break;
        case MOBIL_TEXTFIELD_TAG:
        {
            //[self moveScrollDown];
        }
            break;
        default:
            break;
    }
    
    
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}// return NO to not change text


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark-
#pragma mark movesScrollview UP and DOWN-
-(void)moveScrolUP:(int)ht
{
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, ht);
    [UIView commitAnimations];
    
    
    CGRect frame = self.scrollView.frame;
    frame.origin.y = -ht;
    
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView commitAnimations];
    
}
-(void)moveScrollDown
{
    CGRect frame = CGRectMake(20, 18, 290, 350);
    if ([Utility isIphone_5]) {
        frame = CGRectMake(20, 18, 290, 440);
    }
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, 0); // (0,ofset.y) yt
    [UIView commitAnimations];
    
}
- (BOOL)validateEmailWithString:(NSString *)inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = aRange.location;
        //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            //NSLog(@"topleveldomains:%@",topLevelDomain);
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                
                
                NSRange rang = [inputText rangeOfString:@"@"];
                
                if (inputText) {
                    
                    NSString *stringO = [inputText substringWithRange:NSMakeRange(rang.location - 1,1 )];
                    
                    if ([stringO isEqualToString:@"."]) {
                        
                        return FALSE;
                    }
                    
                }
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    return FALSE;
}


#pragma mark-
#pragma mark UITextViewDelegate Methods-

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.tag == WHAT_HAPPENED_TAG) {
        textView.inputAccessoryView = numberToolbar;
    }
    if (self.line1.textColor == [UIColor lightGrayColor]) {
        self.line1.text = @"";
        self.line1.textColor = [UIColor blackColor];
    }
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    offset = self.scrollView.contentOffset;
    int t;
    if ([Utility isIphone_5]) {
        t= 200;
    }
    else{
        t = 260;
    }
    [self moveScrolUP:t];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if(textView.text.length == 0)
    {
        self.line1.text = @"Line 1 \nLine 2 \nLine 3";
        self.line1.textColor = [UIColor lightGrayColor];
    }

    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
      return YES;
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
    
}

#pragma mark
#pragma mark IBActions-
-(void)showAnimationWithxValye:(CGFloat)xVal andyValue:(CGFloat)yVal
{
    int SendBtnY = sendButton.frame.origin.y;
    SendBtnY = SendBtnY+50;
    CGRect frame = CGRectMake(sendButton.frame.origin.x, SendBtnY, sendButton.frame.size.width, sendButton.frame.size.height);
    if (self.tabelView) {
        int yValue = dropDownButton.frame.origin.y+30;
        TableFrame = CGRectMake(-5, yValue, 290+2, 100);
        
        
    }
    else
    {
        [self showTableView];
    }
    
    
    
    [UIView beginAnimations:@"AnimateScroll" context:nil];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(xVal,yVal+140);
    sendButton.frame = frame;
    if (self.tabelView) {
        self.tabelView.frame = TableFrame;
    }
    [UIView commitAnimations];
}
-(void)revertAnimation:(CGFloat)xVal andYValue:(CGFloat)yVal
{
    int SendBtnY = sendButton.frame.origin.y;
    SendBtnY = SendBtnY-50;
    int yValue = dropDownButton.frame.origin.y+30;
    CGRect tempTableFrame = CGRectMake(-5, yValue, 290+2, 0);
    
    CGRect frame = CGRectMake(sendButton.frame.origin.x, SendBtnY, sendButton.frame.size.width, sendButton.frame.size.height);
    [UIView beginAnimations:@"AnimateScrollBack" context:nil];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0,yVal);
    
    self.scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width,self.view.frame.size.height+310);
    if ([Utility isIphone_5]) {
        
        self.scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width,self.view.frame.size.height+230);
        //self.scrollView.contentOffset = CGPointMake(0,yVal);
        
        
    }
    if (self.tabelView) {
        self.tabelView.frame = tempTableFrame;
    }
    sendButton.frame = frame;
    [UIView commitAnimations];
}
-(void)showThelist
{
    if (shouldShowDropDownList) {
        
        shouldShowDropDownList = NO;
        contentOffSet = self.scrollView.contentOffset.y;
        xoffset= self.scrollView.contentOffset.x;
        yoffset = contentOffSet;
        
        
        CGFloat contentYVAlue = self.scrollView.contentSize.height;
        contentYVAlue = contentYVAlue+180;
        
        self.scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width,self.view.frame.size.height+600);
        if ([Utility isIphone_5]) {
            self.scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width,self.view.frame.size.height+500);
            yoffset = contentOffSet;
        }
        [self showAnimationWithxValye:xoffset andyValue:yoffset];
        
    }
    else{
        
        [self hideTableView];
    }
    
}
-(void)showTableView
{
    //self.officeArray = [[NSMutableArray alloc] initWithObjects:@"Lismore",@"Ballina", nil];
    
    self.officeArray = [[NSMutableArray alloc] initWithArray:[appDelegate.appInfoDict objectForKey:OFFICE_INFO_KEY]];
    
    int yValue = dropDownButton.frame.origin.y+30;
    
    self.tabelView = [[UITableView alloc]initWithFrame:CGRectMake(-5, yValue, 290+2, 330) style:UITableViewStyleGrouped];
    TableFrame = self.tabelView.frame;
    self.tabelView.delegate = self;
    self.tabelView.dataSource = self;
    [self.tabelView setBounces:NO];
    [self.tabelView setScrollEnabled:NO];
    [self.tabelView setBackgroundColor:[UIColor clearColor]];
    self.tabelView.backgroundView = [[[UIView alloc] initWithFrame:self.tabelView.bounds] autorelease];
    self.tabelView.backgroundView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.tabelView];
    [self.scrollView sendSubviewToBack:self.tabelView];
    
    
}
-(void)hideTableView
{
    shouldShowDropDownList = YES;
    yoffset = yoffset-90;
    if ([Utility isIphone_5]) {
        yoffset = 230;
    }
    xoffset = 0;
    [self revertAnimation:xoffset andYValue:yoffset];
}
-(void)dismissView
{
    [self.confirmationView setHidden:YES];
}
-(void)callBack
{
    
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)addPhotos
{
    if (![self.addPhotoLabel.text isEqualToString:@"Photos 4/4"]) {
        [self showActionSheet];
    }
    
    
}
-(void)sendData
{
    
    [self.indecatorView setHidden:NO];
    
    [self performSelector:@selector(sendmail) withObject:nil afterDelay:0.0];
    
}

-(void)sendmail
{
    
    emailRecieverString = @"claims@cmibinsurance.com.au";
    
    
    if(self.nametextField.text == nil)
    {
        self.nametextField.text = @"";
        
    }
    if (self.emailTextField.text == nil )
    {
        self.emailTextField.text = @"";
        
    }
    if(self.mobileTextField.text == nil)
    {
        self.mobileTextField.text = @"";
        
    }
    if (self.line1.text == nil) {
        self.line1.text = @"";
    }
    
    
    if ([self.line1.text isEqualToString:@"Line 1 \nLine 2 \nLine 3"]) {
        self.line1.text = @"";
    }
    MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
     [controller setSubject:@"General Claim"];
    
    NSArray *toRecipients = [NSArray arrayWithObject:self.emailRecieverString];
    [controller setToRecipients:toRecipients];
    [controller setCcRecipients:[NSArray arrayWithObjects:self.emailTextField.text, nil]];
    
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
    
    
    
    
    NSString * bodyString = @"";
    
    bodyString = [NSString stringWithFormat:@"<table><tr><td>Name:</td><td>%@</td></tr><tr><td>Email:</td><td>%@</td></tr><tr><td>Mobile:</td><td>%@</td></tr><tr><td>What happened?</td ><td>%@</td></tr>",self.nametextField.text, self.emailTextField.text, self.mobileTextField.text, self.line1.text];
    
    if (imageCount == 0) {
        
        bodyString = [bodyString stringByAppendingString:@"</table>"];
        
    }
    else if(checForEmptyArray == 0)
    {
        
        bodyString = [bodyString stringByAppendingString:@"<tr><td>No GPS data available for photos.</td></tr></table>"];
        for (int i = 0; i<imageCount; i++)
        {
            switch (i) {
                case 0:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage1.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage1.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image1"];
                }
                    break;
                case 1:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage2.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage2.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image2"];
                }
                    break;
                case 2:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage3.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage3.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image3"];
                }
                    break;
                case 3:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage4.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage4.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image4"];
                }
                    break;
                    
                default:
                    break;
            }
            
        }
        
    }
    
    
    else
    {
        
        for (int i = 0; i<[self.latiArray count]; i++)
        {
            
            
            NSString * dateStr = nil;
            
            NSString *longitudeVal = [self.longiArray objectAtIndex:i];
            NSString *latitudeVal = [self.latiArray objectAtIndex:i];
            if (i<[self.dateTimeArray count]) {
                dateStr = [self.dateTimeArray objectAtIndex:i];
            }
            
            
            NSString *combineStr = @"";
            
            if ([longitudeVal hasPrefix:@"No"] || [latitudeVal hasPrefix:@"No"] )
            {
                combineStr = @"No GPS data available for photo.";
                
                
            }
            else
            {
                combineStr = [NSString stringWithFormat:@"Long: %f  Lat: %f",[longitudeVal floatValue], [latitudeVal floatValue]];
                
            }
            
            int val = i+1;
            bodyString = [bodyString stringByAppendingString:[NSString stringWithFormat:@"<tr><td>Photo %d: %@</td></tr>", val,combineStr]];
            if (dateStr) {
                bodyString = [bodyString stringByAppendingString:[NSString stringWithFormat:@"<tr><td>DateTime: %@</td></tr>", dateStr]];
            }
            
            switch (i) {
                case 0:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage1.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage1.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image1"];
                }
                    break;
                case 1:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage2.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage2.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image2"];
                }
                    break;
                case 2:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage3.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage3.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image3"];
                }
                    break;
                case 3:
                {
                    //NSData * imageData = UIImagePNGRepresentation(self.uploadImage4.image);
                    NSData * imageData = UIImageJPEGRepresentation(self.uploadImage4.image,0.4);
                    [controller addAttachmentData:imageData mimeType:@"image/png" fileName:@"image4"];
                }
                    break;
                    
                default:
                    break;
            }
            
        }
        
    }
    
    
    
    bodyString = [bodyString stringByAppendingString:@"</table>"];
    
    
    
    [controller setMessageBody:bodyString isHTML:YES];
    [self.indecatorView setHidden:YES];
    
    if (controller)
        [self presentModalViewController:controller animated:YES];
    
    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    imageCount = 0;
    
    [self.nametextField setText:@""];
    [self.emailTextField setText:@""];
    [self.mobileTextField setText:@""];
    [self.line1 setText:@""];
    
    
    [self.nametextField setPlaceholder:@"Name"];
    [self.emailTextField setPlaceholder:@"Email"];
    [self.mobileTextField setPlaceholder:@"Mobile"];
    self.line1.text = @"Line 1 \nLine 2 \nLine 3";
    self.line1.textColor = [UIColor lightGrayColor];
    
    self.uploadImage2.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    self.uploadImage1.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    self.uploadImage3.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    self.uploadImage4.image = [UIImage imageNamed:UPLOAD_PHOTO_IMAGE];
    
    switch (result) {
        case MFMailComposeResultSent:
            [self.confirmationView setHidden:NO];
            break;
            
        default:
            break;
    }
    
    checForEmptyArray = 0;
    [self.addPhotoLabel setText:@"Photos 0/4"];
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
    
    if (self.latiArray) {
        [self.latiArray removeAllObjects];
    }
    
    if (self.longiArray) {
        [self.longiArray removeAllObjects];
        
    }
    if (self.dateTimeArray) {
        [self.dateTimeArray removeAllObjects];
    }
    //  self.latitude = nil;
    // self.longitude = nil;
    
    self.longitude_libPhoto =  nil;
    self.latitude_libPhoto =  nil;
    
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark upload photo method and delegate-

-(void)showActionSheet
{
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Choose from library", nil];
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:[self capturePhoto];break;
            
        case 1:[self getPhotFromPhotoLibrary];break;
            
        default:
            break;
    }
}

-(void)capturePhoto
{
    // [self findCurrentLocation];
    @try
    {
        isCamera = YES;
        
        UIImagePickerController*   photoPicker = [[UIImagePickerController alloc] init];
        photoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        photoPicker.delegate = self;
        
        [self presentModalViewController:photoPicker animated:YES];
        [photoPicker release];
        
        [self performSelector:@selector(ShowStatusBar:) withObject:NO afterDelay:0.5f];
        
        
    }
    @catch (NSException *exception)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Camera" message:@"Camera is not available  " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
    
}

-(void)ShowStatusBar :(BOOL) show
{
    if(!show)
    {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        
        for(UIView *v in appDelegate.window.subviews)
        {
            if([v isKindOfClass:[UIImageView class]])
            {
                UIImageView *imageView = (UIImageView *)v;
                [imageView setBackgroundColor:[UIColor clearColor]];
                [imageView setImage:[UIImage imageNamed:@""]];
                //[v removeFromSuperview];
                
            }
            
        }
    }
    }
    else
    {
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
        {
            for(UIView *v in appDelegate.window.subviews)
            {
                if([v isKindOfClass:[UIImageView class]])
                {
                    UIImageView *imageView = (UIImageView *)v;
                    [imageView setImage:[UIImage imageNamed:STAUS_BAR_DARK_GREY]];
                }
                
            }
            
        }

        
    }


}

-(void)getPhotFromPhotoLibrary
{
    isCamera = NO;
    UIImagePickerController*  pickerLibrary = [[UIImagePickerController alloc] init];
    pickerLibrary.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [pickerLibrary setDelegate:self];
    [pickerLibrary.navigationBar setTintColor:[UIColor colorWithRed:20.0/255.0f green:16.0/255.0f blue:117.0/255.0f alpha:1.0]];
    
    [self presentModalViewController:pickerLibrary animated:YES];
    [pickerLibrary release];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{

    UIImage * myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    NSURL *referenceURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:referenceURL resultBlock:^(ALAsset *asset) {
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        NSDictionary *metadata = rep.metadata;
        
        NSDictionary * dict = [metadata objectForKey:@"{GPS}"];
        NSDictionary * dateTimeDict = [metadata objectForKey:@"{TIFF}"];
        if (dict) {
            self.longitude_libPhoto = [dict objectForKey:@"Longitude"];
            self.latitude_libPhoto= [dict objectForKey:@"Latitude"];
            
        }
        if (!isCamera) {
            if (self.longitude_libPhoto) {
                NSLog(@"hello");
                checForEmptyArray = checForEmptyArray+1;
                [self.longiArray addObject:[NSString stringWithFormat:@"%f",[self.longitude_libPhoto floatValue]]];
                self.longitude_libPhoto = nil;
                
            }
            else{
                [self.longiArray addObject:@"No GPS data available for photo."];
            }
            if (self.latitude_libPhoto) {
                NSLog(@"hi");
                checForEmptyArray = checForEmptyArray+1;
                [self.latiArray addObject:[NSString stringWithFormat:@"%f",[self.latitude_libPhoto floatValue]]];
                self.latitude_libPhoto = nil;
            }
            else{
                [self.latiArray addObject:@"No GPS data available for photo."];
            }
            
            
            
            if (dateTimeDict) {
                NSString * dateTime = [dateTimeDict objectForKey:@"DateTime"];
                if (dateTime) {
                    [self.dateTimeArray addObject:dateTime];
                }
            }
            else{
                [self.dateTimeArray addObject:@"No date and time available for photo."];
            }
            
            
            
        }
        
        
        
        
    } failureBlock:^(NSError *error) {
        // error handling
        // NSLog(@"error is %@",error);
    }];
    
    
    if (isCamera) {
        if (self.longitude) {
            NSLog(@"hello");
            checForEmptyArray = checForEmptyArray+1;
            [self.longiArray addObject:[NSString stringWithFormat:@"%f",[self.longitude floatValue]]];
            // self.longitude = nil;
            
        }
        else{
            [self.longiArray addObject:@"No GPS data available for photo."];
        }
        if (self.latitude) {
            NSLog(@"hi");
            checForEmptyArray = checForEmptyArray+1;
            [self.latiArray addObject:[NSString stringWithFormat:@"%f",[self.latitude floatValue]]];
            // self.latitude = nil;
        }
        else{
            [self.latiArray addObject:@"No GPS data available for photo."];
        }
        
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString * DateStr  = [DateFormatter stringFromDate:[NSDate date]];
        
        if (DateStr) {
            
            [self.dateTimeArray addObject:DateStr];
        }
    }
    
    imageCount++;
    if (imageCount == 1) {
        
        uploadImage1.image = myImage;
        [[self.uploadImage1 layer]setBorderWidth:1.0f];
        [[self.uploadImage1 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.addPhotoLabel setText:@"Photos 1/4"];
        
    }else if (imageCount ==2)
    {
        uploadImage2.image = myImage;
        [[self.uploadImage2 layer]setBorderWidth:1.0f];
        [[self.uploadImage2 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.addPhotoLabel setText:@"Photos 2/4"];
        
    }else if(imageCount == 3){
        uploadImage3.image = myImage;
        [[self.uploadImage3 layer]setBorderWidth:1.0f];
        [[self.uploadImage3 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.addPhotoLabel setText:@"Photos 3/4"];
        
    }else if (imageCount == 4)
    {
        uploadImage4.image = myImage;
        [[self.uploadImage4 layer]setBorderWidth:1.0f];
        [[self.uploadImage4 layer]setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.addPhotoLabel setText:@"Photos 4/4"];
        
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self ShowStatusBar:YES];


}
-(void)findCurrentLocation
{
    
    
    isCamera = YES;
    
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.latitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    
}
-(NSString *)Base64Encode:(NSData *)Inputdata{
    //Point to start of the data and set buffer sizes
    const uint8_t* input = (const uint8_t*)[Inputdata bytes];
    NSInteger length = [Inputdata length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];}

-(void)dealloc
{
    [super dealloc];
    [emailTextField release];
    [mobileTextField release];
    [nametextField release];
    [line1 release];
    [officeArray release];
    [tabelView release];
    if (latiArray) {
        [latiArray release];
    }
    if (longiArray) {
        [longiArray release];
    }
    if (locationManager) {
        [locationManager release];
    }
    
    
}



#pragma mark-
#pragma mark tableView Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [self.officeArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    OfficeInfo * info = [self.officeArray objectAtIndex:indexPath.row];
    
    
    
    UITableViewCell *cell  = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
    }
    //cell.textLabel.text = [self.officeArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text=info.location;
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            self.emailRecieverString = @"equiries@gardian.com.au";
            break;
            
//        case 1:
//            self.emailRecieverString = @"biloela@ribpl.com.au";
//            break;
//            
//        case 2:
//            self.emailRecieverString = @"nq@ribpl.com.au";
//            break;
//            
//        case 3:
//            self.emailRecieverString = @"gladstone@ribpl.com.au";
//            break;
//            
//        case 4:
//            self.emailRecieverString = @"sq@ribpl.com.au";
//            break;
//            
//        case 5:
//            self.emailRecieverString = @"emerald@ribpl.com.au";
//            break;
//            
//        case 6:
//            self.emailRecieverString = @"cq@ribpl.com.au";
//            break;
            
            
        default:
            break;
    }
    [title1 setText:@"Cairns"];
    [self hideTableView];
    
}
#pragma mark-
#pragma mark UIWebViewDelegate methods-

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    if ([absoluteStr hasSuffix:@"Claims"])
    {
        int controllers = self.navigationController.viewControllers.count;
        for (int i = 0; i<controllers; i++) {
            if ([[[self.navigationController viewControllers] objectAtIndex:i] isKindOfClass:[ClaimWhatToDo class]]) {
                [self.navigationController popViewControllerAnimated:YES];
                return YES;
            }
        }
        
        
        ClaimWhatToDo *claimWhatToDo = [[ClaimWhatToDo alloc] initWithNibName:@"ClaimWhatToDo" bundle:nil];
        [self.navigationController pushViewController:claimWhatToDo animated:YES];
        [claimWhatToDo release];
    }
    
    return YES;
    
}
@end
