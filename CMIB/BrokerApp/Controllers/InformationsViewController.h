//
//  InformationsViewController.h
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface InformationsViewController : GAITrackedViewController<UIWebViewDelegate>
{
    NSString * telephoneNumber;
}
@property (nonatomic , retain) NSString * telephoneNumber;

-(void)showInformation;
-(void)initializeTheInitialView;
- (void)openUrl:(UITapGestureRecognizer *) gr;

@end
