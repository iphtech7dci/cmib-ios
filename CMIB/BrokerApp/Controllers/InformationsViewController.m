//
//  InformationsViewController.m
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import "InformationsViewController.h"
#import "Constants.h"
#import "Utility.h"

@interface InformationsViewController ()

@end

@implementation InformationsViewController
@synthesize telephoneNumber;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    //self.navigationController.title = @"Information";
    self.screenName = @"Information";
    
    
//    // Gourav May 21 Start
//    
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
//    button.backgroundColor=[UIColor clearColor];
//    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
//    
//    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
//    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem = backButton;
//    [button release];
//    [backButton release];
//    
//    
//    // Gourav May 21 End
    
    

    
    
    
    [self.navigationItem setTitle:@"Information"];
    
    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:NSLocalizedString(@"INFORMATION", @"comment")];
    
    
   // [self initializeTheInitialView];
    
    [self showInformation];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden: NO animated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callBack
{
    self.tabBarController.selectedIndex = 0;
}

-(void)showInformation
{
    
    UIScrollView * scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0,320,self.view.frame.size.height)];
    scrollview.contentSize = CGSizeMake(320, 300);
    scrollview.scrollEnabled =YES;
    [scrollview setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:scrollview];
    
    CGFloat yValue =  0.0;
    yValue = yValue+25;
    
    
   // NSString *description = [NSString stringWithFormat:@"<html><head><style> #claim_view{font-family:helvetica; font-size:12; margin:10;}a.ex1:link {color:blue; text-decoration:underline;} a.ex2:link {color:black; text-decoration:none;} </style></head><body id='claim_view'><p>The ABC Insurance Brokers app is a demo Brokerapp so we can show you the features and benefits of developing an app for your brokerage. The following is some standard Info screen text that many brokers use.<br><br>This application has been developed to provide useful information and tools when you need to contact us, lodge a claim, or require fast access to your insurance portfolio details.<br><br> Data within this app, such as contact details etc, will synchronise and update when the app is launched and you are able to connect to the internet.<br><br> If we develop new features for the app, updates will be advised and will be available for download via iTunes or Google Play app stores.</p><p><b>Location Services</b></br>Some services on this app make use of location-based data. We use this information to provide the requested location-based service, not to identify you. The app collects GPS and timestamp data from the photos that you send us via the Claims & Photos forms. The information includes the longitude and latitude of where the photo was taken as well as the time the photo was taken. </p><p><b>Google Analytics</b></br>This app uses Google Analytics to tell us how our app is used. It does not collect personal information or identify you. Our use of Google Analytics is bound by the Google Analytics Terms of Service.</p><b>Push Notifications</b></br>We may send alerts to your device in the form of a Push Notification. You can manage how you receive push notifications or turn off the feature specifically for this app within the settings section of your device. </p>Our full Privacy Policy is available on our website.</p><p>This app has been developed for us by Brokerapps Pty Ltd which is a product of Bark Productions.</br>See <a class='ex2' href='http://www.barkproductions.net.au'>www.barkproductions.net.au</a> & <a class='ex2' href='http://www.brokerapps.com.au'>www.brokerapps.com.au</a> </br></p></body></html>" ];
    
    NSString *description = [NSString stringWithFormat:@"<html><head><style> #claim_view{font-family:helvetica; font-size:12; margin:10;}a.ex1:link {color:blue; text-decoration:underline;} a.ex2:link {color:black; text-decoration:none;} </style></head><body id='claim_view'><p>This application has been developed to provide useful information and tools when you need to contact us, lodge a claim, or require fast access to your insurance portfolio details.<br><br>Data within this app, such as contact details etc, will synchronise and update when the app is launched and you are able to connect to the internet.<br><br>if we develop new features for the app, updates will be advised and will be available for download via iTunes.</p><p><b>Location Services</b></br>Some services on this app make use of location-based data. We use this information to provide the requested location-based service, not to identify you. The app collects GPS and timestamp data from the photos that you send us via the Claims & Photos form. The information includes the longitude and latitude of where the photo was taken as well as the time the photo was taken.  </p><p><b>Google Analytics</b></br>This app uses Google Analytics to tell us how our app is used. It does not collect personal information or identify you. Our use of Google Analytics is bound by the Google Analytics Terms of Service.</p>Our full Privacy Policy is available on our website.</p><a class='ex2' href='http://www.barkproductions.net.au'> www.barkproductions.net.au</a> " ];
    
    // <u><a class='ex2' href='http://www.assetib.com.au/privacy-policy/'>http://www.assetib.com.au/privacy-policy</a></u>
    
    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 10, 320, self.view.frame.size.height+280)];
    if ([Utility isIphone_5]) {
        webview.frame = CGRectMake(0, 0, 320, self.view.frame.size.height+240);
    }
    
    [webview.scrollView setBounces:NO];
    [webview setOpaque:NO];
    [webview setDelegate:self];
    [webview setBackgroundColor:[UIColor clearColor]];
    [webview.scrollView setScrollEnabled:NO];
    webview.dataDetectorTypes = (UIDataDetectorTypePhoneNumber|UIDataDetectorTypeLink);
    [webview loadHTMLString:description baseURL:nil];
    [scrollview addSubview:webview];
    [webview release];
    
    yValue = yValue+self.view.frame.size.height+50;
    
    scrollview.contentSize = CGSizeMake(320, yValue); // 150
    
    if ([Utility isIphone_5]) {
        scrollview.contentSize = CGSizeMake(320, yValue-80);
    }

}

-(void)initializeTheInitialView
{
    CGFloat yValue = 20.0;
    CGFloat labelYValue = 20.0;
    CGFloat labelXValue = 150.0;
    CGFloat labelWidth = 180.0;
    CGFloat labeHieght = 40.0;
    CGFloat buttonHieght = 40.0;
    CGFloat buttonWidth = 280.0;
    CGFloat littleSpace = 5.0;
    CGFloat xValue = 20.0;
    
    
    UIImageView * infoTagView = [[UIImageView alloc]initWithFrame:CGRectMake(20, yValue,30,40)];
    [infoTagView setImage:[UIImage imageNamed:INFO_TAG]];
    [self.view addSubview:infoTagView];
    [infoTagView release];
    
    
    UILabel * infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(labelXValue, labelYValue, labelWidth, labeHieght)];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [infoLabel setText:@"Information Sheets"];
    [self.view addSubview:infoLabel];
    [infoLabel release];
    
    
    yValue = yValue+40+20;
    
    UIImageView * blackBarImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, yValue, 320, 30)];
    [blackBarImage setImage:[UIImage imageNamed:BLACK_BAR_IMAGE]];
    [self.view addSubview: blackBarImage];
    [blackBarImage release];
    
    
    UILabel * barText = [[UILabel alloc]initWithFrame:CGRectMake(20, yValue, 300, 30)];
    [barText setBackgroundColor:[UIColor clearColor]];
    [barText setText:@"View/Download Documents"];
    [barText setFont:[UIFont boldSystemFontOfSize:15]];
    [barText setTextColor:[UIColor whiteColor]];
    [self.view addSubview:barText];
    [barText release];
    
    yValue = yValue+30+15;
    
    
    UIButton * insuranceBtn = [[UIButton alloc] initWithFrame:CGRectMake(xValue, yValue, buttonWidth, buttonHieght)];
    [insuranceBtn setBackgroundImage:[UIImage imageNamed:PDF_DOWNLOAD_BTN] forState:UIControlStateNormal];
    
    [insuranceBtn addTarget:self action:@selector(doInsurance) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:insuranceBtn];
    
    
    UILabel * insuranceLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+20, yValue, buttonWidth, buttonHieght)];
    [insuranceLabel setBackgroundColor:[UIColor clearColor]];
    [insuranceLabel setText:@"W. R. Barkely Insurance Aus"];
    [insuranceLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [self.view addSubview:insuranceLabel];
    
    
    yValue = yValue+ buttonHieght+ littleSpace;
    
    UIButton * managementBtn = [[UIButton alloc] initWithFrame:CGRectMake(xValue, yValue, buttonWidth, buttonHieght)];
    [managementBtn setBackgroundImage:[UIImage imageNamed:PDF_DOWNLOAD_BTN] forState:UIControlStateNormal];
    
    [managementBtn addTarget:self action:@selector(doManagement) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:managementBtn];
    
    UILabel * managementLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+20, yValue, buttonWidth, buttonHieght)];
    [managementLabel setBackgroundColor:[UIColor clearColor]];
    [managementLabel setText:@"Management Liability"];
    [managementLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [self.view addSubview:managementLabel];

    yValue = yValue+ buttonHieght+ littleSpace;
    
    UIButton * profrssionalBtn = [[UIButton alloc] initWithFrame:CGRectMake(xValue, yValue, buttonWidth, buttonHieght)];
    [profrssionalBtn setBackgroundImage:[UIImage imageNamed:PDF_DOWNLOAD_BTN] forState:UIControlStateNormal];
    
    [profrssionalBtn addTarget:self action:@selector(doProfessional) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:profrssionalBtn];
    
    UILabel * professionalLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+20, yValue, buttonWidth, buttonHieght)];
    [professionalLabel setBackgroundColor:[UIColor clearColor]];
    [professionalLabel setText:@"Professional Indemnity"];
    [professionalLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [self.view addSubview:professionalLabel];
    
    yValue = yValue+ buttonHieght+ littleSpace;
    
    UIButton * liabilityBtn = [[UIButton alloc] initWithFrame:CGRectMake(xValue, yValue, buttonWidth, buttonHieght)];
    [liabilityBtn setBackgroundImage:[UIImage imageNamed:PDF_DOWNLOAD_BTN] forState:UIControlStateNormal];
    
    [liabilityBtn addTarget:self action:@selector(doLiability) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:liabilityBtn];
    
    UILabel * liabilityLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+20, yValue, buttonWidth, buttonHieght)];
    [liabilityLabel setBackgroundColor:[UIColor clearColor]];
    [liabilityLabel setText:@"Public & Products Liability"];
    [liabilityLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [self.view addSubview:liabilityLabel];
    
    yValue = yValue+ buttonHieght+ littleSpace;
    
    UIButton * corporateBtn = [[UIButton alloc] initWithFrame:CGRectMake(xValue, yValue, buttonWidth, buttonHieght)];
    [corporateBtn setBackgroundImage:[UIImage imageNamed:PDF_DOWNLOAD_BTN] forState:UIControlStateNormal];
    
    [corporateBtn addTarget:self action:@selector(doCorporate) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:corporateBtn];
    
    
    UILabel * corporateLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+20, yValue, buttonWidth, buttonHieght)];
    [corporateLabel setBackgroundColor:[UIColor clearColor]];
    [corporateLabel setText:@"W.R Barkley Corporate"];
    [corporateLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [self.view addSubview:corporateLabel];
    
}


#pragma mark-
#pragma mark IBActions-

-(void)doInsurance
{
    
}
-(void)doManagement
{
    
}
-(void)doProfessional
{
    
}
 -(void)doLiability
{
    
}
-(void)doCorporate
{
    
}


- (void)openUrl:(UITapGestureRecognizer *) gr
{
    UIGestureRecognizer *rec = (UIGestureRecognizer *)gr;
    
    id hitLabel = [self.view hitTest:[rec locationInView:self.view] withEvent:UIEventTypeTouches];
    
    if ([hitLabel isKindOfClass:[UILabel class]]) {
        self.telephoneNumber = ((UILabel *)hitLabel).text;
        
        
        NSString * str = [NSString stringWithFormat:@"http://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
        
        
       // UIAlertView * alert = [[UIAlertView alloc] initWithTitle:self.telephoneNumber message:@"Would you like to call this number?" delegate:self cancelButtonTitle:@"Reject" otherButtonTitles:@"Call", nil];
        
        //[alert show];
        //[alert release];
        
    }
}

#pragma mark AlertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
      
        NSString * str = [NSString stringWithFormat:@"http://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    
    if ([absoluteStr hasPrefix:@"http://"])
    {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:absoluteStr]];
        return NO;
    }
    return YES;
    
}
@end
