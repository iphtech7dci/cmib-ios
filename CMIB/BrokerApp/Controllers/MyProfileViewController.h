//
//  MyProfileViewController.h
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PolicyView.h"
#import "GAITrackedViewController.h"

@class AppDelegate;


@interface MyProfileViewController : GAITrackedViewController<UITextFieldDelegate, PolicyViewDelegate>
{
    UITextField * brokerText;
    UITextField * brokerOfficeText;
    UITextField * releventText;
    
    int policyNumber;
    int labelTagIncreamenter;
    
    
    CGFloat yValue ;
    UIButton * saveButton;
    UIButton * editButton;
    UILabel * additionalPlicy;
    UIButton * addButton;
    
    
    CGFloat xValue ;
    
    CGFloat width ;
    CGFloat hieght ;
    CGFloat textFieldHeight ;
    CGFloat littleSpace ;
    CGFloat largeSpace ;
    CGFloat extraLargeSpace ;
    CGFloat fontSize ;
    CGFloat bottumButtonWidth ;
    CGFloat labelImageXValue ;
    CGFloat labelImageDimention ;
    CGFloat deleteButtonXvalue ;
    CGFloat deleteButtonDimetions ;
    CGFloat viewHeight;
    
    UIScrollView * scrollview;
    
    AppDelegate * appDelegate;
    
    NSMutableArray * array ;
    
    UITextField * policyNameText1 ;
    UITextField * policyNumberText1;
    UITextField * policyNameText2;
    UITextField * policyNumberText2;
    UITextField * policyText2 ;
    UITextField * policyNumberText;
    
    BOOL shouldEdit;
    
    NSMutableArray *tagsArray;
    NSMutableArray * labeltagArray;
    CGFloat fixedFormHeight;
    
    NSMutableDictionary * dict;
    
    NSMutableArray *dataArray;
    UIToolbar *numberToolbar;
    CGPoint offset;
}

@property (nonatomic, retain) NSMutableArray *dataArray;
@property (nonatomic, retain) NSMutableDictionary * dict;
@property (nonatomic, retain) NSMutableArray * labeltagArray ;
@property (nonatomic, retain) NSMutableArray * array ;
@property (nonatomic, retain) UIScrollView * scrollview;
@property (nonatomic, retain) UITextField * releventText;
@property (nonatomic, retain) UITextField * brokerOfficeText;
@property (nonatomic, retain) UITextField * brokerText;
@property (nonatomic, retain) NSMutableArray *tagsArray;

-(void)getData;
-(void)initailizeNavigationBar;
-(void) moveViewDown;
-(void)dismissKeyboard;
-(void)makeForm;
-(void)createDataFields:(BOOL)isAll;
-(void)moveScrolUP:(int )tag;
-(void)moveScrollDown;
-(void)editData;
-(void)saveData;
-(void)addPolicy:(UIButton *)sender;
-(void)layoutScrollView;
-(void)deleteView:(Policy *)policy;
-(void)updatePolicy:(Policy *)policy;
-(void)dealloc;
-(void) makeBigHightOfScrollView;
-(void)moveScrollDownForTextfield;


@end
