//
//  MyProfileViewController.m
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import "MyProfileViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "ProfileInfo.h"
#import "Policy.h"
#import "PolicyView.h"
#import "Utility.h"


#define  VIEW_TAG 201
#define DELETE_BTN_TAG 1000
#define LABEL_TAG  401
#define LABEL_TAG_MULTIPLIER 2

#define POLICY_VIEW_TAG 1000;
#define DELETE_BTN_1 21
#define DELETE_BTN_2 22
#define DELETE_BTN_3 23

#define BROKER_NAME    29
#define BROKER_OFFICE  30

#define POLICY_NAME_TAG 31
#define POLICY_NUMBER_TAG 32
#define POLICY_RELEVENT_TAG 33


#define POLICY_NAME_INDEX_TAG 41
#define POLICY_NUMBER_INDEX_TAG 42
#define POLICY_RELEVENT_INDEX_TAG 43

@interface MyProfileViewController ()
{
    CGFloat yVal;
}

@end

@implementation MyProfileViewController
@synthesize brokerText;
@synthesize brokerOfficeText;
@synthesize releventText;
@synthesize scrollview;
@synthesize array;
@synthesize tagsArray;
@synthesize labeltagArray;
@synthesize dict;
@synthesize dataArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
        
    }
    return self;
}


-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    //[self.navigationController.navigationBar setTintColor:[UIColor orangeColor]];
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    self.title = @"My Profile";
    
    
    
//    // Gourav May 21 Start
//    
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
//    button.backgroundColor=[UIColor clearColor];
//    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
//    
//    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
//    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem = backButton;
//    [button release];
//    [backButton release];
//
//    
//    // Gourav May 21 End
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"My Profile";
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)] ;
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;

//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
//    button.backgroundColor=[UIColor clearColor];
//    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
//
//    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
//    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem = backButton;
//    [button release];
//    [backButton release];
    /*   UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(prevBtn)];
     UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBtn)];
     UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
     */   UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)];
    NSArray *itemArray = [[NSArray alloc] initWithObjects:item3, nil];
    numberToolbar.items = itemArray;
    
    [numberToolbar sizeToFit];
    //   [item1 release];
    //   [item2 release];
    [item3 release];
    [itemArray release];
    
    
    
    [self initailizeNavigationBar];
    
    [self makeForm];
    
    [self getData];
    
    if ([self.dataArray count] == 0) {
        
        Policy *policy = [[Policy alloc] init];
        [policy setPIndex:[NSNumber numberWithInt:1]];
        [self.dataArray addObject:policy];
        [policy release];
        //Allow all fields enalble
        appDelegate.shouldAllowEditing = YES;
    }
    else
    {
        //Disable
        appDelegate.shouldAllowEditing = YES;
        
    }
    
    [self createDataFields:YES];
    [self layoutScrollView];
    
    
    
    //   numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)], nil];
    //   [numberToolbar sizeToFit];
    
    
    
    // Do any additional setup after loading the view from its nib.
    
    
    [self.navigationItem setTitle:@"My Profile"];
    
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:NSLocalizedString(@"MY PROFILE", @"comment")];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)callBack
{
    self.tabBarController.selectedIndex = 0;
}



-(void) moveViewDown
{
    [self moveScrollDown];
    [UIView beginAnimations:@"ResizeForKeyboard" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x,64,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}

- (void)prevBtn
{
    
}

- (void)nextBtn
{
    
}

-(void)dismissKeyboard
{
    
    [self moveViewDown];
    [self moveScrollDownForTextfield];
    [self.view endEditing:YES];
    // self.scrollview.contentOffset = CGPointMake(0,-50);
}
-(void)moveScrollDownForTextfield
{
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    
    self.scrollview.contentSize = CGSizeMake(280, yValue);
    [UIView commitAnimations];
    
}


-(void)makeForm
{
    
    self.scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(20, 0, 290, 370)] ;
    if ([Utility isIphone_5]) {
        scrollview.frame = CGRectMake(20, 0, 290, 450);
    }
    [self.scrollview setBounces:YES];
    [self.scrollview setScrollEnabled:YES];
    [self.scrollview setContentSize:CGSizeMake(290, 10)];
    [self.view addSubview:self.scrollview];
    
    xValue = 0.0;
    yValue = 20.0;
    width = 280.0;
    hieght = 15.0;
    textFieldHeight = 30.0;
    littleSpace = 3;
    largeSpace =10.0;
    extraLargeSpace = 12.0;
    fontSize = 12.0;
    bottumButtonWidth = 138;
    
    
    UILabel * brokerHeading = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, hieght)];
    [brokerHeading setBackgroundColor:[UIColor clearColor]];
    [brokerHeading setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [brokerHeading setText:@"Relationship Manager"];
    [self.scrollview addSubview:brokerHeading];
    [brokerHeading release];
    
    yValue = yValue+hieght+littleSpace;
    
    self.brokerText = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.brokerText setPlaceholder:@"Text"];///astha
    [self.brokerText setTag:BROKER_NAME];
    [self.brokerText setBorderStyle:UITextBorderStyleRoundedRect];
    [self.brokerText setDelegate:self];
    //  [self.brokerText setInputView:numberToolbar];
    [self.scrollview addSubview:self.brokerText];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    
    /*
    UILabel * brokerOffice = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, hieght)];
    [brokerOffice setBackgroundColor:[UIColor clearColor]];
    [brokerOffice setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [brokerOffice setText:@"Broker Office"];
    [self.scrollview addSubview:brokerOffice];
    [brokerOffice release];
    
    yValue = yValue+hieght+littleSpace;
    
    self.brokerOfficeText = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.brokerOfficeText setPlaceholder:@"Text"];///astha
    [self.brokerOfficeText setTag:BROKER_OFFICE];
    [self.brokerOfficeText setBorderStyle:UITextBorderStyleRoundedRect];
    [self.brokerOfficeText setDelegate:self];
    [self.scrollview addSubview:self.brokerOfficeText];
    
    yValue = yValue+textFieldHeight+largeSpace+extraLargeSpace;
     */
    
    
    /* UILabel * relevent = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, hieght)];
     [relevent setBackgroundColor:[UIColor clearColor]];
     [relevent setFont:[UIFont boldSystemFontOfSize:fontSize]];
     [relevent setText:@"Relevent Insurers"];
     [self.scrollview addSubview:relevent];
     [relevent release];
     
     yValue = yValue+hieght+littleSpace;
     
     
     self.releventText = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
     [self.releventText setPlaceholder:@"Text"];///astha
     [self.releventText setBorderStyle:UITextBorderStyleRoundedRect];
     [self.releventText setDelegate:self];
     [self.releventText setTag:10001];
     [self.scrollview addSubview:self.releventText];
     
     yValue = yValue+textFieldHeight+largeSpace+extraLargeSpace;
     
     
     
     */
    UILabel * myPolicyHeading = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, hieght)];
    [myPolicyHeading setBackgroundColor:[UIColor clearColor]];
    [myPolicyHeading setFont:[UIFont boldSystemFontOfSize:fontSize]];
    //[myPolicyHeading setTextColor:[UIColor colorWithRed:29.0/255.0f green:66.0/255.0f blue:111.0/255.0f alpha:1.0]];
    [myPolicyHeading setTextColor:[UIColor colorWithRed:(241/255.f) green:(122/255.f) blue:(45/255.f) alpha:1.0f]];
    [myPolicyHeading setText:@"My Policies"];
    [self.scrollview addSubview:myPolicyHeading];
    [myPolicyHeading release];
    
    yValue = yValue+textFieldHeight+largeSpace-10;
    
    
    
    fixedFormHeight = yValue;
    
    //    UILabel * policyName = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, hieght)];
    //    [policyName setBackgroundColor:[UIColor clearColor]];
    //    [policyName setFont:[UIFont boldSystemFontOfSize:fontSize]];
    //    [policyName setText:@"Policy Name"];
    //    [self.scrollview addSubview:policyName];
    //    [policyName release];
    //
    //
    //    UIImageView * labelImge = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageXValue, yValue, labelImageDimention, labelImageDimention)];
    //    [labelImge setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    //    [self.scrollview addSubview:labelImge];
    //
    //    UILabel * policyLabel = [[UILabel alloc]initWithFrame:CGRectMake(labelImageXValue, yValue, labelImageDimention, labelImageDimention)];
    //    [policyLabel setBackgroundColor:[UIColor clearColor]];
    //    [policyLabel setFont:[UIFont systemFontOfSize:12]];
    //    [policyLabel setTextAlignment:NSTextAlignmentCenter];
    //    [policyLabel setText:@"1"];
    //    [policyLabel setTextColor:[UIColor whiteColor]];
    //    [self.scrollview addSubview:policyLabel];
    //
    //    yValue = yValue+hieght+littleSpace;
    //
    //     policyNameText1 = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    //    [policyNameText1 setPlaceholder:@"Text"];///astha
    //    [policyNameText1 setBorderStyle:UITextBorderStyleRoundedRect];
    //     [policyNameText1 setDelegate:self];
    //    [self.scrollview addSubview:policyNameText1];
    //
    //
    //    yValue = yValue+textFieldHeight+largeSpace;
    //
    //    UIImageView * labelImge1 = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageXValue, yValue, labelImageDimention, labelImageDimention)];
    //    [labelImge1 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    //    [self.scrollview addSubview:labelImge1];
    //
    //    UILabel * policyLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(labelImageXValue, yValue, labelImageDimention, labelImageDimention)];
    //    [policyLabel1 setBackgroundColor:[UIColor clearColor]];
    //    [policyLabel1 setFont:[UIFont systemFontOfSize:12]];
    //    [policyLabel1 setTextAlignment:NSTextAlignmentCenter];
    //    [policyLabel1 setText:@"1"];
    //    [policyLabel1 setTextColor:[UIColor whiteColor]];
    //    [self.scrollview addSubview:policyLabel1];
    //
    //
    //
    //    UILabel * policyNumber1 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, hieght)];
    //    [policyNumber1 setBackgroundColor:[UIColor clearColor]];
    //    [policyNumber1 setFont:[UIFont boldSystemFontOfSize:fontSize]];
    //    [policyNumber1 setText:@"Policy Number"];
    //    [self.scrollview addSubview:policyNumber1];
    //    [policyNumber1 release];
    //
    //
    //    yValue = yValue+hieght+littleSpace;
    //
    //     policyNumberText1 = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    //    [policyNumberText1 setPlaceholder:@"Text"];///astha
    //    [policyNumberText1 setBorderStyle:UITextBorderStyleRoundedRect];
    //     [policyNumberText1 setDelegate:self];
    //    [self.scrollview addSubview:policyNumberText1];
    //
    //    yValue = yValue+textFieldHeight+largeSpace;
    //
    //    fixedFormHeight = yValue;
    //
    //    //***********
    //
    //
    //    UIView * policyNameView1 = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, width, viewHeight)];
    //    [policyNameView1 setBackgroundColor:[UIColor clearColor]];
    //    [policyNameView1 setTag:VIEW_TAG];
    //
    //    [self.tagsArray addObject:[NSNumber numberWithInt:policyNameView1.tag]];
    //
    //    UIImageView * labelImge2 = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
    //    [labelImge2 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    //    [policyNameView1 addSubview:labelImge2];
    //    [labelImge2 release];
    //
    //    UILabel * policyLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
    //    [policyLabel2 setBackgroundColor:[UIColor clearColor]];
    //    [policyLabel2 setFont:[UIFont systemFontOfSize:12]];
    //    [policyLabel2 setTextAlignment:NSTextAlignmentCenter];
    //    [policyLabel2 setText:@"2"];
    //    [policyLabel2 setTag:VIEW_TAG*LABEL_TAG];
    //    [policyLabel2 setTextColor:[UIColor whiteColor]];
    //    [policyNameView1 addSubview:policyLabel2];
    //
    //    [self.labeltagArray addObject:[NSNumber numberWithInt:policyLabel2.tag]];
    //    [policyLabel2 release];
    //
    //    UILabel * policyName2 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, 0, width, hieght)];
    //    [policyName2 setBackgroundColor:[UIColor clearColor]];
    //    [policyName2 setFont:[UIFont boldSystemFontOfSize:fontSize]];
    //    [policyName2 setText:@"Policy Name"];
    //    [policyNameView1 addSubview:policyName2];
    //    [policyName2 release];
    //
    //
    //    //yValue = yValue+hieght+littleSpace;
    //
    //    policyNameText2 = [[UITextField alloc]initWithFrame:CGRectMake(xValue, 17, width, textFieldHeight)];
    //    [policyNameText2 setPlaceholder:@"Text"];///astha
    //    [policyNameText2 setBorderStyle:UITextBorderStyleRoundedRect];
    //    [policyNameText2 setDelegate:self];
    //    [policyNameView1 addSubview:policyNameText2];
    //
    //
    //    UIButton * deleteButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [deleteButton1 setTag:VIEW_TAG*DELETE_BTN_TAG];
    //    [deleteButton1 setFrame:CGRectMake(deleteButtonXvalue, 17+5,deleteButtonDimetions , deleteButtonDimetions)];
    //    [deleteButton1 setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
    //    [deleteButton1 addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
    //    [policyNameView1 addSubview:deleteButton1];
    //
    //    [self.scrollview addSubview:policyNameView1];
    //    [policyNameView1 release];
    //
    //
    //    ///*************
    //
    //
    //    //******
    //    yValue = yValue+50+largeSpace;
    //
    //
    //
    //
    //    UIView * policyNameView2 = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, width, viewHeight)];
    //    [policyNameView2 setBackgroundColor:[UIColor clearColor]];
    //    [policyNameView2 setTag:(VIEW_TAG + 1 )];
    //
    //    [self.tagsArray addObject:[NSNumber numberWithInt:policyNameView2.tag]];
    //
    //    UIImageView * labelImge3 = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
    //    [labelImge3 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    //    [policyNameView2 addSubview:labelImge3];
    //    [labelImge3 release];
    //
    //    UILabel * policyLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
    //    [policyLabel3 setBackgroundColor:[UIColor clearColor]];
    //    [policyLabel3 setFont:[UIFont systemFontOfSize:12]];
    //    [policyLabel3 setTextAlignment:NSTextAlignmentCenter];
    //    [policyLabel3 setTag:policyNameView2.tag*LABEL_TAG];
    //    [policyLabel3 setText:@"2"];
    //    [policyLabel3 setTextColor:[UIColor whiteColor]];
    //    [policyNameView2 addSubview:policyLabel3];
    //   [self.labeltagArray addObject:[NSNumber numberWithInt:policyLabel3.tag]];
    //    [policyLabel3 release];
    //
    //
    //    UILabel * policyNumber2 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, 0, width, hieght)];
    //    [policyNumber2 setBackgroundColor:[UIColor clearColor]];
    //    [policyNumber2 setFont:[UIFont boldSystemFontOfSize:fontSize]];
    //    [policyNumber2 setText:@"Policy Number"];
    //    [policyNameView2 addSubview:policyNumber2];
    //   [policyNumber2 release];
    //
    //
    //   // yValue = yValue+hieght+littleSpace;
    //
    //     policyNumberText2 = [[UITextField alloc]initWithFrame:CGRectMake(xValue, 17, width, textFieldHeight)];
    //    [policyNumberText2 setPlaceholder:@"Text"];///astha
    //    [policyNumberText2 setBorderStyle:UITextBorderStyleRoundedRect];
    //     [policyNumberText2 setDelegate:self];
    //    [policyNameView2 addSubview:policyNumberText2];
    //
    //
    //    UIButton * deleteButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [deleteButton2 setTag: (VIEW_TAG+1)*DELETE_BTN_TAG];
    //    [deleteButton2 setFrame:CGRectMake(deleteButtonXvalue, 17+5,deleteButtonDimetions , deleteButtonDimetions)];
    //    [deleteButton2 setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
    //    [deleteButton2 addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
    //    [policyNameView2 addSubview:deleteButton2];
    //
    //    [self.scrollview addSubview:policyNameView2];
    //    [policyNameView2 release];
    //
    //
    //
    //    yValue = yValue+viewHeight+extraLargeSpace;
    
    //***
    
    addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setImage:[UIImage imageNamed:ADD_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
    [addButton setFrame:CGRectMake(xValue, yValue, 24, 24)];
    [addButton setTintColor:[UIColor orangeColor]];
    [addButton addTarget:self action:@selector(addPolicy:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollview addSubview:addButton];
    
    
    additionalPlicy = [[UILabel alloc]initWithFrame:CGRectMake(25, yValue+5, 240, hieght)];
    [additionalPlicy setBackgroundColor:[UIColor clearColor]];
    [additionalPlicy setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [additionalPlicy setTextColor:[UIColor colorWithRed:(241/255.f) green:(122/255.f) blue:(45/255.f) alpha:1.0f]];
    [additionalPlicy setText:@"Additional Policy"];
    [self.scrollview addSubview:additionalPlicy];
    [additionalPlicy release];
    
    yValue = yValue+24+extraLargeSpace;
    
    editButton = [[UIButton alloc] initWithFrame:CGRectMake(0 , yValue,bottumButtonWidth, 42)];
    [editButton setBackgroundImage:[UIImage imageNamed:ADD_PHOTO_IMAGE] forState:UIControlStateNormal];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [[editButton titleLabel] setFont:[UIFont boldSystemFontOfSize:14]];
    [editButton addTarget:self action:@selector(editData) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollview addSubview:editButton];
    
    xValue = xValue+bottumButtonWidth+2;
    
    saveButton = [[UIButton alloc] initWithFrame:CGRectMake(xValue , yValue,bottumButtonWidth, 42)];
    [saveButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [[saveButton titleLabel] setFont:[UIFont boldSystemFontOfSize:14]];
    [saveButton addTarget:self action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollview addSubview:saveButton];
    
    yValue = yValue+42;
    [self.scrollview setContentSize:CGSizeMake(280, yValue+100)];
    
}


-(void)createDataFields:(BOOL)isAll
{
    
    for (PolicyView *view in [self.scrollview subviews]) {
        
        if ([view isKindOfClass:[PolicyView class]]) {
            [view setMyDelegate:nil];
            [view removeFromSuperview];
        }
        
    }
    
    CGFloat yVal = fixedFormHeight;
    CGFloat xVal = 0.0;
    
    int i = 0;
    
    if (isAll) {
        i = 0;
    }
    else
    {
        i = [self.dataArray count] - 1;
        
    }
    
    for (; i<[self.dataArray count]; i++)
    {
        
        Policy *policy = (Policy *)[self.dataArray objectAtIndex:i];
        [policy setPIndex:[NSNumber numberWithInt:(i+1)]];
        
        PolicyView *policyV = [[PolicyView alloc] initWithPolicy:policy];
        [policyV setMyDelegate:self];
        [policyV setFrame:CGRectMake(xVal, yVal, 280, 165)]; //110   ==h
        [self.scrollview addSubview:policyV];
        [policyV release];
        
        
    }
    
    
}


#pragma mark-
#pragma mark movesScrollview UP and DOWN-


-(void)moveScrolUP:(int )tag
{
    //CGSize size = self.scrollview.contentSize;
    offset = self.scrollview.contentOffset;
    
    int height = offset.y;
    
    
    CGRect frame;
    
    UIView *view_policy = [self.scrollview viewWithTag:tag];
    
    if (view_policy) {
        
        frame = [view_policy frame];
        height =  frame.origin.y;
        
    }
    else{
        height = 50;
    }
    
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollview.contentOffset = CGPointMake(0, height);
    [UIView commitAnimations];
    
    
    
    
}

-(void)moveScrollDown
{
    
    CGFloat requiredMaxY = self.scrollview.contentSize.height-self.scrollview.frame.size.height;
    if (requiredMaxY < 0) {
        requiredMaxY = 0;
    }
    
    int offY = self.scrollview.contentOffset.y;
    if (offY > requiredMaxY) {
        offY = requiredMaxY;
    }
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollview.contentOffset = CGPointMake(0, offY);
    [UIView commitAnimations];
    
}


#pragma mark-
#pragma mark IBActions-
-(void)editData
{
    appDelegate.shouldAllowEditing = YES;
}
-(void)getData
{
    
    ProfileInfo * info = [appDelegate.dataHandle getData];
    
    if (info)
    {
        if(info.brokerOffice)
            brokerOfficeText.text = info.brokerOffice;
        
        if(info.myBroker)
            brokerText.text = info.myBroker;
        
        
        NSData * data = info.policyData;
        
        if (data) {
            self.dataArray = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        }
        else
        {
            self.dataArray  = [NSMutableArray array];
        }
        
    }
    else
    {
        
        self.dataArray  = [NSMutableArray array];
        
    }
    
}

-(void)saveData
{
    
    
    ProfileInfo * info = [[ProfileInfo alloc]init];
    
    if (brokerText.text == nil) {
        brokerText.text = @"";
    }
    if (brokerOfficeText.text == nil) {
        brokerOfficeText.text = @"";
    }
    
    info.myBroker = brokerText.text;
    info.brokerOffice = brokerOfficeText.text;
    
    
    /*if (releventText.text == nil) {
     releventText.text = @"";
     }*/
    
    //info.releventInsurer = releventText.text;
    
    
    /* if (policyNameText1.text) {
     [self.dict setObject:policyNameText1.text forKey:POLICYNAME];
     }
     if (policyNumberText1.text) {
     [self.dict setObject:policyNumberText1.text forKey:POLICYNUMBER];
     }
     
     [self.array addObject:self.dict];
     if (policyNameText2.text) {
     [self.dict setObject:policyNameText2.text forKey:POLICYNAME];
     }
     if (policyNumberText2.text) {
     [self.dict setObject:policyNumberText2.text forKey:POLICYNUMBER];
     }
     
     [self.array addObject:self.dict];*/
    
    if ([self.dataArray count]>0)
    {
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.dataArray];
        
        info.policyData = data;
    }
    
    
    BOOL delete = [appDelegate.dataHandle deleteInformation];
    
    if (delete)
    {
        BOOL success = [appDelegate.dataHandle InsertBrokerData:info];
        
        if(success)
        {
            NSLog(@"data inserted");
        }
    }
    
    [info release];
    
    appDelegate.shouldAllowEditing = NO;
    
}

-(void)addPolicy:(UIButton *)sender
{
    
    Policy *policy = [[Policy alloc] init];
    int index = 1;
    
    
    if ([self.dataArray count] > 0) {
        
        index = [[[self.dataArray lastObject] pIndex] intValue] + 1;
    }
    
    [policy setPIndex:[NSNumber numberWithInt:index]];
    [self.dataArray addObject:policy];
    [self createDataFields:YES];
    [self layoutScrollView];
    
}


-(void)layoutScrollView
{
    
    yVal = fixedFormHeight;
    CGFloat extraVal = 10;
    CGRect frame;
    
    for (int i =0; i<[self.dataArray count]; i++)
    {
        Policy *policy = [self.dataArray objectAtIndex:i];
        int tag = [policy.pIndex intValue] * POLICY_VIEW_TAG;
        
        UIView *view_policy = [self.scrollview viewWithTag:tag];
        
        if (view_policy) {
            
            frame = [view_policy frame];
            frame.origin.y = yVal;
            view_policy.frame = frame;
            
            yVal = yVal + frame.size.height + extraVal;
            
        }
        
        int lblTag = tag + POLICY_RELEVENT_INDEX_TAG;
        UILabel *view_lbl = (UILabel *)[self.scrollview viewWithTag:lblTag];
        [view_lbl setText:[NSString stringWithFormat:@"%d",(i+1)]];
        
        lblTag = tag + POLICY_RELEVENT_INDEX_TAG;
        view_lbl = (UILabel *)[self.scrollview viewWithTag:lblTag];
        [view_lbl setText:[NSString stringWithFormat:@"%d",(i+1)]];
        
    }
    
    
    frame = [addButton frame];
    frame.origin.y = yVal;
    addButton.frame = frame;
    
    frame = [additionalPlicy frame];
    frame.origin.y = yVal+5;
    additionalPlicy.frame = frame;
    
    yVal = yVal + frame.size.height + extraVal;
    
    frame = [editButton frame];
    frame.origin.y = yVal;
    editButton.frame = frame;
    
    frame = [saveButton frame];
    frame.origin.y = yVal;
    saveButton.frame = frame;
    
    yVal = yVal + frame.size.height + extraVal;
    
    [self.scrollview setContentSize:CGSizeMake(280, yVal)];
    yValue=yVal;
    
    
    
}

#pragma mark-
#pragma mark- PolicyView Delegate methods-

-(void)deleteView:(Policy *)policy
{
    
    if (policy) {
        [self.dataArray removeObject:policy];
        [self createDataFields:YES];
        [self layoutScrollView];
    }
    
}

-(void)updatePolicy:(Policy *)policy
{
    
    if (policy) {
        
        int index = [policy.pIndex intValue];
        index = index - 1;
        
        if (index < 0) {
            index = 0;
        }
        
        if ([self.dataArray count] > index)
        {
            [self.dataArray replaceObjectAtIndex:index withObject:policy];
        }
    }
    
}

#pragma mark-
#pragma mark TextField Delegate Methods-
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.inputAccessoryView = numberToolbar;
    
    if (textField.tag == 10001) {
        [self moveScrolUP:50];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self dismissKeyboard];
    return [textField resignFirstResponder];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag >2) {
        if (textField.tag % 2 == 0) {
            [self.dict setObject:textField.text forKey:POLICYNAME];
            [self.array addObject:textField.text];
        }
        else{
            [self.dict setObject:textField.text forKey:POLICYNUMBER];
        }
        
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self makeBigHightOfScrollView];
    
    if (appDelegate.shouldAllowEditing) {
        return YES;
    }
    return NO;
}
-(void) makeBigHightOfScrollView
{
    [self.scrollview setContentSize:CGSizeMake(280, yValue+170)];
}

-(void)dealloc
{
    [super dealloc];
    [array release];
    [dict release];
    [tagsArray release];
    [labeltagArray release];
}
@end
