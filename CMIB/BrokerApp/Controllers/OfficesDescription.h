//
//  OfficesDescription.h
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "GAITrackedViewController.h"

@class AppDelegate;
@class OfficeInfo;

@interface OfficesDescription : GAITrackedViewController<UIWebViewDelegate , UIAlertViewDelegate , UITableViewDataSource, UITableViewDelegate>
{
    OfficeInfo * officeInfo ;
    AppDelegate * appdDelegate;
    UIScrollView *scrollInfo;
    OfficeInfo *myOfficeInfo;
     NSString * urlString;
    NSString * telephoneNumber;
    NSMutableArray *imageArray;
    
    UITableView * tableView;
    NSMutableArray * tableHeadingArray;
    NSMutableArray * tablecontentArray;
    
    NSString* streetAddress;
    NSString * postalAdrress;
}
@property (nonatomic, retain) NSMutableArray * imageArray;
@property (nonatomic, retain) NSString* streetAddress;
@property (nonatomic, retain) NSString * postalAdrress;
@property (nonatomic, retain) NSMutableArray * tableHeadingArray;
@property (nonatomic, retain) NSMutableArray * tablecontentArray;
@property (nonatomic, retain) UITableView * tableView;
@property (nonatomic, retain) NSString * telephoneNumber;
@property (nonatomic, retain) NSString * urlString;
@property (nonatomic, retain) OfficeInfo * officeInfo  ;
@property (nonatomic, retain) UIScrollView *scrollInfo;


-(void)displayOfficeInfoWithArray:(OfficeInfo *)info;
-(void)initailizeNavigationBar;
//-(void)processdata;
//-(void)generateOfficeButtinswithIndex:(NSInteger)index;
-(void)callBack;
//-(void)moveToDescription:(UIButton* )sender;
-(void)showViewMap;
-(void)openUrl:(UITapGestureRecognizer *) gr;
@end
