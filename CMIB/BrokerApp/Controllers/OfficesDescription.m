//
//  OfficesDescription.m
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
#import "OfficesViewController.h"
#import "Constants.h"
#import "ASIHTTPRequest.h"
#import "XMLReader.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"
#import "OfficesDescription.h"
#import "AllStaffList.h"
#import <QuartzCore/QuartzCore.h>
#define OFFICE_TEXT_COLOR [UIColor colorWithRed:61.0/255.0f green:61.0/255.0f blue:61.0/255.0f alpha:1.0]

@interface OfficesDescription ()

@end

@implementation OfficesDescription
@synthesize officeInfo,tablecontentArray;
@synthesize scrollInfo,imageArray;
@synthesize urlString,tableHeadingArray;
@synthesize telephoneNumber;
@synthesize tableView;
//@synthesize headerArray;
//@synthesize contentArray;
@synthesize streetAddress;
@synthesize postalAdrress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:CLAIM_BUTTON_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    appdDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     [self.view setBackgroundColor:[UIColor clearColor]];
    self.title = @"Offices & Info";
    self.screenName = @"Offices & Info";
    
    
    [self initailizeNavigationBar];
   self.imageArray    = [[NSMutableArray alloc]init];
//    self.addressString = [[NSString alloc]init];
       self.tablecontentArray = [[NSMutableArray alloc]init];
    self.tableHeadingArray = [[NSMutableArray alloc]init];
    
    if (officeInfo) {
        if (officeInfo.openingHours && ![officeInfo.openingHours isEqualToString:@""]) {
            
            
            [self.tableHeadingArray addObject:@"Office Hours:"];
            [self.tablecontentArray addObject:officeInfo.openingHours];
            [self.imageArray   addObject:@"clockIcon.png"];
            
        }

        if (officeInfo.OfficePhone && ![officeInfo.OfficePhone isEqualToString:@""]) {
            
            
            [self.tableHeadingArray addObject:@"Phone:"];
            [self.tablecontentArray addObject:officeInfo.OfficePhone];
            [self.imageArray   addObject:@"phonePic.png"];
            
        }
       
        if (officeInfo.tollFreeNumber && ![officeInfo.tollFreeNumber isEqualToString:@""]) {
            
            
            [self.tableHeadingArray addObject:@"Toll Free:"];
            [self.tablecontentArray addObject:officeInfo.tollFreeNumber];
            [self.imageArray   addObject:@"phonePic.png"];
            
        }

        
        if (officeInfo.address_1 && ![officeInfo.address_1 isEqualToString:@""]) {
            self.streetAddress = officeInfo.address_1;
            
        }
        if (officeInfo.addess_2 && ![officeInfo.addess_2 isEqualToString:@""]) {
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,officeInfo.addess_2];
        }
        if (officeInfo.suburb && ![officeInfo.suburb isEqualToString:@""]) {
            
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,officeInfo.suburb];
        }
        if (officeInfo.state && ![officeInfo.state isEqualToString:@""]) {  // 12 june
            
            NSString * string = officeInfo.state;
            if (officeInfo.postCode && ![officeInfo.postCode isEqualToString:@""])
            {
                string = [NSString stringWithFormat:@"%@ %@", string, officeInfo.postCode];
            }
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,string];
            
        }
        
        if (officeInfo.postalAddress_1 && ![officeInfo.postalAddress_1 isEqualToString:@""]) {
            self.postalAdrress = officeInfo.postalAddress_1;
        }
        
        if (officeInfo.postalAddress_2 && ![officeInfo.postalAddress_2 isEqualToString:@""]) {
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",self.postalAdrress,officeInfo.postalAddress_2];
        }
        
        if (officeInfo.postalAddressSuburb && ![officeInfo.postalAddressSuburb isEqualToString:@""]) {
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",self.postalAdrress,officeInfo.postalAddressSuburb];
        }
        
        if (officeInfo.postalAddressState && ![officeInfo.postalAddressState isEqualToString:@""]) {
            
            NSString * str = officeInfo.postalAddressState;
            if (officeInfo.postalAddressPostCode && ![officeInfo.postalAddressPostCode isEqualToString:@""])
            {
                str = [NSString stringWithFormat:@"%@ %@", officeInfo.postalAddressState, officeInfo.postalAddressPostCode];
            }
            
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",postalAdrress,str];
            
        }
        
        
        if (self.streetAddress && ![self.streetAddress isEqualToString:@""]) {
            
            [self.tableHeadingArray addObject:@""];
            [self.tablecontentArray addObject:self.streetAddress];
            [self.imageArray   addObject:@"locationPic.png"];
            
        }
        if (self.postalAdrress && ![self.postalAdrress isEqualToString:@""]) {
            
            [self.tableHeadingArray addObject:@""];
            [self.tablecontentArray addObject:self.postalAdrress];
            //[self.imageArray   addObject:@"locationPic.png"];
            
        }
        
    }
    
    [self displayOfficeInfoWithArray:officeInfo];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    //[self displayOfficeInfoWithArray:officeInfo];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark shoe result method

-(void)displayOfficeInfoWithArray:(OfficeInfo *)info
{
    self.scrollInfo = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 0, 290, 360)];
    
    self.scrollInfo.contentSize = CGSizeMake(280, 20);
    
    if([Utility isIphone_5])
    {
        self.scrollInfo.frame = CGRectMake(20, 20, 290, 480);
    }
    
    self.scrollInfo.scrollEnabled =YES;
    [self.scrollInfo setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollInfo];
    
    
    CGFloat yValue = 10;
    CGFloat width = 280;
    CGFloat height = 15;
    // CGFloat lineSpace = 20;
    //CGFloat litleSpace = 3.0;
    CGFloat largeSpace = 8.0;
    CGFloat xValue = 0.0;
    // CGFloat lineHeight = 2.0;
    // CGFloat tableHeight = 350;
    CGRect frame;
    frame= CGRectMake(16, 18+60-35-10-20, 20, 20);
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:frame];  //CGRectMake(5,10, 17, 22)];
    [imgView setImage:[UIImage imageNamed:@"OfficeProfileHomeIcon.png"]];
    
    UILabel * locationLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+40, yValue+60-35-10-20, width+10, height+7+10)];
    [locationLabel setBackgroundColor:[UIColor clearColor]];
    locationLabel.text = info.location;
    [locationLabel setFont:[UIFont boldSystemFontOfSize:18.0f]];
    UILabel * offficeLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue+40, yValue+60-35+20+2-6-20, width-10, height)];
    [offficeLable setBackgroundColor:[UIColor clearColor]];
    //[offficeLable setTextColor:[UIColor blackColor]];
     offficeLable.text = @"Office";
    offficeLable.font=[offficeLable.font fontWithSize:12];
     // [offficeLable setFont:[UIFont boldSystemFontOfSize:12.0f]];
    //[offficeLable.text setFont: [UIFont systemFontOfSize:11]];
    //[offficeLable setFont:[UIFont systemFontSize:12.0f]];
    //[locationLabel setTextColor:[UIColor colorWithRed:46.0/255.0f green:90.0/255.0f blue:173.0/255.0f alpha:1.0]];
    [locationLabel setTextColor:[UIColor blackColor]];
  //  [locationLabel setTextColor:[UIColor colorWithRed:29.0/255.0f green:66.0/255.0f blue:111.0/255.0f alpha:1.0]];
    [self.scrollInfo addSubview:locationLabel];
    [self.scrollInfo addSubview:offficeLable];
    [self.scrollInfo addSubview:imgView];
    [locationLabel release];
    
    yValue = yValue + height+7-20;
    height = 12;
    
    int ht = [self.tablecontentArray count]* 60;
    if ([self.streetAddress length]>0) {
        ht = ht+20+10;
    }
    if ([self.postalAdrress length]>0) {
        ht = ht+20+10;
    }
    
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(1, yValue+40, 280, ht-150) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setBounces:NO];
    self.tableView.separatorStyle = NO;
    [self.tableView setScrollEnabled:NO];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.backgroundView = [[[UIView alloc] initWithFrame:self.tableView.bounds] autorelease];
    self.tableView.backgroundView.backgroundColor = [UIColor clearColor];
//    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    self.tableView.separatorInset = UIEdgeInsetsZero;
//    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.layer.borderColor = [UIColor blackColor].CGColor;
    self.tableView.layer.borderWidth = 1.0f;
    
    [self.scrollInfo addSubview:self.tableView];
    
    if ([Utility isIphone_5]) {
        self.tableView.frame = CGRectMake(1, yValue+40, 280, ht-150);
    }
    
    yValue = yValue+ht+largeSpace;
    
    
    self.urlString = [NSString stringWithFormat:@"%@", info.googleMapUrl];
    self.urlString = [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    UIButton * linkButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    [linkButton setFrame:CGRectMake(xValue, yValue-70, width+6, 44)];
    [linkButton setBackgroundImage:[UIImage imageNamed:MAP_VIEW_IMAGE] forState:UIControlStateNormal];  //link button image
    [linkButton addTarget:self  action:@selector(showViewMap) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollInfo addSubview:linkButton];
    
    UILabel * label  = [[UILabel alloc]initWithFrame:CGRectMake(xValue+40, yValue-80, 200, 44)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:13]];
    //[label setText:@"View Location on Google Maps"];
    [self.scrollInfo addSubview:label];
    [label release];
    
    
    
    // yValue = yValue + fontSize.height + largeSpace;
    yValue = yValue + 44 + largeSpace;
    [self.scrollInfo setContentSize:CGSizeMake(width, yValue)];
    
    UIButton *staffBtn = [[UIButton alloc] initWithFrame:CGRectMake(xValue, yValue-70, width+6, 44)];
    //[staffBtn setBackgroundImage:[UIImage imageNamed:@"staffinfo.png"] forState:UIControlStateNormal];
    [staffBtn setBackgroundImage:[UIImage imageNamed:@"Buckinghamshire Staff Button.png"] forState:UIControlStateNormal];
    [staffBtn addTarget:self action:@selector(viewStaff) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollInfo addSubview:staffBtn];
    [staffBtn release];
    UILabel *ll = [[UILabel alloc] initWithFrame:CGRectMake(xValue+52, yValue-70+4, 200, 35)];
    ll.text = [NSString stringWithFormat:@"%@ Staff",info.location];
    appdDelegate.allkey = NO;
    if ([info.location isEqualToString:@"Melbourne"])
    {
        
        appdDelegate.checklocation = @"Melbourne";
    }
   else if ([info.location isEqualToString:@"Sydney"])
    {
        appdDelegate.checklocation = @"Sydney";
    }
    else if ([info.location isEqualToString:@"Brisbane"])
    {
        appdDelegate.checklocation = @"Brisbane";
    }
    else
    {
        appdDelegate.allkey = YES;
    }

  
    
    
    [ll setFont:[UIFont systemFontOfSize:14]];
    [ll setBackgroundColor:[UIColor clearColor]];
    [ll setTextColor:[UIColor whiteColor]];
    [self.scrollInfo addSubview:ll];

       yValue = yValue + 44 + largeSpace;
    [self.scrollInfo setContentSize:CGSizeMake(width, yValue+20)];
    
}


-(void)viewStaff
{
    // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"In Progress" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //  [alert show];
    AllStaffList * staffList = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
    //staffList.locationSelected = myOfficeInfo.location;
    staffList.type=16;
    
    staffList.isStaffDetail = YES;
    [self.navigationController pushViewController:staffList animated:YES];
    [staffList release];
    
}


#pragma mark-
#pragma mark tableView Delegate;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [self.tableHeadingArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView1  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if(indexPath.row==2)
        NSLog(@"");
    
    static NSString *CellIdentifier = @"Cell";
    
    UILabel * headerLabel;
    
    UILabel * textLabel = nil;
    UITapGestureRecognizer * gesture;
    UIImageView *imgView;
    
    UITableViewCell *cell  = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
        
        
        if([self.imageArray count]>indexPath.row)
        {
            
            
            CGRect frame;
            if(indexPath.row ==0)
            {
                frame= CGRectMake(8, 12, 15, 15);
            }
            else if(indexPath.row  ==1)
            {
                frame= CGRectMake(8, 12, 12, 12);
            }
            else if(indexPath.row ==2)
            {
                frame= CGRectMake(8, 12, 11, 15);
            }
            else if(indexPath.row == 3)
            {
                frame= CGRectMake(8, 12, 11, 15);
            }
            
            if([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Website:"])
                frame= CGRectMake(8, 12, 15, 15);
            
            imgView = [[UIImageView alloc]initWithFrame:frame];//CGRectMake(20,10, 11, 15)];
            [imgView setImage:[UIImage imageNamed:[self.imageArray objectAtIndex:indexPath.row]]];
            [cell.contentView addSubview:imgView];
        }
        
        
        
        headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 12, 280, 15)];
        if(indexPath.row==0)
            headerLabel.frame =CGRectMake(30, 12, 80, 15);
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:[UIFont systemFontOfSize:12]];
        [headerLabel setText:[self.tableHeadingArray objectAtIndex:indexPath.row]];
        //[headerLabel setTextColor:[UIColor lightGrayColor]];
        
        [headerLabel setTextColor:OFFICE_TEXT_COLOR];
        
        [cell.contentView addSubview:headerLabel];
        [headerLabel release];
        textLabel.tag = 1121;
        
        if ([self.streetAddress length] > 0)
        {
            if  (indexPath.row<([self.tablecontentArray count] - ([self.tablecontentArray count]-1))){
                
                textLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 26, 200, 15)]; //(45, 12, 200, 15)
                if(indexPath.row==0)
                    textLabel.frame =CGRectMake(105 , 12, 200, 15);
                if (indexPath.row>0) {
                    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                    [textLabel addGestureRecognizer:gesture];
                    [gesture release];
                }
                
                
                [textLabel setBackgroundColor:[UIColor clearColor]];  //clearColor
                [textLabel setUserInteractionEnabled:YES];
                [textLabel setFont:[UIFont systemFontOfSize:12]];
                [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
                [textLabel setTextColor:OFFICE_TEXT_COLOR];
                
                [cell.contentView addSubview:textLabel];
                [textLabel release];
                
                
            }
            else
            {
                
                
                if([[self.tableHeadingArray objectAtIndex:indexPath.row] length]>0)
                {
                    textLabel = [[UILabel alloc]initWithFrame:CGRectMake(105, 11, 200, 15)];
                    if ([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Phone:"])
                    {
                        gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                        [textLabel addGestureRecognizer:gesture];
                        [gesture release];
                    }
                    else if ([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Toll Free:"])
                    {
                        gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                        textLabel.tag = 115;
                        [textLabel addGestureRecognizer:gesture];
                        [gesture release];
                        
                    }
                    
                    [textLabel setBackgroundColor:[UIColor clearColor]];  //clearColor
                    [textLabel setUserInteractionEnabled:YES];
                    [textLabel setFont:[UIFont systemFontOfSize:12]];
                    [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
    
                    [textLabel setTextColor:OFFICE_TEXT_COLOR];
                    
                    [cell.contentView addSubview:textLabel];
                    [textLabel release];
                }
                else
                {
                    UITextView * textLabel = [[UITextView alloc]initWithFrame:CGRectMake(23, 5, 250, 70)];
                    [textLabel setBackgroundColor:[UIColor clearColor]];
                    [textLabel setScrollEnabled:NO];
                    [textLabel setFont:[UIFont systemFontOfSize:12]];
                    [textLabel setUserInteractionEnabled:NO];
                    [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
                    [cell.contentView addSubview:textLabel];
                
                    [textLabel setTextColor:OFFICE_TEXT_COLOR];
                    [textLabel release];
                }
            }
            
        }
        else{
            textLabel = [[UILabel alloc]initWithFrame:CGRectMake(75, 12, 200, 15)];
            if (indexPath.row>0) {
                gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                [textLabel addGestureRecognizer:gesture];
                [gesture release];
            }
            
            [textLabel setBackgroundColor:[UIColor clearColor]];
            [textLabel setUserInteractionEnabled:YES];
            [textLabel setFont:[UIFont systemFontOfSize:12]];
            [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
            [textLabel setTextColor:OFFICE_TEXT_COLOR];
            
            [cell.contentView addSubview:textLabel];
            [textLabel release];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //CGSize fontSize;
    
    /*
    if ([self.streetAddress length]>0) {
        fontSize = [self.streetAddress sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        if (indexPath.row == ([self.tableHeadingArray count]-2)) {
            return (20+fontSize.height+20);
        }
        
    }
    if ([self.postalAdrress length]>0) {
        fontSize = [self.postalAdrress sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        if (indexPath.row == ([self.tableHeadingArray count]-1)) {
            return (20+fontSize.height);
        }
        
    }
    
     */
    if([tableHeadingArray count]==4)
    {
        if(indexPath.row==0)
            return 20;
        else if (indexPath.row==1)
            return 20;
        else if (indexPath.row==2)
            return 22;
        else if (indexPath.row==3)
            return 53;
        
    }
    else{
        
        if(indexPath.row==0)
            return 20;
        else if (indexPath.row==1)
            return 20;
        else if (indexPath.row==2)
            return 22;
        else if (indexPath.row==3)
            return 60;
        else if (indexPath.row==4)
            return 40;
        
    }
    
    
    return 30;

    
}




-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)showViewMap
{
    self.urlString = [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL * url = [NSURL URLWithString:self.urlString];
    [[UIApplication sharedApplication]openURL:url];
}

#pragma mark-
#pragma mark UIWebViewDelegate methods-

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    if ([absoluteStr hasSuffix:@"Claims"])
    {
        self.urlString = [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:self.urlString];
        [[UIApplication sharedApplication]openURL:url];
    }
    
    return YES;
    
}

#pragma mark AlertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSString * str = [NSString stringWithFormat:@"tel://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
    }
}


- (void)openUrl:(UITapGestureRecognizer *) gr
{
    UIGestureRecognizer *rec = (UIGestureRecognizer *)gr;
    
    id hitLabel = [self.view hitTest:[rec locationInView:self.view] withEvent:UIEventTypeTouches];
    
    if ([hitLabel isKindOfClass:[UILabel class]]) {
        self.telephoneNumber = ((UILabel *)hitLabel).text;
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:self.telephoneNumber message:@"Would you like to call this number?" delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
        
        [alert show];
        [alert release];
        
    }
}

-(UIView *)drawLine:(UILabel *)label
{
    CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:label.frame.size lineBreakMode:UILineBreakModeWordWrap];
    
    UIView *viewUnderline=[[[UIView alloc] init] autorelease];
    
    
    viewUnderline.frame = CGRectMake(label.frame.origin.x, (label.frame.origin.y + label.frame.size.height)-3, expectedLabelSize.width, 1);
    viewUnderline.backgroundColor=[UIColor blueColor];
    
    return viewUnderline;
}


-(void)dealloc
{
    [super dealloc];
}

@end
