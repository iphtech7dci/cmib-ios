//
//  OfficesViewController.h
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "GAITrackedViewController.h"


@class AppDelegate , OfficeInfo;

@interface OfficesViewController : GAITrackedViewController<UIWebViewDelegate , UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate * appDelegate;
    NSInteger buttonHeightFromTop;
    UIScrollView * scrollView;
    NSMutableArray * officeArray;
    UIScrollView * scrollInfo ;
    
    NSString * urlString;
    NSString * telephoneNumber;
    UITableView * tableView;
    NSMutableArray * tableHeadingArray;
    NSMutableArray * tablecontentArray;
    
    NSString* streetAddress;
    NSString * postalAdrress;
    
    
    NSMutableArray *imageArray;
    OfficeInfo * officeInfo;
    OfficeInfo *myOfficeInfo;
    
    
    
}


@property(nonatomic, retain) NSMutableArray *imageArray;
@property (nonatomic, retain) OfficeInfo * officeInfo;

@property (nonatomic, retain) NSString* streetAddress;
@property (nonatomic, retain) NSString * postalAdrress;
@property (nonatomic, retain) NSMutableArray * tableHeadingArray;
@property (nonatomic, retain) NSMutableArray * tablecontentArray;
@property (nonatomic, retain) UITableView * tableView;
@property (nonatomic, retain) NSString * telephoneNumber;
@property (nonatomic, retain) NSString * urlString;
@property (nonatomic, retain) UIView * loadingView;
@property (nonatomic, retain) NSMutableArray * officeArray;
@property (nonatomic, retain) UIScrollView * scrollView;
@property (nonatomic, retain) UIScrollView * scrollInfo;


-(void)displayOfficeInfoWithArray:(OfficeInfo *)info;
-(void)initailizeNavigationBar;
-(void)processdata;
-(void)generateOfficeButtinswithIndex:(NSInteger)index;
-(void)callBack;
-(void)moveToDescription:(UIButton* )sender;
-(void)showViewMap;
-(void)openUrl:(UITapGestureRecognizer *) gr;


@end
