//
//  OfficesViewController.m
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import "OfficesViewController.h"
#import "Constants.h"
#import "ASIHTTPRequest.h"
#import "XMLReader.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"
#import "OfficesDescription.h"
#import "AllStaffList.h"
#import <QuartzCore/QuartzCore.h>

#define OFFICE_TEXT_COLOR [UIColor colorWithRed:61.0/255.0f green:61.0/255.0f blue:61.0/255.0f alpha:1.0]



@interface OfficesViewController ()

@end

@implementation OfficesViewController


@synthesize scrollView;
@synthesize officeArray;
@synthesize loadingView;
@synthesize scrollInfo;
@synthesize urlString;
@synthesize telephoneNumber;
@synthesize tableView;
@synthesize tablecontentArray;
@synthesize tableHeadingArray;
@synthesize streetAddress;
@synthesize postalAdrress;
@synthesize imageArray;
@synthesize officeInfo;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];

        
    }
    return self;
}

-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
       
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:CLAIM_BUTTON_IMAGE] ;
   
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    

     [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    
    [self initailizeNavigationBar];
    
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Offices & Info";
    self.screenName = @"Offices & Info";
    appDelegate      = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
    self.officeArray = [[NSMutableArray alloc] initWithArray:[appDelegate.appInfoDict objectForKey:OFFICE_INFO_KEY]];
    
    
    
    self.tablecontentArray = [[NSMutableArray alloc]init];
    self.tableHeadingArray = [[NSMutableArray alloc]init];
    self.imageArray        = [[NSMutableArray alloc]init];
    
    if (self.officeArray && [self.officeArray count] > 0) {
        //;
        OfficeInfo * info = [self.officeArray objectAtIndex:0];
        
        officeInfo = info;
        
       // info.tollFreeNumber = @"www.mrib.com.au";      //for testing Vish
        
        
        if (info.openingHours && ![info.openingHours isEqualToString:@""]) {
            
            // gourav 28 Oct
            [self.tableHeadingArray addObject:@"Office Hours:"];
            [self.tablecontentArray addObject:info.openingHours];
            [self.imageArray addObject:@"clockIcon.png"];
            
        }
        if (info.OfficePhone && ![info.OfficePhone isEqualToString:@""]) {
            
//            [self.tableHeadingArray addObject:@"Office Phone"];
//            [self.tablecontentArray addObject:info.OfficePhone];
            
            
            [self.tableHeadingArray addObject:@"Phone:     "];
            [self.tablecontentArray addObject:[NSString stringWithFormat:@"%@",info.OfficePhone]];
            [self.imageArray addObject:PHONE_ICON_IMAGE];
            
            
        }
        if (info.tollFreeNumber && ![info.tollFreeNumber isEqualToString:@""])
        {
            
            [self.tableHeadingArray addObject:@"Website:"]; //@"Toll Free"  **Change according new reqerment*
            [self.tablecontentArray addObject:info.tollFreeNumber];
            
//            [self.tableHeadingArray addObject:@"Fax: "];
//            [self.tablecontentArray addObject:[NSString stringWithFormat:@"%@",info.tollFreeNumber]];
            [self.imageArray addObject:@"websiteIcon.png"];
            
            
        }
        
      
         /*
        if (info.address_1 && ![info.address_1 isEqualToString:@""]) {
            self.streetAddress = info.address_1;
        }
        if (info.addess_2 && ![info.addess_2 isEqualToString:@""]) {
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,info.addess_2];
        }
        if (info.suburb && ![info.suburb isEqualToString:@""]) {
            
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,info.suburb];
        }
        if (info.state && ![info.state isEqualToString:@""]) {  // 12 june
            
            NSString * string = info.state;
            if (info.postCode && ![info.postCode isEqualToString:@""])
            {
                string = [NSString stringWithFormat:@"%@ %@", string, info.postCode];
            }
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,string];
            
        }
        
        if (info.postalAddress_1 && ![info.postalAddress_1 isEqualToString:@""]) {
            self.postalAdrress = info.postalAddress_1;
        }
        
        if (info.postalAddress_2 && ![info.postalAddress_2 isEqualToString:@""]) {
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",self.postalAdrress,info.postalAddress_2];
        }
        
        if (info.postalAddressSuburb && ![info.postalAddressSuburb isEqualToString:@""]) {
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",self.postalAdrress,info.postalAddressSuburb];
        }
        
        if (info.postalAddressState && ![info.postalAddressState isEqualToString:@""]) {
            
            NSString * str = info.postalAddressState;
            if (info.postalAddressPostCode && ![info.postalAddressPostCode isEqualToString:@""])
            {
                str = [NSString stringWithFormat:@"%@ %@", info.postalAddressState, info.postalAddressPostCode];
            }
            
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",postalAdrress,str];
            
        }
        
        
        if (self.streetAddress && ![self.streetAddress isEqualToString:@""]) {
            
            [self.tableHeadingArray addObject:@"Street Address"];
            [self.tablecontentArray addObject:self.streetAddress];
            
            
        }
        if (self.postalAdrress && ![self.postalAdrress isEqualToString:@""]) {
            
            [self.tableHeadingArray addObject:@"Postal Address"];
            [self.tablecontentArray addObject:self.postalAdrress];
            
            
        }
         
        */
        
        

        if (officeInfo.address_1 && ![officeInfo.address_1 isEqualToString:@""]) {
            self.streetAddress = officeInfo.address_1;
        }
        if (officeInfo.addess_2 && ![officeInfo.addess_2 isEqualToString:@""]) {
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,officeInfo.addess_2];
        }
        if (officeInfo.suburb && ![officeInfo.suburb isEqualToString:@""]) {
            
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,officeInfo.suburb];
        }
        if (officeInfo.state && ![officeInfo.state isEqualToString:@""]) {  // 12 june
            
            NSString * string = officeInfo.state;
            if (officeInfo.postCode && ![officeInfo.postCode isEqualToString:@""])
            {
                string = [NSString stringWithFormat:@"%@ %@", string, officeInfo.postCode];
            }
            self.streetAddress = [NSString stringWithFormat:@"%@\n%@",self.streetAddress,string];
            
        }
        
        if (officeInfo.postalAddress_1 && ![officeInfo.postalAddress_1 isEqualToString:@""]) {
            self.postalAdrress = officeInfo.postalAddress_1;
        }
        
        if (officeInfo.postalAddress_2 && ![officeInfo.postalAddress_2 isEqualToString:@""]) {
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",self.postalAdrress,officeInfo.postalAddress_2];
        }
        
        if (officeInfo.postalAddressSuburb && ![officeInfo.postalAddressSuburb isEqualToString:@""]) {
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",self.postalAdrress,officeInfo.postalAddressSuburb];
        }
        
        if (officeInfo.postalAddressState && ![officeInfo.postalAddressState isEqualToString:@""]) {
            
            NSString * str = officeInfo.postalAddressState;
            if (officeInfo.postalAddressPostCode && ![officeInfo.postalAddressPostCode isEqualToString:@""])
            {
                str = [NSString stringWithFormat:@"%@ %@", officeInfo.postalAddressState, officeInfo.postalAddressPostCode];
            }
            
            self.postalAdrress = [NSString stringWithFormat:@"%@\n%@",postalAdrress,str];
            
        }
        
        
        if (self.streetAddress && ![self.streetAddress isEqualToString:@""]) {
            
            [self.tableHeadingArray addObject:@""];
            [self.tablecontentArray addObject:self.streetAddress];
            [self.imageArray addObject:LOCATION_ICON_IMAGE];
            
            
        }
        if (self.postalAdrress && ![self.postalAdrress isEqualToString:@""]) {
            
            [self.tableHeadingArray addObject:@""];
            [self.tablecontentArray addObject:self.postalAdrress];
            
            
        }
        
        // [self displayOfficeInfoWithArray:info];
        
    }
    
   // [self.view setBackgroundColor:[UIColor colorWithRed:235.0/255.0f green:226.0/255.0f blue:233.0/255.0f alpha:1.0]];
   // [self displayOfficeInfoWithArray:officeInfo];  // officeInfo

    
    
    [self processdata];
    [self.view setBackgroundColor:[UIColor colorWithRed:235.0/255.0f green:226.0/255.0f blue:233.0/255.0f alpha:1.0]];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
     [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    
   /*
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleBlackTranslucent]; // UIStatusBarStyleLightContent
        UIImageView *statusBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        [statusBarView setBackgroundColor:[UIColor colorWithRed:231.0f/255.0f green:72.0f/255.0f blue:27.0f/255.0f alpha:1.0f]];
        [statusBarView setImage:[UIImage imageNamed:STAUS_BAR_RED]];
        [appDelegate.window addSubview:statusBarView];
    }
    */
    
}

-(void)processdata
{
    
    buttonHeightFromTop = 0;
    
    if ([self.officeArray count]>1) {
        
        self.scrollView = [[UIScrollView alloc]init];
        self.scrollView.frame = CGRectMake(20, 20, 280, 350);
        self.scrollView.contentSize = CGSizeMake(250, 10);
   
        self.scrollView.scrollEnabled =YES;
        [self.scrollView setShowsHorizontalScrollIndicator:NO];
        
        if([Utility isIphone_5])
        {
            self.scrollView.frame = CGRectMake(20, 20, 280, 440);
        }
        
        [self.view addSubview:self.scrollView];
    
        for (int i=0; i<[self.officeArray count]; i++) {
            
            [self generateOfficeButtinswithIndex:i];
            buttonHeightFromTop = buttonHeightFromTop+44+5;
            self.scrollView.contentSize = CGSizeMake(280, buttonHeightFromTop);
        }
    }
    else
    {
        if ([self.officeArray count]>0) {
            
            OfficeInfo * info = [self.officeArray objectAtIndex:0];
            
            if(info)
            {
                [self displayOfficeInfoWithArray:info];
                
            }
        }
        
    }
}

-(void)generateOfficeButtinswithIndex:(NSInteger)index
{
    CGRect officeInfoButtonFrame = CGRectMake(0, buttonHeightFromTop, 280, 44);
    CGRect buttonTitle = CGRectMake(10, buttonHeightFromTop+10, 260, 20);
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
     button.tag = index;
    UIImage * directNoImage = [UIImage imageNamed:OFFICEINFOTITLE_BUTTON];
    [button setBackgroundImage:directNoImage forState:UIControlStateNormal];
    [button setFrame:officeInfoButtonFrame];
    
    [button addTarget:self action:@selector(moveToDescription:) forControlEvents:UIControlEventTouchUpInside];
   
    UILabel * textLabel = [[UILabel alloc]initWithFrame:buttonTitle];
    textLabel.backgroundColor = [UIColor clearColor];
   
    OfficeInfo * info = [self.officeArray objectAtIndex:index];
    textLabel.text = info.location;
    AllStaffList * staffList = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
    staffList.locationSelected = info.location;
    textLabel.textColor = [UIColor whiteColor];
   // NSLog(@"text = %@", info.location);
    
    [self.scrollView addSubview:button];
    [self.scrollView addSubview:textLabel];
  
    
 }
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



#pragma mark-
#pragma mark display office info

-(void)displayOfficeInfoWithArray:(OfficeInfo *)info
{
    myOfficeInfo = info;
    
    self.scrollInfo = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 0, 290, 420)];
    
    self.scrollInfo.contentSize = CGSizeMake(250, 10);
    
    if([Utility isIphone_5])
    {
        self.scrollInfo.frame = CGRectMake(20, 20, 290, 440);
    }
    
    self.scrollInfo.scrollEnabled =YES;
    [self.scrollInfo setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollInfo];
    
    
    CGFloat yValue = 20;
    CGFloat width  = 280;
    CGFloat height = 15;
    // CGFloat lineSpace = 20;
    //CGFloat litleSpace = 3.0;
    CGFloat largeSpace = 8.0;
    CGFloat xValue     = 0.0;
    // CGFloat lineHeight = 2.0;
    // CGFloat tableHeight = 350;
    
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(20, yValue-10, 22, 24)];
    [imgView setImage:[UIImage imageNamed:OFFICE_PROFILE_IMAGE]];
    [self.scrollInfo addSubview:imgView];
    
    UILabel * locationLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+50, yValue - 10, width, height+7)];
    [locationLabel setBackgroundColor:[UIColor clearColor]];
    locationLabel.text = info.location;
    [locationLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
   //[locationLabel setTextColor:[UIColor colorWithRed:38.0/255.0f green:92.0/255.0f blue:124.0/255.0f alpha:1.0]];
    [locationLabel setTextColor:BACKGROUND_COLOR_PINK];
    
    [self.scrollInfo addSubview:locationLabel];
    [locationLabel release];
    
    yValue = yValue + height+7;
    
    
    
    UILabel * officeLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+50, yValue-15, width, height+7)];
    [officeLabel setBackgroundColor:[UIColor clearColor]];
    officeLabel.text = @"Office";
    [officeLabel setFont:[UIFont boldSystemFontOfSize:12.0f]];
    //[locationLabel setTextColor:[UIColor colorWithRed:38.0/255.0f green:92.0/255.0f blue:124.0/255.0f alpha:1.0]];
    [officeLabel setTextColor:[UIColor darkGrayColor]];
    
    [self.scrollInfo addSubview:officeLabel];
    [officeLabel release];
    
    yValue = yValue + height+7;
    
    height = 12;
    
    int ht = ([self.tablecontentArray count]-1)* 50;
//    if ([self.streetAddress length]>0) {
//        ht = ht+20;
//    }
//    if ([self.postalAdrress length]>0) {
//        ht = ht+20;
//    }
    
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, yValue, 280, ht - 5) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setBounces:NO];
    [self.tableView setScrollEnabled:NO];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.backgroundView = [[[UIView alloc] initWithFrame:self.tableView.bounds] autorelease];
    self.tableView.backgroundView.backgroundColor = [UIColor whiteColor]; //whiteColor
    
//    self.tableView.layer.borderColor = BACKGROUND_COLOR_PINK.CGColor;
//    self.tableView.layer.borderWidth = 1.0f;
//    
    [self.scrollInfo addSubview:self.tableView];
    
    if ([Utility isIphone_5]) {
        self.tableView.frame = CGRectMake(0, yValue, 280, ht - 5);
    }
    
    yValue = yValue+ht+largeSpace;
    
    self.urlString = [NSString stringWithFormat:@"%@", info.googleMapUrl];
    self.urlString = [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if (info.googleMapUrl && ![info.googleMapUrl isEqualToString:@""]) {
    UIButton * linkButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
   
    [linkButton setFrame:CGRectMake(xValue, yValue, width, 44)];
    [linkButton setBackgroundImage:[UIImage imageNamed:MAP_VIEW_IMAGE] forState:UIControlStateNormal];  //link button image
    [linkButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];

    [linkButton addTarget:self  action:@selector(showViewMap) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollInfo addSubview:linkButton];
   // [linkButton release];
        
//    UILabel * label  = [[UILabel alloc]initWithFrame:CGRectMake(xValue+52, yValue+90, 200, 44)];
//    [label setBackgroundColor:[UIColor clearColor]];
//    [label setFont:[UIFont systemFontOfSize:16]];
//    [label setText:@"View office on Google Map"];
//    [label setTextColor:[UIColor whiteColor]];
//    [self.scrollInfo addSubview:label];
//    [label release];
    
    }
    
    if(info.location && ![info.location isEqualToString:@""])
    {
    yValue = yValue+largeSpace;
    
    UIButton *staffBtn = [[UIButton alloc] initWithFrame:CGRectMake(xValue, yValue-8, width, 44)];
    [staffBtn setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateNormal];
     //   [staffBtn setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState: UIControlStateHighlighted];

    [staffBtn addTarget:self action:@selector(viewStaff) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollInfo addSubview:staffBtn];
    [staffBtn release];
    
    UILabel *ll = [[UILabel alloc] initWithFrame:CGRectMake(xValue+52, yValue-3, width-65, 30)];
    ll.text = [NSString stringWithFormat:@"%@ Staff",info.location];
    [ll setFont:[UIFont systemFontOfSize:14]];
    [ll setBackgroundColor:[UIColor clearColor]];
    [ll setTextColor:[UIColor whiteColor]];
    [self.scrollInfo addSubview:ll];
    // yValue = yValue + fontSize.height + largeSpace;
    yValue = yValue + 44 + largeSpace;
    [self.scrollInfo setContentSize:CGSizeMake(width, yValue+20)];


        
        
    }
    
    
    
    
    
    
    
    
     /*   CGSize fontSize = [self.urlString sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(width, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
    
   
    UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectMake(xValue-6, yValue, width, height+50)];
    [webview setDelegate:self];
    [[webview scrollView] setScrollEnabled:NO];
    
  //  NSString *htmlString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body {font-family:\"Helvetica\"; font-size:11; color:black;}a:link {COLOR: Blue;}</style></head><body style=\"margin:15\";><b><a href=\"View Map\">View Map</a></b></body></html>"];
    
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body {font-family:\"Helvetica\"; font-size:11; color:black;}a:link {font-family:\"Helvetica\"; font-size:11; color: Blue;}</style></head><body><b><a href=\"View\">View Map</a></b></body></html>"];
    
    [webview loadHTMLString:htmlString baseURL:nil];
    
    [self.scrollInfo addSubview:webview];
    [webview release];
    */
    
  /*  UITextView * urlText = [[UITextView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width+10, fontSize.height)];
    [urlText setEditable:NO];
    [urlText setScrollEnabled:NO];
    [urlText setDataDetectorTypes:UIDataDetectorTypeLink];
    [urlText setBackgroundColor:[UIColor clearColor]];
    [urlText setFont:[UIFont systemFontOfSize:12]];
    [urlText setText:self.urlString];
    [self.scrollInfo addSubview:urlText];
    
    */
    
    
   // yValue = yValue + fontSize.height + largeSpace;
    yValue = yValue + 44 + largeSpace;
    [self.scrollInfo setContentSize:CGSizeMake(width, yValue+50)];
    
}



-(void)viewStaff
{
    // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"In Progress" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //  [alert show];
    AllStaffList * staffList = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
    staffList.locationSelected = myOfficeInfo.location;
    staffList.type=16;
    
    staffList.isStaffDetail = YES;
    [self.navigationController pushViewController:staffList animated:YES];
    [staffList release];
    
}


#pragma mark-
#pragma mark tableView Delegate;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableHeadingArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView1  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if(indexPath.row==2)
        NSLog(@"");
    
    static NSString *CellIdentifier = @"Cell";
    
    UILabel * headerLabel;
    
    UILabel * textLabel = nil;
    UITapGestureRecognizer * gesture;
    UIImageView *imgView;
    
    UITableViewCell *cell  = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
        
        
        if([self.imageArray count]>indexPath.row)
        {
            
            
            CGRect frame;
            if(indexPath.row ==0)
            {
                frame= CGRectMake(8, 13, 15, 15);
            }
            else if(indexPath.row  ==1)
            {
                frame= CGRectMake(8, 12, 12, 12);
            }
            else if(indexPath.row ==2)
            {
                frame= CGRectMake(8, 12, 11, 15);
            }
            else if(indexPath.row == 3)
            {
                frame= CGRectMake(8, 12, 11, 15);
            }

            if([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Website:"])
                frame= CGRectMake(8, 12, 15, 15);
                
            imgView = [[UIImageView alloc]initWithFrame:frame];//CGRectMake(20,10, 11, 15)];
            [imgView setImage:[UIImage imageNamed:[self.imageArray objectAtIndex:indexPath.row]]];
            [cell.contentView addSubview:imgView];
        }

        
        
        headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 12, 280, 15)];
        if(indexPath.row==0)
            headerLabel.frame =CGRectMake(30, 12, 70, 15);
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:[UIFont systemFontOfSize:11]];
        [headerLabel setText:[self.tableHeadingArray objectAtIndex:indexPath.row]];
        //[headerLabel setTextColor:[UIColor lightGrayColor]];
        
        [headerLabel setTextColor:OFFICE_TEXT_COLOR];
        
        [cell.contentView addSubview:headerLabel];
        [headerLabel release];
        textLabel.tag = 1121;
        
        if ([self.streetAddress length] > 0)
        {
            if  (indexPath.row<([self.tablecontentArray count] - ([self.tablecontentArray count]-1))){
                
                textLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 26, 200, 15)]; //(45, 12, 200, 15)
                if(indexPath.row==0)
                    textLabel.frame =CGRectMake(105 , 12, 200, 15);
                if (indexPath.row>0) {
                    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                    [textLabel addGestureRecognizer:gesture];
                    [gesture release];
                }
                
                
                [textLabel setBackgroundColor:[UIColor clearColor]];  //clearColor
                [textLabel setUserInteractionEnabled:YES];
                [textLabel setFont:[UIFont systemFontOfSize:11]];
                [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
                [textLabel setTextColor:OFFICE_TEXT_COLOR];
                [cell.contentView addSubview:textLabel];
                [textLabel release];
                
                
            }
            else{
                
                
                if([[self.tableHeadingArray objectAtIndex:indexPath.row] length]>0)
                {
                    textLabel = [[UILabel alloc]initWithFrame:CGRectMake(105, 12, 200, 15)];
                    if ([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Phone:"])
                    {
                        gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                        [textLabel addGestureRecognizer:gesture];
                        [gesture release];
                    }
                    else if ([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Website:"])
                    {
                        gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                        textLabel.tag = 1125;
                        [textLabel addGestureRecognizer:gesture];
                        [gesture release];

                    }
                
                    [textLabel setBackgroundColor:[UIColor clearColor]];  //clearColor
                    [textLabel setUserInteractionEnabled:YES];
                    [textLabel setFont:[UIFont systemFontOfSize:11]];
                    [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
                    [textLabel setTextColor:OFFICE_TEXT_COLOR];
                    [cell.contentView addSubview:textLabel];
                    [textLabel release];
                }

                else
                {
                    UITextView * textLabel = [[UITextView alloc]initWithFrame:CGRectMake(23, 5, 250, 70)];
                    [textLabel setBackgroundColor:[UIColor clearColor]];
                    [textLabel setScrollEnabled:NO];
                    [textLabel setFont:[UIFont systemFontOfSize:11]];
                    [textLabel setUserInteractionEnabled:NO];
                    [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
                    [cell.contentView addSubview:textLabel];
                    [textLabel setTextColor:OFFICE_TEXT_COLOR];
                    [textLabel release];
                }
            }
            
        }
        else{
            textLabel = [[UILabel alloc]initWithFrame:CGRectMake(75, 30, 200, 15)];
            if (indexPath.row>0) {
                gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                [textLabel addGestureRecognizer:gesture];
                [gesture release];
            }
            
            [textLabel setBackgroundColor:[UIColor clearColor]];
            [textLabel setUserInteractionEnabled:YES];
            [textLabel setFont:[UIFont systemFontOfSize:11]];
            [textLabel setText:[self.tablecontentArray objectAtIndex:indexPath.row]];
            [textLabel setTextColor:OFFICE_TEXT_COLOR];
            [cell.contentView addSubview:textLabel];
            [textLabel release];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize fontSize;
    
    if ([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Phone:"])
    {
        return 25;
    }
    else if ([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Website:"]){
        return 25;
    }
    else if ([[self.tableHeadingArray objectAtIndex:indexPath.row] hasPrefix:@"Office Hours"]){
        return 25;
    }
   if(![[self.tableHeadingArray objectAtIndex:indexPath.row] length]>0)
   {
      
       
    if ([self.streetAddress length]>0) {
        fontSize = [self.streetAddress sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        if (indexPath.row == ([self.tableHeadingArray count]-1)) {
            return (40+fontSize.height);
        }
        
    }
    if ([self.postalAdrress length]>0) {
        fontSize = [self.postalAdrress sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        if (indexPath.row == ([self.tableHeadingArray count]-1)) {
            return (40+fontSize.height);
        }
       
        
    }
   }
    return 40;
   
    
    /*
    CGSize fontSize;
    if ([self.streetAddress length]>0) {
        fontSize = [self.streetAddress sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        if (indexPath.row == ([self.tableHeadingArray count]-2)) {
            return (20+fontSize.height);
        }
        
    }
    if ([self.postalAdrress length]>0) {
        fontSize = [self.postalAdrress sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        if (indexPath.row == ([self.tableHeadingArray count]-1)) {
            return (20+fontSize.height);
        }
        
    }
    
    return 60;
     */
    
}


#pragma mark-
#pragma mark IBActions-

-(void)callBack
{
    //self.tabBarController.selectedIndex = 0;
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)moveToDescription:(UIButton* )sender
{
    NSLog(@"123");
    int index = sender.tag;
    
    if ([self.officeArray count] > index)
    {
         OfficeInfo * info = [self.officeArray objectAtIndex:index];
        
        if (info) {
            OfficesDescription * desc =[[OfficesDescription alloc]initWithNibName:@"OfficesDescription" bundle:nil];
            desc.officeInfo = info;
            [self.navigationController pushViewController:desc animated:YES];
            [desc release];
        }
        
    }
}


-(void)showViewMap
{
    self.urlString = [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL * url = [NSURL URLWithString:self.urlString];
    [[UIApplication sharedApplication]openURL:url];
}
#pragma mark-
#pragma mark UIWebViewDelegate methods-

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    if ([absoluteStr hasSuffix:@"View"])
    {
        self.urlString = [self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:self.urlString];
        [[UIApplication sharedApplication]openURL:url];
    }
    
    return YES;
    
}

#pragma mark AlertView Delegate method


- (void)openUrl:(UITapGestureRecognizer *) gr
{
    UIGestureRecognizer *rec = (UIGestureRecognizer *)gr;
    
    id hitLabel = [self.view hitTest:[rec locationInView:self.view] withEvent:UIEventTypeTouches];
    
    if ([hitLabel isKindOfClass:[UILabel class]]) {
        self.telephoneNumber = ((UILabel *)hitLabel).text;
        
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:self.telephoneNumber message:@"Would you like to call this number?" delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
        
        [alert show];
        [alert release];
        
        
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSString * str = [NSString stringWithFormat:@"tel://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
    }
}-(UIView *)drawLine:(UILabel *)label
{
    CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:label.frame.size lineBreakMode:UILineBreakModeWordWrap];
    
    UIView *viewUnderline=[[[UIView alloc] init] autorelease];
    
    
    viewUnderline.frame = CGRectMake(label.frame.origin.x, (label.frame.origin.y + label.frame.size.height)-3, expectedLabelSize.width, 1);
    viewUnderline.backgroundColor=[UIColor blueColor];
    
    return viewUnderline;
}

@end
