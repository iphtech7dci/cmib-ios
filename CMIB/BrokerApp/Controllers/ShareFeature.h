//
//  ShareFeature.h
//  BrokerApp
//
//  Created by iPHTech2 on 27/05/13.
//
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import  <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "Facebook.h"
#import "FBConnect.h"
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "GAITrackedViewController.h"




@interface ShareFeature : GAITrackedViewController <FBSessionDelegate, FBRequestDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate, UIAlertViewDelegate>
{
    Facebook * facebook;
    AppDelegate *appdelegate;
}

@property (nonatomic, retain) Facebook * facebook;
-(void)initailizeNavigationBar;
-(void)setShareButtons;
-(void)callBack;
-(void)doTextShare: (UIButton *)button;
-(void)doEmailShare;
-(void)doFacebookShare;
-(void)doTwitterShare;


@end
