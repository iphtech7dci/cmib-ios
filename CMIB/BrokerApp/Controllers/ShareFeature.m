//
//  ShareFeature.m
//  BrokerApp
//
//  Created by iPHTech2 on 27/05/13.
//
//

#import "ShareFeature.h"
#import "Constants.h"
#import "Utility.h"

@interface ShareFeature ()

@end

//////vikas

NSString *appId = @"1422061578096161"; // 1532445120323425

@implementation ShareFeature
////Vikas
@synthesize facebook;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    //////Vikas
    self.facebook = [[Facebook alloc]initWithAppId:appId];
    //float currentVersion = 6.0;
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appdelegate.navController = self.navigationController;
  
   UIImageView * bgimageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    bgimageView.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    if ([Utility isIphone_5]) {
        bgimageView.frame = CGRectMake(0, 0, 320, 568);
    }
    [self.view addSubview:bgimageView];
    [bgimageView release];
    
    
    
    [self initailizeNavigationBar];
    [self setShareButtons];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

   self.title = @"Share via";
    self.screenName = @"Share via";
    self.navigationController.navigationBar.frame = CGRectMake(0, 20, 320, 44);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    
}
-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    //[button setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
    
    //call target
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(280, 0, 30, 30)];
    photoImage.image = [UIImage imageNamed:EXPORT_BUTTON_IMAGE];
    [button addTarget:self action:@selector(doExport) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];

    
    
}

-(void)setShareButtons
{
    CGRect textShareFrame = CGRectMake(20, 20, 280, 44);
    CGRect EmailShareFrame = CGRectMake(20, 68+4, 280, 44);
    CGRect facebookShareFrame = CGRectMake(20, 116+8, 280, 44);
    CGRect twitterShareFrame = CGRectMake(20, 164+12, 280, 44);
    
    UIButton *textShareButton = [[UIButton alloc] initWithFrame:textShareFrame];
    [textShareButton setBackgroundImage:[UIImage imageNamed:SHARETEXT_IMAGE] forState:UIControlStateNormal];
    [textShareButton setBackgroundImage:[UIImage imageNamed:@"shareEmailBtnImage.png"] forState:UIControlStateHighlighted];

    [textShareButton addTarget:self action:@selector(doTextShare:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *emailShareButton = [[UIButton alloc] initWithFrame:EmailShareFrame];
    [emailShareButton setBackgroundImage:[UIImage imageNamed:SHAREEMAIL_IMAGE] forState:UIControlStateNormal];
    [emailShareButton setBackgroundImage:[UIImage imageNamed:@"shareEmailBtnImage.png"] forState:UIControlStateHighlighted];
    [emailShareButton addTarget:self action:@selector(doEmailShare) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *facebookShareButton = [[UIButton alloc] initWithFrame:facebookShareFrame];
    [facebookShareButton setBackgroundImage:[UIImage imageNamed:SHAREFACEBOOK_IMAGE] forState:UIControlStateNormal];
    [facebookShareButton setBackgroundImage:[UIImage imageNamed:@"shareEmailBtnImage.png"] forState:UIControlStateHighlighted];

    [facebookShareButton addTarget:self action:@selector(doFacebookShare) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *twitterShareButton = [[UIButton alloc] initWithFrame:twitterShareFrame];
    [twitterShareButton setBackgroundImage:[UIImage imageNamed:SHARETWITTER_IMAGE] forState:UIControlStateNormal];
    [twitterShareButton setBackgroundImage:[UIImage imageNamed:@"shareEmailBtnImage.png"] forState:UIControlStateHighlighted];

    [twitterShareButton addTarget:self action:@selector(doTwitterShare) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:textShareButton];
    [self.view addSubview:emailShareButton];
    [self.view addSubview:facebookShareButton];
    [self.view addSubview:twitterShareButton];
    
    [textShareButton release];
    [emailShareButton release];
    [facebookShareButton release];
    [twitterShareButton release];
    
    
    
    //setlabels
    
    CGRect textLabelFrame = CGRectMake(70, 33, 100, 17);
    CGRect emailLabelFrame = CGRectMake(70, 81+4, 100, 17);
    CGRect facebookLabelFrame = CGRectMake(70, 129+8, 100, 17);
    CGRect twitterLabelFrame = CGRectMake(70, 177+12, 100, 17);
    
    
    UILabel * textLabel = [[UILabel alloc]initWithFrame:textLabelFrame];
    [textLabel setBackgroundColor:[UIColor clearColor]];
    textLabel.text = @"Text";
    [textLabel setTextColor:[UIColor whiteColor]];
    
    UILabel * emailLabel = [[UILabel alloc]initWithFrame:emailLabelFrame];
    [emailLabel setBackgroundColor:[UIColor clearColor]];
    emailLabel.text = @"Email";
    [emailLabel setTextColor:[UIColor whiteColor]];
    
    UILabel * facebookLabel = [[UILabel alloc]initWithFrame:facebookLabelFrame];
    [facebookLabel setBackgroundColor:[UIColor clearColor]];
    facebookLabel.text = @"Facebook";
   [facebookLabel setTextColor:[UIColor whiteColor]];
    
    
    UILabel * twitterLabel = [[UILabel alloc]initWithFrame:twitterLabelFrame];
    [twitterLabel setBackgroundColor:[UIColor clearColor]];
    twitterLabel.text = @"Twitter";
    [twitterLabel setTextColor:[UIColor whiteColor]];
    
    
    [self.view addSubview:textLabel];
    [self.view addSubview:emailLabel];
    [self.view addSubview:facebookLabel];
    [self.view addSubview:twitterLabel];
    
    [textLabel release];
    [emailLabel release];
    [facebookLabel release];
    [twitterLabel release];
    
    
    
    
    
    
    
    
}
#pragma mark
#pragma mark IBActions-

-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doExport{
    
}

-(void)doTextShare: (UIButton *)button
{
    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
  
    
    if([MFMessageComposeViewController canSendText])
    {
        NSString * link = [NSString stringWithFormat:@"%@ iTunes - %@ \nGoogle Play - %@",SHARE_SUBJECT , SHARE_LINK , SHARE_ANDROID_LINK];// [NSString stringWithFormat:@"%@ \n %@",SHARE_SUBJECT, SHARE_LINK];
        controller.body = link;
        
         //controller.recipients = @"";
        //[controller.navigationBar setTintColor:[UIColor colorWithRed:20.0/255.0f green:16.0/255.0f blue:117.0/255.0f alpha:1.0]];
        [[controller navigationBar] setTintColor:[UIColor whiteColor]];
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }

}

-(void)doEmailShare
{
    if ([MFMailComposeViewController canSendMail])
    {
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSString * str = [NSString stringWithFormat:@"%@ iTunes - <a href=\"%@\">%@</a><br>Google Play - <a href=\"%@\">%@</a>",SHARE_SUBJECT , SHARE_LINK , SHARE_LINK , SHARE_ANDROID_LINK,SHARE_ANDROID_LINK];//[NSString stringWithFormat:@"<a href=\"%@\">%@</a>",SHARE_LINK, SHARE_SUBJECT];
    
    [picker setSubject:@"CMIB Insurance Brokerapp"];
    //[picker.navigationBar setTintColor:[UIColor colorWithRed:20.0/255.0f green:16.0/255.0f blue:117.0/255.0f alpha:1.0]];
    [[picker navigationBar] setTintColor:[UIColor whiteColor]];
    
     
    NSString *emailBody = str;
    [picker setMessageBody:emailBody isHTML:YES];
    //[self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:nil];
    
    [picker release];
   
    }

}

-(void)doFacebookShare
{
    appdelegate.isFaceBookSharing = YES;
    
    float currentVersion = 6.0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion)
    {
        appdelegate.isFaceBookSharing = NO;
        //if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            
        {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
           // NSString * link = [NSString stringWithFormat:@"%@ \n %@",SHARE_SUBJECT, SHARE_LINK];

            NSString *shareText = [NSString stringWithFormat:@"%@ iTunes - %@ \nGoogle Play - %@",SHARE_SUBJECT , SHARE_LINK , SHARE_ANDROID_LINK];
            [controller setInitialText:shareText];
           // NSURL * url = [NSURL URLWithString:SHARE_LINK];
          //  [controller addURL:url];

            [self presentViewController:controller animated:YES completion:Nil];
        }
    }else
    {
        //[indicator setHidden:NO];
        NSArray* permissions = [[NSArray alloc] initWithObjects:@"publish_stream", nil];
        [self.facebook authorize:permissions delegate:self];
        
        [permissions release];
    }

    
    
}
-(void)doTwitterShare
{
    float currentVersion = 6.0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= currentVersion)
    {
        //if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            NSString * link = [NSString stringWithFormat:@"%@\niTunes:%@ Google Play:%@",SHARE_SUBJECT_TWITTER , SHARE_LINK , SHARE_ANDROID_LINK];//[NSString stringWithFormat:@"%@ \n %@",SHARE_SUBJECT, SHARE_LINK];
            [tweetSheet setInitialText:link];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
 
    }
    else
    {
        TWTweetComposeViewController *vc =[[[TWTweetComposeViewController alloc] init] autorelease];
        
        vc =[[TWTweetComposeViewController alloc] init];
        
         NSString * share =[NSString stringWithFormat:@"%@ iTunes - %@ \nGoogle Play - %@",SHARE_SUBJECT , SHARE_LINK , SHARE_ANDROID_LINK]; //[NSString stringWithFormat:@"%@ \n %@", SHARE_SUBJECT, SHARE_LINK];
        [vc setInitialText:share];
     //   [vc addURL:[NSURL URLWithString:SHARE_LINK]];
        
        //Show tweet sheet
        [self presentModalViewController:vc animated:YES];
        
        
    }
    

}

///// Vikas

#pragma mark-
#pragma mark Facebook Delefate Method



- (void)fbDidLogin
{
    NSLog(@"login");
    
    
    NSString *iTuneslink = SHARE_LINK;
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   appId, @"app_id",
                                   iTuneslink, @"link",
                                   // @"http://iphmusic.com/icon@2x.png", @"picture",
								   SHARE_SUBJECT, @"name",
								   @"Make Life Easier", @"caption",
                                   @"", @"description",
								   @"", @"actions",nil];

    
    
	[self.facebook requestWithGraphPath:@"me/feed" andParams:params andHttpMethod:@"POST" andDelegate:self];
    
}


-(void)fbDidNotLogin:(BOOL)cancelled {
	NSLog(@"did not login");
}

- (void)request:(FBRequest *)request didLoad:(id)result {
	if ([result isKindOfClass:[NSArray class]]) {
		result = [result objectAtIndex:0];
	}
	NSLog(@"Result of API call: %@", result);
    
   appdelegate.isFaceBookSharing = NO;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Message has been published successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show]
    ;
    [alert release];
    
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Failed with error: %@", [error localizedDescription]);
    NSLog(@"Failed with error: %@", [error description]);
    NSLog(@"Failed with error: %@", [error userInfo]);
    
   // [indicator setHidden:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"There is limited or no internet connectivity. Please try later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show]
    ;
    [alert release];
    
}

-(void)requestLoading:(FBRequest *)request
{
    
    
    
}
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response
{
    
    
    
}

#pragma mark-
#pragma mark Mail and Message delegate method

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Result: saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Result: sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Result: failed");
            break;
        default:
            NSLog(@"Result: not sent");
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
    
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissModalViewControllerAnimated:YES];
    
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
    else if (result == MessageComposeResultSent)
        NSLog(@"SMS sent");
    else
        NSLog(@"Message failed");
}



@end
