//
//  DataHandle.h
//  BrokerApp
//
//  Created by iPHTech2 on 12/06/13.
//
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ProfileInfo.h"

@class AppDelegate;

@interface DataHandle : NSObject

{
    sqlite3 *database;
    AppDelegate * appDelegate;
}

- (id)initWithDbConnection:(sqlite3 *)dbConnection;

-(BOOL)InsertBrokerData:(ProfileInfo *)info;
-(BOOL)deleteInformation;
-(ProfileInfo * )getData;
@end
