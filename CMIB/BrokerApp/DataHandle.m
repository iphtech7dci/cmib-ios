//
//  DataHandle.m
//  BrokerApp
//
//  Created by iPHTech2 on 12/06/13.


#import "DataHandle.h"
#import "AppDelegate.h"
#import "Utility.h"

@implementation DataHandle

- (id)initWithDbConnection:(sqlite3 *)dbConnection
{
	if ((self = [super init]))
	{
		database = dbConnection ;
	}
	return self;
}

-(BOOL)InsertBrokerData:(ProfileInfo *)info
{
   sqlite3_stmt *inserData_stmt = nil;
    
    if (inserData_stmt == nil)
    {
        
        NSString * str = @"INSERT into BrokerInfo(MyBroker,BrokerOffice, Insurer, Policies) VALUES (?,?,?,?)";
        
        const char *sql = [str UTF8String];
        
        if (sqlite3_prepare_v2(database, sql, -1, &inserData_stmt, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return NO;
		}
    }
    
    sqlite3_bind_text(inserData_stmt,1,[info.myBroker UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(inserData_stmt,2,[info.brokerOffice UTF8String], -1,SQLITE_TRANSIENT);
	sqlite3_bind_text(inserData_stmt,3,[info.releventInsurer UTF8String] , -1,SQLITE_TRANSIENT);
    sqlite3_bind_blob(inserData_stmt, 4,[info.policyData bytes], [info.policyData length], SQLITE_TRANSIENT);
    
    int success = sqlite3_step(inserData_stmt);
	
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to update project into the database with message '%s'.", sqlite3_errmsg(database));
		return NO;
	}
	sqlite3_reset(inserData_stmt);
	return YES;
}

-(BOOL)deleteInformation
{
    sqlite3_stmt * delete_stmt = nil;
    if (delete_stmt == nil) {
        NSString * str = @"DELETE from BrokerInfo";
        const char *sql = [str UTF8String];
        
        if (sqlite3_prepare_v2(database, sql, -1, &delete_stmt, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return NO;
		}
    }
    int success = sqlite3_step(delete_stmt);
	
	if (success == SQLITE_ERROR) {
		NSAssert1(0, @"Error: failed to update project into the database with message '%s'.", sqlite3_errmsg(database));
		return NO;
	}
	sqlite3_reset(delete_stmt);
	return YES;
}

-(ProfileInfo * )getData
{
    sqlite3_stmt * getData_stmt = nil;
    
    
    if(getData_stmt == nil)
    {
        NSString * str = @"Select * from BrokerInfo";
        const char *sql = [str UTF8String];
        
        
        if (sqlite3_prepare_v2(database, sql, -1, &getData_stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(getData_stmt) == SQLITE_ROW)
            {
                ProfileInfo * info = [[[ProfileInfo alloc]init]autorelease];
                info.myBroker = [Utility stringFromCString:(char *) sqlite3_column_text(getData_stmt, 0)];
               info.brokerOffice =  [Utility stringFromCString:(char *) sqlite3_column_text(getData_stmt, 1)];
                info.releventInsurer = [Utility stringFromCString:(char *) sqlite3_column_text(getData_stmt, 2)];
                NSData *data = [[NSData alloc] initWithBytes:sqlite3_column_blob(getData_stmt, 3)length:sqlite3_column_bytes(getData_stmt, 3)];
                
                info.policyData = data;
                [data release];
                sqlite3_finalize(getData_stmt);
                return info;
            }
       
        }
    }
    
    return nil;
}
@end
