//
//  DirectNumbers.h
//  BrokerApp
//
//  Created by iPHTech2 on 01/06/13.
//
//

#import <UIKit/UIKit.h>
#import "OfficeInfo.h"


@class AppDelegate;

@interface DirectNumbers : UIViewController<UITableViewDataSource , UITableViewDelegate , UISearchBarDelegate, UITextFieldDelegate>

{
    OfficeInfo * offices;
    AppDelegate * appDelegate;
    UITableView * myTableView;
    NSMutableArray * contentArray;
    UISearchBar * searchBar;
    NSMutableArray * searchedStaff;
    NSMutableArray * combinedArray;
    NSMutableArray * officeArray;
    NSString *obriencheck;
    BOOL searching;
}
@property(nonatomic, retain) NSMutableArray * officeArray;
@property(nonatomic,retain)NSString *obriencheck;
@property(nonatomic, retain) NSMutableArray * combinedArray;
@property(nonatomic, retain) NSMutableArray * searchedStaff;
@property(nonatomic, retain) UISearchBar * searchBar;
@property(nonatomic, retain) NSMutableArray * contentArray;
@property(nonatomic, retain) UITableView * myTableView;

@property(nonatomic, retain) UIImageView * searchBackGroundView;
@property(nonatomic, retain) UITextField * searchTextField;


-(void)initailizeNavigationBar;
-(void)initializeBlackBar;
-(void)initailizeSearchView;
-(void) searchTableView;
-(void)callBack;
-(void)showStaffList;
-(void)doAfterHoursAction;
-(void)doShowInsuranceNumbers;
-(void)brienGlassMethod;
-(void)dealloc;



@end
