//
//  DirectNumbers.m
//  BrokerApp
//
//  Created by iPHTech2 on 01/06/13.
//
//

#import "DirectNumbers.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "AllStaffList.h"
#import "InsurerNumber.h"
#import "AfterHoursContact.h"
#import "OBrienGlassViewController.h"
#import "StaffFilteredLocations.h"

@interface DirectNumbers ()

@end

@implementation DirectNumbers
@synthesize myTableView,obriencheck;
@synthesize contentArray;
@synthesize searchBar;
@synthesize searchedStaff;
@synthesize combinedArray;
@synthesize officeArray;
@synthesize searchBackGroundView;
@synthesize searchTextField;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    }
    return self;
}

-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor clearColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"Directory";
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    button.backgroundColor=[UIColor clearColor];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
   // [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"Nav_Bar_WithOut_UnderLine.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
    
    
}

-(void)initializeBlackBar
{
    /* CGFloat xValue = 0.0;
     CGFloat yValue = 0.0;
     CGFloat width  = 320.0;
     CGFloat height = 30.0;
     
     UIImageView * blackBarImageView = [[UIImageView alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
     blackBarImageView.image = [UIImage imageNamed:BLACK_BAR_IMAGE];
     [self.view addSubview:blackBarImageView];
     [blackBarImageView release];
     
     UILabel * detalLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue+20, yValue+9, width-40, height-18)];
     [detalLabel setBackgroundColor:[UIColor clearColor]];
     [detalLabel setText:@"Details"];
     [detalLabel setFont:[UIFont boldSystemFontOfSize:14]];
     [detalLabel setTextColor:[UIColor whiteColor]];
     [self.view addSubview:detalLabel];
     [detalLabel release];
     */
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    //self.view.backgroundColor=[UIColor colorWithRed:237.0/255.0f green:237.0/255.0f blue:237.0/255.0f alpha:1.0];
    //self.view.backgroundColor=[UIColor redColor];
    [self initailizeNavigationBar];
    self.officeArray  = [[NSMutableArray alloc]init];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.officeArray = [appDelegate.appInfoDict objectForKey:OFFICE_INFO_KEY];
    self.searchedStaff = [[NSMutableArray alloc]init];
//    self.combinedArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts",nil];
//    self.contentArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts" ,nil];
    
    if ([self.obriencheck  isEqual: @"Yes"]) {
        self.combinedArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts" , @"Insurers Assist Numbers",@"O'Brien Glass",nil];
        self.contentArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts" , @"Insurers Assist Numbers",@"O'Brien Glass",nil];
    }
    else{
        self.combinedArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts" , @"Insurers Assist Numbers",nil];
        self.contentArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts" , @"Insurers Assist Numbers",nil];
        
    }
    
    
    
    //self.combinedArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts", @"O'Brien Glass",nil];
    
//    self.combinedArray = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts",@"Insurers Assist Numbers",nil];
//    self.contentArray  = [[NSMutableArray alloc]initWithObjects:@"Key Staff",@"After Hours Contacts",@"Insurers Assist Numbers",nil];
    
    
    [self initializeBlackBar];
    
    [self initailizeSearchView];
    
    
    self.myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 12, 320, self.view.frame.size.height) style:UITableViewStylePlain];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    [self.myTableView setBounces:NO];
    [self.myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.myTableView setScrollEnabled:NO];
    [self.myTableView setBackgroundColor:[UIColor clearColor]];
    self.myTableView.backgroundView = [[[UIView alloc] initWithFrame:self.myTableView.bounds] autorelease];
    self.myTableView.backgroundView.backgroundColor = [UIColor clearColor];

    //self.myTableView.tableHeaderView = self.searchBar ;
	self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.view addSubview:self.myTableView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    searching = NO;
    self.searchBar.text = nil;
    self.searchBar.showsCancelButton = NO;
    if (self.myTableView) {
        [self.myTableView reloadData];
    }
    
    
    [self setSearchBarInDefaultForm];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleBlackTranslucent]; // UIStatusBarStyleLightContent
        //UIImageView *statusBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
       // [statusBarView setBackgroundColor:[UIColor colorWithRed:231.0f/255.0f green:72.0f/255.0f blue:27.0f/255.0f alpha:1.0f]];
       // [statusBarView setImage:[UIImage imageNamed:STAUS_BAR_DARK_GREY]];
       // [appDelegate.window addSubview:statusBarView];
    }
    
    [self.navigationItem setTitle:@"Directory"];
    
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:NSLocalizedString(@"DIRECTORY", @"comment")];
    
    
}
-(void)initailizeSearchView
{
    /*
    searchBackGroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 48)];
    [searchBackGroundView setImage:[UIImage imageNamed:@"SearchViewWithText.png"]];
    [self.view addSubview:searchBackGroundView];
   
    
    searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(28, 0, 320, 48)];
    
   // searchTextField.placeholder = @"Search";
    
    searchTextField.delegate = self;
    [self.view addSubview:searchTextField];
    */
    
    
    searchBackGroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 48)];
    [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
   // [self.view addSubview:searchBackGroundView];
    
    
    searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, 320, 48)];
    searchTextField.textColor = [UIColor whiteColor];
    
    // searchTextField.placeholder = @"Search";
    
    searchTextField.delegate = self;
    [self.view addSubview:searchTextField];
    
    
//    UIImageView *redImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 48, 320, 5)];
//    [redImageView setImage:[UIImage imageNamed:@"DarkGreyImage.png"]];
//    [self.view addSubview:redImageView];

    

    
    
/*
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [self.searchBar setDelegate:self];
    [self.searchBar setPlaceholder:@"Search"];
    [self.searchBar setTintColor:[UIColor blackColor]];
    
    UIView * customView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 43, 320, 1)];
    [customView1 setBackgroundColor:[UIColor lightGrayColor]];
    [customView1 setAlpha:0.3f];
    [self.searchBar addSubview:customView1];
    */
    
    
    //[customView1 release];

    
}

#pragma mark-
#pragma mark UITextFieldDelegate Methods-
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    return YES;
//}// return NO to disallow editing.
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    //[textField resignFirstResponder];
    
    
//    if(textField.text.length==0)
//    {
//        [searchBackGroundView setImage:[UIImage imageNamed:@"SearchViewWithText.png"]];
//    }
    
    
  
    //[searchBackGroundView setImage:[UIImage imageNamed:@"SearchViewWithText.png"]];
    
    
    searchTextField.text =@"";
    searching = NO;
    myTableView.scrollEnabled = YES;
    [self.searchedStaff removeAllObjects];
    [self.myTableView reloadData];
    [textField resignFirstResponder];
    [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
    

    
    
    return YES;
}// return NO to disallow editing.

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    self.myTableView.scrollEnabled =YES;
    self.searchBar.showsCancelButton = YES;
    
    
    if(textField.text.length==0)
    {
        //[textField resignFirstResponder];
        [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite_Blank.png"]];
    }

    
    
    
}// became first responder
// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length==0 )
    {
        [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
    }
    
    
    
    [textField resignFirstResponder];
    
    if ([textField.text length] > 0 )
    {
        searching = YES;
        
        myTableView.scrollEnabled = YES;
        
    }

    
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ([textField.text length] > 0 )
    {
       // searching = YES;
//        self.myTableView.scrollEnabled = YES;
//        [self searchTableView];
//        [self.myTableView reloadData];
        
       // [self performSelector:@selector(searchingCalled) withObject:nil afterDelay:0.1f];
        
        
        if(textField.text.length != 1)
        {
            searching = YES;
            [self performSelector:@selector(searchingCalled) withObject:nil afterDelay:0.1f];
        }
        

        
    }
    else
    {
        
        if([string length] >0)
        {
            searching = YES;
            [self performSelector:@selector(searchingCalled) withObject:nil afterDelay:0.1f];
        }
        else
        {
         [self setSearchBarInDefaultForm];
        }

    }

    
    
    return YES;
}// return NO to not change text


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


-(void)searchingCalled
{
    self.myTableView.scrollEnabled = YES;
    [self searchTableView];
    [self.myTableView reloadData];
}

-(void)setSearchBarInDefaultForm
{
    searchTextField.text =@"";
    searching = NO;
    myTableView.scrollEnabled = YES;
    [self.searchedStaff removeAllObjects];
    [self.myTableView reloadData];
    [searchTextField resignFirstResponder];
    [searchBackGroundView setImage:[UIImage imageNamed:@"SearchBarWhite.png"]];
    
}


#pragma mark-
#pragma mark tableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   // if (searching) {
    //    return 1;
    //}
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (searching) {
        return [self.searchedStaff count];
    }
    else
        return [self.combinedArray count];
    
//    switch (section){
//        case 0:
//            return 4;
//            break;
//       // case 1:
//         //   return 1;
//          //  break;
//            
//        default:
//            break;
//    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell  = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    CGRect officeInfoButtonFrame = CGRectMake(20, 10, 280, 44);
    CGRect buttonTitle = CGRectMake(30, 10, 200, 44);
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.tag = indexPath.row;
    
    UIImage * directNoImage;
    directNoImage = [UIImage imageNamed:OFFICEINFOTITLE_BUTTON];
    
//    if([[self.combinedArray objectAtIndex:indexPath.row] isEqualToString:@"After Hours Contacts"])
//        directNoImage = [UIImage imageNamed:MOTOR_VEHICLE_CLAIM];

    
    [button setBackgroundImage:directNoImage forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [button setFrame:officeInfoButtonFrame];
    
    [button addTarget:self action:@selector(moveToDescription:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * textLabel = [[UILabel alloc]initWithFrame:buttonTitle];
    textLabel.backgroundColor = [UIColor clearColor];
     [textLabel setTextColor:[UIColor whiteColor]];
    
    //OfficeInfo * info = [self.officeArray objectAtIndex:index];
    //textLabel.text = info.location;
    
    textLabel.text=[self.combinedArray objectAtIndex:indexPath.row];
    
    // NSLog(@"text = %@", info.location);
    
   // [self.view addSubview:button];
    //[self.view addSubview:textLabel];
    
    
   
    
    if (searching) {
        textLabel.text = [self.searchedStaff objectAtIndex:indexPath.row];
    }
//    else{
//        if (indexPath.section == 0) {
//            textLabel.text = [self.combinedArray objectAtIndex:indexPath.row];
//        }
//        else{
//           // textLabel.text = @"O'Brien Glass";
//        }
//    }
    [cell.contentView addSubview:button];
    [cell.contentView addSubview:textLabel];
 [textLabel release];
    //cell.textLabel.text = cellText;
    return cell;
}


/*-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (searching)
    {
        NSString * text = [self.searchedStaff objectAtIndex:indexPath.row];
        
        if (text !=nil && [text isEqualToString:@"O'Brien Glass"]) {
            [self brienGlassMethod];
        }
        
        if (text !=nil && [text isEqualToString:@"Key Staff"]) {
            [self showStaffList];
        }
        
        if (text !=nil && [text isEqualToString:@"After Hours Contacts"]) {
            [self doAfterHoursAction];
        }
        
        if (text !=nil && [text isEqualToString:@"Insurers Assist Numbers"]) {
            [self doShowInsuranceNumbers];
        }
        
    }
    else
    {
        if (indexPath.section == 0 ) {
            switch (indexPath.row) {
                case 0:
                    [self showStaffList];
                    break;
                case 1:
                    [self doAfterHoursAction];
                    break;
                case 2:
                    [self doShowInsuranceNumbers];
                    
                default:
                    break;
            }
        }
        else{
            [self brienGlassMethod];
        }
    }
    
}*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52;
}

-(void)moveToDescription:(UIButton* )sender
{
    
    
    int index = sender.tag;
    
    //[self setSearchBarInDefaultForm];
    
    switch (index) {
        case 0:
            [self showStaffList];
            break;
        case 1:
            [self doAfterHoursAction];
            break;
        case 2:
            [self doShowInsuranceNumbers];
            break;
        case 3:
            [self brienGlassMethod];
            break;
         
        default:
            break;
    }
    
//    if(index==2)
//    {
//        [self brienGlassMethod];
//    }
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
#pragma mark-
#pragma mark searcgBar Delegate methods

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    
    return [self.searchBar resignFirstResponder];
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    self.myTableView.scrollEnabled =YES;
    self.searchBar.showsCancelButton = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    searching = NO;
    self.searchBar.showsCancelButton = NO;
    self.searchBar.text = nil;
    [self.searchBar resignFirstResponder];
    [self.myTableView reloadData];
    
    
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] > 0 )
    {
        searching = YES;
        self.myTableView.scrollEnabled = YES;
        [self searchTableView];
        [self.myTableView reloadData];
        
    }
    else
    {
        searching = NO;
        myTableView.scrollEnabled = YES;
        [self.searchedStaff removeAllObjects];
        [self.myTableView reloadData];
        [self.searchBar resignFirstResponder];
        
    }
    
    
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar1
{
    [self.searchBar resignFirstResponder];
    
    if ([searchBar1.text length] > 0 )
    {
        searching = YES;
        
        myTableView.scrollEnabled = YES;
        
    }
}


-(void) searchTableView
{
    
    NSString *searchText = searchTextField.text;//searchBar.text;
    
    NSMutableArray *nameArr = [[NSMutableArray alloc] init];
    
    NSMutableArray * searchAry = [[NSMutableArray alloc]init];
    
    
    
    for (int i = 0; i < [self.combinedArray count]; i++)
    {
        NSString * name  = [self.combinedArray objectAtIndex:i];
        
        [nameArr addObject:name];
        
    }
    
    for(int i = 0; i<[nameArr count]; i++)
    {
        NSString * sTemp = [nameArr objectAtIndex:i];
        NSRange titleResultRange = [sTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
        
        if (titleResultRange.length > 0)
        {
            
            
            [searchAry addObject:[self.combinedArray objectAtIndex:i]];
            
            
            
        }
        
        
    }
    
    self.searchedStaff = [[[[NSOrderedSet alloc] initWithArray:searchAry] array] mutableCopy];
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    for(id subview in [self.searchBar subviews])
    {
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview setEnabled:YES];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.searchBar resignFirstResponder];
    
    //self.title = @"DIRECTORY";
    
    
}


#pragma mark-
#pragma mark IBActions-

-(void)callBack
{
  // [self.navigationController popViewControllerAnimated:YES];
    
    if(![self.navigationController popViewControllerAnimated:YES])
                self.tabBarController.selectedIndex = 0;
    else
        [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)showStaffList
{
    if ([self.officeArray count]>1) {
        StaffFilteredLocations * list = [[StaffFilteredLocations alloc]initWithNibName:@"StaffFilteredLocations" bundle:nil];
        list.officeListArray = self.officeArray;
        [self.navigationController pushViewController:list animated:YES];
        [list release];
    }
    else{
        AllStaffList * staffList = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
        
        staffList.isStaffDetail = YES;
        [self.navigationController pushViewController:staffList animated:YES];
        [staffList release];
    }
    
}

-(void)doAfterHoursAction
{
    AfterHoursContact * contact = [[AfterHoursContact alloc]initWithNibName:@"AfterHoursContact" bundle:nil];
    [self.navigationController pushViewController:contact animated:YES];
    [contact release];
}
-(void)doShowInsuranceNumbers
{
    
    AllStaffList * list = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
    list.isStaffDetail = NO;
    [self.navigationController pushViewController:list animated:YES];
    [list release];
    
}
-(void)brienGlassMethod
{
    OBrienGlassViewController * contact = [[OBrienGlassViewController alloc]initWithNibName:@"OBrienGlassViewController" bundle:nil];
    [self.navigationController pushViewController:contact animated:YES];
    [contact release];
}

-(void)dealloc
{
    [super dealloc];
    [officeArray release];
    if (contentArray) {
        [contentArray release];
    }
    searchBar = nil;
    searchedStaff = nil;
    /*
     if (searchBar) {
     [searchBar release];
     }
     if (searchedStaff) {
     [searchedStaff release];
     }
     if (combinedArray) {
     [combinedArray release];
     }
     */
    
}

@end
