//
//  InsuranceDetailViewController.h
//  BrokerApp
//
//  Created by iPHTech2 on 07/06/13.
//
//

#import <UIKit/UIKit.h>
#import "InsuranceInfo.h"
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "GAITrackedViewController.h"


@interface InsuranceDetailViewController : GAITrackedViewController<UITextViewDelegate , UIGestureRecognizerDelegate , UIWebViewDelegate , UIAlertViewDelegate >
{
    InsuranceInfo * insuInfo;
    IBOutlet UIScrollView *scrollView;
    UITapGestureRecognizer *gestureRec;
    NSString * telephoneNumber;
    
}
@property (nonatomic, retain) NSString * telephoneNumber;
@property (nonatomic, retain) InsuranceInfo * insuInfo;

//- (void)createLine:(CGRect)rect withLabel:(UILabel *)label;
-(void)initailizeNavigationBar;
-(void)createDescriptionView;
- (void)openUrl:(UITapGestureRecognizer *) gr;
-(void)callBack;
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;


@end
