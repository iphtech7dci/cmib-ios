//
//  InsuranceDetailViewController.m
//  BrokerApp
//
//  Created by iPHTech2 on 07/06/13.
// the new description
// new changes


#import "InsuranceDetailViewController.h"
#import "Constants.h"


@interface InsuranceDetailViewController ()
@end

@implementation InsuranceDetailViewController
@synthesize insuInfo;
@synthesize telephoneNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    //Do any additional setup after loading the view from its nib.
    //    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 245, 40)];
    //    [label setBackgroundColor:[UIColor clearColor]];
    //    [label setText:@"Insurers Assist Numbers"];
    //    [label setFont:[UIFont boldSystemFontOfSize:17]];
    //    [label adjustsFontSizeToFitWidth];
    //    [label setTextColor:[UIColor whiteColor]];
    //    self.navigationItem.titleView = label;
    //    [label release];
    
    
    
    // gourav start 05 nov
    
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(30, 0, 195, 44);
    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:17];
    titleView.textAlignment = NSTextAlignmentLeft;
    titleView.textColor = [UIColor whiteColor];
    titleView.shadowColor = [UIColor darkGrayColor];
    titleView.shadowOffset = CGSizeMake(0, -1);
    titleView.text = @"Insurers Assist"; // Insurers Assist Numbers
    //titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    // gourav end 05 nov
    
    
    
    [self initailizeNavigationBar];
    
 //   self.trackedViewName = @"Insurers Assist";  // Insurers Assist Numbers
    
    [self createDescriptionView];
    //[scrollView setContentSize:CGSizeMake(300, 1050)];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
}


-(void)createDescriptionView
{
    
    
    CGFloat lineWidth = 320.0;
    CGFloat lineheight = 2.0;
    CGFloat largeSpace = 8.0;
    
    CGFloat width = 280.0;
    CGFloat xValue = 0.0;
    CGFloat yValue = 100.0;
    CGFloat height = 15;
    CGFloat fontSize = 12.0;
    
    
    /* CGSize nameSize = [insuInfo.company sizeWithFont:[UIFont boldSystemFontOfSize:25] constrainedToSize:CGSizeMake(200, 1000) lineBreakMode:NSLineBreakByCharWrapping];
     
     CGFloat totalWidth = nameSize.width + 70;
     CGFloat xSpace = (320-totalWidth)/2;
     */
    NSString* logoname = [NSString stringWithFormat:@"%@.png", insuInfo.idName];
    //NSString *logoname = @"rt.png";
    logoname = [logoname lowercaseString];
    UIImage *img = [UIImage imageNamed:logoname];
    
    
    
    
    if (img) {
        
        UIImageView * logo = [[UIImageView alloc]initWithFrame:CGRectMake(20, 18, 280, 70)];
        logo.backgroundColor=[UIColor whiteColor];
        [logo setContentMode:UIViewContentModeScaleAspectFit];
        [logo setImage:img];
        [self.view addSubview:logo];
        [logo release];
        //xSpace = xSpace + 70;
    }
    else
    {
        /*nameSize = [insuInfo.company sizeWithFont:[UIFont boldSystemFontOfSize:25] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:NSLineBreakByCharWrapping];
         xSpace = (320-nameSize.width)/2;
         */
        
        
        UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 16, 320, 70)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:25]];
        titleLabel.adjustsFontSizeToFitWidth = YES;
        [titleLabel setText:insuInfo.company];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview:titleLabel];
        [titleLabel release];
        
    }
    
    
    
    UIImageView * shadow = [[UIImageView alloc]initWithFrame:CGRectMake(xValue, yValue-10, lineWidth, lineheight)];
    [shadow setImage:[UIImage imageNamed:SHADOWIMAGE]];
    [self.view addSubview:shadow];
    
    yValue = yValue+lineheight+largeSpace;
    
    
    scrollView.frame = CGRectMake(20, 102  , 280, 450 );
    [scrollView setCanCancelContentTouches:YES];
    
    //[scrollView setBackgroundColor:[UIColor redColor]];
    
    
    yValue = 10.0;
    
    /*  if (insuInfo.company && ![insuInfo.company isEqualToString:@""]) {
     UILabel * companyHeader = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
     [companyHeader setBackgroundColor:[UIColor clearColor]];
     [companyHeader setFont:[UIFont boldSystemFontOfSize:fontSize]];
     [companyHeader setText:@"Company"];
     [scrollView addSubview:companyHeader];
     [companyHeader release];
     
     yValue = yValue+height+1;
     [scrollView setContentSize:CGSizeMake(width, yValue)];
     
     
     UILabel * companyText = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
     [companyText setBackgroundColor:[UIColor clearColor]];
     [companyText setFont:[UIFont systemFontOfSize:fontSize]];
     [companyText setText:insuInfo.company];
     [scrollView addSubview:companyText];
     [companyText release];
     
     yValue = yValue+height+largeSpace;
     [scrollView setContentSize:CGSizeMake(width, yValue)];
     }
     */
    
    gestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
    [gestureRec setDelegate:self];
    
    
    
    
    if (insuInfo.phoneNumber_1 && ![insuInfo.phoneNumber_1 isEqualToString:@""]) {
        UILabel * phone1 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [phone1 setBackgroundColor:[UIColor clearColor]];
        [phone1 setFont:[UIFont boldSystemFontOfSize:fontSize]];
        [phone1 setText:@"Claims Assist Phone Number"];
        [scrollView addSubview:phone1];
        [phone1 release];
        
        yValue = yValue+height+1;
        [scrollView setContentSize:CGSizeMake(width, yValue)];
        
        UITapGestureRecognizer * geture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
        UILabel * phoneText1 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [phoneText1 setUserInteractionEnabled:YES];
        [phoneText1 addGestureRecognizer:geture1];
        [phoneText1 setBackgroundColor:[UIColor clearColor]];
        [phoneText1 setTextColor:[UIColor blackColor]];
        [phoneText1 setFont:[UIFont systemFontOfSize:fontSize]];
        [phoneText1 setText:insuInfo.phoneNumber_1];
        
        //NSMutableAttributedString * str = [[NSMutableAttributedString alloc]initWithString:insuInfo.phoneNumber_1];
        
        //[phoneText1 setAttributedText:str];
        
        // NSMutableAttributedString *mat = [str mutableCopy];
        //[mat addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)} range:NSMakeRange (0, mat.length)];
        // phoneText1.attributedText = mat;
        
        //  UIView * view = [self drawLine:phoneText1];
        [scrollView addSubview:phoneText1];
        //[scrollView addSubview:view];
        [phoneText1 release];
        [geture1 release];
        
        
        yValue = yValue+height+largeSpace;
        [scrollView setContentSize:CGSizeMake(width, yValue)];
    }
    
    if (insuInfo.phoneNumber_2 && ![insuInfo.phoneNumber_2 isEqualToString:@""]){
        
        UILabel * phone2 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [phone2 setBackgroundColor:[UIColor clearColor]];
        [phone2 setFont:[UIFont boldSystemFontOfSize:fontSize]];
        [phone2 setText:@"If Calling From Overseas"];
        [scrollView addSubview:phone2];
        [phone2 release];
        
        yValue = yValue+height+1;
        [scrollView setContentSize:CGSizeMake(width, yValue)];
        
        
        NSString * phoneNumberString =  phoneNumberString =[NSString stringWithFormat:@"%@",insuInfo.phoneNumber_2];
        NSString * sttr = nil;
        if ([insuInfo.phoneNumber_2 length]>3){
            sttr =[insuInfo.phoneNumber_2 substringToIndex:3];
        }
        
        if ([sttr length]>2) {
            if (![sttr isEqualToString:@"+61"]){
                phoneNumberString = [NSString stringWithFormat:@"+61 %@",phoneNumberString];
            }
        }
        
        
        UITapGestureRecognizer * geture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
        UILabel * phoneText2 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [phoneText2 setBackgroundColor:[UIColor clearColor]];
        [phoneText2 setUserInteractionEnabled:YES];
        [phoneText2 addGestureRecognizer:geture2];
        [phoneText2 setFont:[UIFont systemFontOfSize:fontSize]];
        [phoneText2 setText:phoneNumberString];
        [phoneText2 setTextColor:[UIColor blackColor]];
        
        
        // NSMutableAttributedString *mat = [phoneText2.attributedText mutableCopy];
        //[mat addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)} range:NSMakeRange (0, mat.length)];
        //phoneText2.attributedText = mat;
        
        //UIView * view = [self drawLine:phoneText2];
        [scrollView addSubview:phoneText2];
        //[scrollView addSubview:view];
        [phoneText2 release];
        [geture2 release];
        
        yValue = yValue+height+largeSpace;
        [scrollView setContentSize:CGSizeMake(width, yValue)];
        
    }
    /* if (insuInfo.phoneNumber_3 && ![insuInfo.phoneNumber_3 isEqualToString:@""]) {
     UILabel * phone3 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
     [phone3 setBackgroundColor:[UIColor clearColor]];
     [phone3 setFont:[UIFont boldSystemFontOfSize:fontSize]];
     [phone3 setText:@"Phone number 3"];
     [scrollView addSubview:phone3];
     [phone3 release];
     yValue = yValue+height+1;
     [scrollView setContentSize:CGSizeMake(width, yValue)];
     UILabel * phoneText3 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
     [phoneText3 setUserInteractionEnabled:YES];
     [phoneText3 addGestureRecognizer:gestureRec];
     [phoneText3 setBackgroundColor:[UIColor clearColor]];
     [phoneText3 setFont:[UIFont systemFontOfSize:fontSize]];
     [phoneText3 setText:insuInfo.phoneNumber_3];
     [phoneText3 setTextColor:[UIColor blueColor]];
     [gestureRec release];
     //NSMutableAttributedString *mat = [phoneText3.attributedText mutableCopy];
     //[mat addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)} range:NSMakeRange (0, mat.length)];
     // phoneText3.attributedText = mat;
     UIView * view = [self drawLine:phoneText3];
     
     [scrollView addSubview:phoneText3];
     [scrollView addSubview:view];
     [phoneText3 release];
     
     
     yValue = yValue+height+largeSpace;
     [scrollView setContentSize:CGSizeMake(width, yValue)];
     
     }
     */
    
    if (insuInfo.website && ![insuInfo.website isEqualToString:@""]) {
        
        UILabel * website = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [website setBackgroundColor:[UIColor clearColor]];  // clearColor
        [website setText:@"Website"];
        [website setFont:[UIFont boldSystemFontOfSize:fontSize]];
        [scrollView addSubview:website];
        [website release];
        yValue = yValue+height+1;
        [scrollView setContentSize:CGSizeMake(width, yValue)];
        
        
        UITextView * webText = [[UITextView alloc]initWithFrame:CGRectMake(xValue-4, yValue, width, height+15)];
        [webText setBackgroundColor:[UIColor clearColor]];
        [webText setEditable:NO];
        [webText setScrollEnabled:NO];
        [webText setFont:[UIFont systemFontOfSize:fontSize]];
        [webText setText:insuInfo.website];
        [webText setDataDetectorTypes:UIDataDetectorTypeLink];
        [scrollView addSubview:webText];
        [webText release];
        
        yValue = yValue+height+largeSpace;
        [scrollView setContentSize:CGSizeMake(width, yValue)];
        
    }
    if (insuInfo.description && ![insuInfo.description isEqualToString:@""]) {
        
        NSString * description = [NSString stringWithFormat:@"<html><head><body style=\"font-family:Helvetica; font-size:12px;\">%@</body></head><html>", insuInfo.description];
        CGSize fontSize = [description sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(width, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        
        UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width, fontSize.height)];
        
        if([insuInfo.idName isEqualToString:@"AHI"])
        {
            webview.frame = CGRectMake(xValue-7, yValue, width, fontSize.height+20);
            largeSpace=largeSpace+20;
        }
        
        
        // gourav 05 nov
        
        if([insuInfo.idName isEqualToString:@"ClubMarine"])
        {
            webview.frame = CGRectMake(xValue-7, yValue, width, fontSize.height+30);
            largeSpace=largeSpace+30;
        }
        // gourav 05 nov
        
        
        
        //[webview.scrollView setBounces:NO];
        [webview setOpaque:NO];
        [webview setBackgroundColor:[UIColor clearColor]];
        [webview.scrollView setScrollEnabled:NO];
        [webview loadHTMLString:description baseURL:nil];
        [scrollView addSubview:webview];
        
        NSLog(@"%f", fontSize.height);
        [webview release];
        
        yValue = yValue+fontSize.height+largeSpace;
        [scrollView setContentSize:CGSizeMake(width, yValue)];
        
        
        // gourav 05 nov start    for preventing the more scrolling in this case
        if([insuInfo.idName isEqualToString:@"ace"])
            [scrollView setContentSize:CGSizeMake(width, yValue-500)];
        // gourav 05 end
        
    }
    
    //[self.scrollView setContentSize:CGSizeMake(width, yValue+50)];
    
    
}





- (void)openUrl:(UITapGestureRecognizer *) gr
{
    UIGestureRecognizer *rec = (UIGestureRecognizer *)gr;
    
    id hitLabel = [self.view hitTest:[rec locationInView:self.view] withEvent:UIEventTypeTouches];
    
    if ([hitLabel isKindOfClass:[UILabel class]]) {
        self.telephoneNumber = ((UILabel *)hitLabel).text;
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:self.telephoneNumber message:@"Would you like to call this number?" delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
        
        [alert show];
        [alert release];
        
    }
}



-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UILabel class]])
    {
        return YES;
    }
    
    return YES;
}

#pragma mark AlertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSString * str = [NSString stringWithFormat:@"tel://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark-
#pragma mark UIWebViewDelegate methods-

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    NSArray * array = [absoluteStr componentsSeparatedByString:@"/"];
    NSString * linkString = [array lastObject];
    
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkString]];
    
    return YES;
    
}


-(UIView *)drawLine:(UILabel *)label
{
    CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:label.frame.size lineBreakMode:UILineBreakModeWordWrap];
    
    UIView *viewUnderline=[[[UIView alloc] init] autorelease];
    
    
    viewUnderline.frame = CGRectMake(label.frame.origin.x, (label.frame.origin.y + label.frame.size.height)-3, expectedLabelSize.width, 1);
    viewUnderline.backgroundColor=[UIColor blueColor];
    
    return viewUnderline;
}


@end
