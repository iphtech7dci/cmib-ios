//
//  InsuredDetailDiscription.h
//  Optimus1
//
//  Created by iphtech7 on 22/04/14.
//
//

#import <Foundation/Foundation.h>

@interface InsuredDetailDiscription : NSObject
{
    NSString * firstName;
    NSString * surname;
    NSString * DOB;
    NSString * phone;
    NSString * email;
    NSString * address1;
    NSString * address2;
    NSString * postcode;
    
    
}

@property (strong,nonatomic)NSString * firstName;
@property (strong,nonatomic)NSString * surname;
@property (strong,nonatomic)NSString * DOB;
@property (strong,nonatomic)NSString * phone;
@property (strong,nonatomic)NSString * email;
@property (strong,nonatomic)NSString * address1;
@property (strong,nonatomic)NSString * address2;
@property (strong,nonatomic)NSString * postcode;
@end
