//
//  InsuredDetails.m
//  Optimus1
//
//  Created by iPHTech13 on 21/04/14.
//
//

#import "InsuredDetails.h"
#import "ClaimsAndPhotos.h"
#import "Constants.h"
#import "ClaimWhatToDo.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"
#import "InsuredDetailDiscription.h"

#define NAME_TEXTFIELD_TAG 201
#define EMAIL_TEXTFIELD_TAG  205
#define MOBIL_TEXTFIELD_TAG 204

#define SURNAME_TAG 202
#define ADDRESS1_TAG 206
#define ADDRESS2_TAG 207
#define DOB_TAG 203
#define POSTCODE_TAG 208




@interface InsuredDetails ()
{
    CGFloat yValue;
    int currentTxtFldTag;
    
}

@end

@implementation InsuredDetails


@synthesize line1,surnameTextField,address1TextField,address2TextField,dateOfBirthTextField,postCodeTextField;
//@synthesize line2;
//@synthesize line3;
@synthesize nametextField;
@synthesize emailTextField;
@synthesize mobileTextField;

@synthesize indecatorView;
@synthesize confirmationView;
@synthesize scrollView;
@synthesize latitude;
@synthesize longitude;

@synthesize officeArray;
@synthesize emailRecieverString;
@synthesize latiArray;
@synthesize longiArray;

@synthesize dateTimeArray;
@synthesize longitude_libPhoto;
@synthesize latitude_libPhoto;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initializeNavigationBar
{
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"Insured Details";
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(270, 0, 20, 22)];
    photoImage.image = [UIImage imageNamed:@"whiteManImg.png"];
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // self.view.backgroundColor=[UIColor redColor];
    appDelegate = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
    
    //imageCount = 0;
    shouldShowDropDownList = YES;
    checForEmptyArray = 0;
    self.latiArray = [[NSMutableArray alloc]init];
    self.longiArray = [[NSMutableArray alloc]init];
    self.dateTimeArray = [[NSMutableArray alloc]init];
    self.screenName = @"Claims & Photos";
    UIImageView * bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    if ([Utility isIphone_5]) {
        bgImage.frame = CGRectMake(0, 0, 320, 568);
    }
    bgImage.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    [self.view addSubview:bgImage];
    [bgImage release];
    
    [self initializeNavigationBar];
    [self makeForm];
    
    [self assignSavedvalue];
    
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)] ;
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
 //   numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)],[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(nextBtn)],[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:@"Previous" style:UIBarButtonItemStyleDone target:self action:@selector(prevBtn)], nil];
    
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(prevBtn)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBtn)];
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)];
    NSArray *itemArray = [[NSArray alloc] initWithObjects:item3, spaceItem, item1, item2, nil];
    numberToolbar.items = itemArray;
    
    [numberToolbar sizeToFit];
    [item1 release];
    [item2 release];
    [item3 release];
    [itemArray release];
    
    self.nametextField.inputAccessoryView = numberToolbar;
    self.surnameTextField.inputAccessoryView = numberToolbar;
    self.dateOfBirthTextField.inputAccessoryView = numberToolbar;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.timeZone = [NSTimeZone defaultTimeZone];
    datePicker.minuteInterval = 5;
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    
    [self.dateOfBirthTextField setInputView:datePicker];

    self.mobileTextField.inputAccessoryView = numberToolbar;
    self.emailTextField.inputAccessoryView = numberToolbar;
    self.address1TextField.inputAccessoryView = numberToolbar;
    self.address2TextField.inputAccessoryView = numberToolbar;
    self.postCodeTextField.inputAccessoryView = numberToolbar;

    
    self.confirmationView  = [[UIView alloc]init];
    [self.confirmationView setAlpha:0.8];
    [[self.confirmationView layer] setCornerRadius:10];
    
    self.confirmationView.frame = CGRectMake(30, 150, 260, 100);
    if ([Utility isIphone_5]) {
        self.confirmationView.frame = CGRectMake(30, 180, 260, 100);
    }
    [self.confirmationView setBackgroundColor:[UIColor darkGrayColor]];
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 160, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setNumberOfLines:2];
    [label setText:@"Your message has been sent"];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:DONE_BUTTON_IMAGE] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(105, 65, 60, 30)];
    
    [self.confirmationView addSubview:btn];
    [self.confirmationView addSubview:label];
    [self.confirmationView setHidden:YES];
    [self.view addSubview:self.confirmationView];
    [self.view bringSubviewToFront:self.indecatorView];
    
    [label release];
    
    
}
-(void) moveViewDown
{
    [UIView beginAnimations:@"ResizeForKeyboard" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x,64,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}

-(void)dismissKeyboard {
    [self moveScrollDownForTextfield];
   // [self moveViewDown];
    [self.view endEditing:YES];
}
-(void)nextBtn
{
    if(currentTxtFldTag<208)
    {
        currentTxtFldTag=currentTxtFldTag + 1;
    }

    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
    
  //  [self moveViewDown];
  //  [self.view endEditing:YES];
    
    // self.scrollview.contentOffset = CGPointMake(0,-50);
}
-(void)prevBtn
{
    if(currentTxtFldTag>201)
    {
    currentTxtFldTag=currentTxtFldTag -1;
    }
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
    
    
}
-(void) datePickerValueChanged:(id) sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    UIDatePicker *picker = (UIDatePicker*)sender;
    NSString *theDate = [dateFormat stringFromDate:picker.date];

    self.dateOfBirthTextField.text = [NSString stringWithFormat:@"%@",theDate];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    locationManager.delegate = self;
    [locationManager startUpdatingHeading];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    if (locationManager) {
        locationManager.delegate = nil;
        [locationManager stopUpdatingLocation];
    }
}
#pragma mark
#pragma mark Form make-

-(void)makeForm
{
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 370)];
    
    self.scrollView.contentSize = CGSizeMake(250, 10);
    self.scrollView.backgroundColor=[UIColor whiteColor];
    
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(0, 0, 320, 460);
    }
    
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollView];
    
     yValue = 20;
    CGFloat width = 280;
    CGFloat height = 15;
    
    CGFloat litleSpace = 5.0;
    CGFloat largeSpace = 8.0;
    CGFloat xValue = 20.0;
  
    CGFloat textFieldHeight = 30;
    
    UILabel * nameGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [nameGeadingLable setBackgroundColor:[UIColor clearColor]];
    [nameGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [nameGeadingLable setText:@"First Name"];//astha
    [scrollView addSubview:nameGeadingLable];
    [nameGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    
    
    self.nametextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.nametextField setPlaceholder:@"First Name"];//astha
    [self.nametextField setTag:NAME_TEXTFIELD_TAG];
    //self.nametextField.inputAccessoryView = numberToolbar;
    [self.nametextField.layer setCornerRadius:2.0f];
    [self.nametextField setBorderStyle:UITextBorderStyleRoundedRect];
    [self.nametextField setDelegate:self];
    self.nametextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.nametextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    
    UILabel * emailLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [emailLable setBackgroundColor:[UIColor clearColor]];
    [emailLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [emailLable setText:@"Surname"];//astha
    [scrollView addSubview:emailLable];
    [emailLable release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.surnameTextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.surnameTextField setPlaceholder:@"Surname"];///astha
    [self.surnameTextField setTag:SURNAME_TAG];
    // self.surnameTextField.inputAccessoryView = numberToolbar;
    [self.surnameTextField.layer setCornerRadius:2.0f];
    [self.surnameTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [self.surnameTextField setDelegate:self];
   // [self.surnameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    self.surnameTextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.surnameTextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * mobileLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [mobileLable setBackgroundColor:[UIColor clearColor]];
    [mobileLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [mobileLable setText:@"Date of Birth"];//astha
    [scrollView addSubview:mobileLable];
    [mobileLable release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.dateOfBirthTextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.dateOfBirthTextField setPlaceholder:@"Date of Birth"];///astha
      [self.dateOfBirthTextField.layer setCornerRadius:2.0f];
    [self.dateOfBirthTextField setBorderStyle:UITextBorderStyleRoundedRect];
      [self.dateOfBirthTextField setTag:DOB_TAG];
    [self.dateOfBirthTextField setDelegate:self];
    self.dateOfBirthTextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.dateOfBirthTextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * phoneLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [phoneLable setBackgroundColor:[UIColor clearColor]];
    [phoneLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [phoneLable setText:@"Phone"];//astha
    [scrollView addSubview:phoneLable];
    [phoneLable release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.mobileTextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.mobileTextField setPlaceholder:@"Phone"];///astha
    [self.mobileTextField.layer setCornerRadius:2.0f];
    [self.mobileTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [self.mobileTextField setKeyboardType:UIKeyboardTypeNumberPad];
    [self.mobileTextField setTag:MOBIL_TEXTFIELD_TAG];
    [self.mobileTextField setDelegate:self];
    self.mobileTextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.mobileTextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * email2Lable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [email2Lable setBackgroundColor:[UIColor clearColor]];
    [email2Lable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [email2Lable setText:@"Email"];//astha
    [scrollView addSubview:email2Lable];
    [email2Lable release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.emailTextField setPlaceholder:@"Email"];///astha
    [self.emailTextField.layer setCornerRadius:2.0f];
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.emailTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [self.emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.emailTextField setTag:EMAIL_TEXTFIELD_TAG];
    [self.emailTextField setDelegate:self];
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
 //   [self.emailTextField addTarget:self action:@selector(DidEnd:)          forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.scrollView addSubview:self.emailTextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * address1 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [address1 setBackgroundColor:[UIColor clearColor]];
    [address1 setFont:[UIFont boldSystemFontOfSize:13.0]];
    [address1 setText:@"Address 1"];//astha
    [scrollView addSubview:address1];
    [address1 release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.address1TextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.address1TextField setPlaceholder:@"Address 1"];///astha
    [self.address1TextField.layer setCornerRadius:2.0f];
    [self.address1TextField setBorderStyle:UITextBorderStyleRoundedRect];
    //[self.address1TextField setKeyboardType:UIKeyboardTypeNumberPad];
    [self.address1TextField setTag:ADDRESS1_TAG];
    [self.address1TextField setDelegate:self];
    self.address1TextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
  // [self.address1TextField addTarget:self action:@selector(DidEnd:)                  forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.scrollView addSubview:self.address1TextField];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * address2 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [address2 setBackgroundColor:[UIColor clearColor]];
    [address2 setFont:[UIFont boldSystemFontOfSize:13.0]];
    [address2 setText:@"Address 2"];//astha
    [scrollView addSubview:address2];
    [address2 release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.address2TextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.address2TextField setPlaceholder:@"Address 2"];///astha
    [self.address2TextField.layer setCornerRadius:2.0f];
    [self.address2TextField setBorderStyle:UITextBorderStyleRoundedRect];
   // [self.address2TextField setKeyboardType:UIKeyboardTypeNumberPad];
    [self.address2TextField setTag:ADDRESS2_TAG];
    [self.address2TextField setDelegate:self];
    self.address2TextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.address2TextField];

    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * postCode = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [postCode setBackgroundColor:[UIColor clearColor]];
    [postCode setFont:[UIFont boldSystemFontOfSize:13.0]];
    [postCode setText:@"Postcode"];//astha
    [scrollView addSubview:postCode];
    [postCode release];
    
    
    yValue = yValue +height+litleSpace;
    
    self.postCodeTextField = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.postCodeTextField setPlaceholder:@"Postcode"];///astha
    [self.postCodeTextField.layer setCornerRadius:2.0f];
    [self.postCodeTextField setBorderStyle:UITextBorderStyleRoundedRect];
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    if([countryCode isEqualToString:@"UK"])
    {
        
    }
    else
    {
         [self.postCodeTextField setKeyboardType:UIKeyboardTypeNumberPad];
    }
    [self.postCodeTextField setTag:POSTCODE_TAG];
    [self.postCodeTextField setDelegate:self];
    self.postCodeTextField.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.postCodeTextField];

    
    
    
    
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    sendButton = [[UIButton alloc] initWithFrame:CGRectMake(xValue , yValue,width, 42)];
    [sendButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [sendButton setTitle:@"Save & Go Back" forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [sendButton addTarget:self action:@selector(sendData) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:sendButton];
    
    yValue = yValue+50;
    self.scrollView.contentSize = CGSizeMake(200, yValue);
    
}

-(void) assignSavedvalue
{
    if(appDelegate.arrayInsuredetail!=nil)
    {
        InsuredDetailDiscription *dtl=[[InsuredDetailDiscription alloc]init];
        dtl=[appDelegate.arrayInsuredetail objectAtIndex:0];
        nametextField.text=dtl.firstName;
        surnameTextField.text=dtl.surname;
        dateOfBirthTextField.text=dtl.DOB;
        mobileTextField.text=dtl.phone;
        emailTextField.text=dtl.email;
        address1TextField.text=dtl.address1;
        address2TextField.text=dtl.address2;
        postCodeTextField.text=dtl.postcode;

    }
}





#pragma mark-
#pragma mark UITextFieldDelegate Methods-
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.scrollView.contentSize = CGSizeMake(200, yValue+200);
    return YES;
}// return NO to disallow editing.

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTxtFldTag = textField.tag;
    switch (textField.tag) {
            
        case SURNAME_TAG:
        {
         //   textField.inputAccessoryView = numberToolbar;
            
            break;

        }
        case NAME_TEXTFIELD_TAG:
        {
          //  textField.inputAccessoryView = numberToolbar;
      
                        break;
        }

        case DOB_TAG:
        case MOBIL_TEXTFIELD_TAG:
        case EMAIL_TEXTFIELD_TAG:
        case ADDRESS1_TAG:
        case ADDRESS2_TAG:
        case POSTCODE_TAG:
        {
            //textField.inputAccessoryView = numberToolbar;
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
               
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                     t = t +70;
                }
            }
            
            // gourav 06 Nov start
            if(textField.tag == ADDRESS2_TAG)
                t= t+70;
            if(textField.tag == POSTCODE_TAG)
                t= t+115;
            // gourav 06 Nov end
            
            [self moveScrolUP:t];
            break;
        }
 /*       case MOBIL_TEXTFIELD_TAG:
        {
            
           // textField.inputAccessoryView = numberToolbar;
            
            currentTxtFldTag = textField.tag;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                //t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }

                else
                {
                    t = t +70;
                }

            }
            
            [self moveScrolUP:t];
            break;
        }
        case EMAIL_TEXTFIELD_TAG:
        {
            
           // textField.inputAccessoryView = numberToolbar;
            currentTxtFldTag = textField.tag;
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
               // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }


            }
            
            [self moveScrolUP:t];
            break;
        }
        case ADDRESS1_TAG:
        {
            
            //textField.inputAccessoryView = numberToolbar;
            currentTxtFldTag = textField.tag;
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
               // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }


            }
            
            [self moveScrolUP:t];
            break;
        }

        case ADDRESS2_TAG:
        {
            
           // textField.inputAccessoryView = numberToolbar;
            
            currentTxtFldTag = textField.tag;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
               // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }

                else
                {
                    t = t +70;
                }

            }
            
            [self moveScrolUP:t];
            break;
        }
        case POSTCODE_TAG:
        {
            
           // textField.inputAccessoryView = numberToolbar;
            
            currentTxtFldTag = textField.tag;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
               // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }


            }
            
            [self moveScrolUP:t];
            break;
        }
*/

        default:
            break;
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    self.scrollView.contentSize = CGSizeMake(200, yValue);
//    [self moveScrollDownForTextfield];
    switch (textField.tag) {
        case (NAME_TEXTFIELD_TAG):
        {
            //[self moveScrollDown];
        }
            break;
        case (EMAIL_TEXTFIELD_TAG):
        {
            BOOL check=[self validateEmailWithString:textField.text];
            if(check==YES)
            {
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email ID" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                      [alert show];
                        self.emailTextField.text = @"";
                        [alert release];
                        

            }
            
            // [self moveScrollDown];
            
        }
            break;
        case MOBIL_TEXTFIELD_TAG:
        {
            //[self moveScrollDown];
        }
            break;
        default:
            break;
    }
    
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([[textField text] length] == 0) {
        if([string isEqualToString:@" "] || [string isEqualToString:@"\""]){
            NSLog(@"blank space can not inserted at initial position");
            return NO;
        }
    }
    
    if ([string isEqualToString:@". "]) {
        return NO;
    }
    
    return YES;
    // return NO to not change text
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    [self moveScrollDownForTextfield];
    return YES;
}

#pragma mark-
#pragma mark movesScrollview UP and DOWN-
-(void)moveScrolUP:(int)ht
{
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, ht);
    [UIView commitAnimations];
    
    
    CGRect frame = self.scrollView.frame;
    frame.origin.y = -ht;
    
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView commitAnimations];
    
}

-(void)moveScrollDown
{
    CGRect frame = CGRectMake(20, 18, 290, 350);
    if ([Utility isIphone_5]) {
        frame = CGRectMake(20, 18, 290, 440);
    }
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    
    self.scrollView.contentOffset = CGPointMake(0, offset.y);
    [UIView commitAnimations];
    
}
-(void)moveScrollDownForTextfield
{
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    
     self.scrollView.contentOffset = CGPointMake(0, 0); //yt
    
   // self.scrollView.contentSize = CGSizeMake(200, yValue);
    [UIView commitAnimations];

}
- (BOOL)validateEmailWithString:(NSString *)inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = aRange.location;
        //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            //NSLog(@"topleveldomains:%@",topLevelDomain);
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                
                
                NSRange rang = [inputText rangeOfString:@"@"];
                
                if (inputText) {
                    
                    NSString *stringO = [inputText substringWithRange:NSMakeRange(rang.location - 1,1 )];
                    
                    if ([stringO isEqualToString:@"."]) {
                        
                        return FALSE;
                    }
                    
                }
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    return FALSE;
}


#pragma mark-
#pragma mark UITextViewDelegate Methods-

/*- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.tag == WHAT_HAPPENED_TAG) {
        textView.inputAccessoryView = numberToolbar;
    }
    if (self.line1.textColor == [UIColor lightGrayColor]) {
        self.line1.text = @"";
        self.line1.textColor = [UIColor blackColor];
    }
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    offset = self.scrollView.contentOffset;
    int t;
    if ([Utility isIphone_5]) {
        t= 200;
    }
    else{
        t = 260;
    }
    [self moveScrolUP:t];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    return YES;
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
    
}*/

#pragma mark
#pragma mark IBActions-
-(void)callBack
{
    
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)sendData
{
    InsuredDetailDiscription *dtl = [[InsuredDetailDiscription alloc]init];
    
    dtl.firstName=nametextField.text;
    dtl.surname=surnameTextField.text;
    dtl.DOB=dateOfBirthTextField.text;
    dtl.phone=mobileTextField.text;
    dtl.email=emailTextField.text;
    dtl.address1=address1TextField.text;
    dtl.address2=address2TextField.text;
    dtl.postcode=postCodeTextField.text;
    
    appDelegate.arrayInsuredetail = [[NSMutableArray alloc]init];
    [appDelegate.arrayInsuredetail addObject:dtl];
    
    if(nametextField.text.length >0 || surnameTextField.text.length >0 || dateOfBirthTextField.text.length >0|| mobileTextField.text.length >0|| emailTextField.text.length >0|| address1TextField.text.length >0|| address2TextField.text.length >0|| postCodeTextField.text.length >0)  // gourav 09 sep
    {
         appDelegate.checkForInsured=YES;
    }
    else
        appDelegate.checkForInsured=NO;
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end

