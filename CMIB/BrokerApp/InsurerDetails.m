//
//  InsurerDetails.m
//  BrokerApp
//
//  Created by iPHTech2 on 05/06/13.
//
//

#import "InsurerDetails.h"
#import "Constants.h"


@interface InsurerDetails ()

@end

@implementation InsurerDetails
//@synthesize insuInfo;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 30, 30)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 245, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setText:@"Insurers Assist Numbers"];
    [label setFont:[UIFont boldSystemFontOfSize:17]];
    [label adjustsFontSizeToFitWidth];
    [label setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = label;
    [label release];
    
    [self initailizeNavigationBar];
    
    
    //[self createDescriptionView];
    
    [self.scrollView setContentSize:CGSizeMake(300, 1000)];
    
}

-(void)createDescriptionView
{

   /* CGFloat titleLabelXalue = 140.0;
    CGFloat titleLabelYValue = 30.0;
    CGFloat titleLabelWidth = 180.0;
    CGFloat titleLabelHeight = 40.0;
    CGFloat lineWidth = 320.0;
    CGFloat lineheight = 2.0;
    CGFloat largeSpace = 8.0;
    CGFloat littleSpace = 0.0;
    CGFloat width = 280.0;
    CGFloat xValue = 0.0;
    CGFloat yValue = 100.0;
    CGFloat height = 15;
    CGFloat fontSize = 12.0;
    
    
    
    UIImageView * logo = [[UIImageView alloc]initWithFrame:CGRectMake(80, 25, 50, 50)];
    [logo setImage:[UIImage imageNamed:@"logo.png"]];
    [self.view addSubview:logo];
    
    
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(titleLabelXalue, titleLabelYValue, titleLabelWidth, titleLabelHeight)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:40]];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [titleLabel setText:insuInfo.idName];
    [self.view addSubview:titleLabel];
    
    
    UIImageView * shadow = [[UIImageView alloc]initWithFrame:CGRectMake(xValue, yValue-10, lineWidth, lineheight)];
    [shadow setImage:[UIImage imageNamed:SHADOWIMAGE]];
    [self.view addSubview:shadow];
   // UIView * view = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, lineWidth, lineheight)];
    //[view setBackgroundColor:[UIColor lightGrayColor]];
    //[self.view addSubview:view];
    
    yValue = yValue+lineheight+largeSpace;
    
     self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(20, 110  , 280, 400 )];
    if ([Utility isIphone_5]) {
        [self.scrollView setFrame:CGRectMake(20, 110  , 280, 500 )];
    }
    [self.scrollView setContentSize:CGSizeMake(width, 10)];
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setBounces:NO];
     [self.scrollView setScrollEnabled:YES];
    [self.view addSubview:self.scrollView];
    
    yValue = 10.0;
    
    if (insuInfo.company && ![insuInfo.company isEqualToString:@""]) {
        UILabel * companyHeader = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [companyHeader setBackgroundColor:[UIColor clearColor]];
        [companyHeader setFont:[UIFont boldSystemFontOfSize:fontSize]];
        [companyHeader setText:@"Company"];
        [self.scrollView addSubview:companyHeader];
        [companyHeader release];
        
        yValue = yValue+height+1;
        [self.scrollView setContentSize:CGSizeMake(width, yValue)];
        
        
        UILabel * companyText = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [companyText setBackgroundColor:[UIColor clearColor]];
        [companyText setFont:[UIFont systemFontOfSize:fontSize]];
        [companyText setText:insuInfo.company];
        [self.scrollView addSubview:companyText];
        [companyText release];
        
        yValue = yValue+height+largeSpace;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
    }
    
    if (insuInfo.phoneNumber_1 && ![insuInfo.phoneNumber_1 isEqualToString:@""]) {
    UILabel * phone1 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [phone1 setBackgroundColor:[UIColor clearColor]];
    [phone1 setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [phone1 setText:@"Phone number 1"];
    [self.scrollView addSubview:phone1];
    [phone1 release];
    
        yValue = yValue+height+1;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
    
        UITextView * phoneText1 = [[UITextView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width, height)];
       [phoneText1 setBackgroundColor:[UIColor clearColor]];
        [phoneText1 setEditable:NO];
        [phoneText1 setScrollEnabled:NO];
        [phoneText1 setFont:[UIFont systemFontOfSize:fontSize]];
        [phoneText1 setText:insuInfo.phoneNumber_1];
        [phoneText1 setDataDetectorTypes:UIDataDetectorTypePhoneNumber];
        [self.scrollView addSubview:phoneText1];
        [phoneText1 release];
    
    yValue = yValue+height+largeSpace;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
    }
    
     if (insuInfo.phoneNumber_2 && ![insuInfo.phoneNumber_2 isEqualToString:@""]) {
    
    UILabel * phone2 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [phone2 setBackgroundColor:[UIColor clearColor]];
    [phone2 setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [phone2 setText:@"Phone number 2"];
    [self.scrollView addSubview:phone2];
    [phone2 release];
    
    yValue = yValue+height+1;
          [self.scrollView setContentSize:CGSizeMake(width, yValue)];
         
         UITextView * phoneText2 = [[UITextView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width, height)];
         [phoneText2 setBackgroundColor:[UIColor clearColor]];
         [phoneText2 setEditable:NO];
         [phoneText2 setScrollEnabled:NO];
         [phoneText2 setFont:[UIFont systemFontOfSize:fontSize]];
         [phoneText2 setText:insuInfo.phoneNumber_2];
         [phoneText2 setDataDetectorTypes:UIDataDetectorTypePhoneNumber];
         [self.scrollView addSubview:phoneText2];
         [phoneText2 release];
    
    
          yValue = yValue+height+largeSpace;
          [self.scrollView setContentSize:CGSizeMake(width, yValue)];
    
     }
    if (insuInfo.phoneNumber_3 && ![insuInfo.phoneNumber_3 isEqualToString:@""]) {
    
    UILabel * phone3 = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [phone3 setBackgroundColor:[UIColor clearColor]];
    [phone3 setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [phone3 setText:@"Phone number 3"];
    [self.scrollView addSubview:phone3];
    [phone3 release];
    
    yValue = yValue+height+1;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
        
        UITextView * phoneText3 = [[UITextView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width, height)];
      
        [phoneText3 setEditable:NO];
        [phoneText3 setScrollEnabled:NO];
        [phoneText3 setDataDetectorTypes:UIDataDetectorTypePhoneNumber];
        [phoneText3 setBackgroundColor:[UIColor clearColor]];
        [phoneText3 setFont:[UIFont systemFontOfSize:fontSize]];
        [phoneText3 setText:insuInfo.phoneNumber_3];
        
        [self.scrollView addSubview:phoneText3];
        [phoneText3 release];
        
       
    yValue = yValue+height+largeSpace;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
        
    }
    
    if (insuInfo.website && ![insuInfo.website isEqualToString:@""]) {
        
        UILabel * website = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
        [website setBackgroundColor:[UIColor clearColor]];
        
        [website setText:@"Website"];
        [website setFont:[UIFont boldSystemFontOfSize:fontSize]];
        [self.scrollView addSubview:website];
        [website release];
        
        yValue = yValue+height+1;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
        
        
        
        UITextView * webText = [[UITextView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width, height)];
        [webText setBackgroundColor:[UIColor clearColor]];
        [webText setEditable:NO];
        [webText setScrollEnabled:NO];
        [webText setFont:[UIFont systemFontOfSize:fontSize]];
        [webText setText:insuInfo.website];
        [webText setDataDetectorTypes:UIDataDetectorTypeLink];
        [self.scrollView addSubview:webText];
        [webText release];
        
        yValue = yValue+height+largeSpace;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
        
    }
    if (insuInfo.description && ![insuInfo.description isEqualToString:@""]) {
        
        NSString * description = [NSString stringWithFormat:@"<html><head><body>%@</body></head><html>", insuInfo.description];
        CGSize fontSize = [description sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(width, 1000) lineBreakMode:UILineBreakModeCharacterWrap];

        UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(xValue, yValue, width, fontSize.height)];
       
        //[webview.scrollView setBounces:NO];
        [webview setOpaque:NO];
        [webview setBackgroundColor:[UIColor clearColor]];
        [webview.scrollView setScrollEnabled:NO];
        [webview loadHTMLString:description baseURL:nil];
        [self.scrollView addSubview:webview];
        [webview release];
        
        yValue = yValue+fontSize.height+largeSpace;
         [self.scrollView setContentSize:CGSizeMake(width, yValue)];
        
    }
    
    //[self.scrollView setContentSize:CGSizeMake(width, yValue+50)];
    
    */
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
