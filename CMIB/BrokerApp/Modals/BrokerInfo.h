//
//  BrokerInfo.h
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import <Foundation/Foundation.h>

@interface BrokerInfo : NSObject
{
     NSString * ID;
     NSString * title;
     NSString * website;
     NSString * claims_instructions;
     NSString * claims_instructions_phoneNumber;
     NSString * after_hours_info;
     NSString * after_hours_phoneNumber;
    NSString * brokerobrien;
}


@property (nonatomic, retain) NSString * ID;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSString * claims_instructions;
@property (nonatomic, retain) NSString * claims_instructions_phoneNumber;
@property (nonatomic, retain) NSString * after_hours_info;
@property (nonatomic, retain) NSString * after_hours_phoneNumber;
@property (nonatomic, retain) NSString * brokerobrien;

@end
