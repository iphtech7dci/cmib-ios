//
//  BrokerInfo.m
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import "BrokerInfo.h"

@implementation BrokerInfo

@synthesize  ID;
@synthesize title;
@synthesize website;
@synthesize claims_instructions;
@synthesize claims_instructions_phoneNumber;
@synthesize after_hours_info;
@synthesize after_hours_phoneNumber,brokerobrien;

@end
