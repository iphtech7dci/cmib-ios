//
//  Constants.h
//  MobileLearning
//
//  Created by An Mai on 4/8/09.
//  Copyright 2009 IMT Solutions. All rights reserved.
//


//#define BACKGROUND_COLOR_PINK   [UIColor colorWithRed:205.0/255.0f green:15.0/255.0f blue:32.0/255.0f alpha:1.0]

#define BACKGROUND_COLOR_PINK   [UIColor colorWithRed:16.0/255.0f green:74.0/255.0f blue:144.0/255.0f alpha:1.0]

#define BACKGROUND_COLOR_ORANGE  [UIColor colorWithRed:231.0/255.0f green:72.0/255.0f blue:27.0/255.0f alpha:1.0]

/////*****Astha
#define CLAIM_BUTTON             @"claimButton.png"
#define DIRECTNUMBERS_BUTTON     @"numberButtons.png"
#define OFFICEINFO_BUTTON        @"homeButton.png"
#define PHOTOS_BUTTON            @"cameraButton.png"
#define SHARE_BUTTON             @"shareButton.png"
#define WEBSITE_BUTTON           @"linkButton.png"
////******Astha
#define BACKGROUNG_IMAGE         @"backgroundImage@2x.png"

#define CLIENT_ID                @"1137"

/////*****Gourav Dixit
#define CLAIM_BUTTON_SELECTED             @"claimButtonSelected.png"
#define DIRECTNUMBERS_BUTTON_SELECTED     @"numberButtonsSelected.png"
#define OFFICEINFO_BUTTON_SELECTED        @"homeButtonSelected.png"
#define PHOTOS_BUTTON_SELECTED            @"cameraButtonSelected.png"
#define SHARE_BUTTON_SELECTED             @"shareButtonSelected.png"
#define WEBSITE_BUTTON_SELECTED           @"linkButtonSelected.png"
////******Gourav Dixit


//*News
#define NEWS_HEADING             @"NewsHeading.png"
#define  NEWS_BLUE_HEADING       @"NewsHeading.png"
#define PRESS_ROOM_LOGO            @"pressRoomLogo.png"


#define STAUS_BAR_RED                     @"StatusBarRed_BackGround.png"
#define STAUS_BAR_ORANGE                  @"StatusBarOrange_BackGround.png"
#define STAUS_BAR_DARK_GREY               @"StatusBarDarkGrey_BackGround.png"

// button under text

#define     SHAREBUTTON_TEXT         @"Share"
#define     OFFICEINFOBUTTON_TEXT    @"Offices & Info"
#define     WEBSITEBUTTON_TEXT       @"Website"
#define     CLAIMPHOTOSBUTTON_TEXT   @"Claim & Photos"
#define     WHATTODOBUTTON_TEXT      @"Claims"
#define     DIRECTNOBUTTON_TEXT      @"Directory"
#define     WHATTODO_TEXT            @"What to do"

//share images

#define SHARETEXT_IMAGE          @"shareTextBtnImage.png"
#define MOTER_IMAGE              @"Moter.png"
#define SHAREFACEBOOK_IMAGE      @"sharefacebookBtnImage.png"
#define SHAREEMAIL_IMAGE         @"shareEmail.png"
#define SHARETWITTER_IMAGE       @"shareTwitterBtnImage.png"
#define NAVIGATIONBAR_IMAGE      @"navigationImage.png"
#define NAVIGATIONBAR_PINK_IMAGE @"navigationImagePink.png"
#define NAVIGATIONBAR_GREY_IMAGE @"navigationImageGrey.png"

#define  NEWS_HEADING1           @"news_head.png"


#define BACK_BUTTON_IMAGE       @"backBtnImage.png"
#define EXPORT_BUTTON_IMAGE     @"exportArrow.png"
#define CLAIM_BUTTON_IMAGE      @"ClaimRightBar.png"

#define EXPORT_BUTTON_IMAGEe     @"home.png"
#define PUREBACKGROUNDIMAGE     @"pureBackgroundImage.png"


//*app name for PN

#define APP_NAME_FOR_PN        @"cmib"

//officeinfo

#define OFFICE_INFO_KEY           @"officeInfo"
#define STAFF_INFO_KEY            @"staffInfo"
#define INSURANCE_INFO_KEY        @"insuranceInfo"
#define BROKER_INFO_KEY           @"brokerInfo"
#define OFFICEINFOTITLE_BUTTON    @"MotorVehicleClaim.png"
#define MOTOR_VEHICLE_CLAIM       @"MotorVehicleClaim.png"
#define DIRECT_IMG                @"directBtn.png"
#define HOME_Image                @"home.png"
#define HOME_RED_Image            @"HomeRedIcon.png"

#define LINK_BUTTON_IMAGE         @"linkBtnImage.png"
#define MAP_VIEW_IMAGE            @"mapViewImg.png"
#define MAP_VIEW_BLANK_IMAGE      @"mapViewImg_blank.png"


#define PHONE_ICON_IMAGE          @"phonePic.png"
#define LOCATION_ICON_IMAGE       @"locationPic.png"
#define PROFILE_ICON_IMAGE        @"profilePic.png"
#define OFFICE_PROFILE_IMAGE      @"OfficeProfileHomeIcon.png"

// claim photos
#define TEXTFILED_BACKGROUND    @"TextFields_BG.png"
#define TEXTVIEW_BACKGROUND     @"TextView_BG.png"

#define ADD_PHOTO_IMAGE         @"addPhotoImage.png"
#define ADD_PHOTO_IMAGEee       @"addPhotoImage1.png"
#define CAMERA_IMAGE            @"cameraImage.png"
#define SEND_BUTTON_IMAGE       @"blackBarImage@2x.png"
#define UPLOAD_PHOTO_IMAGE      @"uploadPhotoBox.png"
#define SUBHEADING_TEXT         @"For prompt notification of a potential claim, complete this form and attach photos if applicable."


#define USE_OUR_GENERAL_CLAIM   @"Use our General Claim notification or the more detailed Motor Vehicle Claims notifications tools in the event of a potential claim, please complete as many fields as you can and take photos of damage.When you have finished click Send and we will be notified via email."

//#define MOTERVEHICLE_TEXT    @"Complete as many of fields in each section as possible. When you're finished, click the Finished & Send button, you will receive a CC via email. Data you enter will save until you reset it."
#define MOTERVEHICLE_TEXT    @"This Motor Vehicle claims section has been developed to assist you gathering information about an accident involving your vehicle. Complete as many of fields in each section as possible and use the Photos section to capture images of the incident. When you're finished, click the Finished & Send button, you will receive a CC via email. Data you enter will save until you reset it."

#define DONE_BUTTON_IMAGE     @"doneBtn.png"
#define DROPDOWN_BUTTON_IMAGE @"dropDownBtnImage.png"


//claim what to do

#define QUESTION_MARK_IMAGE        @"questionMark.png"

// directnumbers

#define DIRECT_NUMBER_BTN        @"directnumbersBtn.png"
#define TELEPHONE_IMAGE          @"telephoneImage.png"
#define BLACK_BAR_IMAGE          @"blackBarImage.png"
#define BLUE_BTN_IMAGE           @"blueBtnImage.png"
#define STAFF_TEXT_STRING        @"Key Staff"
#define AFTER_HOURS_TEXT_STRING  @"After Hours Contacts"
#define INSURANCE_NUMBERS_STRING @"Insurers Assist Numbers"
#define BLUE_BTN_TEXT_STRING     @"O'Brien Glass"
#define ACCESSORY_IMAGE          @"accessory.png"
#define ACCESSORY_RED_IMAGE      @"AccessoryRed.png"

#define LINKEDIN_IMAGE           @"linkedinBtn.png"
#define GOTOWEBSITE_BTN_IMAGE    @"goToWebsiteBtnImage.png"

#define INFO_TAG            @"infoTag.png"
#define PDF_DOWNLOAD_BTN    @"pdfDownloadBtn.png"
#define SHADOWIMAGE         @"shadowImage.png"

#define SHARE_LINK    @"https://goo.gl/SXkdBb"  //@"https://goo.gl/SXkdBb"
#define SHARE_ANDROID_LINK @"https://goo.gl/Z2ddDi"  // @"http://goo.gl/QDGx2v"
// https://itunes.apple.com/au/app/Gardian Group Brokerapp/id917656704?ls=1&mt=8

#define INFOMATION_TEXT   @"This application has been developed to provide useful information when you need to contact us, lodge a claim, or have fast access to your insurance portfolio details.\n\nAs well as general contact and claims information you can complete and lodge an initial claim notification. You can also upload images relevant to your claim. Your smart phone may include geotag data (GPS) and timestamp information on photos that you take with your phones camera. This information is uploaded with the images you send us.\n\nThis app is developed by Bark Productions."
#define INFO_PHONE_NUMBER     @"61.733690300"

#define ADD_POLICY_BUTTON_IMAGE      @"addPolicyBtn.png"
#define DELETE_POLICY_BUTTON_IMAGE   @"deletePolicyBtn.png"
#define POLICYNUMBERIMAGE            @"policuNumberImage.png"

#define POLICYNAME       @"policyName"
#define POLICYNUMBER     @"policyNumber"


#define SHARE_SUBJECT   @"Check out the CMIB Brokerapp for useful information and tools when you need to contact us, lodge a claim, or require fast access to your insurance portfolio details."

#define SHARE_SUBJECT_TWITTER   @"Check out the CMIB Brokerapp for useful information, claims tools & contacts."

// Check out the MRIB Group Brokerapp for useful information and tools when you need to contact us, lodge a claim, or require fast access to your insurance portfolio details.

#define AFTER_HOURS_SCREEN_TEXT @"Claims will never occur at a convenient time. If it's outside our normal business hours, please use the contact details provided below to get in contact with us or relevant service providers:"

#define OBRIEN_TEXT     @"<p> Glass emergency? We recommend O'Brien® for all your building and vehicle glass repair and replacement needs.</p><li>  Serving Australians for almost 90 years</li><li>    All workmanship guaranteed</li><li>    Dedicated 24 hour National Crisis Hotline</li><p>  Call O’Brien® now for immediate help with your glass emergency or any glazing requirement.</p>"


