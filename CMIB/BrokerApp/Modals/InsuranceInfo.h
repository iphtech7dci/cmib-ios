//
//  InsuranceInfo.h
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import <Foundation/Foundation.h>

@interface InsuranceInfo : NSObject
{
    NSString * company;
    NSString * idName;
    NSString * phoneNumber_1;
    NSString * phoneNumber_2;
    NSString * phoneNumber_3;
    NSString * website;
    NSString * logoRefrence;
    NSString * description;
    
}

@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * idName;
@property (nonatomic, retain) NSString * phoneNumber_1;
@property (nonatomic, retain) NSString * phoneNumber_2;
@property (nonatomic, retain) NSString * phoneNumber_3;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSString * logoRefrence;
@property (nonatomic, retain) NSString * description;


@end
