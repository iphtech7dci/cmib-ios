//
//  OfficeInfo.h
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import <Foundation/Foundation.h>

@interface OfficeInfo : NSObject
{
    NSString  * location;
    NSString  * openingHours;
    NSString  * OfficePhone;
    NSString  * tollFreeNumber;
    NSString  * address_1;
    NSString  * addess_2;
    NSString  * suburb;
    NSString  * state;
    NSString  * postCode;
    NSString  * googleMapUrl;
    NSString  * postalAddress_1;
    NSString  * postalAddress_2;
    NSString  * postalAddressSuburb;
    NSString  * postalAddressState;
    NSString  * postalAddressPostCode;
    
    
    
    
}

@property (nonatomic , retain) NSString  * location;
@property (nonatomic , retain) NSString  * openingHours;
@property (nonatomic , retain) NSString  * OfficePhone;
@property (nonatomic , retain) NSString  * tollFreeNumber;
@property (nonatomic , retain) NSString  * address_1;
@property (nonatomic , retain) NSString  * addess_2;
@property (nonatomic , retain) NSString  * suburb;
@property (nonatomic , retain) NSString  * state;
@property (nonatomic , retain) NSString  * postCode;
@property (nonatomic , retain) NSString  * googleMapUrl;
@property (nonatomic , retain) NSString  * postalAddress_1;
@property (nonatomic , retain) NSString  * postalAddress_2;
@property (nonatomic , retain) NSString  * postalAddressSuburb;
@property (nonatomic , retain) NSString  * postalAddressState;
@property (nonatomic , retain) NSString  * postalAddressPostCode;


@end
