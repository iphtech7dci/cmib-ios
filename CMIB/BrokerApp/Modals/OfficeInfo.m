//
//  OfficeInfo.m
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import "OfficeInfo.h"

@implementation OfficeInfo


@synthesize  location;
@synthesize openingHours;
@synthesize OfficePhone;
@synthesize tollFreeNumber;
@synthesize address_1;
@synthesize addess_2;
@synthesize suburb;
@synthesize state;
@synthesize postCode;
@synthesize googleMapUrl;
@synthesize postalAddress_1;
@synthesize postalAddress_2;
@synthesize postalAddressSuburb;
@synthesize postalAddressState;
@synthesize postalAddressPostCode;

@end
