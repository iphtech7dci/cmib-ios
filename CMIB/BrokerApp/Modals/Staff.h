//
//  Staff.h
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import <Foundation/Foundation.h>

@interface Staff : NSObject
{
    NSString  * firstName;
    NSString  * lastName;
    NSString  * position;
    NSString  * phone;
    NSString  * email;
    NSString  * mobile;
    NSString  * linkedin;
    NSString  * twitter;
    NSString  * officeLocation;
    NSString  * postalAddress_1;
    NSString  * postalAddress_2;
    NSString  * postalAdressSuburb;
    NSString  * postalAddressState;
    NSString  * postalAddressPostalCode;
    
}

@property (nonatomic, retain) NSString  * firstName;
@property (nonatomic, retain) NSString  * lastName;
@property (nonatomic, retain) NSString  * position;
@property (nonatomic, retain) NSString  * phone;
@property (nonatomic, retain) NSString  * email;
@property (nonatomic, retain) NSString  * mobile;
@property (nonatomic, retain) NSString  * linkedin;
@property (nonatomic, retain) NSString  * twitter;
@property (nonatomic, retain) NSString  * officeLocation;
@property (nonatomic, retain) NSString  * postalAddress_1;
@property (nonatomic, retain) NSString  * postalAddress_2;
@property (nonatomic, retain) NSString  * postalAdressSuburb;
@property (nonatomic, retain) NSString  * postalAddressState;
@property (nonatomic, retain) NSString  * postalAddressPostalCode;

@end
