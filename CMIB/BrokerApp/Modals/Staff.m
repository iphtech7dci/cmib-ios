//
//  Staff.m
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import "Staff.h"

@implementation Staff

    @synthesize  firstName;
    @synthesize lastName;
    @synthesize position;
    @synthesize phone;
    @synthesize email;
    @synthesize mobile;
    @synthesize linkedin;
    @synthesize twitter;
    @synthesize officeLocation;
    @synthesize postalAddress_1;
    @synthesize postalAddress_2;
    @synthesize postalAdressSuburb;
    @synthesize postalAddressState;
    @synthesize postalAddressPostalCode;

@end
