//
//  UMXPathQuery.h
//  PracticeQuiz
//
//  Created by iPHTech2 on 25/04/13.
//  Copyright (c) 2013 UM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UMXPathQuery : NSObject
NSArray *PerformHTMLXPathQuery(NSData *document, NSString *query);
NSArray *PerformXMLXPathQuery(NSData *document, NSString *query);
@end
