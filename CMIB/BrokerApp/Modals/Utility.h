//
//  Utility.h
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+(BOOL)isIphone_5;
+(NSString *)serverAddress;
+(NSString *)xmlPathInDocDirectory;

+ (NSString *)stringFromCString:(char *)string;
+ (NSString *)serverAddressOfDocument;
+(NSString *)xmlPathInNews;
@end
