//
//Utility.m
//BrokerApp
//Created by iPHTech2 on 25/05/13.

#import "Utility.h"
#import "Constants.h"
@implementation Utility

+(BOOL)isIphone_5
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    if (screenHeight == 568) {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(NSString *)serverAddress
{
    
    NSString *urlStr = [NSString stringWithFormat:@"http://brokerapps.com.au/xml.php?ID=1137"];
    
   // NSString *urlStr = [NSString stringWithFormat:@"%@",@"http://brokerapps.com.au/xml.php?ID=1137"];
    
    return [NSString stringWithFormat:@"http://brokerapps.com.au/xml.php?ID=1137"];
}

+(NSString *)serverAddressOfDocument
{
    NSString * urlStr = [NSString stringWithFormat:@"http://brokerapps.com.au/xml_blog.php"]; // for news
    return urlStr;
    
}

+(NSString *)xmlPathInNews
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *xmlPathInDocDir = [documentsDirectory stringByAppendingPathComponent:@"NewsInfo.xml"];
    
    return xmlPathInDocDir;
}

+(NSString *)xmlPathInDocDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    NSString *xmlPathInDocDir = [documentsDirectory stringByAppendingPathComponent:@"officeInfo.xml"];
    
    return xmlPathInDocDir;
}

+ (NSString *)stringFromCString:(char *)string {
    return (string == NULL)? @"":[NSString stringWithUTF8String:string];
}


@end
