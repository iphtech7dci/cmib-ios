//
//  XMLReader.h
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
@class OfficeInfo;

@interface XMLReader : NSObject
{
    NSData * data;
    AppDelegate *appDelegate;
}

@property(nonatomic, retain) NSData * data;

-(NSData *)allocXml;
-(NSMutableArray *)ListOfOffices;
-(NSMutableArray *)listOfStaff;
-(NSMutableDictionary *)BrokerDictionary;
-(NSMutableArray *)insuranceArray;
@end
