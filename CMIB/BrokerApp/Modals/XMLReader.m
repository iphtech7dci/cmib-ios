//
//  XMLReader.m
//  BrokerApp
//
//  Created by iPHTech2 on 28/05/13.
//
//

#import "XMLReader.h"
#import "OfficeInfo.h"
#import "BrokerInfo.h"
#import "Staff.h"
#import "InsuranceInfo.h"
#import "UMXPathQuery.h"
#import "Utility.h"
#import "DirectNumbers.h"


@implementation XMLReader

@synthesize data;

-(NSData *)allocXml
{
    
	NSData *result = nil;
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error = nil;
	NSString * xmlPath = [Utility xmlPathInDocDirectory];
	BOOL success = [fileManager fileExistsAtPath:xmlPath];
    if (success) {
        result = [NSData dataWithContentsOfFile:xmlPath] ;
      
    }
	else
    {
        NSLog(@"error is %@", error);
    }
    return result;
    
    
}

-(NSMutableDictionary *)BrokerDictionary
{
    NSMutableDictionary * brokerDict = [[NSMutableDictionary alloc]init];
    appDelegate      = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
     appDelegate.directVC = [[DirectNumbers alloc]initWithNibName:@"DirectNumbers" bundle:nil];
    NSArray *brokerArray = PerformXMLXPathQuery(self.data, @"/xml/broker");
    for (NSDictionary * brokerInfoDict in brokerArray) {
        
        NSMutableArray *items = [brokerInfoDict objectForKey:@"nodeChildArray"];
        BrokerInfo * broInfo = [[BrokerInfo alloc]init];
        
        for (int i = 0; i <[items count]; i ++) {
			NSString *nodeName = [[items objectAtIndex:i] objectForKey:@"nodeName"];
            if([nodeName isEqualToString:@"ID"])
            {
               // NSArray *idi = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				broInfo.ID = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"title"])
            {
               // NSArray *title = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				broInfo.title = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"website"])
            {
                //NSArray *web = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
               // NSArray *ints = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				//broInfo.website = [[ints objectAtIndex:0] objectForKey:@"nodeContent"] ;
				broInfo.website = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"claims_instruction"])
            {
                NSArray *ints = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				broInfo.claims_instructions = [[ints objectAtIndex:0] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"claims_instruction_phone_number"])
            {
               // NSArray *cipn = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				broInfo.claims_instructions_phoneNumber = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"after_hours_info"])
            {
               // NSArray *afterhrs = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				broInfo.after_hours_info = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"after_hours_phone_number"])
            {
              //  NSArray *afterhrspn = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				broInfo.after_hours_phoneNumber = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"obrien"])
            {
                //  NSArray *afterhrspn = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
                broInfo.brokerobrien = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
                appDelegate.directVC.obriencheck = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
           
                
                
            
            
            }
            
        }
       // DirectNumbers *directVC = [[DirectNumbers alloc]initWithNibName:@"DirectNumbers" bundle:nil];
        
    [brokerDict setObject:broInfo forKey:@"broker"];
    [broInfo release];
    
    }
return brokerDict;
    
}
-(NSMutableArray *)ListOfOffices
{
    
    
    NSMutableArray * officesArray = [NSMutableArray arrayWithCapacity:15];
    NSArray *office = PerformXMLXPathQuery(self.data, @"/xml/office_info/office");
    for (NSDictionary * officeInfoDict in office) {
        
        NSMutableArray *items = [officeInfoDict objectForKey:@"nodeChildArray"];
        OfficeInfo * getOfficeInfoArray = [[OfficeInfo alloc]init];
        for (int i = 0; i <[items count]; i ++) {
			NSString *nodeName = [[items objectAtIndex:i] objectForKey:@"nodeName"];
            if([nodeName isEqualToString:@"location"])
            {
               
				getOfficeInfoArray.location = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"opening_hours"])
            {

				getOfficeInfoArray.openingHours = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
               
            }
            else if ([nodeName isEqualToString:@"office_phone"])
            {
                
				getOfficeInfoArray.OfficePhone = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"toll_free_number"])
            {
               
                getOfficeInfoArray.tollFreeNumber =[[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
               
            }
            else if ([nodeName isEqualToString:@"suburb"])
            {
                
				getOfficeInfoArray.suburb = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"state"])
            {
                
				getOfficeInfoArray.state = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
               
            }
            else if ([nodeName isEqualToString:@"postcode"])
            {
               
				getOfficeInfoArray.postCode = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
               
            }
            else if ([nodeName isEqualToString:@"postal_address_suburb"])
            {
                getOfficeInfoArray.postalAddressSuburb = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
               
            }
            else if ([nodeName isEqualToString:@"postal_address_state"])
            {
               
				getOfficeInfoArray.postalAddressState = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"postal_address_post_code"])
            {
                
				getOfficeInfoArray.postalAddressPostCode = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"address_1"])
            {
                NSArray *add1 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				getOfficeInfoArray.address_1 = [[add1 objectAtIndex:0] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"address_2"])
            {
                NSArray *add2 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				getOfficeInfoArray.addess_2 = [[add2 objectAtIndex:0] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"google_map_url"])
            {
                NSArray *url = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				getOfficeInfoArray.googleMapUrl = [[url objectAtIndex:0] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"postal_address_1"])
            {
                NSArray *postadd1 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				getOfficeInfoArray.postalAddress_1 = [[postadd1 objectAtIndex:0] objectForKey:@"nodeContent"] ;
                
            }
            else if ([nodeName isEqualToString:@"postal_address_2"])
            {
                NSArray *postalAdd2 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				getOfficeInfoArray.postalAddress_2 = [[postalAdd2 objectAtIndex:0] objectForKey:@"nodeContent"] ;
                
            }
            
                
        }
        [officesArray addObject:getOfficeInfoArray];
        [getOfficeInfoArray release];
        
    }
return officesArray;
}

-(NSMutableArray *)listOfStaff
{
   
    
    NSMutableArray * staffArray = [NSMutableArray arrayWithCapacity:15];
    NSArray *staffmembr = PerformXMLXPathQuery(self.data, @"/xml/staff_info/staff");
    for (NSDictionary * staffInfoDict in staffmembr) {
        
         Staff * staf = [[Staff alloc]init];
        NSMutableArray *items = [staffInfoDict objectForKey:@"nodeChildArray"];
        
        for (int i = 0; i <[items count]; i ++) {
			NSString *nodeName = [[items objectAtIndex:i] objectForKey:@"nodeName"];
            if([nodeName isEqualToString:@"first_name"])
            {
               // NSArray *firstName = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.firstName = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"last_name"])
            {
                //NSArray *lastname = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.lastName = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"position"])
            {
               // NSArray *position = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.position = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"phone"])
            {
               // NSArray *phone = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.phone = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"email"])
            {
                //NSArray *email = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.email = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"mobile"])
            {
               // NSArray *mobil = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.mobile = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"linkedin"])
            {
               // NSArray *linkedin = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.linkedin = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"twitter"])
            {
               // NSArray *twitter = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.twitter = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"office_location"])
            {
                //NSArray *offLocation = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.officeLocation = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"postal_address1"])
            {
                NSArray *postAdd1 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.postalAddress_1 = [[postAdd1 objectAtIndex:0] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"postal_address2"])
            {
               // NSArray *postAdd2 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.postalAddress_2 = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"postal_address_suburb"])
            {
                //NSArray *postAddSuburb = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.postalAdressSuburb = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"postal_address_state"])
            {
               // NSArray *postalAddState = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.postalAddressState = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"postal_address_postal_code"])
            {
                //NSArray *papc = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				staf.postalAddressPostalCode = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
        }
        [staffArray addObject:staf];
        [staf release];
    }
    return staffArray;
}

-(NSMutableArray *)insuranceArray
{
    NSMutableArray * insurancesArray = [NSMutableArray arrayWithCapacity:8];
    NSArray *insurance = PerformXMLXPathQuery(self.data, @"/xml/insurance_info/insurance");
    for (NSDictionary * insuranceDict in insurance) {
        
        InsuranceInfo * insu = [[InsuranceInfo alloc]init];
        
        NSMutableArray *items = [insuranceDict objectForKey:@"nodeChildArray"];
        
        for (int i = 0; i <[items count]; i ++) {
			NSString *nodeName = [[items objectAtIndex:i] objectForKey:@"nodeName"];
            if([nodeName isEqualToString:@"company"])
            {
                //NSArray *compName = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.company = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"id_name"])
            {
               // NSArray *idname = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.idName = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"phone_number1"])
            {
                //NSArray *phone1 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.phoneNumber_1 = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"phone_number2"])
            {
               // NSArray *phone2 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.phoneNumber_2 = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"phone_number3"])
            {
               // NSArray *phone3 = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.phoneNumber_3 = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"website"])
            {
               // NSArray *website = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.website = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            if([nodeName isEqualToString:@"logo_reference"])
            {
               // NSArray *logo = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.logoRefrence = [[items objectAtIndex:i] objectForKey:@"nodeContent"] ;
            }
            
            if([nodeName isEqualToString:@"description"])
            {
                NSArray *desc = [[items objectAtIndex:i] objectForKey:@"nodeChildArray"];
				insu.description = [[desc objectAtIndex:0] objectForKey:@"nodeContent"] ;
            }
            
        }
        [insurancesArray addObject:insu];
        [insu release];
        
        
    }
return insurancesArray;
}
@end
