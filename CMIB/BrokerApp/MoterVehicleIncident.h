//
//  MoterVehicleIncident.h
//  Optimus1
//
//  Created by iPHTech13 on 18/04/14.
//
//

#import "GAITrackedViewController.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSData+Base64.h"
#import <CoreLocation/CoreLocation.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
//#import <ImageIO/CGImageSource.h>
//#import <ImageIO/CGImageProperties.h>
//#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "GAITrackedViewController.h"
#import "AppDelegate.h"

@interface MoterVehicleIncident : GAITrackedViewController<UIWebViewDelegate,MFMailComposeViewControllerDelegate,CLLocationManagerDelegate>
{
    AppDelegate *appdelegate;
    UIButton * sendButton;
    UIButton * resetBtn;
    //NSString *emailRecieverString;
     //int imageCount;
    int checForEmptyArray;
    CLLocationManager *locationManager;
    
    UIView * confirmationView;  // gourav 09 sep
    UIView * indecatorView; // gourav 09 sep
    
}
-(void)doFacebookShare;
-(void) doTextShare;

@property (nonatomic, retain) UIView * confirmationView;  // gourav 09 sep
@property (nonatomic, retain) UIView * indecatorView; // gourav 09 sep

@end
