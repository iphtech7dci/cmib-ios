//
//  MoterVehicleIncident.m
//  Optimus1
//
//  Created by iPHTech13 on 18/04/14.
//
//

#import "MoterVehicleIncident.h"
#import "Utility.h"
#import "Constants.h"
#import "InsuredDetails.h"
#import "YourVehicleDetails.h"
#import "ThirdParty.h"
#import "AccidentDetails.h"
#import "AddPhotos.h"
#import "InsuredDetailDiscription.h"
#import "VehicleDetailDescriptioin.h"
#import "ThirdPartyDescription.h"
#import "AccidentDetail.h"
#import "PhotosDiscription.h"
#import "ClaimWhatToDo.h"

@interface MoterVehicleIncident ()
{
    UIScrollView *scrollView;
    UIImageView *insuredImgView;
    UIImageView *vehicleImgView;
    UIImageView *trdPartyImgView;
    UIImageView *accidentImgView;
    UIImageView *photosImgView;
    InsuredDetailDiscription *dtl;
    VehicleDetailDescriptioin *vehicle;
    ThirdPartyDescription *trdPrty;
    AccidentDetail *acc;
    PhotosDiscription *photo;
    
    NSString * table1;
    
    int totalNumberOfPolicy;
}

@end

@implementation MoterVehicleIncident

@synthesize confirmationView;  // gourav 09 sep
@synthesize indecatorView;  // gourav 09 sep


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // self.view.backgroundColor=[UIColor redColor];
    // Do any additional setup after loading the view from its nib.
    
    
    //imageCount = 0;
    checForEmptyArray = 0;
    
    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 370)];
    scrollView.backgroundColor=[UIColor whiteColor];
    scrollView.contentSize = CGSizeMake(250, 10);
    if([Utility isIphone_5])
    {
        scrollView.frame = CGRectMake(0, 0, 320, 500);
        
    }

    
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appdelegate.navController = self.navigationController;
    
    UIImageView * bgimageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    bgimageView.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    if ([Utility isIphone_5]) {
        bgimageView.frame = CGRectMake(0, 0, 320, 568);
    }
    [self.view addSubview:bgimageView];
    [bgimageView release];
    
    
    
    [self initailizeNavigationBar];
    [self setShareButtons];
    
    self.navigationController.navigationBar.frame = CGRectMake(0, 20, 320, 44);

    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        [UIColor clearColor],NSBackgroundColorAttributeName,nil];
        
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        
  //  self.title = @"Vehicle Incident"; // Vehicle Incident
    
    
    self.screenName = @"Vehicle Incident";// Vehicle Incident
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    
    
    
    // gourav 09 sep start     for adding the confirmation view after sending the details by email
    self.confirmationView  = [[UIView alloc]init];
    [self.confirmationView setAlpha:0.8];
    [[self.confirmationView layer] setCornerRadius:10];
    
    self.confirmationView.frame = CGRectMake(30, 150, 260, 100);
    if ([Utility isIphone_5]) {
        self.confirmationView.frame = CGRectMake(30, 180, 260, 100);
    }
    [self.confirmationView setBackgroundColor:[UIColor darkGrayColor]];
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 160, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setNumberOfLines:2];
    [label setText:@"Your message has been sent"];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:DONE_BUTTON_IMAGE] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(100, 65, 60, 30)];
    
    [self.confirmationView addSubview:btn];
    [self.confirmationView addSubview:label];
    [self.confirmationView setHidden:YES];
    [self.view addSubview:self.confirmationView];
    [self.view bringSubviewToFront:self.indecatorView];
    
    [label release];
    
    // gourav 09 sep end



}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    locationManager.delegate = self;
    [locationManager startUpdatingHeading];
    [self setTikImage];
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    
}

-(void) setTikImage
{
    if(appdelegate.checkForInsured==YES)
    {
        insuredImgView.image=nil;
        [insuredImgView setImage:[UIImage imageNamed:@"tickImg.png"]];
    }
    else
    {
        insuredImgView.image=nil;
        [insuredImgView setImage:[UIImage imageNamed:@"InsureImg.png"]];
    }
    if(appdelegate.checkFprVehicle==YES)
    {
        vehicleImgView.image=nil;
        [vehicleImgView setImage:[UIImage imageNamed:@"tickImg.png"]];
    }
    else
    {
         vehicleImgView.image=nil;
        [vehicleImgView setImage:[UIImage imageNamed:@"vehicle.png"]];
    }

    if(appdelegate.checkForTrdParty==YES)
    {
        trdPartyImgView.image=nil;
        [trdPartyImgView setImage:[UIImage imageNamed:@"tickImg.png"]];
    }
    else
    {
        trdPartyImgView.image=nil;
        [trdPartyImgView setImage:[UIImage imageNamed:@"thirdPartyImg.png"]];
    }

    if(appdelegate.checkForAccident==YES)
    {
        accidentImgView.image=nil;
        [accidentImgView setImage:[UIImage imageNamed:@"tickImg.png"]];
    }
    else
    {
        accidentImgView.image=nil;
        [accidentImgView setImage:[UIImage imageNamed:@"acidentImg.png"]];
    }

    if(appdelegate.checkForPhotos==YES)
    {
        photosImgView.image=nil;
        [photosImgView setImage:[UIImage imageNamed:@"tickImg.png"]];
    }
    else
    {
        photosImgView.image=nil;
        [photosImgView setImage:[UIImage imageNamed:@"photo.png"]];
    }

    
    
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    //[button setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
    
    //call target
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(280, 0, 20, 20)];
    photoImage.image = [UIImage imageNamed:CLAIM_BUTTON_IMAGE];
    [button addTarget:self action:@selector(doExport) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
    
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 50)];
    label.textAlignment = UITextAlignmentCenter;
    [label setFont:[UIFont boldSystemFontOfSize:19.0f]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [label setText:@"Vehicle Incident"];
    [self.navigationItem setTitleView:label];
    [label release];
    
    
    
}

-(void)setShareButtons
{
    CGRect textShareFrame       = CGRectMake(20, 20+80+70, 280, 44);
    CGRect EmailShareFrame      = CGRectMake(20, 68+80+70+4, 280, 44);
    CGRect facebookShareFrame   = CGRectMake(20, 116+80+70+8, 280, 44);
    CGRect twitterShareFrame    = CGRectMake(20, 164+80+70+12, 280, 44);
    CGRect photosFrame          = CGRectMake(20, 212+80+70+16, 280, 44);
    CGRect finishFrame          = CGRectMake(20, 260+90+70+35, 280, 44);
    CGRect reset                = CGRectMake(20, 308+90+70+35, 280, 44);
    
    
    UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectMake(12, 0, 290, 150)];
    [webview setDelegate:self];
    [[webview scrollView] setScrollEnabled:NO];
    
   //   NSString *htmlString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body {font-family:\"Helvetica\"; font-size:12; color:black;}a:link {COLOR: 00728D;}</style></head><body style=\"margin:10\";>%@ <b><a href=\"Claims\">More claims information here.</a></b></body></html>", SUBHEADING_TEXT];
    
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body {font-family:\"Helvetica\"; font-size:12; color:black;}a:link {COLOR: orange;}</style></head><body style=\"margin:10\";>%@ </body></html>", MOTERVEHICLE_TEXT];
    
    [webview loadHTMLString:htmlString baseURL:nil];
    
    [scrollView addSubview:webview];
    [webview release];
    
//    UIView * customView1 = [[UIView alloc]initWithFrame:CGRectMake(20, 80+50+20, 280, 1)];
//    [customView1 setBackgroundColor:[UIColor lightGrayColor]];
//    [customView1 setAlpha:0.3f];
//    [scrollView addSubview:customView1];
//    [customView1 release];


    
    UIButton *textShareButton = [[UIButton alloc] initWithFrame:textShareFrame];
    [textShareButton setBackgroundImage:[UIImage imageNamed:OFFICEINFOTITLE_BUTTON] forState:UIControlStateNormal];
    [textShareButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [textShareButton addTarget:self action:@selector(doTextShare) forControlEvents:UIControlEventTouchUpInside];
    insuredImgView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 20, 20)];
    //insuredImgView.backgroundColor=[UIColor redColor];
    [insuredImgView setImage:[UIImage imageNamed:@"InsureImg.png"]];
    [textShareButton addSubview:insuredImgView];
    
    
    UIButton *emailShareButton = [[UIButton alloc] initWithFrame:EmailShareFrame];
    [emailShareButton setBackgroundImage:[UIImage imageNamed:OFFICEINFOTITLE_BUTTON] forState:UIControlStateNormal];
    [emailShareButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [emailShareButton addTarget:self action:@selector(doEmailShare) forControlEvents:UIControlEventTouchUpInside];
    vehicleImgView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 20, 20)];
   // vehicleImgView.backgroundColor=[UIColor redColor];
    [vehicleImgView setImage:[UIImage imageNamed:@"vehicle.png"]];
    [emailShareButton addSubview:vehicleImgView];

    
    
    UIButton *facebookShareButton = [[UIButton alloc] initWithFrame:facebookShareFrame];
    [facebookShareButton setBackgroundImage:[UIImage imageNamed:OFFICEINFOTITLE_BUTTON] forState:UIControlStateNormal];
    [facebookShareButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [facebookShareButton addTarget:self action:@selector(doFacebookShare) forControlEvents:UIControlEventTouchUpInside];
    trdPartyImgView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 20, 20)];
    //trdPartyImgView.backgroundColor=[UIColor redColor];
    [trdPartyImgView setImage:[UIImage imageNamed:@"thirdPartyImg.png"]];
   [facebookShareButton addSubview:trdPartyImgView];
    
    
    UIButton *twitterShareButton = [[UIButton alloc] initWithFrame:twitterShareFrame];
    [twitterShareButton setBackgroundImage:[UIImage imageNamed:OFFICEINFOTITLE_BUTTON] forState:UIControlStateNormal];
    [twitterShareButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [twitterShareButton addTarget:self action:@selector(doTwitterShare) forControlEvents:UIControlEventTouchUpInside];
   accidentImgView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 20, 20)];
    //accidentImgView.backgroundColor=[UIColor redColor];
    [accidentImgView setImage:[UIImage imageNamed:@"acidentImg.png"]];
    [twitterShareButton addSubview:accidentImgView];

    
    UIButton *photoButton = [[UIButton alloc] initWithFrame:photosFrame];
    [photoButton setBackgroundImage:[UIImage imageNamed:OFFICEINFOTITLE_BUTTON] forState:UIControlStateNormal];
    [photoButton setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [photoButton addTarget:self action:@selector(doAddImage) forControlEvents:UIControlEventTouchUpInside];
     photosImgView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 20, 20)];
    //photosImgView.backgroundColor=[UIColor redColor];
    [photosImgView setImage:[UIImage imageNamed:@"photo.png"]];
   [photoButton addSubview:photosImgView];

    
     sendButton = [[UIButton alloc] initWithFrame:finishFrame];
    [sendButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [sendButton setTitle:@"Finish & Send" forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [sendButton addTarget:self action:@selector(finish) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:sendButton];
    
    resetBtn = [[UIButton alloc] initWithFrame:reset];
    [resetBtn setBackgroundImage:[UIImage imageNamed:ADD_PHOTO_IMAGEee] forState:UIControlStateNormal];
    [resetBtn setTitle:@"Reset" forState:UIControlStateNormal];
    [[resetBtn titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [resetBtn addTarget:self action:@selector(resetBtn) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:resetBtn];


    
    
    [scrollView addSubview:textShareButton];
    [scrollView addSubview:emailShareButton];
    [scrollView addSubview:facebookShareButton];
    [scrollView addSubview:twitterShareButton];
    [scrollView addSubview:photoButton];
    
    [textShareButton release];
    [emailShareButton release];
    [facebookShareButton release];
    [twitterShareButton release];
    [photoButton release];
    
    
    
    //setlabels
    
    CGRect textLabelFrame = CGRectMake(70, 33+80+70, 200, 17);
    CGRect emailLabelFrame = CGRectMake(70, 81+80+70+4, 200, 17);
    CGRect facebookLabelFrame = CGRectMake(70, 129+80+70+8, 200, 17);
    CGRect twitterLabelFrame = CGRectMake(70, 177+80+70+12, 200, 17);
    CGRect photoLabelFrame = CGRectMake(70, 225+80+70+16, 200, 17);
    
    
    UILabel * textLabel = [[UILabel alloc]initWithFrame:textLabelFrame];
    [textLabel setBackgroundColor:[UIColor clearColor]];
    textLabel.text = @"Insured Details";
    [textLabel setTextColor:[UIColor whiteColor]];
    
    UILabel * emailLabel = [[UILabel alloc]initWithFrame:emailLabelFrame];
    [emailLabel setBackgroundColor:[UIColor clearColor]];
    emailLabel.text = @"Your Vehicle Details";
    [emailLabel setTextColor:[UIColor whiteColor]];

    UILabel * facebookLabel = [[UILabel alloc]initWithFrame:facebookLabelFrame];
    [facebookLabel setBackgroundColor:[UIColor clearColor]];
    facebookLabel.text = @"Third Party";
    [facebookLabel setTextColor:[UIColor whiteColor]];

    
    
    
    UILabel * twitterLabel = [[UILabel alloc]initWithFrame:twitterLabelFrame];
    [twitterLabel setBackgroundColor:[UIColor clearColor]];
    twitterLabel.text = @"Accident Details";
    [twitterLabel setTextColor:[UIColor whiteColor]];

    
    UILabel * photoLabel = [[UILabel alloc]initWithFrame:photoLabelFrame];
    [photoLabel setBackgroundColor:[UIColor clearColor]];
    photoLabel.text = @"Photos";
    [photoLabel setTextColor:[UIColor whiteColor]];


    
    
    [scrollView addSubview:textLabel];
    [scrollView addSubview:emailLabel];
    [scrollView addSubview:facebookLabel];
    [scrollView addSubview:twitterLabel];
    [scrollView addSubview:photoLabel];
    
    [textLabel release];
    [emailLabel release];
    [facebookLabel release];
    [twitterLabel release];
    [photoLabel release];
    
    
    [scrollView setScrollEnabled:YES];
    scrollView.contentSize = CGSizeMake(250, 212+310+50);
    if([Utility isIphone_5])
        scrollView.contentSize = CGSizeMake(250, 212+355+50);
    
    [self.view addSubview:scrollView];
    
    
    
    
}
-(void)finish
{
    //[self.indecatorView setHidden:NO];
   [self performSelector:@selector(sendmail) withObject:nil afterDelay:0.0];;

}

-(void)resetBtn
{
    appdelegate.positionOfPhotos=0;
    appdelegate.countOfAddPolicy=1;
    appdelegate.arrayInsuredetail=[[NSMutableArray alloc]init];
    appdelegate.arrayInsuredetail=nil;
    
    appdelegate.arrayVehicleDetail=[[NSMutableArray alloc]init];
    appdelegate.arrayVehicleDetail=nil;
    
    appdelegate.arrayThirdParty=[[NSMutableArray alloc]init];
    appdelegate.arrayThirdParty=nil;
    
    appdelegate.arrayAccidentDetail=[[NSMutableArray alloc]init];
    appdelegate.arrayAccidentDetail=nil;
    
    appdelegate.arrayPhotos=[[NSMutableArray alloc]init];
    appdelegate.arrayPhotos=nil;
    
    appdelegate.checkForInsured     =NO;
    appdelegate.checkForAccident    =NO;
    appdelegate.checkFprVehicle     =NO;
    appdelegate.checkForTrdParty    =NO;
    appdelegate.checkForPhotos      =NO;
    
    appdelegate.imageCount = 0; // gourav 09 sep
    
    table1 = @"RESET"; // gourav 09 sep
   
    [self setTikImage];
}

-(void)sendmail
{
    totalNumberOfPolicy = 0;
    
    MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:@"Motor Vehicle Claim"];
    
    NSArray *toRecipients = [NSArray arrayWithObject:@"claims@cmibinsurance.com.au"];
    
    
    [controller setToRecipients:toRecipients];
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
    NSString * bodyString;
    
   
    
    dtl     = [[InsuredDetailDiscription alloc]init];
    vehicle = [[VehicleDetailDescriptioin alloc]init];
    trdPrty = [[ThirdPartyDescription alloc]init];
    acc     = [[AccidentDetail alloc]init];
 //   photo=[[PhotosDiscription alloc]init];
    
    if(appdelegate.arrayInsuredetail!=nil||appdelegate.arrayVehicleDetail!=nil||appdelegate.arrayAccidentDetail!=nil||appdelegate.arrayPhotos!=nil||appdelegate.arrayThirdParty!=nil)
    {

        if([appdelegate.arrayInsuredetail count]>0)
        {
            dtl = [appdelegate.arrayInsuredetail objectAtIndex:0];
            [controller setCcRecipients:[NSArray arrayWithObject:dtl.email]];
        }
        if([appdelegate.arrayVehicleDetail count]>0)
        {
            vehicle = [appdelegate.arrayVehicleDetail objectAtIndex:0];
        }
        
        [self insuredAndVehicle];
        
        if([appdelegate.arrayThirdParty count]>0)
        {
            for(int i=0;i<[appdelegate.arrayThirdParty count];i++)
            {
                totalNumberOfPolicy++;
              trdPrty=[appdelegate.arrayThirdParty objectAtIndex:i];
                 [self thirdParty];
            }
            
        }
        
       
        
        if([appdelegate.arrayAccidentDetail count]>0)
        {
            acc=[appdelegate.arrayAccidentDetail objectAtIndex:0];
        }
        
        [self AccidentDtl];
        
        if([appdelegate.arrayPhotos count]>0)
        {

            photo = (PhotosDiscription *)[appdelegate.arrayPhotos objectAtIndex:0];
            
            [self addImageDetal:photo];
        }
        
        NSLog(@"JEET");
    }

    
    bodyString = @"";
    
    // bodyString=table1;
    if(![table1 isEqualToString:@"RESET"])  // gourav 09 sep
        bodyString=table1;

    
    
    if (appdelegate.imageCount == 0) {
        
        bodyString = [bodyString stringByAppendingString:@"</table>"];
        
    }
    else
    {
        
       // bodyString = [bodyString stringByAppendingString:@"<tr><td>No GPS data available for photos.</td></tr></table>"];
        for (int i = 0; i<appdelegate.imageCount; i++)
        {
            switch (i) {
                case 0:
                {
//                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage1.image);
                    [controller addAttachmentData:photo.photo1 mimeType:@"image/png" fileName:@"image1"];
                }
                    break;
                case 1:
                {
//                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage2.image);
                    [controller addAttachmentData:photo.photo2 mimeType:@"image/png" fileName:@"image2"];
                }
                    break;
                case 2:
                {
//                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage3.image);
                    [controller addAttachmentData:photo.photo3 mimeType:@"image/png" fileName:@"image3"];
                }
                    break;
                case 3:
                {
//                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage4.image);
                    [controller addAttachmentData:photo.photo4 mimeType:@"image/png" fileName:@"image4"];
                }
                    break;
                case 4:
                {
                    //                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage4.image);
                    [controller addAttachmentData:photo.photo5 mimeType:@"image/png" fileName:@"image4"];
                }
                    break;

                case 5:
                {
                    //                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage4.image);
                    [controller addAttachmentData:photo.photo6 mimeType:@"image/png" fileName:@"image4"];
                }
                    break;

                case 6:
                {
                    //                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage4.image);
                    [controller addAttachmentData:photo.photo7 mimeType:@"image/png" fileName:@"image4"];
                }
                    break;

                case 7:
                {
                    //                    NSData * imageData = UIImagePNGRepresentation(self.uploadImage4.image);
                    [controller addAttachmentData:photo.photo8 mimeType:@"image/png" fileName:@"image4"];
                }
                    break;

                    
                default:
                    break;
            }
            
        }
        
    }
    
    
    bodyString = [bodyString stringByAppendingString:@"</table>"];
    
    
    
    [controller setMessageBody:bodyString isHTML:YES];
        if (controller)
        [self presentModalViewController:controller animated:YES];
    
    
}
-(void) insuredAndVehicle
{
    if (!([dtl.firstName rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.firstName=@"";
    }
    if (!([dtl.surname rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.surname=@"";
    }
    if (!([dtl.DOB rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.DOB=@"";
    }
    if (!([dtl.phone rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.phone=@"";
    }
    if (!([dtl.email rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.email=@"";
    }
    if (!([dtl.address1 rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.address1=@"";
    }
    if (!([dtl.address2 rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.address2=@"";
    }
    if (!([dtl.postcode rangeOfString:@"null"].location==NSNotFound))
    {
        dtl.postcode=@"";
    }
    
    if (!([vehicle.make rangeOfString:@"null"].location==NSNotFound))
    {
        vehicle.make=@"";
    }
    if (!([vehicle.model rangeOfString:@"null"].location==NSNotFound))
    {
        vehicle.model=@"";
    }
    if (!([vehicle.rego rangeOfString:@"null"].location==NSNotFound))
    {
        vehicle.rego=@"";
    }
    if (!([vehicle.color rangeOfString:@"null"].location==NSNotFound))
    {
        vehicle.color=@"";
    }
    if (!([vehicle.policyNumber rangeOfString:@"null"].location==NSNotFound))
    {
        vehicle.policyNumber=@"";
    }
    
    table1 = @"";
    
    table1 = [NSString stringWithFormat:@"<table><tr><td>First Name:</td><td>%@</td></tr><tr><td>Surname:</td><td>%@</td></tr><tr><td>Date of Birth:</td><td>%@</td></tr><tr><td>Phone:</td ><td>%@</td></tr><tr><td>Email:</td><td>%@</td></tr><tr><td>Address 1:</td><td>%@</td></tr><tr><td>Address 2:</td><td>%@</td></tr><tr><td>Postcode:</td><td>%@</td></tr><tr><td>Make:</td><td>%@</td></tr><tr><td>Model & Year:</td><td>%@</td></tr><tr><td>Rego:</td><td>%@</td></tr><tr><td>Colour:</td><td>%@</td></tr><tr><td>Policy Number:</td><td>%@</td></tr>",dtl.firstName, dtl.surname, dtl.DOB, dtl.phone,dtl.email,dtl.address1,dtl.address2,dtl.postcode,vehicle.make,vehicle.model,vehicle.rego,vehicle.color,vehicle.policyNumber];
    
    table1 = [table1 stringByAppendingString:@"</table>"];
        
   }
-(void)thirdParty
{
    if (!([trdPrty.driverOfVehicle rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.driverOfVehicle=@"";
    }
    if (!([trdPrty.address rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.address=@"";
    }
    if (!([trdPrty.phoneNumber rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.phoneNumber=@"";
    }
    if (!([trdPrty.driver rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.driver=@"";
    }
    if (!([trdPrty.address2 rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.address2=@"";
    }
    if (!([trdPrty.vehicle rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.vehicle=@"";
    }
    if (!([trdPrty.registrationNumber rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.registrationNumber=@"";
    }
    if (!([trdPrty.policyNumber rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.policyNumber=@"";
    }
    if (!([trdPrty.LicenceNumber rangeOfString:@"null"].location==NSNotFound))
    {
        trdPrty.LicenceNumber=@"";
    }
    NSString * table2=@"";
    table2=[NSString stringWithFormat:@"<table><tr><td>POLICY NO:</td><td>%d</td></tr><tr><td>Driver of Vehicle:</td><td>%@</td></tr><tr><td>Address:</td><td>%@</td></tr><tr><td>Phone Number:</td><td>%@</td></tr><tr><td>Vehicle:</td><td>%@</td></tr><tr><td>Registration Number:</td><td>%@</td></tr><tr><td>Insurer & Policy Number:</td><td>%@</td></tr><tr><td>Licence Number:</td><td>%@</td></tr>",totalNumberOfPolicy,trdPrty.driverOfVehicle,trdPrty.address,trdPrty.phoneNumber,trdPrty.vehicle,trdPrty.registrationNumber,trdPrty.policyNumber,trdPrty.LicenceNumber];
    table2 = [table2 stringByAppendingString:@"</table>"];
    table1=[table1 stringByAppendingString:table2];
  
}
-(void) AccidentDtl
{
    if (!([acc.date rangeOfString:@"null"].location==NSNotFound))
    {
        acc.date=@"";
    }
    if (!([acc.time rangeOfString:@"null"].location==NSNotFound))
    {
        acc.time=@"";
    }
    if (!([acc.location rangeOfString:@"null"].location==NSNotFound))
    {
        acc.location=@"";
    }
    if (!([acc.PoliceOfficerAndStation rangeOfString:@"null"].location==NSNotFound))
    {
        acc.PoliceOfficerAndStation=@"";
    }
    if (!([acc.incidentDescription rangeOfString:@"null"].location==NSNotFound))
    {
        acc.incidentDescription=@"";
    }

    NSString * table3=@"";
    table3=[NSString stringWithFormat:@"<table><tr><td>Date:</td><td>%@</td></tr><tr><td>Time:</td><td>%@</td></tr><tr><td>Location:</td><td>%@</td></tr><tr><td>Police Officer and Station:</td><td>%@</td></tr><tr><td>Incident Description:</td><td>%@</td></tr>",acc.date,acc.time,acc.location,acc.PoliceOfficerAndStation,acc.incidentDescription];
     table3 = [table3 stringByAppendingString:@"</table>"];
    table1=[table1 stringByAppendingString:table3];
}


- (void)addImageDetal:(PhotosDiscription *)photos
{
   
    NSString * table4=@"";
    table4 = [NSString stringWithFormat:@"<table>"];
    for(int i = 0; i< [photos.latitudeArray count]; i++)
    {
        NSString *longStr = @"";
        NSString *dateTimeStr = @"";
        if([photos.longitudeArray count] >= i)
            longStr = [photos.longitudeArray objectAtIndex:i];
        
        //if(photos.dateTimeArray != nil && [photos.dateTimeArray count] > 0)  // gourav 09 sep
        if([photos.dateTimeArray count] >= i)
            dateTimeStr = [photos.dateTimeArray objectAtIndex:i];
        
        NSString *str = [NSString stringWithFormat:@"<br><br>  Image%i: Latitude = %@  Longitude: %@  DateTime: %@",i+1, [photos.latitudeArray objectAtIndex:i], longStr,dateTimeStr];
        table4 = [table4 stringByAppendingString:str];
    }
    
 //   table4 = [NSString stringWithFormat:@"<table><tr><td>Date:</td><td>%@</td></tr><tr><td>Time:</td><td>%@</td></tr><tr><td>Location:</td><td>%@</td></tr><tr><td>Police Officer and Station:</td><td>%@</td></tr><tr><td>Incident Description:</td><td>%@</td></tr>",acc.date,acc.time,acc.location,acc.PoliceOfficerAndStation,acc.incidentDescription];
    table4 = [table4 stringByAppendingString:@"</table>"];
    table1=[table1 stringByAppendingString:table4];

}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    // Notifies users about errors associated with the interface

    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Result: saved");
            break;
        case MFMailComposeResultSent:
           [self.confirmationView setHidden:NO];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Result: failed");
            break;
        default:
            NSLog(@"Result: not sent");
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
    
}

-(void)dismissView
{
    [self.confirmationView setHidden:YES];
}



-(void) doTextShare
{
    InsuredDetails *ins=[[InsuredDetails alloc]initWithNibName:@"InsuredDetails" bundle:nil];
    [self.navigationController pushViewController:ins animated:YES];
    
}

-(void)doEmailShare
  {
      YourVehicleDetails *detail = [[YourVehicleDetails alloc]initWithNibName:@"YourVehicleDetails" bundle:nil];
      [self.navigationController pushViewController:detail animated:YES];
  }
-(void) doFacebookShare
{
    ThirdParty *trd=[[ThirdParty alloc]initWithNibName:@"ThirdParty" bundle:nil];
    [self.navigationController pushViewController:trd animated:YES];
}
-(void)doTwitterShare
{
    AccidentDetails *dtl1=[[AccidentDetails alloc]initWithNibName:@"AccidentDetails" bundle:nil];
    [self.navigationController pushViewController:dtl1 animated:YES];
}
-(void) doAddImage
{
    /*
   // PhotosDiscription   *photoDesc  = [[PhotosDiscription alloc] init];
    
    //photoDesc  =(PhotosDiscription *)[appdelegate.arrayPhotos objectAtIndex:0];
    
    if(!photo.photo8)
    {
        AddPhotos *photo1=[[AddPhotos alloc]initWithNibName:@"AddPhotos" bundle:nil];
        [self.navigationController pushViewController:photo1 animated:YES];
    }
    
    //[photoDesc release];
     */
    
    AddPhotos *photo1=[[AddPhotos alloc]initWithNibName:@"AddPhotos" bundle:nil];
    [self.navigationController pushViewController:photo1 animated:YES];
}

-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)doExport{
    
}

#pragma mark-
#pragma mark UIWebViewDelegate methods-

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    if ([absoluteStr hasSuffix:@"Claims"])
    {
        int controllers = self.navigationController.viewControllers.count;
        for (int i = 0; i<controllers; i++) {
            if ([[[self.navigationController viewControllers] objectAtIndex:i] isKindOfClass:[ClaimWhatToDo class]]) {
                [self.navigationController popViewControllerAnimated:YES];
                return YES;
            }
        }
        
        
        ClaimWhatToDo *claimWhatToDo = [[ClaimWhatToDo alloc] initWithNibName:@"ClaimWhatToDo" bundle:nil];
        [self.navigationController pushViewController:claimWhatToDo animated:YES];
        [claimWhatToDo release];
    }
    
    return YES;
    
}


@end
