//
//  NewsDetail.h
//  W. R. Berkley Australia Brokerapp
//
//  Created by iPHTech12 on 17/02/14.
//
//--------VKV---------//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@class AppDelegate;

@interface NewsDetail : GAITrackedViewController<UIWebViewDelegate, UIGestureRecognizerDelegate>
{
    UIScrollView * scrollView ;
    AppDelegate *appDelegate;
    NSInteger index;
    UILabel *NewsHeading;
    UILabel *lbl;
    UIImageView *imgView;
    UILabel *heading;
    UILabel *NewsDate;
    UIWebView *webview;
    int newsCount;
    BOOL goForword; 
    
    
    NSString * numberStr;  // Gourav 04 Dec
    
    
}

@property (nonatomic, retain) NSString * numberStr;  // Gourav 04 Dec

@property(retain, nonatomic) NSMutableArray *sortedArray;

@property(strong, nonatomic) UIScrollView * scrollView ;
@property NSInteger index;
@property NSInteger indexForNews;

@property NSInteger indexOfRoomArray;
@property BOOL isComminfFromHome;

@end
