//
//  NewsDetail.m
//  W. R. Berkley Australia Brokerapp
//
//  Created by iPHTech12 on 17/02/14.
//
//-------VKV----------//

#import "NewsDetail.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "PressRoomInfo.h"

@interface NewsDetail ()

@end

@implementation NewsDetail
@synthesize scrollView;
@synthesize index;
@synthesize indexOfRoomArray;
@synthesize indexForNews;
@synthesize sortedArray;
@synthesize numberStr;  // Gourav 04 Dec
@synthesize isComminfFromHome;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

}

-(void)createNewsView
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor clearColor];
    UIImageView *backGroundImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 320, 460)];
    backGroundImgView.backgroundColor = [UIColor whiteColor];
    if([Utility isIphone_5])
    {
        backGroundImgView.frame = CGRectMake(0, 20, 320, 528);
    }
    [self.view addSubview:backGroundImgView];
    
    [self initailizeNavigationBar];
    self.screenName = @"News";
    sortedArray = [[NSMutableArray alloc] init];
    goForword = YES;
    
    
    // [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.scrollView = [[UIScrollView alloc]init];
    self.scrollView.frame = CGRectMake(18, 64, 310, 290);
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(18, 15 + 64, 290, 356);
    }
    
    self.scrollView.contentSize = CGSizeMake(315, 10);
    self.scrollView.backgroundColor=[UIColor clearColor];  // clearColor
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    index=0;
    
    int count = (int)[appDelegate.pressRoomArray count];
    
    indexOfRoomArray = count-1;
    
    
    if (!self.isComminfFromHome) {
        indexForNews = count -1;
    }
    
    PressRoomInfo *room=[appDelegate.pressRoomArray objectAtIndex:indexForNews];
    
    
    
    //Gourav 10 March
    
    float numberofLines = (room.title.length/30.0);
    if (numberofLines >1.0) {
        numberofLines++;
    }
    else
        numberofLines = 1.0;
    
    //Gourav 10 March
   
    
    NewsDate=[[UILabel alloc]initWithFrame:CGRectMake(5, 5, 290, 20)];//(7, 5, 310, 25)];//neha
    //[NewsDate setTextColor:[UIColor colorWithRed:60.0f/255.0f green:168.0f/255.0f blue:224.0f/255.0f alpha:1]];
    NewsDate.backgroundColor=[UIColor clearColor];
    [NewsDate setTextColor:[UIColor orangeColor]];
    NewsDate.font = [UIFont boldSystemFontOfSize:14.0f];//sumit
    NewsDate.text=room.date;
    
    
    
    NewsHeading=[[UILabel alloc]initWithFrame:CGRectMake(5, NewsDate.frame.origin.y + NewsDate.frame.size.height, 270, 22*numberofLines)];//(7, NewsDate.frame.origin.y + NewsDate.frame.size.height + 5 , 290, 40)];//neha
    NewsHeading.backgroundColor=[UIColor clearColor];   // clearColor
    [NewsHeading setTextColor:[UIColor blackColor]];
    NewsHeading.text=room.title;
    NewsHeading.lineBreakMode=NSLineBreakByWordWrapping;
    NewsHeading.numberOfLines=numberofLines; //Gourav 10 March
    //    //Gourav 10 March
    //    if (room.title.length >30) {
    //        [NewsHeading setFrame:CGRectMake(15, 10, 290, 25 * (room.title.length/30))];
    //    }
    //
    //    //Gourav 10 March
    [NewsHeading setFont:[UIFont systemFontOfSize:18.8f]];//neha
    //[NewsHeading setTextColor:[UIColor colorWithRed:60.0f/255.0f green:168.0f/255.0f blue:224.0f/255.0f alpha:1]];
    
    webview = [[UIWebView alloc]initWithFrame:CGRectMake(-2, NewsDate.frame.origin.y + NewsDate.frame.size.height+50, 285, 1520)];//neha
    [webview setDelegate:self];
    [[webview scrollView] setScrollEnabled:NO];
    
    [webview setBackgroundColor:[UIColor clearColor]];
    [[webview scrollView] setBackgroundColor:[UIColor clearColor]];
    [webview setOpaque:NO];
    
    
    NSString *htmlString = [NSString stringWithFormat:@"<div style='text-align:justify; font-size:12px;font-family:helvetica;'>%@",room.long1];//[NSString
    [webview loadHTMLString:htmlString baseURL:nil];
    
    [self.scrollView addSubview:webview];
    [webview release];
    
    
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:NewsHeading];
    [self.scrollView addSubview:NewsDate];
    
    CGSize fontSize = [htmlString sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(285, 1460) lineBreakMode:NSLineBreakByWordWrapping];
    
    [self initializeTheView];
    //    [self.scrollView addSubview:NewsDetail];
    [self setContentSizeOFScrollView:fontSize];
    
    //    self.scrollView.contentSize= CGSizeMake(280, fontSize.height-65);
    //
    //    if([Utility isIphone_5])
    //    {
    //        self.scrollView.contentSize= CGSizeMake(284, fontSize.height-75);
    //    }
    
}



-(void)initializeTheView
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    //    UIImage * NewsBtn = [UIImage imageNamed:LATEST_NEWS];
    //    [button setBackgroundImage:NewsBtn forState:UIControlStateNormal];
    [button setFrame:CGRectMake(290, 302, 29, 52)];
    button.backgroundColor= [UIColor clearColor];
    if ([Utility isIphone_5])
    {
        [button setFrame:CGRectMake(292, 377, 29, 58)];
    }
    //    [button addTarget:self action:@selector(moveToDescription:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //lbl=[[UILabel alloc] initWithFrame:CGRectMake(25,5, 200, 20)];
    lbl=[[UILabel alloc] initWithFrame:CGRectMake(25,13, 200, 20)];//neha
    lbl.backgroundColor=[UIColor clearColor];
    [lbl setFont:[UIFont boldSystemFontOfSize:10.0f]];
    [lbl setTextColor:[UIColor colorWithRed:60.0f/255.0f green:168.0f/255.0f blue:224.0f/255.0f alpha:1]];

    
    //ashish 1oct
    UIImageView *img_View=[[UIImageView alloc] initWithFrame:CGRectMake(-2, 120+85+85+30 + 44, 322, 70)];
    if ([Utility isIphone_5])
    {
        [img_View setFrame:CGRectMake(-2, 120+85+85+30 + 120, 325, 80)];
    }
    img_View.backgroundColor=[UIColor clearColor];
    UIImage * NewsHeadingImg = [UIImage imageNamed:NEWS_BLUE_HEADING];
    [img_View setImage:NewsHeadingImg];
    
    UISwipeGestureRecognizer *swipeLeftSide = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swapToNews:)];
    swipeLeftSide.delegate = self;
    [swipeLeftSide setDirection:UISwipeGestureRecognizerDirectionRight];
    [img_View addGestureRecognizer:swipeLeftSide];
    
    [img_View addSubview:lbl];
    //heading = [[UILabel alloc] initWithFrame:CGRectMake(25,20, 268, 45)];
    heading = [[UILabel alloc] initWithFrame:CGRectMake(25,3, 268, 78)];//neha
    heading.backgroundColor=[UIColor clearColor];
    [heading setTextColor:[UIColor whiteColor]];
    
    UITapGestureRecognizer *tapGetdure = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToDescription)];
    tapGetdure.delegate = self;
    tapGetdure.numberOfTapsRequired=1;
    [img_View setUserInteractionEnabled: YES];
    [img_View addGestureRecognizer:tapGetdure];
    
    
    
    
    //[button addSubview:lbl];
    // [lbl release];
    [self.view addSubview:button];
    // [button release];
    [img_View addSubview:heading];
    [heading release];
    [self.view addSubview:img_View];
    [img_View release];
    
    [self ParsingCompleted];
}


- (void)ParsingCompleted
{
    
    
    NSArray *array=[appDelegate.pressRoomArray  sortedArrayUsingComparator:^NSComparisonResult(id obj11,id obj12){
        
        PressRoomInfo *press = obj11;
        PressRoomInfo *press1 = obj12;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDate *date1 = [dateFormat dateFromString:press.date];
        
        NSDate *date2 =[dateFormat dateFromString:press1.date];
        
        id ob1 = date1;
        id ob2 = date2;
        //return [ob1 compare: ob2];
        if(ob1>ob2)
            return NSOrderedDescending;
        else if(ob1<ob2)
            return NSOrderedDescending;
        return NSOrderedSame;
    }];
    
    [sortedArray addObjectsFromArray:array];
    //index=index+1;
    
    
    // gourav 05 Nov start
//    if(indexForNews == [appDelegate.pressRoomArray count]-1)
//        indexForNews = 0;
//    else
//        indexForNews = indexForNews+1;
    
    
    
    
    if(indexForNews ==0)
        indexForNews = appDelegate.pressRoomArray.count-1;
    else
        indexForNews = indexForNews -1;
    
    
    //index=indexForNews+1;
    
    // gourav 05 Nov end
    
    //index=indexForNews+1;
    
    if([array count]>=index){
        
        
        PressRoomInfo *room = [array objectAtIndex:indexForNews];
        NSString *str=room.title;
        
        NSLog(@"Room title is the : %@ " , str);
        NSLog(@"%lu", (unsigned long)[array count]);
        
        heading.text=str;
        lbl.text = room.date;
        lbl.textColor = [UIColor orangeColor];
        heading.lineBreakMode=NSLineBreakByWordWrapping;
        heading.numberOfLines=0;
        heading.font=[UIFont systemFontOfSize:12];
    }
    
    indexOfRoomArray = indexForNews;  // gourav 05 Nov
    
    // appDelegate.pressRoomArray=[NSArray arrayWithArray:array];
}


-(void)initailizeNavigationBar
{
    self.navigationController.navigationBar.hidden = YES;
    
    UIImageView * navigationBarImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 322, 44)];
    navigationBarImgView.image = [UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE];
    [navigationBarImgView setUserInteractionEnabled:YES];
    [self.view addSubview:navigationBarImgView];
    

    
//    if(isComminfFromHome)
//    {
//        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 44, 44)];
//        [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
//        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
//        [navigationBarImgView addSubview:button];
//        [button release];
//    }
    
    
    if(true)
    {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(15, 6, 60, 30)];
        [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
        [navigationBarImgView addSubview:button];
        [button release];
    }
    
    
    UILabel *titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 320, 44)];
    titleLbl.text = @"News";
   // [titleLbl setFont:[UIFont fontWithName:@"Helvetica" size:18.0]];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.center = navigationBarImgView.center;
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLbl];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 50, 30, 50, 30)];
    imageview.image = [UIImage imageNamed:PRESS_ROOM_LOGO] ;
     [self.view addSubview:imageview];
    
    
    
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
//    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
//    
//    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
//    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem = backButton;
//    [button release];
//    [backButton release];
    
//    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 30, 30)];
//    imageview.image = [UIImage imageNamed:PRESS_ROOM_LOGO] ;
//    
//    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
//    self.navigationItem.rightBarButtonItem = backButton1;
//    [imageview release];
//    [backButton1 release];
}


- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [self createNewsView];
    
    
    [self initailizeNavigationBar];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)swapToNews:(UISwipeGestureRecognizer*)Gesture
{
     goForword = NO;
    
    //goForword = YES;

    BOOL isRepeat=NO;
    
    if(goForword)
    {
        if(index == (sortedArray.count-1))
        {
            index=0;
            isRepeat =YES;
        }
    }
    else
    {
        /*
        if(index < (sortedArray.count-1))
        {
            index=index+1;
            isRepeat =NO;
        }
        else
        {
            index=0;
            isRepeat =YES;
        }
         */
        
        // gourav 05 nov
        if(indexOfRoomArray == 0)
        {
            index= appDelegate.pressRoomArray.count -1;
            isRepeat =NO;
        }
        else
        {
            index=indexOfRoomArray-1;
            isRepeat =YES;
        }
        
        // end
    }
    
    
    if(index <= (sortedArray.count-1))
    {
        if(isRepeat && index ==0)
            index =0;
        else if(goForword)
            index = index +1;
        
        PressRoomInfo *room = [sortedArray objectAtIndex:index];
        NSString *str=room.title;
        heading.text=str;
        lbl.text = room.date;
        
        //[self setFrameOfNewsWithNewsTitle:room.title]; //Gourav 10 March
        
        NSLog(@"Top date is ----- %@,     index ==== %ld",room.date, (long)index);
        heading.lineBreakMode=NSLineBreakByWordWrapping;
        heading.numberOfLines=0;
        heading.font=[UIFont systemFontOfSize:12];
        
        NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
        style.lineHeightMultiple = 0.75;
        //   style.LineHeight = 10.f;
        NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
        heading.attributedText = [[NSAttributedString alloc] initWithString:str
                                                                 attributes:attributtes];
        
        if(heading.text.length >50)
            [heading setFrame:CGRectMake(25, 28-10-10, heading.frame.size.width, heading.frame.size.height)];
        else
            [heading setFrame:CGRectMake(25, 24-10-10, heading.frame.size.width, heading.frame.size.height)];
        
    }
    indexOfRoomArray = index;
}


-(void)moveToDescription
{
    
    PressRoomInfo *room_news_highlight;
    
    if(indexOfRoomArray >=0 && goForword)
    {
        //indexOfRoomArray = indexOfRoomArray -1;
        
        
        
        PressRoomInfo *room=[appDelegate.pressRoomArray objectAtIndex:indexOfRoomArray];
        NewsHeading.text=room.title;
        NewsDate.text=room.date;
        
        [self setFrameOfNewsWithNewsTitle:room.title]; //Gourav 10 March
        
        NSString *htmlString = [NSString stringWithFormat:@"<div style='text-align:justify; font-size:12px;font-family:helvetica;'>%@",room.long1];
        CGSize fontSize = [htmlString sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(285, 1490) lineBreakMode:NSLineBreakByWordWrapping];

        [self setContentSizeOFScrollView:fontSize];
        
        [webview loadHTMLString:htmlString baseURL:nil];
        
//        if(indexOfRoomArray ==0)
//            indexOfRoomArray = appDelegate.pressRoomArray.count-1;
//        else
//            indexOfRoomArray = indexOfRoomArray -1;
        
        //room_news_highlight=[appDelegate.pressRoomArray objectAtIndex:indexOfRoomArray-1];
        
    }
    else
    {     // only here
        goForword = NO;
        index = indexOfRoomArray;
        
//        // gourav 05 Nov
//        if(indexOfRoomArray ==0)
//            indexOfRoomArray = appDelegate.pressRoomArray.count-1;
        
        PressRoomInfo *room=[sortedArray objectAtIndex:indexOfRoomArray];
        NewsHeading.text=room.title;
        NewsDate.text=room.date;
        
        NSLog(@"heading date is ----- %@      index ===== %ld",room.date, (long)indexOfRoomArray);
        
        
        NSString *htmlString = [NSString stringWithFormat:@"<div style='text-align:justify; font-size:12px;font-family:helvetica;'>%@",room.long1];
        CGSize fontSize = [htmlString sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(285, 1000) lineBreakMode:NSLineBreakByWordWrapping];
        
         [self setContentSizeOFScrollView:fontSize];

        
        [webview loadHTMLString:htmlString baseURL:nil];
        
        //room_news_highlight=[appDelegate.pressRoomArray objectAtIndex:indexOfRoomArray];
        
    }
    
    if(indexOfRoomArray ==0)
        indexOfRoomArray = appDelegate.pressRoomArray.count-1;
    else
        indexOfRoomArray = indexOfRoomArray -1;
    
    
    // gourav 05 Nov start
//    if(indexOfRoomArray ==0)
//        indexOfRoomArray = appDelegate.pressRoomArray.count-1;
    
    room_news_highlight=[appDelegate.pressRoomArray objectAtIndex:indexOfRoomArray];

//    if(indexOfRoomArray ==0)
//        indexOfRoomArray = appDelegate.pressRoomArray.count-1;
    
    //PressRoomInfo *room_news_highlight=[appDelegate.pressRoomArray objectAtIndex:indexOfRoomArray-1];
    NSString *str=room_news_highlight.title;
    heading.text=str;
    
    lbl.text = room_news_highlight.date;
    heading.lineBreakMode=NSLineBreakByWordWrapping;
    heading.numberOfLines=0;
    heading.font=[UIFont systemFontOfSize:12];
    
    // gourav 05 Nov end
    
    
    if(index < (appDelegate.pressRoomArray.count-1) && goForword)
        index = index +1;
    
/*
    if(indexOfRoomArray >=1 && goForword)
    {
        PressRoomInfo *room=[appDelegate.pressRoomArray objectAtIndex:indexOfRoomArray-1];
        NSString *str=room.title;
        heading.text=str;
        
        lbl.text = room.date;
        heading.lineBreakMode=UILineBreakModeWordWrap;
        heading.numberOfLines=0;
        heading.font=[UIFont systemFontOfSize:12];
    }
 */
    
    
}

-(void) setContentSizeOFScrollView :(CGSize) fontSize
{
    
    self.scrollView.contentSize= CGSizeMake(280, fontSize.height+60+20 +30);
    
    if([Utility isIphone_5])
    {
        self.scrollView.contentSize= CGSizeMake(280, fontSize.height+60+20+30);
    }

}

-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
    NSLog(@"total number of view controllers = %lu",(unsigned long)self.navigationController.viewControllers.count);
    
    for (int i =0; i < self.navigationController.viewControllers.count ; i++) {
        NSLog(@"%@",[self.navigationController.viewControllers objectAtIndex:i]);
    }

    
   // [self.navigationController popToRootViewControllerAnimated:YES];
    
    if(!isComminfFromHome)
        [self.tabBarController setSelectedIndex:0];
    
}

// Gourav 04 Dec start


#pragma mark-
#pragma mark UIWebViewDelegate methods-

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

//    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
//        // Catch links
//        
//      return NO; // if you want to cancel the request, else YES
//        
//      
//    }
    
    self.numberStr = @"";
    
    NSURL *url = [request URL];
    NSString *absoluteStr = [url absoluteString];
    
    if ([absoluteStr hasPrefix:@"tel:"]) {
        self.numberStr = [absoluteStr stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        //self.numberStr = [absoluteStr stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
        
        NSString* str = [self.numberStr stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:str message:@"Would you like to call this number" delegate:self  cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
        [alert show];
        [alert release];
        return NO;
        
        
    }
    
    else if ([absoluteStr hasPrefix:@"http://"])
    {
        [[UIApplication sharedApplication] openURL:url];    //[NSURL URLWithString:@"http://google.com"]
        return NO;
        
    }
    
    return YES;

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.numberStr]];
    }
}


-(void)setFrameOfNewsWithNewsTitle:(NSString *)newsTitle
{

    //Gourav 10 March
    
    float numberofLines = (newsTitle.length/30.0);
    
    if (numberofLines >1.0) {
        numberofLines++;
    }
    else
        numberofLines = 1.0;
    
    [NewsHeading setFrame:CGRectMake(5, NewsHeading.frame.origin.y, NewsHeading.frame.size.width, 22*numberofLines)];
     NewsHeading.numberOfLines=numberofLines; //Gourav 10 March
    
    
    [NewsDate setFrame:CGRectMake(5, NewsHeading.frame.origin.y + NewsHeading.frame.size.height+5, 290, 20)];
   
    [webview setFrame:CGRectMake(-2, NewsDate.frame.origin.y + NewsDate.frame.size.height+20, 285, 1520)];
    
    //Gourav 10 March
    
   
//
//    
//    webview = [[UIWebView alloc]initWithFrame:CGRectMake(12, NewsDate.frame.origin.y + NewsDate.frame.size.height+20, 285, 780)];//neha
    
}

// Gourav 04 Dec end

@end
