//
//  OBrienGlassViewController.h
//  City Rural
//
//  Created by iPHTech2 on 29/06/13.
//
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"



@interface OBrienGlassViewController : GAITrackedViewController<UIAlertViewDelegate, UIGestureRecognizerDelegate>

{
    UIScrollView * scrollView;
    NSString * telephoneNumber;
}
@property(nonatomic, retain) NSString * telephoneNumber;
@property(nonatomic, retain) UIScrollView * scrollView;

-(void)initailizeNavigationBar;
-(void)createDescriptionView;
-(void)gotoWebsite;
- (void)openUrl:(UITapGestureRecognizer *) gr;
-(void)callBack;

@end
