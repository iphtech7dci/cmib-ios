//
//  OBrienGlassViewController.m
//  City Rural
//
//  Created by iPHTech2 on 29/06/13.
//
//

#import "OBrienGlassViewController.h"
#import "Constants.h"
#import "Utility.h"

@interface OBrienGlassViewController ()

@end

@implementation OBrienGlassViewController
@synthesize scrollView;
@synthesize telephoneNumber;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 30, 30)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];

//    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 245, 40)];
//    [label setBackgroundColor:[UIColor clearColor]];
//    [label setText:@"Insurers Assist Numbers"];
//    [label setFont:[UIFont boldSystemFontOfSize:17]];
//    [label adjustsFontSizeToFitWidth];
//    [label setTextColor:[UIColor whiteColor]];
//    self.navigationItem.titleView = label;
//    [label release];
    
    
    // gourav start 05 nov
    
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 200, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(-5, 0, 195, 44);
    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:17];
    titleView.textAlignment = NSTextAlignmentLeft;
    titleView.textColor = [UIColor whiteColor];
    titleView.shadowColor = [UIColor darkGrayColor];
    titleView.shadowOffset = CGSizeMake(0, -1);
    titleView.text = @"Insurers Assist Numbers";
    //titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    // gourav end 05 nov

    
    
    self.screenName = @"O'Brien Glass";
	[self initailizeNavigationBar];
    
    [self createDescriptionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)createDescriptionView
{
    
    //CGFloat titleLabelXalue = 140.0;
    //CGFloat titleLabelYValue = 30.0;
    //CGFloat titleLabelWidth = 180.0;
    // CGFloat titleLabelHeight = 40.0;
    CGFloat lineWidth = 320.0;
    CGFloat lineheight = 2.0;
    CGFloat largeSpace = 8.0;
    // CGFloat littleSpace = 0.0;
    CGFloat width = 280.0;
    CGFloat xValue = 0.0;
    CGFloat yValue = 100.0;
    CGFloat height = 15;
    //CGFloat fontSize = 12.0;
    
    NSString* logoname = [NSString stringWithFormat:@"obrien.png"];
    
    UIImage *img = [UIImage imageNamed:logoname];
    
    if (img) {
        
        UIImageView * logo = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 320, 70)];
        [logo setContentMode:UIViewContentModeScaleAspectFit];
        [logo setImage:img];
        [self.view addSubview:logo];
        [logo release];
        //xSpace = xSpace + 70;
    }
    else
    {
        
        UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 16, 320, 70)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:25]];
        titleLabel.adjustsFontSizeToFitWidth = YES;
        [titleLabel setText:@"O'Brien Glass"];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview:titleLabel];
        [titleLabel release];
    }
    
    UIImageView * shadow = [[UIImageView alloc]initWithFrame:CGRectMake(xValue, yValue-10, lineWidth, lineheight)];
    [shadow setImage:[UIImage imageNamed:SHADOWIMAGE]];
    [self.view addSubview:shadow];
    
    yValue = yValue+lineheight+largeSpace;
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(20, 102, 280, 440)];
    self.scrollView.contentSize = CGSizeMake(250, 10);
    
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(20, 102, 280, 440);
    }
    
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollView];
    
    yValue = 10;
    UILabel * phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue,width,height)];
    [phoneLabel setBackgroundColor:[UIColor clearColor]];
    [phoneLabel setFont:[UIFont boldSystemFontOfSize:12]];
    [phoneLabel setText:@"Phone"];
    [self.scrollView addSubview:phoneLabel];
    [phoneLabel release];
    yValue = yValue+height;
    
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
    UILabel * phoneNumber = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue,width,height)];
    [phoneNumber setBackgroundColor:[UIColor clearColor]];
    [phoneNumber setFont:[UIFont systemFontOfSize:12]];
    [phoneNumber setUserInteractionEnabled:YES];
    [phoneNumber setText:@"1800 023 715"];
    [phoneNumber addGestureRecognizer:gesture];
    [self.scrollView addSubview:phoneNumber];
    [phoneNumber release];
    yValue = yValue+height+largeSpace;
    

    
  UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(xValue, yValue, width, 40);
    [button setBackgroundImage:[UIImage imageNamed:@"goToWebsiteBtnImage.png"] forState:UIControlStateNormal];
    [button setTitle:@"Go to website" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(gotoWebsite) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.scrollView addSubview:button];
    
    yValue = yValue+40;
    
   

    
    NSString * description = [NSString stringWithFormat:@"<html><head><body style=\"font-family:Helvetica; font-size:12px;\">%@</body></head><html>", OBRIEN_TEXT];
   // CGSize fontSize1 = [description sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(width, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
    
    UIWebView *webview = [[UIWebView alloc]initWithFrame:CGRectMake(xValue-7, yValue, width, 160)];
    //[webview.scrollView setBounces:NO];
    [webview setOpaque:NO];
    [webview setBackgroundColor:[UIColor clearColor]];
    [webview.scrollView setScrollEnabled:NO];
    [webview loadHTMLString:description baseURL:nil];
    [self.scrollView addSubview:webview];
    [webview release];
    
    yValue = yValue+160+largeSpace;
    [self.scrollView setContentSize:CGSizeMake(width, yValue)];
    
}

-(void)gotoWebsite
{
    NSString * str = @"http://m.obrienglass.com.au/?utm_source=City+Rural+Brokers&utm_medium=mobile+app&utm_campaign=Broker+Apps";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}
- (void)openUrl:(UITapGestureRecognizer *) gr
{
    UIGestureRecognizer *rec = (UIGestureRecognizer *)gr;
    
    id hitLabel = [self.view hitTest:[rec locationInView:self.view] withEvent:UIEventTypeTouches];
    
    if ([hitLabel isKindOfClass:[UILabel class]]) {
        self.telephoneNumber = ((UILabel *)hitLabel).text;
        
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:self.telephoneNumber message:@"Would you like to call this number?" delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
        
        [alert show];
        [alert release];
        
        
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSString * str = [NSString stringWithFormat:@"tel://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
