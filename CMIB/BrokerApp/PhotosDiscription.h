//
//  PhotosDiscription.h
//  Optimus1
//
//  Created by iphtech7 on 22/04/14.
//
//

#import <Foundation/Foundation.h>

@interface PhotosDiscription : NSObject
{
    NSData * photo1;
    NSData * photo2;
    NSData * photo3;
    NSData * photo4;
    NSData * photo5;
    NSData * photo6;
    NSData * photo7;
    NSData * photo8;

    NSArray *longitudeArray;
    NSArray *latitudeArray;
    NSArray *dateTimeArray;
    
}

@property(nonatomic,strong)NSData * photo1;
@property(nonatomic,strong)NSData * photo2;
@property(nonatomic,strong)NSData * photo3;
@property(nonatomic,strong)NSData * photo4;
@property(nonatomic,strong)NSData * photo5;
@property(nonatomic,strong)NSData * photo6;
@property(nonatomic,strong)NSData * photo7;
@property(nonatomic,strong)NSData * photo8;

@property(nonatomic,strong) NSArray *longitudeArray;
@property(nonatomic,strong) NSArray *latitudeArray;
@property(nonatomic,strong) NSArray *dateTimeArray;

@end
