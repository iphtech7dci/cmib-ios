//
//  PhotosDiscription.m
//  Optimus1
//
//  Created by iphtech7 on 22/04/14.
//
//

#import "PhotosDiscription.h"

@implementation PhotosDiscription
@synthesize photo1,photo2,photo3,photo4,photo5,photo6,photo7,photo8;
@synthesize latitudeArray;
@synthesize longitudeArray;
@synthesize dateTimeArray;

- (id)init
{
    self  = [super init];
    {
        self.latitudeArray = [[NSArray alloc] init];
        self.longitudeArray = [[NSArray alloc] init];
        self.dateTimeArray = [[NSArray alloc] init];
    }
    return self;
}

@end
