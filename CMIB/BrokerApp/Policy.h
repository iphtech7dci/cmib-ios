//
//  Policy.h
//  BrokerApp
//
//  Created by iPHTech2 on 13/06/13.
//
//

#import <Foundation/Foundation.h>

@interface Policy : NSObject
{
    
    NSString *policyName;
    NSString *policyNumber;
    NSString * policyRelevant;

    NSNumber *pIndex;
    
}
@property(nonatomic, retain) NSString * policyRelevant;
@property(nonatomic, retain) NSString *policyName;
@property(nonatomic, retain) NSString *policyNumber;

@property(nonatomic, retain) NSNumber *pIndex;


@end
