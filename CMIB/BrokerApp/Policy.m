//
//  Policy.m
//  BrokerApp
//
//  Created by iPHTech2 on 13/06/13.
//
//

#import "Policy.h"

@implementation Policy

@synthesize policyName;
@synthesize policyNumber;
@synthesize policyRelevant;
@synthesize pIndex;

-(id)init
{
    
    self = [super init];
    
    if (self)
    {
        policyName = @"";
        policyNumber = @"";
        policyRelevant = @"";
        pIndex = [NSNumber numberWithInt:0];
        
    }
    
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"plocy Number %@ policy Name %@ policyRelevant %@", policyNumber, policyName, policyRelevant];
    
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:policyName forKey:@"policyName"];
    [encoder encodeObject:policyNumber forKey:@"policyNumber"];
    [encoder encodeObject:policyRelevant forKey:@"policyRelevant"];
    [encoder encodeObject:pIndex forKey:@"pIndex"];
   
}

-(id)initWithCoder:(NSCoder *)decoder
{
    
    self.policyName = [decoder decodeObjectForKey:@"policyName"];
    self.policyNumber = [decoder decodeObjectForKey:@"policyNumber"];
    self.policyRelevant = [decoder decodeObjectForKey:@"policyRelevant"];
    self.pIndex = [decoder decodeObjectForKey:@"pIndex"];
    
    return self;
}

@end
