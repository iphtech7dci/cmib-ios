//
//  PolicyView.h
//  BrokerApp
//
//  Created by iPHTech2 on 12/06/13.
//
//

#import <UIKit/UIKit.h>

@class Policy;
@class AppDelegate;

@protocol PolicyViewDelegate <NSObject>

-(void)deleteView:(Policy *)policy;
-(void)updatePolicy:(Policy *)newPolicy;
-(void)dismissKeyboard;
-(void)prevBtn;
-(void)nextBtn;
-(void)moveScrolUP:(int)ht;
-(void)moveScrollDown;
-(void)makeBigHightOfScrollView;
-(void)moveScrollDownForTextfield;

@end

@interface PolicyView : UIView <UITextFieldDelegate>
{
    Policy *myPolicy;
    id <PolicyViewDelegate> myDelegate;
    AppDelegate * appDelegate;
    UIToolbar * numberToolBar;
}

@property (nonatomic, retain) Policy *myPolicy;
@property (nonatomic, retain) id myDelegate;

-(id)initWithPolicy:(Policy *)policy;

@end
