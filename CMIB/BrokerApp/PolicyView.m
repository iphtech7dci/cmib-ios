//
//  PolicyView.m
//  BrokerApp
//
//  Created by iPHTech2 on 12/06/13.
//
//

#import "PolicyView.h"
#import "Policy.h"
#import "Constants.h"
#import "AppDelegate.h"

#define DELETE_BTN_1 21
#define DELETE_BTN_2 22
#define DELETE_BTN_3 23

#define POLICY_NAME_TAG 31
#define POLICY_NUMBER_TAG 32
#define POLICY_RELEVENT_TAG 33

#define POLICY_NAME_INDEX_TAG 41
#define POLICY_NUMBER_INDEX_TAG 42
#define POLICY_RELEVENT_INDEX_TAG 43

#define SELF_TAG 1000;

@implementation PolicyView
@synthesize myPolicy;
@synthesize myDelegate;

-(id)initWithPolicy:(Policy *)policy
{
    
    self = [super init];
    
    if (self)
    {
        
        if (policy) {
            self.myPolicy = policy;
        }
        
        numberToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)]  ;
        numberToolBar.barStyle = UIBarStyleBlackTranslucent;
        numberToolBar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)], nil];
        [numberToolBar sizeToFit];
        /*
         numberToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)] ;
         numberToolBar.barStyle = UIBarStyleBlackTranslucent;
         UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(prevBtn)];
         UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBtn)];
         UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
         UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)];
         NSArray *itemArray = [[NSArray alloc] initWithObjects:item3, spaceItem, item1, item2, nil];
         numberToolBar.items = itemArray;
         
         [numberToolBar sizeToFit];
         [item1 release];
         [item2 release];
         [item3 release];
         [itemArray release];
         */
        
        int tag = [policy.pIndex intValue]*SELF_TAG;
        [self setTag:tag];
        
        CGFloat width = 280;
        CGFloat viewHeight = 50;
        CGFloat largeSpace = 10;
        CGFloat xValue = 0;
        CGFloat yValue = 0;
        CGFloat lblHeight = 15;
        CGFloat textFieldHeight = 30;
        
        CGFloat labelImageXValue = 255.0;
        CGFloat labelImageDimention = 17.0;
        CGFloat deleteButtonXvalue = 255.0;
        CGFloat deleteButtonDimetions = 20.0;
        
        [self setBounds:CGRectMake(0, 0, width, 110)];
        
        UIView * policyNameView = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, width, viewHeight)];
        [policyNameView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView * labelImge2 = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
        [labelImge2 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
        [policyNameView addSubview:labelImge2];
        [labelImge2 release];
        
        UILabel * policyNameIndexLbl = [[UILabel alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
        [policyNameIndexLbl setBackgroundColor:[UIColor clearColor]];
        [policyNameIndexLbl setFont:[UIFont systemFontOfSize:12]];
        [policyNameIndexLbl setTextAlignment:NSTextAlignmentCenter];
        [policyNameIndexLbl setTag:(self.tag+ POLICY_NAME_INDEX_TAG)];
        [policyNameIndexLbl setTextColor:[UIColor whiteColor]];
        [policyNameIndexLbl setText:[NSString stringWithFormat:@"%d",[policy.pIndex intValue]]];
        [policyNameView addSubview:policyNameIndexLbl];
        [policyNameIndexLbl release];
        
        UILabel * policyNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(xValue, 0, width, lblHeight)];
        [policyNameLbl setBackgroundColor:[UIColor clearColor]];
        [policyNameLbl setFont:[UIFont boldSystemFontOfSize:12]];
        [policyNameLbl setText:@"Policy Name"];
        [policyNameView addSubview:policyNameLbl];
        [policyNameLbl release];
        
        
        UITextField *policyNameText = [[UITextField alloc]initWithFrame:CGRectMake(xValue, 17, width, textFieldHeight)];
        [policyNameText setTag:(self.tag+POLICY_NAME_TAG)];
        [policyNameText setText:policy.policyName];
        [policyNameText setPlaceholder:@"Text"];
        [policyNameText setDelegate:self];
        [policyNameText setBorderStyle:UITextBorderStyleRoundedRect];
        [policyNameView addSubview:policyNameText];
        [policyNameText release];
        
        
        if ([policy.pIndex intValue] != 1)
        {
            UIButton * deleteButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [deleteButton1 setTag:self.tag+DELETE_BTN_1];
            [deleteButton1 setFrame:CGRectMake(deleteButtonXvalue, 17+5,deleteButtonDimetions , deleteButtonDimetions)];
            [deleteButton1 setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
            [deleteButton1 addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
            [policyNameView addSubview:deleteButton1];
            
            
        }
        
        [self addSubview:policyNameView];
        [policyNameView release];
        
        
        yValue = yValue+viewHeight+largeSpace;
        
        UIView * policyNumberView = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, width, viewHeight)];
        [policyNumberView setBackgroundColor:[UIColor clearColor]];
        
        
        UIImageView * labelImge3 = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
        [labelImge3 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
        [policyNumberView addSubview:labelImge3];
        [labelImge3 release];
        
        UILabel * policyNumberIndexLbl = [[UILabel alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
        [policyNumberIndexLbl setBackgroundColor:[UIColor clearColor]];
        [policyNumberIndexLbl setFont:[UIFont systemFontOfSize:12]];
        [policyNumberIndexLbl setTextAlignment:NSTextAlignmentCenter];
        [policyNumberIndexLbl setTag:self.tag + POLICY_NUMBER_INDEX_TAG];
        [policyNumberIndexLbl setTextColor:[UIColor whiteColor]];
        [policyNumberIndexLbl setText:[NSString stringWithFormat:@"%d",[policy.pIndex intValue]]];
        [policyNumberView addSubview:policyNumberIndexLbl];
        
        [policyNumberIndexLbl release];
        
        
        UILabel * policyNumberLbl = [[UILabel alloc]initWithFrame:CGRectMake(xValue, 0, width, lblHeight)];
        [policyNumberLbl setBackgroundColor:[UIColor clearColor]];
        [policyNumberLbl setFont:[UIFont boldSystemFontOfSize:12.0f]];
        [policyNumberLbl setText:@"Policy Number"];
        [policyNumberView addSubview:policyNumberLbl];
        [policyNumberLbl release];
        
        
        // yValue = yValue+hieght+littleSpace;
        
        UITextField *policyNumberText = [[UITextField alloc]initWithFrame:CGRectMake(xValue, 17, width, textFieldHeight)];
        [policyNumberText setBorderStyle:UITextBorderStyleRoundedRect];
        [policyNumberText setText:policy.policyNumber];
        [policyNumberText setTag:POLICY_NUMBER_TAG + self.tag];
        [policyNumberText setPlaceholder:@"Text"];
        [policyNumberText setDelegate:self];
        [policyNumberView addSubview:policyNumberText];
        [policyNumberText release];
        
        if ([policy.pIndex intValue] != 1)
        {
            UIButton * deleteButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
            [deleteButton2 setTag: self.tag + DELETE_BTN_2];
            [deleteButton2 setFrame:CGRectMake(deleteButtonXvalue, 17+5,deleteButtonDimetions , deleteButtonDimetions)];
            [deleteButton2 setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
            [deleteButton2 addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
            [policyNumberView addSubview:deleteButton2];
        }
        
        [self addSubview:policyNumberView];
        [policyNumberView release];  /////********
        
        
        yValue = yValue+viewHeight+largeSpace;
        
        UIView * policyReleventView = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, width, viewHeight)];
        [policyReleventView setBackgroundColor:[UIColor clearColor]];
        
        
        UIImageView * labelImge4 = [[UIImageView alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
        [labelImge4 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
        [policyReleventView addSubview:labelImge4];
        [labelImge4 release];
        
        UILabel * policyReleventIndexLbl = [[UILabel alloc]initWithFrame:CGRectMake(labelImageXValue, 0, labelImageDimention, labelImageDimention)];
        [policyReleventIndexLbl setBackgroundColor:[UIColor clearColor]];
        [policyReleventIndexLbl setFont:[UIFont systemFontOfSize:12]];
        [policyReleventIndexLbl setTextAlignment:NSTextAlignmentCenter];
        [policyReleventIndexLbl setTag:self.tag + POLICY_RELEVENT_INDEX_TAG];
        [policyReleventIndexLbl setTextColor:[UIColor whiteColor]];
        [policyReleventIndexLbl setText:[NSString stringWithFormat:@"%d",[policy.pIndex intValue]]];
        [policyReleventView addSubview:policyReleventIndexLbl];
        
        [policyReleventIndexLbl release];
        
        
        UILabel * policyRelevntLbl = [[UILabel alloc]initWithFrame:CGRectMake(xValue, 0, width, lblHeight)];
        [policyRelevntLbl setBackgroundColor:[UIColor clearColor]];
        [policyRelevntLbl setFont:[UIFont boldSystemFontOfSize:12.0f]];
        [policyRelevntLbl setText:@"Relevant Insurer"];
        [policyReleventView addSubview:policyRelevntLbl];
        [policyRelevntLbl release];
        
        
        // yValue = yValue+hieght+littleSpace;
        
        UITextField *policyRelevantText = [[UITextField alloc]initWithFrame:CGRectMake(xValue, 17, width, textFieldHeight)];
        [policyRelevantText setBorderStyle:UITextBorderStyleRoundedRect];
        [policyRelevantText setTag:POLICY_RELEVENT_TAG + self.tag];
        [policyRelevantText setText:policy.policyRelevant];
        
        [policyRelevantText setPlaceholder:@"Text"];
        [policyRelevantText setDelegate:self];
        [policyReleventView addSubview:policyRelevantText];
        [policyRelevantText release];
        
        if ([policy.pIndex intValue] != 1)
        {
            UIButton * deleteButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
            [deleteButton3 setTag: self.tag + DELETE_BTN_3];
            [deleteButton3 setFrame:CGRectMake(deleteButtonXvalue, 17+5,deleteButtonDimetions , deleteButtonDimetions)];
            [deleteButton3 setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
            [deleteButton3 addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
            [policyReleventView addSubview:deleteButton3];
        }
        
        [self addSubview:policyReleventView];
        [policyReleventView release];
        
        
        
    }
    
    
    return self;
}


-(void)doDelete:(id)sender
{
    
    if ([myDelegate respondsToSelector:@selector(deleteView:)]) {
        [myDelegate deleteView:self.myPolicy];
    }
    
}

- (void)prevBtn
{
    if ([myDelegate respondsToSelector:@selector(dismissKeyboard)]) {
        [myDelegate prevBtn];
    }
    
}

- (void)nextBtn
{
    if ([myDelegate respondsToSelector:@selector(dismissKeyboard)]) {
        [myDelegate nextBtn];
    }
    
}


-(void)dismissKeyboard
{
    
    if ([myDelegate respondsToSelector:@selector(dismissKeyboard)]) {
        [myDelegate dismissKeyboard];
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.inputAccessoryView = numberToolBar;
    int tag  = textField.tag;
    
    
    
    
    if (textField.tag == self.tag+POLICY_NAME_TAG)
    {
        tag = tag-31;
    }
    else if(textField.tag == self.tag+POLICY_NUMBER_TAG)
    {
        tag = (tag-32);
    }
    else{
        tag = (tag-33);
    }
    [myDelegate moveScrolUP:tag];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    //[myDelegate moveScrollDown];
    if (textField.tag == self.tag+POLICY_NAME_TAG)
    {
        
        [self.myPolicy setPolicyName:textField.text];
    }
    else if(textField.tag == self.tag+POLICY_NUMBER_TAG)
    {
        [self.myPolicy setPolicyNumber:textField.text];
    }
    else
    {
        [self.myPolicy setPolicyRelevant:textField.text];
    }
    
    
    
    if ([myDelegate respondsToSelector:@selector(updatePolicy:)])
    {
        [myDelegate updatePolicy:self.myPolicy];
    }
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[myDelegate moveScrollDown];
    [myDelegate moveScrollDownForTextfield];
    return [textField resignFirstResponder];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [myDelegate makeBigHightOfScrollView];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (appDelegate.shouldAllowEditing) {
        return YES;
    }
    return NO;
}
@end
