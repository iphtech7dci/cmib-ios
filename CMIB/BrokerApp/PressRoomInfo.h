//
//  PressRoomInfo.h
//  W. R. Berkley Australia Brokerapp
//
//  Created by iPHTech12 on 14/02/14.
//
//---------VKV---------//

#import <Foundation/Foundation.h>

@interface PressRoomInfo : NSObject
{
    NSString *id1;
    NSString *date;
    NSString *title;
    NSString *short1;
    NSString *long1;
    NSString *clientid;
}
@property(strong,nonatomic) NSString *id1;
@property(strong,nonatomic) NSString *date;
@property(strong,nonatomic) NSString *title;
@property(strong,nonatomic) NSString *short1;
@property(strong,nonatomic) NSString *long1;
@property(strong,nonatomic) NSString *clientid;

@end
