//
//  ProfileInfo.h
//  BrokerApp
//
//  Created by iPHTech2 on 12/06/13.
//
//

#import <Foundation/Foundation.h>

@interface ProfileInfo : NSObject
{
    NSString * myBroker;
    NSString * brokerOffice;
    NSString * releventInsurer;
    
    NSData * policyData;
}
@property(nonatomic, retain)NSString * brokerOffice;
@property(nonatomic, retain)NSString * releventInsurer;

@property(nonatomic, retain)NSData * policyData;
@property(nonatomic, retain)  NSString * myBroker;
@end
