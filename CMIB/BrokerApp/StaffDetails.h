//
//  StaffDetails.h
//  BrokerApp
//
//  Created by iPHTech2 on 04/06/13.
//
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "GAITrackedViewController.h"
#import "OfficeInfo.h"


@class Staff;
@class AppDelegate;


@interface StaffDetails : GAITrackedViewController<UIAlertViewDelegate , UITableViewDataSource, UITableViewDelegate , MFMailComposeViewControllerDelegate>

{
    Staff * staf;
    UIScrollView * scrollView;
    NSString * telephoneNumber;
    UITableView * staffDetailTable;
    NSMutableArray * headerArray;
    NSMutableArray * contentArray;
    NSMutableArray *imageArray;
    NSString * addressString;
    AppDelegate *appDelgate;
    
    
}

@property(nonatomic,strong) OfficeInfo *officeInfoObj;

@property (nonatomic, retain)  NSString * addressString;
@property (nonatomic, retain) NSMutableArray * headerArray;
@property (nonatomic, retain) NSMutableArray * contentArray;
@property (nonatomic, retain) NSMutableArray * imageArray;
@property (nonatomic, retain) UITableView * staffDetailTable;
@property (nonatomic, retain) NSString * telephoneNumber;
@property(nonatomic, retain) UIScrollView * scrollView;
@property(nonatomic, retain) Staff * staf;
@property NSInteger *index;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain)  NSString * locationString;

-(void)initailizeNavigationBar;
-(void)initializeTheDetailView;
-(void)doLocation;
- (void)openUrl:(UITapGestureRecognizer *) gr;
- (void)openLink:(UITapGestureRecognizer *) gr;
-(void)sendMailToReceipent:(NSString *)receipent;
-(void)callBack;
-(void)doTwitter;
-(void)doLinkedin;
-(void)dealloc;



@end
