//
//  StaffDetails.m
//  BrokerApp
//
//  Created by iPHTech2 on 04/06/13.
//
//

#import "StaffDetails.h"
#import "Staff.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"
#import <QuartzCore/QuartzCore.h>


#define LABEL_COLOR  [UIColor colorWithRed:46.0f/255.0f green:46.0f/255.0f blue:46.0f/255.0f alpha:1.0f]    // gourav 28 Oct

@interface StaffDetails()
{
    float yValueBeforeTable; // gourav 10 Nov
    
}

@end

@implementation StaffDetails
@synthesize staf;
@synthesize scrollView;
@synthesize telephoneNumber;
@synthesize staffDetailTable;
@synthesize  headerArray;
@synthesize contentArray;
@synthesize addressString;
@synthesize index;
@synthesize locationString;
@synthesize imageArray;
@synthesize officeInfoObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setTintColor:[UIColor orangeColor]];
    [self.navigationController setNavigationBarHidden: NO animated:NO];
   
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 22, 22)];
    //imageview.image = [UIImage imageNamed:PROFILE_IMAGE] ;
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
    
    appDelgate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    /*
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleBlackTranslucent]; // UIStatusBarStyleLightContent
        UIImageView *statusBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        [statusBarView setBackgroundColor:[UIColor colorWithRed:231.0f/255.0f green:72.0f/255.0f blue:27.0f/255.0f alpha:1.0f]];
        [statusBarView setImage:[UIImage imageNamed:STAUS_BAR_RED]];
        [appDelgate.window addSubview:statusBarView];
    }
     */

    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initailizeNavigationBar];
    self.screenName = @"Key Staff";
    self.headerArray   = [[NSMutableArray alloc]init];
    self.contentArray  = [[NSMutableArray alloc]init];
    self.imageArray    = [[NSMutableArray alloc]init];
    self.addressString = [[NSString alloc]init];
    if (staf.phone) {
        [self.headerArray  addObject:[NSString stringWithFormat:@"%@",staf.phone]];
        [self.contentArray addObject:@"Direct Phone:"];
        [self.imageArray   addObject:PHONE_ICON_IMAGE];

    }
    
    if (staf.mobile) {
        [self.headerArray  addObject:[NSString stringWithFormat:@"%@",staf.mobile]];
        [self.contentArray addObject:@"Mobile:"];
        [self.imageArray   addObject:PHONE_ICON_IMAGE];
    }
    if (staf.email) {
        [self.headerArray  addObject:staf.email];
        [self.contentArray addObject:@"Email:"];
        [self.imageArray   addObject:@"emailPic.png"];
    }
    
//    if (staf.officeLocation && ![staf.officeLocation isEqualToString:@""]) {
//        NSString * str = staf.officeLocation;
//        self.addressString = str;
//        
//    }
//    if (staf.postalAddress_1) {
//        NSString * str = staf.postalAddress_1;
//                self.addressString = str;
//    }
//    if (staf.postalAddress_2) {
//        self.addressString = [NSString stringWithFormat:@"%@\n%@",self.addressString,staf.postalAddress_2];
//    }
//
//    if (staf.postalAdressSuburb) {
//        self.addressString = [NSString stringWithFormat:@"%@\n%@",self.addressString,staf.postalAdressSuburb];
//    }
//    if (staf.postalAddressState) {
//        self.addressString = [NSString stringWithFormat:@"%@\n%@",self.addressString,staf.postalAddressState];
//    }
//    if (staf.postalAddressPostalCode) {
//        self.addressString = [NSString stringWithFormat:@"%@ %@",self.addressString,staf.postalAddressPostalCode];
//    }
//    if ([self.addressString length]>0) {
//        [self.headerArray  addObject:self.addressString];
//        [self.contentArray addObject:@"Location:"];
//        [self.imageArray   addObject:LOCATION_ICON_IMAGE];
//    }
    
    if (officeInfoObj.address_1 && ![officeInfoObj.address_1 isEqualToString:@""]) {
        self.addressString = officeInfoObj.address_1;
        
    }
    if (officeInfoObj.addess_2 && ![officeInfoObj.addess_2 isEqualToString:@""]) {
        self.addressString = [NSString stringWithFormat:@"%@\n%@",self.addressString,officeInfoObj.addess_2];
    }
    if (officeInfoObj.suburb && ![officeInfoObj.suburb isEqualToString:@""]) {
        
        self.addressString = [NSString stringWithFormat:@"%@\n%@",self.addressString,officeInfoObj.suburb];
    }
    if (officeInfoObj.state && ![officeInfoObj.state isEqualToString:@""]) {  // 12 june
        
        NSString * string = officeInfoObj.state;
        if (officeInfoObj.postCode && ![officeInfoObj.postCode isEqualToString:@""])
        {
            string = [NSString stringWithFormat:@"%@ %@", string, officeInfoObj.postCode];
        }
        self.addressString = [NSString stringWithFormat:@"%@\n%@",self.addressString,string];
        
    }
    if ([self.addressString length]>0) {
            [self.headerArray  addObject:self.addressString];
            [self.contentArray addObject:@"Location:"];
            [self.imageArray   addObject:LOCATION_ICON_IMAGE];
       }

    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.title = @"Staff Directory";
    
    [self initializeTheDetailView];
   //[self.view setBackgroundColor:[UIColor colorWithRed:235.0/255.0f green:226.0/255.0f blue:233.0/255.0f alpha:1.0]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)initializeTheDetailView
{
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 0, 300, 380)];
    self.scrollView.contentSize = CGSizeMake(250, 10);
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(20, 18, 280, 440);
    }
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollView];
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    CGFloat yValue = 10;
    CGFloat width  = 280;
    CGFloat height = 15;
   // CGFloat textviewSubtractXvalue = 7.0;
    CGFloat litleSpace = 1.0;
    CGFloat largeSpace = 8.0;
    //CGFloat xValue = 20.0;
   // CGFloat lineHeight = 2.0;
   
   // CGFloat fontText = 12.0;
    CGFloat buttonHieght = 40;
    
    if ((staf.firstName && ![staf.firstName isEqualToString:@""]) || (staf.lastName && ![staf.lastName isEqualToString:@""])) {
        NSString * name = [NSString stringWithFormat:@"%@ %@",staf.firstName, staf.lastName];
        
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(14+18, yValue+1, 15, 20)];
        [imgView setImage:[UIImage imageNamed:PROFILE_ICON_IMAGE]];
        [self.scrollView addSubview:imgView];
        
        UILabel * positionHeader = [[UILabel alloc]initWithFrame:CGRectMake(36+21, yValue, width, height+5)];
        [positionHeader setTintColor:[UIColor blackColor]];
        [positionHeader setBackgroundColor:[UIColor clearColor]];
        [positionHeader setFont:[UIFont boldSystemFontOfSize:17]];
        [positionHeader setText:name];
       // [positionHeader setTextColor:[UIColor colorWithRed:132.0/255.0f green:18.0/255.0f blue:34.0/255.0f alpha:1.0]];
        //[positionHeader setTextColor:[UIColor colorWithRed:38.0/255.0f green:92.0/255.0f blue:124.0/255.0f alpha:1.0]];
        [positionHeader setTextColor:[UIColor blackColor]];
        
        [self.scrollView addSubview:positionHeader];
        [positionHeader release];
        
        yValue = yValue+height+litleSpace;
        
        UILabel * positionText = [[UILabel alloc]initWithFrame:CGRectMake(36+21, yValue, width-70, height+5)];
        [positionText setBackgroundColor:[UIColor clearColor]];
        [positionText setText:staf.position];
        //gourav 10 nov start
        
        if(staf.position.length >28)
        {
            
            [positionText setNumberOfLines:2];
            CGRect Frame = positionText.frame;
            Frame.size.height +=height+5;
            [positionText setFrame:Frame];

            [positionText setLineBreakMode:NSLineBreakByWordWrapping];
        
            height = Frame.size.height-10;
        }
            
        // gourav 10 nov end
        [positionText setFont:[UIFont boldSystemFontOfSize:15]];
        
        [positionText setTextColor:[UIColor blackColor]];//[UIColor darkGrayColor]];
        [self.scrollView addSubview:positionText];
        [positionText release];
        
        yValue = yValue+height+largeSpace;
        
        /*UIView * line1 = [[UIView alloc]initWithFrame:CGRectMake(xValue, yValue, width, lineHeight)];
        [line1 setBackgroundColor:[UIColor lightGrayColor]];
        [self.scrollView addSubview:line1];
        [line1 release];
        */
       // yValue = yValue+litleSpace+largeSpace;
    }
   // yValue = 20;
    
    
    yValueBeforeTable = yValue;   // gourav  10 Nov
       
   
    self.staffDetailTable = [[UITableView alloc]initWithFrame:CGRectMake(10, yValue+10, 260, 260) style:UITableViewStylePlain];
    self.staffDetailTable.delegate = self;
    self.staffDetailTable.dataSource = self;
    [self.staffDetailTable setBounces:NO];
    [self.staffDetailTable setScrollEnabled:NO];
    [self.staffDetailTable setBackgroundColor:[UIColor whiteColor]]; // whiteColor
    self.staffDetailTable.backgroundView = [[[UIView alloc] initWithFrame:self.staffDetailTable.bounds] autorelease];
    self.staffDetailTable.backgroundView.backgroundColor = [UIColor clearColor];
    [self.staffDetailTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];


    //self.staffDetailTable.layer.borderColor = [UIColor colorWithRed:0.0/255.0f green:114.0/255.0f blue:141.0/255.0f alpha:1.0].CGColor;
    
    self.staffDetailTable.layer.borderColor = [UIColor blackColor].CGColor;
    self.staffDetailTable.layer.borderWidth = 1.0f;
    
    [self.scrollView addSubview:self.staffDetailTable];
    
    if ([Utility isIphone_5]) {
        self.staffDetailTable.frame = CGRectMake(10, yValue+10, 260, 260);
    }

    int ht = 0;
    
    ht = ht+(60*[self.headerArray count]);
    
    if ([self.addressString length]>0) {
        
      CGSize fontSize = [self.addressString sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
       
        ht = ht+fontSize.height;
        
        if (![Utility isIphone_5]) {
            if ([self.headerArray count]>3) {
                self.staffDetailTable.frame = CGRectMake(10, yValue, 260, 270);
            }
        }
    }
    else
    {
        ht = ht + 40;
    }
    
    
    // gourav 10 nov start
    
    CGRect tableFrame = self.staffDetailTable.frame;
    tableFrame.size.height = ht - 20;
    
    [self.staffDetailTable setFrame:tableFrame];
    
    // gourav 10 nov end
    
    
    yValue = yValue+ht-10;
    
    
        
    UIButton *googleMapBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, yValue+40+3, width-20, buttonHieght)];
    if([Utility isIphone_5])
    {
        googleMapBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, yValue+40+3, width-20, buttonHieght)];
    }
    
    [googleMapBtn setBackgroundImage:[UIImage imageNamed:MAP_VIEW_IMAGE] forState:UIControlStateNormal];
    [googleMapBtn setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [googleMapBtn addTarget:self action:@selector(doLocation) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:googleMapBtn];
    [googleMapBtn release];
    
    
    yValue = yValue+buttonHieght+largeSpace;
    
    // gourav 11 Nov start
    
    if (staf.linkedin && ![staf.linkedin isEqualToString:@""]){
    
    UIButton *linkdenBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, yValue+40+3, width-20, buttonHieght)];
    if([Utility isIphone_5])
    {
        linkdenBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, yValue+40+3, width-20, buttonHieght)];
    }
    
    [linkdenBtn setBackgroundImage:[UIImage imageNamed:LINK_BUTTON_IMAGE] forState:UIControlStateNormal];
    [linkdenBtn setBackgroundImage:[UIImage imageNamed:MOTOR_VEHICLE_CLAIM] forState:UIControlStateHighlighted];
    [linkdenBtn addTarget:self action:@selector(doLinkedin) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:linkdenBtn];
    [linkdenBtn release];
        
    }

    

    yValue = yValue+buttonHieght+largeSpace;
    
    
    [self.scrollView setContentSize:CGSizeMake(width, yValue+70)];


}


#pragma mark-  TableView Delegate;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.headerArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UILabel * headerLabel;
    
    UILabel * textLabel;
    UIImageView *imgView;
    
    UITableViewCell *cell  = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InfoCell"];
        
        
        // gourav 11 Nov start
        
         CGRect frame;
         NSString *imageName = [self.imageArray objectAtIndex:indexPath.row];
        
        if([imageName isEqualToString:@"phonePic.png"])
        {
            frame= CGRectMake(5+18, 13, 12, 12);
            
        }
        else if ([imageName isEqualToString:@"emailPic.png"])
        {
            frame= CGRectMake(5+18, 14, 15, 11);
            
        }
        else if ([imageName isEqualToString:@"locationPic.png"])
        {
            frame= CGRectMake(5+18, 12, 11, 15);
            
        }

        imgView = [[UIImageView alloc]initWithFrame:frame];  //CGRectMake(5,10, 17, 22)];
        [imgView setImage:[UIImage imageNamed:imageName]];
        [cell.contentView addSubview:imgView];
        headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(27+21, 12, 280, 15)];
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:[UIFont boldSystemFontOfSize:12]];
        [headerLabel setText:[self.contentArray objectAtIndex:indexPath.row]];
        [headerLabel setTextColor:[UIColor blackColor
                                   ]];//[UIColor colorWithRed:46.0f/255.0f green:46.0f/255.0f blue:46.0f/255.0f alpha:1.0f]];
        
        [cell.contentView addSubview:headerLabel];
        [headerLabel release];

        if ([self.addressString length]>0) {
            if (indexPath.row<([self.headerArray count]-1)) {
                UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
                textLabel = [[UILabel alloc]initWithFrame:CGRectMake(27+21, 28, 210, 15)];
                [textLabel setBackgroundColor:[UIColor clearColor]];
                [textLabel setUserInteractionEnabled:YES];
                [textLabel setFont:[UIFont systemFontOfSize:12]];
                [textLabel addGestureRecognizer:gesture];
                [textLabel setText:[self.headerArray objectAtIndex:indexPath.row]];
                [cell.contentView addSubview:textLabel];
                [textLabel release];
                [gesture release];
            }
            else{
               UITextView * textLabel = [[UITextView alloc]initWithFrame:CGRectMake(22+21, 20, 210, 70)];
                [textLabel setBackgroundColor:[UIColor clearColor]];
                [textLabel setScrollEnabled:NO];
                [textLabel setFont:[UIFont systemFontOfSize:12]];
                [textLabel setUserInteractionEnabled:NO];
                [textLabel setText:[self.headerArray objectAtIndex:indexPath.row]];
                [cell.contentView addSubview:textLabel];
                [textLabel release];
            }

        }
        else{
            UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUrl:)];
            textLabel = [[UILabel alloc]initWithFrame:CGRectMake(27+21, 24, 210, 15)];
            [textLabel setBackgroundColor:[UIColor clearColor]];
            [textLabel setUserInteractionEnabled:YES];
            [textLabel setFont:[UIFont systemFontOfSize:12]];
            [textLabel addGestureRecognizer:gesture];
            [textLabel setText:[self.headerArray objectAtIndex:indexPath.row]];
            [cell.contentView addSubview:textLabel];
            [textLabel release];
            [gesture release];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
     cell.backgroundColor = [UIColor clearColor];
       return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize fontSize;
    if ([self.addressString length]>0) {
      fontSize = [self.addressString sizeWithFont:[UIFont systemFontOfSize:13.5] constrainedToSize:CGSizeMake(320, 1000) lineBreakMode:UILineBreakModeCharacterWrap];
        if (indexPath.row == ([self.headerArray count]-1)) {
            return (37+fontSize.height);
        }

    }
    return 37;
}


#pragma mark-  AlertView Delegate method

-(void)doLocation
{
//    if ([locationString isEqualToString:@"AllKeys"])
//    {
        if([staf.officeLocation isEqualToString:@"Sydney"])
            index=1;

        if([staf.officeLocation isEqualToString:@"Melbourne"])
            index=0;

//        if([staf.officeLocation isEqualToString:@"Adelaide"])
//            index=2;

        if([staf.officeLocation isEqualToString:@"Brisbane"])
            index=2;
//
//        if([staf.officeLocation isEqualToString:@"Perth"])
//            index=4;
//    }
//    else
//        index =0;


    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication  sharedApplication] delegate];

    NSMutableArray * array = [[NSMutableArray alloc] initWithArray:[appDelegate.appInfoDict objectForKey:OFFICE_INFO_KEY]];
    if ([array count]>0) {

        OfficeInfo * info = [array objectAtIndex:index];
//       for(int i=0; i<5;i++)
//        {
//            info = [array objectAtIndex:i];
//            NSLog(@"Link is%@",info.googleMapUrl);
//        }
        NSString * strLink = info.googleMapUrl;
        if([strLink  isEqual: @""])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"URL is not present on server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        else
        {
            strLink = [strLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL * url = [NSURL URLWithString:strLink];
            [[UIApplication sharedApplication]openURL:url];
        }
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSString * str = [NSString stringWithFormat:@"tel://%@",self.telephoneNumber];
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:str];
        [[UIApplication sharedApplication] openURL:url];
    }
}


- (void)openUrl:(UITapGestureRecognizer *) gr
{
    UIGestureRecognizer *rec = (UIGestureRecognizer *)gr;
    
    id hitLabel = [self.view hitTest:[rec locationInView:self.view] withEvent:UIEventTypeTouches];
    
    if ([hitLabel isKindOfClass:[UILabel class]]) {
        self.telephoneNumber = ((UILabel *)hitLabel).text;
        
        if ([self.telephoneNumber isEqualToString:staf.email]) {
            self.telephoneNumber = ((UILabel *)hitLabel).text;
            
            [self sendMailToReceipent:self.telephoneNumber];
        }
        else{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:self.telephoneNumber message:@"Would you like to call this number?" delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel", nil];
            [alert show];
            [alert release];
        }
    }
}


- (void)openLink:(UITapGestureRecognizer *) gr
{
    UIGestureRecognizer *rec = (UIGestureRecognizer *)gr;
    
    id hitLabel = [self.view hitTest:[rec locationInView:self.view] withEvent:UIEventTypeTouches];
    
    if ([hitLabel isKindOfClass:[UILabel class]]) {
        self.telephoneNumber = ((UILabel *)hitLabel).text;
        NSString * linkStr = [NSString stringWithFormat:@"http://%@",self.telephoneNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkStr]];
    }
}


-(UIView *)drawLine:(UILabel *)label
{
    CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:label.frame.size lineBreakMode:UILineBreakModeWordWrap];
    
    UIView *viewUnderline=[[[UIView alloc] init] autorelease];

    viewUnderline.frame = CGRectMake(label.frame.origin.x, (label.frame.origin.y + label.frame.size.height)-3, expectedLabelSize.width, 1);
    viewUnderline.backgroundColor=[UIColor blueColor];
    
    return viewUnderline;
}


-(void)sendMailToReceipent:(NSString *)receipent
{
    MFMailComposeViewController * controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    NSArray *toRecipients = [NSArray arrayWithObject:receipent];
        
    [controller setToRecipients:toRecipients];
    [controller.navigationBar setTintColor:[UIColor whiteColor]];
    if (controller)
        [self presentModalViewController:controller animated:YES];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
  [self dismissModalViewControllerAnimated:YES];  
}


#pragma mark-   IBAction Methods

-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doTwitter
{
    NSString * str = [staf.twitter stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)doLinkedin
{
    NSString * str = [staf.linkedin stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    // gourav start 11 nov
    if(![str hasPrefix:@"http"])
        str = [str stringByReplacingOccurrencesOfString:@"uk.linkedin" withString:@"http://uk.linkedin"];
     
    // gourav end 11 nov
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)dealloc
{
    [super dealloc];
    [staffDetailTable release];
}
@end
