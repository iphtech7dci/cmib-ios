//
//  StaffFilteredLocations.h
//  Northern City Insurance Brokers
//
//  Created by iPHTech2 on 31/07/13.
//
//

#import <UIKit/UIKit.h>

@class AppDelegate, OfficeInfo;

@interface StaffFilteredLocations : UIViewController
{
    NSString * locationString;
    NSInteger buttonHeightFromTop;
    UIScrollView * scrollView;
    NSMutableArray * officeListArray;
    AppDelegate *appdDelegate;
}
@property (nonatomic, retain) NSString * locationString;
@property (nonatomic, retain) NSMutableArray * officeListArray;
@property (nonatomic, retain) UIScrollView * scrollView;


-(void)initailizeNavigationBar;
-(void)moveToDescription:(UIButton* )sender;
-(void)generateOfficeButtinswithIndex:(NSInteger)index;
-(void)processdata;
@end
