//
//  StaffFilteredLocations.m
//  Northern City Insurance Brokers
//
//  Created by iPHTech2 on 31/07/13.
//
//

#import "StaffFilteredLocations.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "AllStaffList.h"
#import "OfficeInfo.h"
#import "Utility.h"

@interface StaffFilteredLocations ()

@end

@implementation StaffFilteredLocations

@synthesize locationString;
@synthesize officeListArray;
@synthesize scrollView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.title = @"Key Staff";
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_GREY_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 20, 20)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
     appdDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
  //  self.view.backgroundColor=[UIColor redColor];
    [self initailizeNavigationBar];
    // [self generateOfficeButtinswithIndex];
    [self processdata];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


#pragma mark-
#pragma mark Office list


-(void)processdata
{
    
    
    
    buttonHeightFromTop = 0;
    
    if ([self.officeListArray count]>1) {
        
        self.scrollView = [[UIScrollView alloc]init];
        self.scrollView.frame = CGRectMake(20, 20, 280, 350);
        self.scrollView.backgroundColor=[UIColor whiteColor];
        self.scrollView.contentSize = CGSizeMake(250, 10);
        
        self.scrollView.scrollEnabled =YES;
        [self.scrollView setShowsHorizontalScrollIndicator:NO];
        
        if([Utility isIphone_5])
        {
            self.scrollView.frame = CGRectMake(20, 20, 280, 440);
        }
        
        [self.view addSubview:self.scrollView];
        
        for (int i=0; i<[self.officeListArray count]; i++) {
            
            [self generateOfficeButtinswithIndex:i];
            buttonHeightFromTop = buttonHeightFromTop+44+5;
            self.scrollView.contentSize = CGSizeMake(280, buttonHeightFromTop);
        }
    }
    
    CGRect officeInfoButtonFrame = CGRectMake(0, buttonHeightFromTop, 280, 44);
    CGRect buttonTitle = CGRectMake(10, buttonHeightFromTop+10, 270, 20);
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.tag = [self.officeListArray count];
    UIImage * directNoImage = [UIImage imageNamed:OFFICEINFOTITLE_BUTTON];
    [button setBackgroundImage:directNoImage forState:UIControlStateNormal];
    [button setFrame:officeInfoButtonFrame];
    
    [button addTarget:self action:@selector(keyStaffBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * textLabel = [[[UILabel alloc]initWithFrame:buttonTitle] autorelease];
    textLabel.backgroundColor = [UIColor clearColor];
    

    textLabel.text = @"List all Key Staff";
    
    textLabel.textColor = [UIColor whiteColor];
    
    [self.scrollView addSubview:button];
    [self.scrollView addSubview:textLabel];
    
    buttonHeightFromTop = buttonHeightFromTop+44+5;
    self.scrollView.contentSize = CGSizeMake(280, buttonHeightFromTop);
    
}

-(void)generateOfficeButtinswithIndex:(NSInteger)index
{
    CGRect officeInfoButtonFrame = CGRectMake(0, buttonHeightFromTop, 280, 44);
    CGRect buttonTitle = CGRectMake(10, buttonHeightFromTop+10, 270, 20);
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.tag = index;
    UIImage * directNoImage = [UIImage imageNamed:OFFICEINFOTITLE_BUTTON];
    [button setBackgroundImage:directNoImage forState:UIControlStateNormal];
    [button setFrame:officeInfoButtonFrame];
    
    [button addTarget:self action:@selector(moveToDescription:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * textLabel = [[[UILabel alloc]initWithFrame:buttonTitle] autorelease];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.textColor = [UIColor whiteColor];
    OfficeInfo * info = [self.officeListArray objectAtIndex:index];
    textLabel.text = info.location;
//    if ([info.location isEqualToString:@"Melbourne"])
//    {
//        appdDelegate.checklocation = @"Melbourne";
//    }
//    else if ([info.location isEqualToString:@"Sydney"])
//    {
//        appdDelegate.checklocation = @"Sydney";
//    }
//    else if ([info.location isEqualToString:@"Brisbane"])
//    {
//        appdDelegate.checklocation = @"Brisbane";
//    }
    NSLog(@"text = %@", info.location);
    
    [self.scrollView addSubview:button];
    [self.scrollView addSubview:textLabel];
    
}



#pragma mark-
#pragma mark IBActions-
-(void)moveToDescription:(UIButton* )sender
{
    
    
    int index = sender.tag;
    
    
    if ([self.officeListArray count] > index)
    {
         appdDelegate.allkey = NO;
        OfficeInfo * info = [self.officeListArray objectAtIndex:index];
        self.locationString = info.location;
        if ([info.location isEqualToString:@"Melbourne"])
        {
            appdDelegate.checklocation = @"Melbourne";
        }
        else if ([info.location isEqualToString:@"Sydney"])
        {
            appdDelegate.checklocation = @"Sydney";
        }
        else if ([info.location isEqualToString:@"Brisbane"])
        {
            appdDelegate.checklocation = @"Brisbane";
        }
        else
        {
            appdDelegate.allkey = YES;
        }
        if (info) {
            AllStaffList * staffList = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
            staffList.locationSelected = self.locationString;
            staffList.isStaffDetail = YES;
            [self.navigationController pushViewController:staffList animated:YES];
            [staffList release];
        }
        
    }
    
    
    
}

-(void)callBack
{
    [[self navigationController] popViewControllerAnimated:YES];
}


-(void)keyStaffBtnClicked
{
    self.locationString = @"AllKeys";
    appdDelegate.allkey = YES;

    AllStaffList * staffList = [[AllStaffList alloc]initWithNibName:@"AllStaffList" bundle:nil];
    staffList.locationSelected = self.locationString;
//    if ([self.locationString isEqualToString:@"Melbourne"])
//    {
//        appdDelegate.checklocation = @"Melbourne";
//    }
//    else if ([self.locationString isEqualToString:@"Sydney"])
//    {
//        appdDelegate.checklocation = @"Sydney";
//    }
//    else if ([self.locationString isEqualToString:@"Brisbane"])
//    {
//        appdDelegate.checklocation = @"Brisbane";
//    }
    staffList.isStaffDetail = YES;
    [self.navigationController pushViewController:staffList animated:YES];
    [staffList release];
}
@end
