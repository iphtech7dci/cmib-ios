//
//  ThirdParty.m
//  Optimus1
//
//  Created by iphtech7 on 21/04/14.
//
//

#import "ThirdParty.h"

#import "ClaimsAndPhotos.h"
#import "Constants.h"
#import "ClaimWhatToDo.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"
#import "ThirdPartyDescription.h"

#define DRIVER_OF_VEHICLE_TEXTFIELD_TAG 200
#define ADDRESS_TEXTFIELD_TAG  201
#define PHONE_NUMBER_TEXTFIELD_TAG 202
#define DRIVER_TAG 203
#define ADDRESS2_TAG 204
#define VEHICLE_TAG 205
#define REG_NO_TAG 206
#define POLOCY_NO_TAG 207
#define LICENCE_TAG 208


@interface ThirdParty ()
{
    CGFloat yValue;
    CGFloat width ;
    CGFloat height;
    CGFloat litleSpace ;
    CGFloat largeSpace ;
    CGFloat xValue ;
    CGFloat textFieldHeight ;
    int valueForAddPolicy;
    int Demo;
    
    int currentTxtFldTag;
    
}

@end

@implementation ThirdParty
@synthesize line1,driverOfVehicle,address,phoneNumber,driver,address2,vehicle,registrationNumber,policyNumber,licenceNum;

@synthesize indecatorView;
@synthesize confirmationView;
@synthesize scrollView;
@synthesize latitude;
@synthesize longitude;
//@synthesize tabelView;
@synthesize officeArray;
@synthesize emailRecieverString;
@synthesize latiArray;
@synthesize longiArray;
//@synthesize photoDateTimeStr;
@synthesize dateTimeArray;
@synthesize longitude_libPhoto;
@synthesize latitude_libPhoto;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)initializeNavigationBar
{
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"Third Party";
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(270, 0, 25, 20)];
    photoImage.image = [UIImage imageNamed:@"whiteTrdPartyImg.png"];
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // self.view.backgroundColor=[UIColor redColor];
    appDelegate = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
    
    //imageCount = 0;
    shouldShowDropDownList = YES;
    checForEmptyArray = 0;
    self.latiArray = [[NSMutableArray alloc]init];
    self.longiArray = [[NSMutableArray alloc]init];
    self.dateTimeArray = [[NSMutableArray alloc]init];
    self.screenName = @"Claims & Photos";
    UIImageView * bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    if ([Utility isIphone_5]) {
        bgImage.frame = CGRectMake(0, 0, 320, 568);
    }
    bgImage.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    [self.view addSubview:bgImage];
    [bgImage release];
    
    [self initializeNavigationBar];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)] ;
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
  //  numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)], nil];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(PreviousBtnClicked:)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(NextBtnClicked:)];
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClicked:)];
    
    NSArray *itemArray = [[NSArray alloc] initWithObjects:item1, item2, spaceItem, item3, nil];
    numberToolbar.items = itemArray;
    [numberToolbar sizeToFit];
    [item1 release];
    [item2 release];
    [item3 release];
    [itemArray release];
    
    Demo = appDelegate.countOfAddPolicy;
    if(Demo>1)
    {
        appDelegate.countOfAddPolicy=1;
        for(int i=0;i<Demo;i++)
        {
            if(i==0)
            {
                [self makeScrollView];
                [self makeForm:i];
                sendButton = [[UIButton alloc] init];
                addButton = [[UIButton alloc]init];
                additionalPlicy=[[UILabel alloc]init];
                [self makeAddPolicy];
                [self makeBtnSave];
                [self assignSavedvalue:i];
            }
            else
            {
                [self addPolicyForValueAssign];
                [self assignSavedvalue:i];
            }
            
        }
    }
    else{
        
        [self makeScrollView];
        [self makeForm:0];
        sendButton = [[UIButton alloc] init];
        addButton = [[UIButton alloc]init];
        additionalPlicy=[[UILabel alloc]init];
        [self makeAddPolicy];
        [self makeBtnSave];
        [self assignSavedvalue:0];
        
    }
    if(![appDelegate.arrayThirdParty count]>0)
    {
        appDelegate.arrayThirdParty=[[NSMutableArray alloc]init];
    }
    

    
    
    self.confirmationView  = [[UIView alloc]init];
    [self.confirmationView setAlpha:0.8];
    [[self.confirmationView layer] setCornerRadius:10];
    
    self.confirmationView.frame = CGRectMake(30, 150, 260, 100);
    if ([Utility isIphone_5]) {
        self.confirmationView.frame = CGRectMake(30, 180, 260, 100);
    }
    [self.confirmationView setBackgroundColor:[UIColor darkGrayColor]];
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 160, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setNumberOfLines:2];
    [label setText:@"Your message has been sent"];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:DONE_BUTTON_IMAGE] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(105, 65, 60, 30)];
    
    [self.confirmationView addSubview:btn];
    [self.confirmationView addSubview:label];
    [self.confirmationView setHidden:YES];
    [self.view addSubview:self.confirmationView];
    [self.view bringSubviewToFront:self.indecatorView];
    
    [label release];
    
    // [self findCurrentLocation];
    //isCamera  = NO;
    
}
-(void) moveViewDown
{
    [UIView beginAnimations:@"ResizeForKeyboard" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x,64,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}



-(void)NextBtnClicked:(id)sender
{
    int tagN = currentTxtFldTag+992;
    
    UITextField *txtFld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag+1)];
    if(!txtFld)
        txtFld = (UITextField *)[self.view viewWithTag:tagN];
    
    [txtFld becomeFirstResponder];
}

-(void)PreviousBtnClicked:(id)sender
{
    int tagN = currentTxtFldTag-992;
    
    UITextField *txtFld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag-1)];
    if(!txtFld)
        txtFld = (UITextField *)[self.view viewWithTag:tagN];
    [txtFld becomeFirstResponder];
}

-(void)DoneBtnClicked:(id)sender
{
    [self moveScrollDownForTextfield];
   // [self moveViewDown];
    [self.view endEditing:YES];
}




-(void)moveScrollDownForTextfield
{
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    
    self.scrollView.contentSize = CGSizeMake(200, yValue);
    [UIView commitAnimations];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    locationManager.delegate = self;
    [locationManager startUpdatingHeading];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    if (locationManager) {
        locationManager.delegate = nil;
        [locationManager stopUpdatingLocation];
    }
    
    [self addObjectrsForPolicy]; // Gourav 11 Feb for solving the issue configured by the Ankit sir
    
}
#pragma mark
#pragma mark Form make-
-(void) makeScrollView
{
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 370)];
    self.scrollView.backgroundColor=[UIColor whiteColor];
    
    self.scrollView.delegate = self;
    
    self.scrollView.contentSize = CGSizeMake(250, 10);
    
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(0, 0, 320, 460);
    }
    
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollView];
    
    yValue = 20;
    width = 280;
    height = 15;
    litleSpace = 5.0;
    largeSpace = 8.0;
    xValue = 20.0;
    textFieldHeight = 30;
}


/*
-(void)makeForm:(int)tagVal
{
    NSLog(@"YVal:%f",yValue);
    NSLog(@"Tag Value:%i", tagVal);
    tagVal = tagVal*1000;
    CGFloat deleteButtonXvalue = 270.0;
    CGFloat deleteButtonDimetions = 20.0;
    
    UIView *formView = [[UIView alloc] initWithFrame:CGRectMake(0, yValue, 320, 522)];
    
    NSLog(@"YVal 1:%f",yValue);
    UILabel * driverOfVehicleLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [driverOfVehicleLable setBackgroundColor:[UIColor clearColor]];
    [driverOfVehicleLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [driverOfVehicleLable setText:@"Driver of Vehicle"];//astha
    [scrollView addSubview:driverOfVehicleLable];
    [driverOfVehicleLable release];
    
    UIImageView * labelImge1 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge1 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge1];
    [labelImge1 release];
    
    UILabel * index = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index setBackgroundColor:[UIColor clearColor]];
    [index setFont:[UIFont systemFontOfSize:12]];
    [index setTextAlignment:NSTextAlignmentCenter];
    index.textColor=[UIColor whiteColor];
    index.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index];
    [index release];
    
    
    yValue = yValue+height+litleSpace;
    
    NSLog(@"YVal 2:%f",yValue);
    
    self.driverOfVehicle = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.driverOfVehicle setPlaceholder:@"Driver of Vehicle"];//astha
    [self.driverOfVehicle setTag:(DRIVER_OF_VEHICLE_TEXTFIELD_TAG + tagVal)];
    [self.driverOfVehicle.layer setCornerRadius:2.0f];
    [self.driverOfVehicle setBorderStyle:UITextBorderStyleRoundedRect];
    [self.driverOfVehicle setDelegate:self];
    [self.scrollView addSubview:self.driverOfVehicle];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 3:%f",yValue);
    
    UILabel * addressLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [addressLable setBackgroundColor:[UIColor clearColor]];
    [addressLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [addressLable setText:@"Address"];//astha
    [scrollView addSubview:addressLable];
    [addressLable release];
    
    UIImageView * labelImge2 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge2 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge2];
    [labelImge2 release];
    
    UILabel * index2 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index2 setBackgroundColor:[UIColor clearColor]];
    [index2 setFont:[UIFont systemFontOfSize:12]];
    [index2 setTextAlignment:NSTextAlignmentCenter];
    index2.textColor=[UIColor whiteColor];
    index2.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index2];
    [index2 release];
    
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 4:%f",yValue);
    
    self.address = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.address setPlaceholder:@"Address"];///astha
    [self.address setTag:(ADDRESS_TEXTFIELD_TAG + tagVal)];
    [self.address.layer setCornerRadius:2.0f];
    [self.address setBorderStyle:UITextBorderStyleRoundedRect];
    [self.address setDelegate:self];
    // [self.surnameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.scrollView addSubview:self.address];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 5:%f",yValue);
    
    UILabel * phoneNumberLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [phoneNumberLable setBackgroundColor:[UIColor clearColor]];
    [phoneNumberLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [phoneNumberLable setText:@"Phone Number"];//astha
    [scrollView addSubview:phoneNumberLable];
    [phoneNumberLable release];
    
    UIImageView * labelImge3 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge3 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge3];
    [labelImge3 release];
    
    UILabel * index3 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index3 setBackgroundColor:[UIColor clearColor]];
    [index3 setFont:[UIFont systemFontOfSize:12]];
    [index3 setTextAlignment:NSTextAlignmentCenter];
    index3.textColor=[UIColor whiteColor];
    index3.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index3];
    [index3 release];
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 6:%f",yValue);
    
    self.phoneNumber = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.phoneNumber setPlaceholder:@"Phone Number"];///astha
    
    [self.phoneNumber.layer setCornerRadius:2.0f];
    [self.phoneNumber setBorderStyle:UITextBorderStyleRoundedRect];
    [self.phoneNumber setKeyboardType:UIKeyboardTypeNumberPad];
    [self.phoneNumber setTag:(PHONE_NUMBER_TEXTFIELD_TAG + tagVal)];
    [self.phoneNumber setDelegate:self];
    [self.scrollView addSubview:self.phoneNumber];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 7:%f",yValue);
    
    UILabel * driverLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [driverLable setBackgroundColor:[UIColor clearColor]];
    [driverLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [driverLable setText:@"Driver"];//astha
    [scrollView addSubview:driverLable];
    [driverLable release];
    
    
    UIImageView * labelImge4 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge4 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge4];
    [labelImge4 release];
    
    UILabel * index4 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index4 setBackgroundColor:[UIColor clearColor]];
    [index4 setFont:[UIFont systemFontOfSize:12]];
    [index4 setTextAlignment:NSTextAlignmentCenter];
    index4.textColor=[UIColor whiteColor];
    index4.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index4];
    [index4 release];
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 8:%f",yValue);
    
    self.driver = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.driver setPlaceholder:@"Driver"];///astha
    [self.driver.layer setCornerRadius:2.0f];
    [self.driver setBorderStyle:UITextBorderStyleRoundedRect];
    [self.driver setTag:(DRIVER_TAG + tagVal)];
    [self.driver setDelegate:self];
    [self.scrollView addSubview:self.driver];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 9:%f",yValue);
    
    UILabel * address2Lable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [address2Lable setBackgroundColor:[UIColor clearColor]];
    [address2Lable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [address2Lable setText:@"Address"];//astha
    [scrollView addSubview:address2Lable];
    [address2Lable release];
    
    UIImageView * labelImge5 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge5 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge5];
    [labelImge5 release];
    
    UILabel * index5 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index5 setBackgroundColor:[UIColor clearColor]];
    [index5 setFont:[UIFont systemFontOfSize:12]];
    [index5 setTextAlignment:NSTextAlignmentCenter];
    index5.textColor=[UIColor whiteColor];
    index5.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index5];
    [index5 release];
    
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 10:%f",yValue);
    
    self.address2 = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.address2 setPlaceholder:@"Address"];///astha
    [self.address2.layer setCornerRadius:2.0f];
    [self.address2 setBorderStyle:UITextBorderStyleRoundedRect];
    [self.address2 setTag:(ADDRESS2_TAG + tagVal)];
    [self.address2 setDelegate:self];
    [self.scrollView addSubview:self.address2];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 11:%f",yValue);
    
    UILabel * vehicleLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [vehicleLabel setBackgroundColor:[UIColor clearColor]];
    [vehicleLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
    [vehicleLabel setText:@"Vehicle"];//astha
    [scrollView addSubview:vehicleLabel];
    [vehicleLabel release];
    
    UIImageView * labelImge6 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge6 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge6];
    [labelImge6 release];
    
    UILabel * index6 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index6 setBackgroundColor:[UIColor clearColor]];
    [index6 setFont:[UIFont systemFontOfSize:12]];
    [index6 setTextAlignment:NSTextAlignmentCenter];
    index6.textColor=[UIColor whiteColor];
    index6.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index6];
    [index6 release];
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 12:%f",yValue);
    
    self.vehicle = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.vehicle setPlaceholder:@"Vehicle"];///astha
    [self.vehicle.layer setCornerRadius:2.0f];
    [self.vehicle setBorderStyle:UITextBorderStyleRoundedRect];
    [self.vehicle setTag:(VEHICLE_TAG + tagVal)];
    [self.vehicle setDelegate:self];
    [self.scrollView addSubview:self.vehicle];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 13:%f",yValue);
    
    UILabel * registrationNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [registrationNumberLabel setBackgroundColor:[UIColor clearColor]];
    [registrationNumberLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
    [registrationNumberLabel setText:@"Registration Number"];//astha
    [scrollView addSubview:registrationNumberLabel];
    [registrationNumberLabel release];
    
    UIImageView * labelImge7 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge7 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge7];
    [labelImge7 release];
    
    UILabel * index7 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index7 setBackgroundColor:[UIColor clearColor]];
    [index7 setFont:[UIFont systemFontOfSize:12]];
    [index7 setTextAlignment:NSTextAlignmentCenter];
    index7.textColor=[UIColor whiteColor];
    index7.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index7];
    [index7 release];
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 14:%f",yValue);
    
    self.registrationNumber = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.registrationNumber setPlaceholder:@"Registration Number"];///astha
    [self.registrationNumber.layer setCornerRadius:2.0f];
    [self.registrationNumber setBorderStyle:UITextBorderStyleRoundedRect];
    [self.registrationNumber setTag:(REG_NO_TAG + tagVal)];
    [self.registrationNumber setDelegate:self];
    [self.scrollView addSubview:self.registrationNumber];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 15:%f",yValue);
    
    UILabel * policyNumberLbl = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [policyNumberLbl setBackgroundColor:[UIColor clearColor]];
    [policyNumberLbl setFont:[UIFont boldSystemFontOfSize:13.0]];
    [policyNumberLbl setText:@"Insure & Policy Number"];//astha
    [scrollView addSubview:policyNumberLbl];
    [policyNumberLbl release];
    
    UIImageView * labelImge8 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge8 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge8];
    [labelImge8 release];
    
    UILabel * index8 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index8 setBackgroundColor:[UIColor clearColor]];
    [index8 setFont:[UIFont systemFontOfSize:12]];
    [index8 setTextAlignment:NSTextAlignmentCenter];
    index8.textColor=[UIColor whiteColor];
    index8.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index8];
    [index8 release];
    
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 16:%f",yValue);
    
    self.policyNumber = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.policyNumber setPlaceholder:@"Insure & Policy Number"];///astha
    [self.policyNumber.layer setCornerRadius:2.0f];
    [self.policyNumber setBorderStyle:UITextBorderStyleRoundedRect];
    [self.policyNumber setTag:(POLOCY_NO_TAG + tagVal)];
    [self.policyNumber setDelegate:self];
    [self.scrollView addSubview:self.policyNumber];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 17:%f",yValue);
    
    UILabel * licenceNUMber = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [licenceNUMber setBackgroundColor:[UIColor clearColor]];
    [licenceNUMber setFont:[UIFont boldSystemFontOfSize:13.0]];
    [licenceNUMber setText:@"Licence Number"];//astha
    [scrollView addSubview:licenceNUMber];
    [licenceNUMber release];
    
    UIImageView * labelImge9 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [labelImge9 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [scrollView addSubview:labelImge9];
    [labelImge9 release];
    
    UILabel * index9 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, yValue, 20, 20)];
    [index9 setBackgroundColor:[UIColor clearColor]];
    [index9 setFont:[UIFont systemFontOfSize:12]];
    [index9 setTextAlignment:NSTextAlignmentCenter];
    index9.textColor=[UIColor whiteColor];
    index9.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [scrollView addSubview:index9];
    [index9 release];
    
    
    yValue = yValue +height+litleSpace;
    NSLog(@"YVal 18:%f",yValue);
    
    self.licenceNum = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.licenceNum setPlaceholder:@"Licence Number"];///astha
    [self.licenceNum.layer setCornerRadius:2.0f];
    [self.licenceNum setBorderStyle:UITextBorderStyleRoundedRect];
    [self.licenceNum setTag:(LICENCE_TAG + tagVal)];
    [self.licenceNum setDelegate:self];
    [self.scrollView addSubview:self.licenceNum];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:1];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, yValue+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:deleteButton];
    }
    
    yValue = yValue+textFieldHeight+largeSpace;
    NSLog(@"YVal 19:%f",yValue);
    
    self.driverOfVehicle.inputAccessoryView = numberToolbar;
    self.address.inputAccessoryView = numberToolbar;
    self.phoneNumber.inputAccessoryView = numberToolbar;
    self.driver.inputAccessoryView = numberToolbar;
    self.address2.inputAccessoryView = numberToolbar;
    self.vehicle.inputAccessoryView = numberToolbar;
    self.registrationNumber.inputAccessoryView = numberToolbar;
    self.policyNumber.inputAccessoryView = numberToolbar;
    self.licenceNum.inputAccessoryView = numberToolbar;
    
    
     NSLog(@"YVal:%f",yValue);
    
}
*/

-(void)makeForm:(int)tagVal
{
 
    tagVal = tagVal*1000;
    CGFloat deleteButtonXvalue = 270.0;
    CGFloat deleteButtonDimetions = 20.0;
    
    CGFloat localYVal = 0.0;
    NSLog(@"YVal 2:%f",yValue);
    
    UIView *formView = [[UIView alloc] initWithFrame:CGRectMake(0, yValue, 320, 522)];
    
    //[formView setBackgroundColor:[UIColor redColor]];
    
    formView.tag = tagVal+5;
  
    
    UILabel * driverOfVehicleLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [driverOfVehicleLable setBackgroundColor:[UIColor clearColor]];
    [driverOfVehicleLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [driverOfVehicleLable setText:@"Driver of Vehicle"];//astha
    [formView addSubview:driverOfVehicleLable];
    [driverOfVehicleLable release];
    
    UIImageView * labelImge1 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge1 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge1];
    [labelImge1 release];
    
    UILabel * index = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index setBackgroundColor:[UIColor clearColor]];
    [index setFont:[UIFont systemFontOfSize:12]];
    [index setTextAlignment:NSTextAlignmentCenter];
    index.textColor=[UIColor whiteColor];
    index.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index];
    [index release];
    
    
    localYVal = localYVal+height+litleSpace;
    
 //   NSLog(@"YVal 2:%f",localYVal);
    
    self.driverOfVehicle = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.driverOfVehicle setPlaceholder:@"Driver of Vehicle"];//astha
    [self.driverOfVehicle setTag:(DRIVER_OF_VEHICLE_TEXTFIELD_TAG + tagVal)];
    [self.driverOfVehicle.layer setCornerRadius:2.0f];
    [self.driverOfVehicle setBorderStyle:UITextBorderStyleRoundedRect];
    [self.driverOfVehicle setDelegate:self];
    self.driverOfVehicle.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [formView addSubview:self.driverOfVehicle];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    
    
    localYVal = localYVal+textFieldHeight+largeSpace;
 //   NSLog(@"YVal 3:%f",localYVal);
    
    UILabel * addressLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [addressLable setBackgroundColor:[UIColor clearColor]];
    [addressLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [addressLable setText:@"Address"];//astha
    [formView addSubview:addressLable];
    [addressLable release];
    
    UIImageView * labelImge2 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge2 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge2];
    [labelImge2 release];
    
    UILabel * index2 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index2 setBackgroundColor:[UIColor clearColor]];
    [index2 setFont:[UIFont systemFontOfSize:12]];
    [index2 setTextAlignment:NSTextAlignmentCenter];
    index2.textColor=[UIColor whiteColor];
    index2.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index2];
    [index2 release];
    
    
    
    localYVal = localYVal +height+litleSpace;
 //   NSLog(@"YVal 4:%f",localYVal);
    
    self.address = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.address setPlaceholder:@"Address"];///astha
    [self.address setTag:(ADDRESS_TEXTFIELD_TAG + tagVal)];
    [self.address.layer setCornerRadius:2.0f];
    [self.address setBorderStyle:UITextBorderStyleRoundedRect];
    [self.address setDelegate:self];
    // [self.surnameTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [formView addSubview:self.address];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    
    localYVal = localYVal+textFieldHeight+largeSpace;
 //   NSLog(@"YVal 5:%f",localYVal);
    
    UILabel * phoneNumberLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [phoneNumberLable setBackgroundColor:[UIColor clearColor]];
    [phoneNumberLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [phoneNumberLable setText:@"Phone Number"];//astha
    [formView addSubview:phoneNumberLable];
    [phoneNumberLable release];
    
    UIImageView * labelImge3 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge3 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge3];
    [labelImge3 release];
    
    UILabel * index3 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index3 setBackgroundColor:[UIColor clearColor]];
    [index3 setFont:[UIFont systemFontOfSize:12]];
    [index3 setTextAlignment:NSTextAlignmentCenter];
    index3.textColor=[UIColor whiteColor];
    index3.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index3];
    [index3 release];
    
    
    localYVal = localYVal +height+litleSpace;
  //  NSLog(@"YVal 6:%f",localYVal);
    
    self.phoneNumber = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.phoneNumber setPlaceholder:@"Phone Number"];///astha
    
    [self.phoneNumber.layer setCornerRadius:2.0f];
    [self.phoneNumber setBorderStyle:UITextBorderStyleRoundedRect];
    [self.phoneNumber setKeyboardType:UIKeyboardTypeNumberPad];
    [self.phoneNumber setTag:(PHONE_NUMBER_TEXTFIELD_TAG + tagVal)];
    [self.phoneNumber setDelegate:self];
     self.phoneNumber.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [formView addSubview:self.phoneNumber];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    localYVal = localYVal+textFieldHeight+largeSpace;
 //   NSLog(@"YVal 7:%f",localYVal);
    
/*    UILabel * driverLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [driverLable setBackgroundColor:[UIColor clearColor]];
    [driverLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [driverLable setText:@"Driver"];//astha
    [formView addSubview:driverLable];
    [driverLable release];
    
    
    UIImageView * labelImge4 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge4 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge4];
    [labelImge4 release];
    
    UILabel * index4 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index4 setBackgroundColor:[UIColor clearColor]];
    [index4 setFont:[UIFont systemFontOfSize:12]];
    [index4 setTextAlignment:NSTextAlignmentCenter];
    index4.textColor=[UIColor whiteColor];
    index4.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index4];
    [index4 release];
    
    
    localYVal = localYVal +height+litleSpace;
 //   NSLog(@"YVal 8:%f",localYVal);
    
    self.driver = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.driver setPlaceholder:@"Driver"];///astha
    [self.driver.layer setCornerRadius:2.0f];
    [self.driver setBorderStyle:UITextBorderStyleRoundedRect];
    [self.driver setTag:(DRIVER_TAG + tagVal)];
    [self.driver setDelegate:self];
    [formView addSubview:self.driver];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    localYVal = localYVal+textFieldHeight+largeSpace;
 //   NSLog(@"YVal 9:%f",localYVal);
    
    UILabel * address2Lable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [address2Lable setBackgroundColor:[UIColor clearColor]];
    [address2Lable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [address2Lable setText:@"Address"];//astha
    [formView addSubview:address2Lable];
    [address2Lable release];
    
    UIImageView * labelImge5 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge5 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge5];
    [labelImge5 release];
    
    UILabel * index5 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index5 setBackgroundColor:[UIColor clearColor]];
    [index5 setFont:[UIFont systemFontOfSize:12]];
    [index5 setTextAlignment:NSTextAlignmentCenter];
    index5.textColor=[UIColor whiteColor];
    index5.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index5];
    [index5 release];
    
    
    
    localYVal = localYVal +height+litleSpace;
  //  NSLog(@"YVal 10:%f",localYVal);
    
    self.address2 = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.address2 setPlaceholder:@"Address"];///astha
    [self.address2.layer setCornerRadius:2.0f];
    [self.address2 setBorderStyle:UITextBorderStyleRoundedRect];
    [self.address2 setTag:(ADDRESS2_TAG + tagVal)];
    [self.address2 setDelegate:self];
    [formView addSubview:self.address2];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    localYVal = localYVal+textFieldHeight+largeSpace; */
//    NSLog(@"YVal 11:%f",localYVal);
    
    UILabel * vehicleLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [vehicleLabel setBackgroundColor:[UIColor clearColor]];
    [vehicleLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
    [vehicleLabel setText:@"Vehicle"];//astha
    [formView addSubview:vehicleLabel];
    [vehicleLabel release];
    
    UIImageView * labelImge6 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge6 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge6];
    [labelImge6 release];
    
    UILabel * index6 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index6 setBackgroundColor:[UIColor clearColor]];
    [index6 setFont:[UIFont systemFontOfSize:12]];
    [index6 setTextAlignment:NSTextAlignmentCenter];
    index6.textColor=[UIColor whiteColor];
    index6.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index6];
    [index6 release];
    
    
    localYVal = localYVal +height+litleSpace;
 //   NSLog(@"YVal 12:%f",localYVal);
    
    self.vehicle = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.vehicle setPlaceholder:@"Vehicle"];///astha
    [self.vehicle.layer setCornerRadius:2.0f];
    [self.vehicle setBorderStyle:UITextBorderStyleRoundedRect];
    [self.vehicle setTag:(VEHICLE_TAG + tagVal)];
    [self.vehicle setDelegate:self];
    self.vehicle.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [formView addSubview:self.vehicle];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    localYVal = localYVal+textFieldHeight+largeSpace;
 //   NSLog(@"YVal 13:%f",localYVal);
    
    UILabel * registrationNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [registrationNumberLabel setBackgroundColor:[UIColor clearColor]];
    [registrationNumberLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
    [registrationNumberLabel setText:@"Registration Number"];//astha
    [formView addSubview:registrationNumberLabel];
    [registrationNumberLabel release];
    
    UIImageView * labelImge7 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge7 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge7];
    [labelImge7 release];
    
    UILabel * index7 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index7 setBackgroundColor:[UIColor clearColor]];
    [index7 setFont:[UIFont systemFontOfSize:12]];
    [index7 setTextAlignment:NSTextAlignmentCenter];
    index7.textColor=[UIColor whiteColor];
    index7.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index7];
    [index7 release];
    
    
    localYVal = localYVal +height+litleSpace;
  //  NSLog(@"YVal 14:%f",localYVal);
    
    self.registrationNumber = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.registrationNumber setPlaceholder:@"Registration Number"];///astha
    [self.registrationNumber.layer setCornerRadius:2.0f];
    [self.registrationNumber setBorderStyle:UITextBorderStyleRoundedRect];
    [self.registrationNumber setTag:(REG_NO_TAG + tagVal)];
    [self.registrationNumber setDelegate:self];
     self.registrationNumber.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [formView addSubview:self.registrationNumber];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    localYVal = localYVal+textFieldHeight+largeSpace;
 //   NSLog(@"YVal 15:%f",localYVal);
    
    UILabel * policyNumberLbl = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [policyNumberLbl setBackgroundColor:[UIColor clearColor]];
    [policyNumberLbl setFont:[UIFont boldSystemFontOfSize:13.0]];
    [policyNumberLbl setText:@"Insurer & Policy Number"];//astha
    [formView addSubview:policyNumberLbl];
    [policyNumberLbl release];
    
    UIImageView * labelImge8 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge8 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge8];
    [labelImge8 release];
    
    UILabel * index8 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index8 setBackgroundColor:[UIColor clearColor]];
    [index8 setFont:[UIFont systemFontOfSize:12]];
    [index8 setTextAlignment:NSTextAlignmentCenter];
    index8.textColor=[UIColor whiteColor];
    index8.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index8];
    [index8 release];
    
    
    
    localYVal = localYVal +height+litleSpace;
 //   NSLog(@"YVal 16:%f",localYVal);
    
    self.policyNumber = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.policyNumber setPlaceholder:@"Insurer & Policy Number"];///astha
    [self.policyNumber.layer setCornerRadius:2.0f];
    [self.policyNumber setBorderStyle:UITextBorderStyleRoundedRect];
    [self.policyNumber setTag:(POLOCY_NO_TAG + tagVal)];
    [self.policyNumber setDelegate:self];
     self.policyNumber.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [formView addSubview:self.policyNumber];
    
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    localYVal = localYVal+textFieldHeight+largeSpace;
//    NSLog(@"YVal 17:%f",localYVal);
    
    UILabel * licenceNUMber = [[UILabel alloc]initWithFrame:CGRectMake(xValue, localYVal, width, height)];
    [licenceNUMber setBackgroundColor:[UIColor clearColor]];
    [licenceNUMber setFont:[UIFont boldSystemFontOfSize:13.0]];
    [licenceNUMber setText:@"Licence Number"];//astha
    [formView addSubview:licenceNUMber];
    [licenceNUMber release];
    
    UIImageView * labelImge9 = [[UIImageView alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [labelImge9 setImage:[UIImage imageNamed:POLICYNUMBERIMAGE]];
    [formView addSubview:labelImge9];
    [labelImge9 release];
    
    UILabel * index9 = [[UILabel alloc]initWithFrame:CGRectMake(width-10, localYVal, 20, 20)];
    [index9 setBackgroundColor:[UIColor clearColor]];
    [index9 setFont:[UIFont systemFontOfSize:12]];
    [index9 setTextAlignment:NSTextAlignmentCenter];
    index9.textColor=[UIColor whiteColor];
    index9.text=[NSString stringWithFormat:@"%d",appDelegate.countOfAddPolicy];
    [formView addSubview:index9];
    [index9 release];
    
    
    localYVal = localYVal +height+litleSpace;
 //   NSLog(@"YVal 18:%f",localYVal);
    
    self.licenceNum = [[UITextField alloc]initWithFrame:CGRectMake(xValue, localYVal, width, textFieldHeight)];
    [self.licenceNum setPlaceholder:@"Licence Number"];///astha
    [self.licenceNum.layer setCornerRadius:2.0f];
    [self.licenceNum setBorderStyle:UITextBorderStyleRoundedRect];
    [self.licenceNum setTag:(LICENCE_TAG + tagVal)];
    [self.licenceNum setDelegate:self];
     self.licenceNum.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [formView addSubview:self.licenceNum];
    
    if(appDelegate.countOfAddPolicy != 1)
    {
        UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteButton setTag:tagVal+5];
        [deleteButton setFrame:CGRectMake(deleteButtonXvalue, localYVal+4,deleteButtonDimetions , deleteButtonDimetions)];
        [deleteButton setImage:[UIImage imageNamed:DELETE_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
        [formView addSubview:deleteButton];
    }
    
    [self.scrollView addSubview:formView];
    [formView release];
    
    localYVal = localYVal+textFieldHeight+largeSpace;
 //   NSLog(@"YVal 19:%f",localYVal);
    
    self.driverOfVehicle.inputAccessoryView = numberToolbar;
    self.address.inputAccessoryView = numberToolbar;
    self.phoneNumber.inputAccessoryView = numberToolbar;
    self.driver.inputAccessoryView = numberToolbar;
    self.address2.inputAccessoryView = numberToolbar;
    self.vehicle.inputAccessoryView = numberToolbar;
    self.registrationNumber.inputAccessoryView = numberToolbar;
    self.policyNumber.inputAccessoryView = numberToolbar;
    self.licenceNum.inputAccessoryView = numberToolbar;
    
    
    NSLog(@"YVal:%f",localYVal);
    yValue +=localYVal;
    
}


-(void) makeAddPolicy
{
    [addButton setFrame:CGRectMake(xValue, yValue, 24, 24)];
    [addButton setImage:[UIImage imageNamed:ADD_POLICY_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [addButton setTintColor:[UIColor orangeColor]];
    [addButton addTarget:self action:@selector(addPolicy) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:addButton];
    
    additionalPlicy.frame = CGRectMake(xValue+30, yValue+2, 240, 20);
    [additionalPlicy setBackgroundColor:[UIColor clearColor]];
    [additionalPlicy setFont:[UIFont boldSystemFontOfSize:14]];
    [additionalPlicy setText:@"Additional 3rd Party"];
    [self.scrollView addSubview:additionalPlicy];
    // [additionalPlicy release];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    
}

-(void) makeBtnSave
{
    
    // addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    sendButton.frame =CGRectMake(xValue , yValue,width, 42);
    [sendButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [sendButton setTitle:@"Save & Go Back" forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [sendButton addTarget:self action:@selector(sendData) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:sendButton];
    
    yValue = yValue+50;
    self.scrollView.contentSize = CGSizeMake(200, yValue);
    
}
-(void) assignSavedvalue :(int )index
{
    if([appDelegate.arrayThirdParty count]!=0||appDelegate.arrayThirdParty==nil)
    {
        ThirdPartyDescription *trd = [[ThirdPartyDescription alloc]init];
        trd=[appDelegate.arrayThirdParty objectAtIndex:index];
        
        driverOfVehicle.text=trd.driverOfVehicle;
        address.text=trd.address;
        phoneNumber.text=trd.phoneNumber;
        driver.text=trd.driver;
        address2.text=trd.address2;
        vehicle.text=trd.vehicle;
        registrationNumber.text=trd.registrationNumber;
        policyNumber.text=trd.policyNumber;
        licenceNum.text=trd.LicenceNumber;
    }
    
}
-(void) addPolicyForValueAssign
{
    appDelegate.countOfAddPolicy++;
    yValue=yValue-35;
    [self makeForm:(appDelegate.countOfAddPolicy - 1)];
    [self makeAddPolicy];
    [self makeBtnSave];
    
}



-(void)addPolicy
{
    
    if(!([appDelegate.arrayThirdParty count]>0)||valueForAddPolicy==1)
    {
       // [self addObjectrsForPolicy];
    }
    
    
    appDelegate.countOfAddPolicy++;
    yValue = yValue-35;
    [self makeForm:(appDelegate.countOfAddPolicy -1 )];
    [self makeAddPolicy];
    [self makeBtnSave];
    valueForAddPolicy=1;
    
    
}

// gourav 11 nov

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
    NSLog(@"content offset %@",NSStringFromCGPoint(self.scrollView.contentOffset));
    NSLog(@"x===%f",self.scrollView.contentOffset.x);
    NSLog(@"y===%f",self.scrollView.contentOffset.y);
    
}

-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
//    NSLog(@"content offset %@",NSStringFromCGPoint(self.scrollView.contentOffset));
//    NSLog(@"x===%f",self.scrollView.contentOffset.x);
//    NSLog(@"y===%f",self.scrollView.contentOffset.y);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    if (self.scrollView.contentOffset.x < 320) {
//        NSLog(@"%@",NSStringFromCGPoint(self.scrollView.contentOffset));
//    }
//    else if (self.scrollView.contentOffset.x >= 320) {
//        NSLog(@"%@",NSStringFromCGPoint(self.scrollView.contentOffset));
//    }
    
    
     NSLog(@"content offset %@",NSStringFromCGPoint(self.scrollView.contentOffset));
     NSLog(@"x===%f",self.scrollView.contentOffset.x);
     NSLog(@"y===%f",self.scrollView.contentOffset.y);
}



#pragma mark-
#pragma mark UITextFieldDelegate Methods-
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.scrollView.contentSize = CGSizeMake(200, yValue+200);
    return YES;
}// return NO to disallow editing.

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    int txtTag = textField.tag%1000;
    
    switch (txtTag) {
        case ADDRESS_TEXTFIELD_TAG:
        {
            //  textField.inputAccessoryView = numberToolbar;
            
           // break;
        }
        case DRIVER_OF_VEHICLE_TEXTFIELD_TAG:
        {
            //  textField.inputAccessoryView = numberToolbar;
            
            //break;
            
        }
            
        case PHONE_NUMBER_TEXTFIELD_TAG:
        case DRIVER_TAG:
        case ADDRESS2_TAG:
        case VEHICLE_TAG:
        case REG_NO_TAG:
        case POLOCY_NO_TAG:
        case LICENCE_TAG:
        {
            
            offset = self.scrollView.contentOffset;
            
            //int t = offset.y;
            
            CGRect frame = textField.frame;
            
            NSLog(@"frame.orign.y====%f",frame.origin.y);
            
            NSLog(@"total forms = %d", appDelegate.countOfAddPolicy);

            
            int formcount=0;
            
            if([[textField superview] isKindOfClass:[UIView class]])
            {
                UIView *tempView = (UIView *) [textField superview];
                formcount =(tempView.tag-5)/1000;
                
                NSLog(@"form count = %d",formcount);
                
            }
        
//            int t = frame.origin.y + ((appDelegate.countOfAddPolicy-1) * 522)-(20*appDelegate.countOfAddPolicy);
            
              int t = frame.origin.y + (formcount * 522)-(20*formcount-1);
            
              [self moveScrolUP:t];
            
            /*
            if(t>0 ||txtTag==PHONE_NUMBER_TEXTFIELD_TAG||txtTag==DRIVER_TAG||txtTag==ADDRESS2_TAG||txtTag==VEHICLE_TAG||txtTag==REG_NO_TAG||txtTag==POLOCY_NO_TAG||txtTag==LICENCE_TAG)
            {
                if (t<50)
                {
                    t = t+100;//t+100;
                }
                else if(t>50)
                {
                    
                    if([Utility isIphone_5])
                    {
                        t = t + 50;
                    }
                    else
                    {
                        t = t +90;// 90
                    }
                }
                
                if(currentTxtFldTag > textField.tag)
                {
                    if([Utility isIphone_5])
                    {
                        t = t - 100;
                    }
                    else
                    {
                        t = t - 140;
                    }
                }
                
                // gourav 06 Nov start
                if(txtTag==LICENCE_TAG||txtTag==POLOCY_NO_TAG||txtTag == REG_NO_TAG)
                    // if(textField.tag == LICENCE_TAG||textField.tag == POLOCY_NO_TAG || textField.tag == REG_NO_TAG)
                    t= t+120;
                // gourav 06 Nov end
                
                
                //[self moveScrolUP:t];
                
                //offset.y = t; //gourav 11 Nov
            }
            
            
            */
            break;
            
            /*
             offset = self.scrollView.contentOffset;
             
             int t = offset.y;
             if (t<50)
             {
             t = t+80;//t+100;
             }
             else if(t>50)
             {
             
             if([Utility isIphone_5])
             {
             t = t + 50;
             }
             else
             {
             t = t +70;// 90
             }
             }
             
             if(currentTxtFldTag > textField.tag)
             {
             if([Utility isIphone_5])
             {
             t = t - 100;
             }
             else
             {
             t = t - 140;
             }
             }
             
             // gourav 06 Nov start
             if(txtTag==LICENCE_TAG||txtTag==POLOCY_NO_TAG||txtTag == REG_NO_TAG)
             // if(textField.tag == LICENCE_TAG||textField.tag == POLOCY_NO_TAG || textField.tag == REG_NO_TAG)
             t= t+120;
             // gourav 06 Nov end
             
             
             [self moveScrolUP:t];
             break;
             */
            
            
            
        }

            
            
            
    /*
    switch (txtTag) {
        case ADDRESS_TEXTFIELD_TAG:
        {
          //  textField.inputAccessoryView = numberToolbar;
            
            break;
        }
        case DRIVER_OF_VEHICLE_TEXTFIELD_TAG:
        {
          //  textField.inputAccessoryView = numberToolbar;

            break;
            
        }
            
        case PHONE_NUMBER_TEXTFIELD_TAG:
        case DRIVER_TAG:
        case ADDRESS2_TAG:
        case VEHICLE_TAG:
        case REG_NO_TAG:
        case POLOCY_NO_TAG:
        case LICENCE_TAG:
        {
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            
            if(t>0 ||txtTag==PHONE_NUMBER_TEXTFIELD_TAG||txtTag==DRIVER_TAG||txtTag==ADDRESS2_TAG||txtTag==VEHICLE_TAG||txtTag==REG_NO_TAG||txtTag==POLOCY_NO_TAG||txtTag==LICENCE_TAG)
            {
                if (t<50)
                {
                    t = t+100;//t+100;
                }
                else if(t>50)
                {
                
                    if([Utility isIphone_5])
                    {
                        t = t + 50;
                    }
                    else
                    {
                        t = t +90;// 90
                    }
                }
            
                if(currentTxtFldTag > textField.tag)
                {
                    if([Utility isIphone_5])
                    {
                        t = t - 100;
                    }
                    else
                    {
                        t = t - 140;
                    }
                }
            
                // gourav 06 Nov start
                if(txtTag==LICENCE_TAG||txtTag==POLOCY_NO_TAG||txtTag == REG_NO_TAG)
                // if(textField.tag == LICENCE_TAG||textField.tag == POLOCY_NO_TAG || textField.tag == REG_NO_TAG)
                    t= t+120;
                // gourav 06 Nov end
            
            
                [self moveScrolUP:t];
                
                offset.y = t; //gourav 11 Nov
            }
            break;

    
            
            
           /*
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50)
            {
                t = t+80;//t+100;
            }
            else if(t>50)
            {
                
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;// 90
                }
            }
            
            if(currentTxtFldTag > textField.tag)
            {
                if([Utility isIphone_5])
                {
                    t = t - 100;
                }
                else
                {
                    t = t - 140;
                }
            }
            
            // gourav 06 Nov start
            if(txtTag==LICENCE_TAG||txtTag==POLOCY_NO_TAG||txtTag == REG_NO_TAG)
           // if(textField.tag == LICENCE_TAG||textField.tag == POLOCY_NO_TAG || textField.tag == REG_NO_TAG)
                t= t+120;
            // gourav 06 Nov end
            
            
            [self moveScrolUP:t];
            break;
            */
            
            
            
        //}
            
    
            
            
            
            
  /*      case DRIVER_TAG:
        {
            
         //   textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                //t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                
                else
                {
                    t = t +70;
                }
                
            }
            
            [self moveScrolUP:t];
            break;
        }
        case ADDRESS2_TAG:
        {
            
         //   textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
                
                
            }
            
            [self moveScrolUP:t];
            break;
        }
        case VEHICLE_TAG:
        {
            
        //    textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
                
                
            }
            
            [self moveScrolUP:t];
            break;
        }
            
        case REG_NO_TAG:
        {
            
       //     textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                
                else
                {
                    t = t +70;
                }
                
            }
            
            [self moveScrolUP:t];
            break;
        }
        case POLOCY_NO_TAG:
        {
            
         //   textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
                
                
            }
            
            [self moveScrolUP:t];
            break;
        }
        case LICENCE_TAG:
        {
            
        //    textField.inputAccessoryView = numberToolbar;
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
            }
            else if(t>50) {
                // t = t +70;
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
                
                
            }
            
            [self moveScrolUP:t];
            break;
        }
            
       */
            
        default:
            break;
    }
    currentTxtFldTag = textField.tag;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([[textField text] length] == 0) {
        if([string isEqualToString:@" "] || [string isEqualToString:@"\""]){
            NSLog(@"blank space can not inserted at initial position");
            return NO;
        }
    }
    
    if ([string isEqualToString:@". "]) {
        return NO;
    }
    
    return YES;
}// return

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // self.scrollView.contentSize = CGSizeMake(200, yValue);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    [self moveScrollDownForTextfield];
    return YES;
}

#pragma mark-
#pragma mark movesScrollview UP and DOWN-
-(void)moveScrolUP:(int)ht
{
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, ht);
    [UIView commitAnimations];
    
    
    CGRect frame = self.scrollView.frame;
    frame.origin.y = -ht;
    
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView commitAnimations];
    
}

-(void)moveScrollDown
{
    CGRect frame = CGRectMake(20, 18, 290, 350);
    if ([Utility isIphone_5]) {
        frame = CGRectMake(20, 18, 290, 440);
    }
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, 0); //(0, offset.y) //yt
    [UIView commitAnimations];
    
}

#pragma mark-  UITextViewDelegate Methods-

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    offset = self.scrollView.contentOffset;
    int t;
    if ([Utility isIphone_5]) {
        t= 200;
    }
    else{
        t = 260;
    }
    [self moveScrolUP:t];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    return YES;
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
    
}

-(void)callBack
{
    //appDelegate.countOfAddPolicy=1;
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addObjectrsForPolicy
{
    [appDelegate.arrayThirdParty removeAllObjects];
    int tag;
    
    BOOL isAnyValueExist = NO;  // gourav 09 sep
    
    for(int i = 0 ; i< appDelegate.countOfAddPolicy; i++)
    {
        tag = i*1000;
        
        ThirdPartyDescription *trd = [[ThirdPartyDescription alloc] init];
        
        UITextField *txtFld = (UITextField *)[self.view viewWithTag:(DRIVER_OF_VEHICLE_TEXTFIELD_TAG+tag)];
        trd.driverOfVehicle = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(ADDRESS_TEXTFIELD_TAG+tag)];
        trd.address = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(PHONE_NUMBER_TEXTFIELD_TAG+tag)];
        trd.phoneNumber = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(DRIVER_TAG+tag)];
        trd.driver = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(ADDRESS2_TAG+tag)];
        trd.address2 = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(VEHICLE_TAG+tag)];
        trd.vehicle = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(REG_NO_TAG+tag)];
        trd.registrationNumber = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(POLOCY_NO_TAG+tag)];
        trd.policyNumber = txtFld.text;
        
        txtFld = (UITextField *)[self.view viewWithTag:(LICENCE_TAG+tag)];
        trd.LicenceNumber = txtFld.text;
        
        if(trd.driverOfVehicle.length >0 || trd.address.length >0 || trd.phoneNumber.length >0|| trd.driver.length >0|| trd.address2.length >0|| trd.vehicle.length >0|| trd.registrationNumber.length >0|| trd.policyNumber.length >0|| trd.LicenceNumber.length >0)  // gourav 09 sep
        {
            isAnyValueExist = YES;
        }
        
        [appDelegate.arrayThirdParty addObject:trd];
    
    }
    
    //gourav 09 sep start
    if(isAnyValueExist)
        appDelegate.checkForTrdParty = YES;
    else
        appDelegate.checkForTrdParty = NO;
    //gourav 09 sep end
    
}

-(void)sendData
{
 
    [self addObjectrsForPolicy];
    
   // appDelegate.checkForTrdParty=YES;
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)doDelete:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    int idx = (btn.tag-5)/1000;
    NSLog(@"Delete Tag:%d",idx);
    
    yValue = 426; //406
    
    for(id obj in [self.scrollView subviews])
    {
        if([obj isKindOfClass:[UIButton class]] || [obj isKindOfClass:[UIImageView class]] || [obj isKindOfClass:[UILabel class]])
        {
            continue;
        }
        UIView *v = (UIView *)obj;
        if(v.tag != 5)
            [v removeFromSuperview];
    }
    
    if([appDelegate.arrayThirdParty count] >idx)
        [appDelegate.arrayThirdParty removeObjectAtIndex:idx];
    appDelegate.countOfAddPolicy --;
   
    int count = appDelegate.countOfAddPolicy;
    
    for(int i = 1; i < count; i++)
    {
        yValue = yValue + 53;
        appDelegate.countOfAddPolicy = i+1;
        [self makeForm:i];
        [self assignSavedvalue:i];
    }
    [self makeAddPolicy];
    [self makeBtnSave];
}

@end
