//
//  ThirdPartyDescription.h
//  Optimus1
//
//  Created by iphtech7 on 22/04/14.
//
//

#import <Foundation/Foundation.h>

@interface ThirdPartyDescription : NSObject
{
    NSString * driverOfVehicle;
    NSString * address;
    NSString * phoneNumber;
    NSString * driver;
    NSString * address2;
    NSString * vehicle;
    NSString * registrationNumber;
    NSString * policyNumber;
    NSString * LicenceNumber;
    
}
@property(nonatomic,strong) NSString * driverOfVehicle;
@property(nonatomic,strong) NSString * address;
@property(nonatomic,strong)NSString * phoneNumber;
@property(nonatomic,strong)NSString * driver;
@property(nonatomic,strong)NSString * address2;
@property(nonatomic,strong)NSString * vehicle;
@property(nonatomic,strong)NSString * registrationNumber;
@property(nonatomic,strong)NSString * policyNumber;
@property(nonatomic,strong)NSString * LicenceNumber;
@end
