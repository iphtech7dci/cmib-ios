//
//  VehicleDetailDescriptioin.h
//  Optimus1
//
//  Created by iphtech7 on 22/04/14.
//
//

#import <Foundation/Foundation.h>

@interface VehicleDetailDescriptioin : NSObject
{
    NSString * make;
    NSString * model;
    NSString * rego;
    NSString * color;
    NSString * policyNumber;
}
@property (strong,nonatomic)NSString * make;
@property (strong,nonatomic)NSString * model;
@property (strong,nonatomic)NSString * rego;
@property (strong,nonatomic)NSString * color;
@property (strong,nonatomic)NSString * policyNumber;

@end

