//
//  ViewController.h
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "XMLReader.h"
#import "Utility.h"
#import "GAI.h"
#import "GAITracker.h"
#import "GAITrackedViewController.h"



@class AppDelegate;

@interface ViewController : GAITrackedViewController<UITabBarDelegate, UIGestureRecognizerDelegate>
{
    
    AppDelegate *appDelegate;
    IBOutlet  UIView *loadingView;
    UIImageView *steadfastImgView;
}

//-(void)openUrlLink;
-(void)doShare;
-(void)openWebsite;
-(void)doClaimPhoto;
-(void)doDirectNo;
-(void)claimWhatToDo;
-(void)doOfficeInfo;
-(void)geberateRequestWithStringURl:(NSString *)urlString withTag:(NSInteger)tag;
-(void)doParseDownloadedXML;
-(void)initializeTheView;
@end
