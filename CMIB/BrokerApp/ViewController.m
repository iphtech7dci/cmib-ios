//
//  ViewController.m
//  BrokerApp
//
//  Created by iPHTech2 on 25/05/13.
//
//

#import "ViewController.h"
#import "Constants.h"
#import "ShareFeature.h"
#import "AppDelegate.h"
#import "ClaimsAndPhotos.h"
#import "ClaimWhatToDo.h"
#import "DirectNumbers.h"
#import "Website.h"
#import "BrokerInfo.h"
#import "Claims.h"
#import "OfficesViewController.h"
#import "XmlParseForNews.h"
#import "PressRoomInfo.h"
#import "NewsDetail.h"


#define FIRST_ROW_BUTTONS_Y_AXIS  120//// astha   130
#define SECOND_ROW_BUTTONS_Y_AXIS 210////astha   215

@interface ViewController ()
{
    UILabel *NewsHeading;
    UILabel *lbl;
    UIImageView *imgView;
    int index;
}
@end
@implementation ViewController
-(void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [[GAI sharedInstance] defaultTracker];
    self.screenName = @"Main menu";
    //[self. sendView:@"Some name"];
    [self initializeTheView];
    [self.view bringSubviewToFront:loadingView];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // NSString *urlString = [Utility serverAddress];
   // NSString * urlStringOfNews=[Utility serverAddressOfDocument];
    [loadingView setHidden:NO];
    [self.view setUserInteractionEnabled:NO];
   // [self geberateRequestWithStringURl:urlString];
   // [self geberateRequestWithStringURl:urlString withTag:1];
   // [self geberateRequestWithStringURl:urlStringOfNews withTag:2];
    //Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleBlackTranslucent]; // UIStatusBarStyleLightContent
        UIImageView *statusBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        //[statusBarView setBackgroundColor:[UIColor clearColor]];
        [statusBarView setImage:[UIImage imageNamed:STAUS_BAR_DARK_GREY]];
        [appDelegate.window addSubview:statusBarView];
    }
    
    // Gourav  June 05 Start
    //sqlite3	      *  database= NULL;
    
    [appDelegate createEditableCopyOfDatabaseIfNeeded];
    [appDelegate openDatabaseConnection];
    appDelegate.dataHandle = [[DataHandle alloc] initWithDbConnection:appDelegate.database];
    
    
    NSString * urlString = [Utility serverAddress];
    NSString * urlStringOfNews=[Utility serverAddressOfDocument];
    
    [self geberateRequestWithStringURl:urlString withTag:1];
    [self geberateRequestWithStringURl:urlStringOfNews withTag:2];
    
    // Gourav June 05 End
    
    
}

-(void)initializeTheView
{
    ////*********astha
    //CGRect backgroundFrame  = CGRectMake(-1, 0, 322, 460-self.tabBarController.tabBar.frame.size.height);
    
    CGRect backgroundFrame       = CGRectMake(-1, 0, 322, 450);
    
    CGRect shareFrame           = CGRectMake(30, FIRST_ROW_BUTTONS_Y_AXIS-10, 80, 80);
    CGRect officeinfoFrame      = CGRectMake(120, FIRST_ROW_BUTTONS_Y_AXIS-10, 80, 80);
    CGRect websiteFrame         = CGRectMake(210, FIRST_ROW_BUTTONS_Y_AXIS-10, 80, 80);
    CGRect clainPhotoFrame      = CGRectMake(30, SECOND_ROW_BUTTONS_Y_AXIS-10, 80, 80);
    CGRect claimWhatTodoFrame   = CGRectMake(120, SECOND_ROW_BUTTONS_Y_AXIS-10, 80, 80);
    CGRect directNoFrame        = CGRectMake(210, SECOND_ROW_BUTTONS_Y_AXIS-10, 80, 80);
    
    CGRect steadfastFrame       = CGRectMake(100, SECOND_ROW_BUTTONS_Y_AXIS+100 , 160, 100  );
    
    int heightIncreamentfactor  = 50;
    
    if ([Utility isIphone_5])
    {
        // backgroundFrame      = CGRectMake(-1, 0, 322, 548-self.tabBarController.tabBar.frame.size.height);
        
        backgroundFrame      = CGRectMake(-1, 0, 322, 548);
        
        shareFrame           = CGRectMake(30, FIRST_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor-10, 80, 80);
        officeinfoFrame      = CGRectMake(120, FIRST_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor-10, 80, 80);
        websiteFrame         = CGRectMake(210, FIRST_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor-10, 80, 80);
        clainPhotoFrame      = CGRectMake(30, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor-10, 80, 80);
        claimWhatTodoFrame   = CGRectMake(120, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor-10, 80, 80);
        directNoFrame        = CGRectMake(210, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor-10, 80, 80);
        steadfastFrame       = CGRectMake(100, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+110 , 160, 100  );
        
        
        /* shareFrame          = CGRectMake(29, FIRST_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+15, 63, 63);
         officeinfoFrame     = CGRectMake(126, FIRST_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+15, 63, 63);
         websiteFrame        = CGRectMake(223, FIRST_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+15, 63, 63);
         clainPhotoFrame     = CGRectMake(29, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+10+15, 63, 63);
         claimWhatTodoFrame  = CGRectMake(126, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+10+15, 63, 63);
         directNoFrame       = CGRectMake(223, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+10+15, 63, 63);
         
         steadfastFrame      = CGRectMake(100, SECOND_ROW_BUTTONS_Y_AXIS+heightIncreamentfactor+110 , 160, 100  );
         */
        
        
        
    }
    steadfastImgView=[[UIImageView alloc] init];
    steadfastImgView.frame=steadfastFrame;
    
    
    [steadfastImgView setImage:[UIImage imageNamed:@"SteadFastImg.png"]];
    
    steadfastImgView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    UIImageView * backGRoundImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:BACKGROUNG_IMAGE]];
    // backGRoundImage.contentMode = UIViewContentModeScaleAspectFill;
    backGRoundImage.frame = backgroundFrame;
    
    UIButton * shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn addTarget:self action:@selector(doShare) forControlEvents:UIControlEventTouchUpInside];
    UIImage * shareImage = [UIImage imageNamed:SHARE_BUTTON];
    [shareBtn setBackgroundImage:shareImage forState:UIControlStateNormal];
    UIImage * shareImageSelected = [UIImage imageNamed:SHARE_BUTTON_SELECTED];
    [shareBtn setImage:shareImageSelected forState:UIControlStateHighlighted];
    shareBtn.frame = shareFrame;
    
    
    UIButton * officeInfoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [officeInfoBtn addTarget:self action:@selector(doOfficeInfo) forControlEvents:UIControlEventTouchUpInside];
    UIImage * officeInfoImage = [UIImage imageNamed:OFFICEINFO_BUTTON];
    [officeInfoBtn setBackgroundImage:officeInfoImage forState:UIControlStateNormal];
    UIImage * officeInfoImageSelected = [UIImage imageNamed:OFFICEINFO_BUTTON_SELECTED];
    [officeInfoBtn setImage:officeInfoImageSelected forState:UIControlStateHighlighted];
    officeInfoBtn.frame = officeinfoFrame;
    
    UIButton * websiteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [websiteBtn addTarget:self action:@selector(openWebsite) forControlEvents:UIControlEventTouchUpInside];
    UIImage * webImage = [UIImage imageNamed:WEBSITE_BUTTON];
    [websiteBtn setBackgroundImage:webImage forState:UIControlStateNormal];
    UIImage * webImageSelected = [UIImage imageNamed:WEBSITE_BUTTON_SELECTED];
    [websiteBtn setImage:webImageSelected forState:UIControlStateHighlighted];
    websiteBtn.frame = websiteFrame;
    
    UIButton * claimPhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [claimPhotoBtn addTarget:self action:@selector(doClaimPhoto) forControlEvents:UIControlEventTouchUpInside];
    UIImage * clainphotoimage = [UIImage imageNamed:PHOTOS_BUTTON];
    [claimPhotoBtn setBackgroundImage:clainphotoimage forState:UIControlStateNormal];
    UIImage * clainphotoimageSelected = [UIImage imageNamed:PHOTOS_BUTTON_SELECTED];
    [claimPhotoBtn setImage:clainphotoimageSelected forState:UIControlStateHighlighted];
    claimPhotoBtn.frame = clainPhotoFrame;
    
    
    
    UIButton * claimWhatTodoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [claimWhatTodoBtn addTarget:self action:@selector(claimWhatToDo) forControlEvents:UIControlEventTouchUpInside];
    UIImage * whatToDoImage = [UIImage imageNamed:CLAIM_BUTTON];
    [claimWhatTodoBtn setBackgroundImage:whatToDoImage forState:UIControlStateNormal];
    UIImage * whatToDoImageSelected = [UIImage imageNamed:CLAIM_BUTTON_SELECTED];
    [claimWhatTodoBtn setImage:whatToDoImageSelected forState:UIControlStateHighlighted];
    claimWhatTodoBtn.frame = claimWhatTodoFrame;
    
    UIButton * directNoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [directNoBtn addTarget:self action:@selector(doDirectNo) forControlEvents:UIControlEventTouchUpInside];
    UIImage * directNoImage = [UIImage imageNamed:DIRECTNUMBERS_BUTTON];
    [directNoBtn setBackgroundImage:directNoImage forState:UIControlStateNormal];
    UIImage * directNoImageSelected = [UIImage imageNamed:DIRECTNUMBERS_BUTTON_SELECTED];
    [directNoBtn setImage:directNoImageSelected forState:UIControlStateHighlighted];
    directNoBtn.frame = directNoFrame;
    
    
    steadfastImgView.backgroundColor=[UIColor clearColor];
    [backGRoundImage addSubview:steadfastImgView];
    
    
    
    UIImageView *bottomIconImgView = [[UIImageView alloc]initWithFrame:CGRectMake(110, claimWhatTodoBtn.frame.origin.y + claimWhatTodoBtn.frame.size.height +13 , 120, 40)];
    
    if([Utility isIphone_5])
    {
        bottomIconImgView.frame = CGRectMake(110, claimWhatTodoBtn.frame.origin.y + claimWhatTodoBtn.frame.size.height + 25 , 120, 40);
    }
   // bottomIconImgView.image = [UIImage imageNamed:@"bottonIcon.png"];
    
    [self.view addSubview:backGRoundImage];
    [self.view addSubview:shareBtn];
    [self.view addSubview:officeInfoBtn];
    [self.view addSubview:websiteBtn];
    [self.view addSubview:claimPhotoBtn];
    [self.view addSubview:claimWhatTodoBtn];
    [self.view addSubview:directNoBtn];
    
    [backGRoundImage release];
    [steadfastImgView release];
    
    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(294, 355, 26, 52)];
    button.backgroundColor= [UIColor clearColor];
    if ([Utility isIphone_5])
    {
        [button setFrame:CGRectMake(294, 420, 31, 60)];
    }
    //lbl=[[UILabel alloc] initWithFrame:CGRectMake(25,2, 200, 20)];
    lbl=[[UILabel alloc] initWithFrame:CGRectMake(25,15, 200, 20)];//neha
    lbl.backgroundColor=[UIColor clearColor];
    [lbl setFont:[UIFont boldSystemFontOfSize:10.0f]];
    [lbl setTextColor:[UIColor colorWithRed:60.0f/255.0f green:168.0f/255.0f blue:224.0f/255.0f alpha:1]];
    
    //ashish 1oct
    imgView=[[UIImageView alloc] initWithFrame:CGRectMake(-2, 120+85+85+78, 322, 70)];
    if ([Utility isIphone_5])
    {
        [imgView setFrame:CGRectMake(-2, 140+ 90 + 90 + 90 + 30, 325, 80)];
    }
    imgView.backgroundColor=[UIColor clearColor];
    UIImage * NewsHeadingImg = [UIImage imageNamed:NEWS_HEADING];
    [imgView setImage:NewsHeadingImg];
    
    UISwipeGestureRecognizer *swipeLeftSide = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swapToNews:)];
    swipeLeftSide.delegate = self;
    [swipeLeftSide setDirection:UISwipeGestureRecognizerDirectionRight];
    [imgView addGestureRecognizer:swipeLeftSide];
    [imgView addSubview:lbl];
    
    //NewsHeading = [[UILabel alloc] initWithFrame:CGRectMake(25,20-10, 268, 45)];
    NewsHeading = [[UILabel alloc] initWithFrame:CGRectMake(0,1, 268, 65)];  //neha
    [NewsHeading setBackgroundColor:[UIColor clearColor]];
    UITapGestureRecognizer *tapGetdure = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToDescription)];
    tapGetdure.delegate = self;
    tapGetdure.numberOfTapsRequired=1;
    [imgView setUserInteractionEnabled: YES];
    [imgView addGestureRecognizer:tapGetdure];
    
    [lbl release];
    [self.view addSubview:button];
    [button release];
    [imgView addSubview:NewsHeading];
    [NewsHeading release];
    [self.view addSubview:imgView];
    [imgView release];
    
    [self.view addSubview:bottomIconImgView];
    
    
    
}

#pragma mark
#pragma mark Button Actions-

-(void)doShare
{
    ShareFeature * share = [[ShareFeature alloc]initWithNibName:@"ShareFeature" bundle:nil];
    [[self navigationController] pushViewController:share animated:YES];
    [share release];
    
}

-(void)doShowWebsite
{
    
    NSMutableDictionary * dictioabary = [[NSMutableDictionary alloc] init];
    
    dictioabary = [appDelegate.appInfoDict objectForKey:BROKER_INFO_KEY];
    
    BrokerInfo * inform = [dictioabary objectForKey:@"broker"];
    
    NSString * link = [NSString stringWithFormat:@"http://%@",inform.website];
    
    NSLog(@"link is %@",link);
    
    link = [link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL * url = [NSURL URLWithString:link];
    
    [[UIApplication sharedApplication]openURL:url];
    
}

-(void)openWebsite
{
    NSMutableDictionary * dictioabary = [appDelegate.appInfoDict objectForKey:BROKER_INFO_KEY];
    
    if (!dictioabary) {
        return;
    }
    
    BrokerInfo * inform = [dictioabary objectForKey:@"broker"];
    
    //  NSString * link = [NSString stringWithFormat:@"http://%@",inform.website];
    NSString *link;
    if([inform.website hasPrefix:@"http://"]||[inform.website hasPrefix:@"https://"])
    {
        link = [NSString stringWithFormat:@"%@",inform.website];
    }
    
    else
    {
        link = [NSString stringWithFormat:@"http://%@",inform.website];
    }
    NSLog(@"link is %@",link);
    
    link = [link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL * url = [NSURL URLWithString:link];
    
    [[UIApplication sharedApplication]openURL:url];
    
}
-(void)doClaimPhoto
{
    Claims * cPhotos = [[Claims alloc]initWithNibName:@"Claims" bundle:nil];
    [self.navigationController pushViewController:cPhotos animated:YES];
    [cPhotos release];
    
    
}
-(void)doDirectNo
{
   // self.tabBarController.selectedIndex = 1;
    
     //   appDelegate.directVC = [[DirectNumbers alloc]initWithNibName:@"DirectNumbers" bundle:nil];
        [self.navigationController pushViewController:appDelegate.directVC animated:YES];
        //[appDelegate.directVC release];
    
}

-(void)claimWhatToDo
{
    ClaimWhatToDo * claim = [[ClaimWhatToDo alloc]initWithNibName:@"ClaimWhatToDo" bundle:nil];
    [self.navigationController pushViewController:claim animated:YES];
    [claim release];
}
-(void)doOfficeInfo
{
    
    OfficesViewController * claim = [[OfficesViewController alloc]initWithNibName:@"OfficesViewController" bundle:nil];
    [self.navigationController pushViewController:claim animated:YES];
    [claim release];
    //self.tabBarController.selectedIndex = 1;
    
}


#pragma mark Generate HTTPRequest


-(void)geberateRequestWithStringURl:(NSString *)urlString withTag:(NSInteger)tag
{
    
    [loadingView setHidden:NO];
    NSURL * url = [NSURL URLWithString:urlString];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setTag:tag];
    [request setTimeOutSeconds:30];
    [request setDelegate:self];
    [request startAsynchronous];
    
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
//    [loadingView setHidden:YES];
//    NSString *responseString = nil;
//    responseString = [request responseString];
//    
//    NSData * xmlData = [request responseData];
//    
//    NSString *xmlPath = [Utility xmlPathInDocDirectory];
//    
//    [[NSFileManager defaultManager] removeItemAtPath:xmlPath error:nil];
//    [xmlData writeToFile:xmlPath atomically:YES];
//    [self doParseDownloadedXML];

    NSString *responseString = nil;
    if(request.tag==1)
    {
        NSLog(@"first request");
        responseString = [request responseString];
        
        NSData * xmlData = [request responseData];
        
        NSString *xmlPath = [Utility xmlPathInDocDirectory];
        
        [[NSFileManager defaultManager] removeItemAtPath:xmlPath error:nil];
        [xmlData writeToFile:xmlPath atomically:YES];
        [self doParseDownloadedXML];
        
    }
    else
    {
        NSLog(@"second request");
        responseString   = [request responseString];
        NSData * xmlData = [request responseData];
        
        // gourav 04 Nov start
        
        NSString *responseString1 = [[NSString alloc] initWithBytes:[xmlData bytes] length:[xmlData length] encoding:NSASCIIStringEncoding];
        NSLog(@"response string \n= %@", responseString1);
        
        responseString1 = [responseString1 stringByReplacingOccurrencesOfString:@"s" withString:@"'s"];
        
        
        xmlData  = [responseString1 dataUsingEncoding:NSUTF8StringEncoding];
        NSString *xmlPath = [Utility xmlPathInNews];
        
        [[NSFileManager defaultManager] removeItemAtPath:xmlPath error:nil];
        [xmlData writeToFile:xmlPath atomically:YES];
        
        [self performSelector:@selector(pasingStartMethodForNews)];
    }

   
    
}

-(void) pasingStartMethodForNews
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"NewsInfo.xml"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    //  UIImage *img =[UIImage imageWithData:dta];
    
    XmlParseForNews *parseClass = [[XmlParseForNews alloc] initParse];
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:data];
    [xmlParser setDelegate:parseClass];
    [xmlParser parse];
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    [loadingView setHidden:YES];
    BOOL fileExist = [[NSFileManager defaultManager]fileExistsAtPath:[Utility xmlPathInDocDirectory]];
    if (fileExist)
    {
        [self doParseDownloadedXML];
    }
    else
    {
         [self.view setUserInteractionEnabled:YES];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Slow or no internet connection"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}

#pragma mark
#pragma mark parse downloaded xml-

-(void)doParseDownloadedXML
{
    XMLReader * reader = [[XMLReader alloc] init];
    NSData *tempData	   = [reader allocXml];
    reader.data		   = tempData;
    
    NSMutableDictionary * brokerDict  = [reader BrokerDictionary];
    NSMutableArray * officesArray     = [reader ListOfOffices];
    NSMutableArray * staffArray       = [reader listOfStaff];
    NSMutableArray * insuranceArray   = [reader insuranceArray];
    
    [appDelegate.appInfoDict setObject:officesArray forKey:OFFICE_INFO_KEY];
    [appDelegate.appInfoDict setObject:staffArray forKey:STAFF_INFO_KEY];
    [appDelegate.appInfoDict setObject:insuranceArray forKey:INSURANCE_INFO_KEY];
    [appDelegate.appInfoDict setObject:brokerDict forKey:BROKER_INFO_KEY];
    
    officesArray = nil;
    staffArray = nil;
    insuranceArray = nil;
    brokerDict = nil;
    
    [self.view setUserInteractionEnabled:YES];
    [reader release];
}


- (void)ParsingCompleted
{
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"clientid = %@",@"56714"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"clientid = %@",CLIENT_ID];
    NSArray *tempArray = [appDelegate.pressRoomArray filteredArrayUsingPredicate:predicate];
    NSSortDescriptor *id1 = [[[NSSortDescriptor alloc] initWithKey:@"id1" ascending:NO]autorelease];
    tempArray =  [tempArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects: id1, nil]];
    
    NSArray *shortedArr = [tempArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        PressRoomInfo *plr1 = a;
        PressRoomInfo *plr2 = b;
        NSNumber *first = @([plr1.id1 floatValue]);
        NSNumber *second = @([plr2.id1 floatValue]);
        return [first compare:second];
    }];
    appDelegate.pressRoomArray = [NSMutableArray arrayWithArray:shortedArr];
    
    PressRoomInfo *room = [appDelegate.pressRoomArray lastObject];
    NSString *str=room.title;
    NewsHeading.text=str;
    lbl.text = room.date;
    lbl.textColor = [UIColor orangeColor];
    NewsHeading.lineBreakMode=NSLineBreakByWordWrapping;
    NewsHeading.numberOfLines=0;
    NewsHeading.font=[UIFont systemFontOfSize:12];
    NewsHeading.textColor = [UIColor whiteColor];
    
    index = appDelegate.pressRoomArray.count-1;
     //[self.view setUserInteractionEnabled:YES];
    if(NewsHeading.text.length >50)
        [NewsHeading setFrame:CGRectMake(25, 15-7+2, NewsHeading.frame.size.width, NewsHeading.frame.size.height)];
    else
        [NewsHeading setFrame:CGRectMake(25, 15-7+2, NewsHeading.frame.size.width, NewsHeading.frame.size.height)];
     [loadingView setHidden:YES];
}
-(void)swapToNews:(UISwipeGestureRecognizer*)Gesture
{
    if(index == 0)
        index = appDelegate.pressRoomArray.count-1;
    else
        index = index-1;
    
    PressRoomInfo *room = [appDelegate.pressRoomArray objectAtIndex:index];
    NSString *str=room.title;
    NewsHeading.text=str;
    lbl.text = room.date;
    
    NewsHeading.lineBreakMode=NSLineBreakByWordWrapping;
    NewsHeading.numberOfLines=2;
    NewsHeading.font=[UIFont systemFontOfSize:12];
    
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.lineHeightMultiple = 0.75;
    //   style.LineHeight = 10.f;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    NewsHeading.attributedText = [[NSAttributedString alloc] initWithString:str
                                                                 attributes:attributtes];
    
    if(NewsHeading.text.length >50)
        [NewsHeading setFrame:CGRectMake(25, 15-7, NewsHeading.frame.size.width, NewsHeading.frame.size.height)];
    else
        [NewsHeading setFrame:CGRectMake(25, 15-7, NewsHeading.frame.size.width, NewsHeading.frame.size.height)];
}


-(void)moveToDescription{
    
    NewsDetail * press = [[NewsDetail alloc] initWithNibName:@"NewsDetail" bundle:nil];
    // press.index = 0;
    press.isComminfFromHome = YES;
    press.indexForNews = index;
    
    [self.navigationController pushViewController:press animated:YES];
    [press release];
}





@end
