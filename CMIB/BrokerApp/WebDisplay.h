//
//  WebDisplay.h
//  BrokerApp
//
//  Created by iPHTech2 on 05/06/13.
//
//

#import <UIKit/UIKit.h>

@interface WebDisplay : UIViewController
{
    UIWebView * webview;
    NSString * urlString;
}
@property (nonatomic, retain) NSString * urlString;
@property (nonatomic, retain) UIWebView * webview; 
@end
