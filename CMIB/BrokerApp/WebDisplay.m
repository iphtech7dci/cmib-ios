//
//  WebDisplay.m
//  BrokerApp
//
//  Created by iPHTech2 on 05/06/13.
//
//

#import "WebDisplay.h"
#import "Utility.h"
#import "Constants.h"


@interface WebDisplay ()

@end

@implementation WebDisplay
@synthesize webview;
@synthesize urlString;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)initailizeNavigationBar
{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:NAVIGATIONBAR_IMAGE] forBarMetrics:UIBarMetricsDefault];
    
   /* UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 0.0, 30, 30)];
    imageview.image = [UIImage imageNamed:TELEPHONE_IMAGE] ;
    
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:imageview];
    self.navigationItem.rightBarButtonItem = backButton1;
    [imageview release];
    [backButton1 release];
    */
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     [self initailizeNavigationBar];
    
    
    self.webview = [[UIWebView alloc]initWithFrame:CGRectMake(0,0,320,460)];
    if ([Utility isIphone_5]) {
        self.webview.frame = CGRectMake(0, 0, 320, 548);
    }
    [self.webview.scrollView setBounces:NO];
    [self.view addSubview:self.webview];
    
    
    
    
    NSURL * url = [NSURL URLWithString:self.urlString];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    [self.webview loadRequest:request];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-
#pragma mark IBAction-

-(void)callBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
