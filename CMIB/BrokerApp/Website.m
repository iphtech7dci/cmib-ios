//
//  Website.m
//  BrokerApp
//
//  Created by iPHTech2 on 11/06/13.
//
//

#import "Website.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "BrokerInfo.h"


@interface Website ()

@end

@implementation Website

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    dict = [appDelegate.appInfoDict objectForKey:BROKER_INFO_KEY];
    
    BrokerInfo * info  = [dict objectForKey:@"broker"];
    
    NSString * link = info.website;
     link = [link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL * url = [NSURL URLWithString:link];
   // [[UIApplication sharedApplication] openURL:url];
    
    UIWebView * web = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    [web setBackgroundColor:[UIColor clearColor]];
    [web.scrollView setBounces:YES];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    [web loadRequest:request];
    [self.view addSubview:web];
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
