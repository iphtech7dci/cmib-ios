//
//  XmlParseForNews.h
//  W. R. Berkley Australia Brokerapp
//
//  Created by iPHTech12 on 17/02/14.
//
//----------VKV---------//

#import <Foundation/Foundation.h>
@class PressRoomInfo;
@class AppDelegate;
@class ViewController;

@interface XmlParseForNews : NSObject<NSXMLParserDelegate>

{
    
    AppDelegate *appDelegate;
    PressRoomInfo *room;
    NSMutableString *character;
}
- (XmlParseForNews *) initParse;

@end
