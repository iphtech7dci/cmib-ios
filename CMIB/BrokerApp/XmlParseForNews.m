//
//  XmlParseForNews.m
//  W. R. Berkley Australia Brokerapp
//
//  Created by iPHTech12 on 17/02/14.
//
//-----------VKV---------//

#import "XmlParseForNews.h"
#import "AppDelegate.h"
#import "PressRoomInfo.h"
#import "ViewController.h"

@implementation XmlParseForNews
- (XmlParseForNews *) initParse
{
    
    self = [super init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return self;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"xml"])
    {
        appDelegate.pressRoomArray = [[NSMutableArray alloc] init];
    }
    if([elementName isEqualToString:@"article"])
    {
        room=[[PressRoomInfo alloc]init];
    }
    
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(!character)
        character = [[NSMutableString alloc] initWithString:string];
    else
        [character appendString:string];
    
    //NSLog(@"CHAR: %@",character);
  // character  = [character stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    NSLog(@"End elements is : %@ " , elementName);
    
    if([elementName isEqualToString:@"xml"])
    {
       // ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
       
        [appDelegate.viewController ParsingCompleted];
        //[viewController ParsingCompleted];
    }
    if([elementName isEqualToString:@"article"])
    {
        [appDelegate.pressRoomArray addObject:room];
        
        
        NSLog(@"appDelegate.pressRoomArray elemements = %d", [appDelegate.pressRoomArray count]);
        
         room = nil;
        [room release];
    }
    if([elementName isEqualToString:@"id"])
    {
        room.id1 = character;
    }
    if([elementName isEqualToString:@"date"])
    {
        room.date = character;
    }
    if([elementName isEqualToString:@"title"])
    {
        room.title = character;
        NSLog(@"title -  %@",character);
    }
    
    if([elementName isEqualToString:@"short"])
    {
        room.short1 = character;
    }
    if([elementName isEqualToString:@"long"])
    {
        room.long1 = character;
    }
    if([elementName isEqualToString:@"clientid"])
    {
        room.clientid = character;
    }
    character=nil;

}




@end
