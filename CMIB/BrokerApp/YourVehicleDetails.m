//
//  YourVehicleDetails.m
//  Optimus1
//
//  Created by iphtech7 on 21/04/14.
//
//

#import "YourVehicleDetails.h"
#import "ClaimsAndPhotos.h"
#import "Constants.h"
#import "ClaimWhatToDo.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "OfficeInfo.h"
#import "VehicleDetailDescriptioin.h"

#define MAKE_TEXTFIELD_TAG 300
#define MODEL_TEXTFIELD_TAG 301
#define REGO_TEXTFIELD_TAG 302
#define COLOR_TEXTFIELD_TAG 303
#define POLICY_TEXTFIELD_TAG 304

@interface YourVehicleDetails ()
{
    CGFloat yValue;
    int currentTxtFldTag;
}

@end

@implementation YourVehicleDetails
@synthesize makeTextView,modelTextView,regoTextView,colourTextView,policyTextView;
@synthesize indecatorView;
@synthesize confirmationView;
@synthesize scrollView;
@synthesize latitude;
@synthesize longitude;

@synthesize officeArray;
@synthesize emailRecieverString;
@synthesize latiArray;
@synthesize longiArray;

@synthesize dateTimeArray;
@synthesize longitude_libPhoto;
@synthesize latitude_libPhoto;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // self.view.backgroundColor=[UIColor redColor];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate *)[[UIApplication  sharedApplication] delegate];
    
   
    shouldShowDropDownList = YES;
    checForEmptyArray = 0;
    self.latiArray = [[NSMutableArray alloc]init];
    self.longiArray = [[NSMutableArray alloc]init];
    self.dateTimeArray = [[NSMutableArray alloc]init];
    self.screenName = @"Claims & Photos";
    UIImageView * bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    if ([Utility isIphone_5]) {
        bgImage.frame = CGRectMake(0, 0, 320, 568);
    }
    bgImage.image = [UIImage imageNamed:PUREBACKGROUNDIMAGE];
    [self.view addSubview:bgImage];
    [bgImage release];
    
    [self initializeNavigationBar];
    
    
    [self makeForm];
    [self assignSavedvalue];
    
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)] ;
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(prevBtn)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBtn)];
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)];
    NSArray *itemArray = [[NSArray alloc] initWithObjects:item3, spaceItem, item1, item2, nil];
    numberToolbar.items = itemArray;
    
    [numberToolbar sizeToFit];
    [item1 release];
    [item2 release];
    [item3 release];
    [itemArray release];
    
    
    self.makeTextView.inputAccessoryView = numberToolbar;
    self.modelTextView.inputAccessoryView = numberToolbar;
    self.regoTextView.inputAccessoryView = numberToolbar;
    self.colourTextView.inputAccessoryView = numberToolbar;
    self.policyTextView.inputAccessoryView = numberToolbar;
    
    
    self.confirmationView  = [[UIView alloc]init];
    [self.confirmationView setAlpha:0.8];
    [[self.confirmationView layer] setCornerRadius:10];
    
    self.confirmationView.frame = CGRectMake(30, 150, 260, 100);
    if ([Utility isIphone_5]) {
        self.confirmationView.frame = CGRectMake(30, 180, 260, 100);
    }
    [self.confirmationView setBackgroundColor:[UIColor darkGrayColor]];
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 160, 40)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:18]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    [label setNumberOfLines:2];
    [label setText:@"Your message has been sent"];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:DONE_BUTTON_IMAGE] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [btn setFrame:CGRectMake(105, 65, 60, 30)];
    
    [self.confirmationView addSubview:btn];
    [self.confirmationView addSubview:label];
    [self.confirmationView setHidden:YES];
    [self.view addSubview:self.confirmationView];
    [self.view bringSubviewToFront:self.indecatorView];
    
    [label release];
    

}
-(void)initializeNavigationBar
{
    [self.navigationController setNavigationBarHidden: NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    self.title = @"Your Vehicle Details";
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 60, 30)];
    [button setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMAGE] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(callBack) forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]  initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    [button release];
    [backButton release];
    
    UIImageView * photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(270, 0, 20, 18)];
    photoImage.image = [UIImage imageNamed:@"whiteCarImg.png"];
    UIBarButtonItem *backButton1 = [[UIBarButtonItem alloc]  initWithCustomView:photoImage];
    self.navigationItem.rightBarButtonItem = backButton1;
    [photoImage release];
    [backButton1 release];
}
-(void) moveViewDown
{
    [UIView beginAnimations:@"ResizeForKeyboard" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.view.frame = CGRectMake(self.view.frame.origin.x,64,self.view.frame.size.width,self.view.frame.size.height);
    [UIView commitAnimations];
}

-(void)dismissKeyboard {
    [self moveScrollDownForTextfield];
   // [self moveViewDown]; //yt
    [self.view endEditing:YES];
  // self.scrollView.contentOffset = CGPointMake(0,0);
}

-(void)nextBtn
{
    if(currentTxtFldTag<304)
    {
        currentTxtFldTag=currentTxtFldTag + 1;
    }
    
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
    
    //  [self moveViewDown];
    //  [self.view endEditing:YES];
    
    // self.scrollview.contentOffset = CGPointMake(0,-50);
}
-(void)prevBtn
{
    if(currentTxtFldTag>300)
    {
        currentTxtFldTag=currentTxtFldTag -1;
    }
    UITextField *txtfld = (UITextField *)[self.view viewWithTag:(currentTxtFldTag)];
    [txtfld becomeFirstResponder];
    
    
}

-(void)moveScrollDownForTextfield
{
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    [UIView commitAnimations];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    locationManager.delegate = self;
    [locationManager startUpdatingHeading];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    if (locationManager) {
        locationManager.delegate = nil;
        [locationManager stopUpdatingLocation];
    }
}
-(void)makeForm
{
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 370)];
    //[self.scrollView setScrollEnabled:YES];
    self.scrollView.backgroundColor=[UIColor whiteColor];

    self.scrollView.contentSize = CGSizeMake(250, 10);
    
    if([Utility isIphone_5])
    {
        self.scrollView.frame = CGRectMake(0, 0, 320, 460);
    }
    
    self.scrollView.scrollEnabled =YES;
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.scrollView];
    
    yValue = 20;
    CGFloat width = 280;
    CGFloat height = 15;
   
    CGFloat litleSpace = 5.0;
    CGFloat largeSpace = 8.0;
    CGFloat xValue = 20.0;
  
    CGFloat textFieldHeight = 30;
       UILabel * makeGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [makeGeadingLable setBackgroundColor:[UIColor clearColor]];
    [makeGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [makeGeadingLable setText:@"Make"];//astha
    [scrollView addSubview:makeGeadingLable];
    [makeGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    self.makeTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.makeTextView setPlaceholder:@"Make"];//astha
    [self.makeTextView setTag:MAKE_TEXTFIELD_TAG];
    [self.makeTextView.layer setCornerRadius:2.0f];
    [self.makeTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.makeTextView setDelegate:self];
     self.makeTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.makeTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * modelGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [modelGeadingLable setBackgroundColor:[UIColor clearColor]];
    [modelGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [modelGeadingLable setText:@"Model & Year"];//astha
    [scrollView addSubview:modelGeadingLable];
    [modelGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    self.modelTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.modelTextView setPlaceholder:@"Model & Year"];//astha
    [self.modelTextView setTag:MODEL_TEXTFIELD_TAG];
    [self.modelTextView.layer setCornerRadius:2.0f];
    [self.modelTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.modelTextView setDelegate:self];
    self.modelTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.modelTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * regoGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [regoGeadingLable setBackgroundColor:[UIColor clearColor]];
    [regoGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [regoGeadingLable setText:@"Registration Number"];//astha
    [scrollView addSubview:regoGeadingLable];
    [regoGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    self.regoTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.regoTextView setPlaceholder:@"Registration Number"];//astha
    [self.regoTextView setTag:REGO_TEXTFIELD_TAG];
    [self.regoTextView.layer setCornerRadius:2.0f];
    [self.regoTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.regoTextView setDelegate:self];
    self.regoTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.regoTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * colorGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [colorGeadingLable setBackgroundColor:[UIColor clearColor]];
    [colorGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [colorGeadingLable setText:@"Colour"];//astha
    [scrollView addSubview:colorGeadingLable];
    [colorGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    self.colourTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.colourTextView setPlaceholder:@"Colour"];//astha
    [self.colourTextView setTag:COLOR_TEXTFIELD_TAG];
    [self.colourTextView.layer setCornerRadius:2.0f];
    [self.colourTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.colourTextView setDelegate:self];
    self.colourTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.colourTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    
    UILabel * policyGeadingLable = [[UILabel alloc]initWithFrame:CGRectMake(xValue, yValue, width, height)];
    [policyGeadingLable setBackgroundColor:[UIColor clearColor]];
    [policyGeadingLable setFont:[UIFont boldSystemFontOfSize:13.0]];
    [policyGeadingLable setText:@"Policy Number"];//astha
    [scrollView addSubview:policyGeadingLable];
    [policyGeadingLable release];
    
    yValue = yValue+height+litleSpace;
    
    
    
    
    self.policyTextView = [[UITextField alloc]initWithFrame:CGRectMake(xValue, yValue, width, textFieldHeight)];
    [self.policyTextView setPlaceholder:@"Policy Number"];//astha
    [self.policyTextView setTag:POLICY_TEXTFIELD_TAG];
    [self.policyTextView.layer setCornerRadius:2.0f];
    [self.policyTextView setBorderStyle:UITextBorderStyleRoundedRect];
    [self.policyTextView setDelegate:self];
    self.policyTextView.autocorrectionType = UITextAutocorrectionTypeNo;  // gourav 09 sep
    [self.scrollView addSubview:self.policyTextView];
    
    yValue = yValue+textFieldHeight+largeSpace;
    sendButton = [[UIButton alloc] initWithFrame:CGRectMake(xValue , yValue,width, 42)];
    [sendButton setBackgroundImage:[UIImage imageNamed:SEND_BUTTON_IMAGE] forState:UIControlStateNormal];
    [sendButton setTitle:@"Save & Go Back" forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:[UIFont boldSystemFontOfSize:18]];
    [sendButton addTarget:self action:@selector(sendData) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:sendButton];
    
    yValue = yValue+50;
    //self.scrollView.contentSize = CGSizeMake(200, 600);

    
    
    
}

-(void) assignSavedvalue
{
    if(appDelegate.arrayVehicleDetail!=nil)
    {
    VehicleDetailDescriptioin *dtl=[[VehicleDetailDescriptioin alloc]init];
    dtl=[appDelegate.arrayVehicleDetail objectAtIndex:0];
    makeTextView.text=dtl.make;
    modelTextView.text=dtl.model;
    regoTextView.text=dtl.rego;
    colourTextView.text=dtl.color;
    policyTextView.text=dtl.policyNumber;
    }
   
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.scrollView.contentSize = CGSizeMake(200, yValue+250);
    return YES;
}// return NO to disallow editing.

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    switch (textField.tag) {
            
        case MAKE_TEXTFIELD_TAG:
        {
            //textField.inputAccessoryView = numberToolbar;
            currentTxtFldTag=textField.tag;
            break;
        }
        case MODEL_TEXTFIELD_TAG:
        {
            currentTxtFldTag=textField.tag;
            break;

        }
        case REGO_TEXTFIELD_TAG:
        {
           currentTxtFldTag=textField.tag;
            
            //gourav 11 nov start
            if(![Utility isIphone_5])
                [self moveScrolUP:70];
            
            //gourav 11 nov end
            
            
            break;

        }
     


        case COLOR_TEXTFIELD_TAG:
        {
           currentTxtFldTag=textField.tag;
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+150;
            }
            else if(t>50) {
                
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
            }
            
            [self moveScrolUP:t];
            break;
        }
        case POLICY_TEXTFIELD_TAG:
        {
            currentTxtFldTag=textField.tag;
            
            offset = self.scrollView.contentOffset;
            
            int t = offset.y;
            if (t<50) {
                t = t+100;
                //gourav 11 nov star
                
                if(![Utility isIphone_5])
                    t =t+50;
                // gourav 11 nov end
            }
            else if(t>50) {
                
                if([Utility isIphone_5])
                {
                    t = t + 50;
                }
                else
                {
                    t = t +70;
                }
            }
            
            [self moveScrolUP:t];
            break;
        }
    }

    }

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
    
    
    

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([[textField text] length] == 0) {
        if([string isEqualToString:@" "] || [string isEqualToString:@"\""]){
            NSLog(@"blank space can not inserted at initial position");
            return NO;
        }
    }
    
    if ([string isEqualToString:@". "]) {
        return NO;
    }
    return YES;
}// return NO to not change text


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self moveScrollDownForTextfield];
    return YES;
}

#pragma mark-
#pragma mark movesScrollview UP and DOWN-
-(void)moveScrolUP:(int)ht
{
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, ht);
    [UIView commitAnimations];
    
    
    CGRect frame = self.scrollView.frame;
    frame.origin.y = -ht;
    
    [UIView beginAnimations:@"MoveUP" context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView commitAnimations];
    
}

-(void)moveScrollDown
{
    CGRect frame = CGRectMake(20, 18, 290, 350);
    if ([Utility isIphone_5]) {
        frame = CGRectMake(20, 18, 290, 440);
    }
    
    [UIView beginAnimations:@"MoveDown" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.scrollView.contentOffset = CGPointMake(0, 0);  // (0, offset.y) //yt
    [UIView commitAnimations];
    
}

#pragma mark-
#pragma mark UITextViewDelegate Methods-

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
        if (self.line1.textColor == [UIColor lightGrayColor]) {
        self.line1.text = @"";
        self.line1.textColor = [UIColor blackColor];
    }
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    offset = self.scrollView.contentOffset;
    int t;
    if ([Utility isIphone_5]) {
        t= 200;
    }
    else{
        t = 260;
    }
    [self moveScrolUP:t];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    return YES;
    
}
- (void)textViewDidChange:(UITextView *)textView{
    
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    
    
}

#pragma mark
#pragma mark IBActions-
-(void)callBack
{
    
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)sendData
{
    VehicleDetailDescriptioin *dtl=[[VehicleDetailDescriptioin alloc]init];
    appDelegate.arrayVehicleDetail =[[NSMutableArray alloc]init];
    
    dtl.make=makeTextView.text;
    dtl.model=modelTextView.text;
    dtl.rego=regoTextView.text;
    dtl.color=colourTextView.text;
    dtl.policyNumber=policyTextView.text;
    [appDelegate.arrayVehicleDetail addObject:dtl];
    

    if(makeTextView.text.length >0 || modelTextView.text.length >0 || regoTextView.text.length >0|| colourTextView.text.length >0|| policyTextView.text.length >0)  // gourav 09 sep
    {
        appDelegate.checkFprVehicle=YES;
    }
    else
        appDelegate.checkFprVehicle=NO;

    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark upload photo method and delegate-

-(void)findCurrentLocation
{
    
    
    //isCamera = YES;
    
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.latitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    
}
-(NSString *)Base64Encode:(NSData *)Inputdata{
    //Point to start of the data and set buffer sizes
    const uint8_t* input = (const uint8_t*)[Inputdata bytes];
    NSInteger length = [Inputdata length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];}

-(void)dealloc
{
    [super dealloc];
       [officeArray release];
    //[tabelView release];
    if (latiArray) {
        [latiArray release];
    }
    if (longiArray) {
        [longiArray release];
    }
    if (locationManager) {
        [locationManager release];
    }
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
